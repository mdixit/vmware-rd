package com.vmware.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

public class APIServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
			response.setContentType("text/html;charset=UTF-8");

			//PrintWriter out = response.getWriter();
			
			String action =  ParamUtil.getString(request, "action");	
			
			//long userId = PortalUtil.getUserId(request);
			
			long companyId = PortalUtil.getCompanyId(request);

			if (action.equalsIgnoreCase("organization")) {
				
				JSONObject responseJSON = JSONFactoryUtil.createJSONObject();
				
				JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
				
				JSONObject orgParentJSONObj = JSONFactoryUtil.createJSONObject();
				
				try {
					List<Organization> orgList = OrganizationLocalServiceUtil.getOrganizations(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					
					_log.info("orgList=="+orgList);
					
					long orgId = 0;
					
					Map<Long,JSONArray> childernMap = new HashMap<Long,JSONArray>();
					
					for(Organization org:orgList){						
						
						
						orgParentJSONObj.put("id", org.getOrganizationId());
						orgParentJSONObj.put("name", org.getName());
						orgId = org.getOrganizationId();
									
						/*if(org.getParentOrganizationId() == org.getOrganizationId()){
							
							orgChildrenJSONObj.put("id", org.getOrganizationId());
							
							orgChildrenJSONObj.put("name", org.getName());
							
							jsonArray.put(orgChildrenJSONObj);
						}*/
						
						try {
							List<Organization> orgChildernList = OrganizationLocalServiceUtil.getOrganizations(companyId, org.getOrganizationId());
						
							_log.info("orgChildernList=="+orgChildernList);
							
							JSONArray childJsonArray1 = JSONFactoryUtil.createJSONArray();							
							
								for(Organization orgChildren:orgChildernList){								
									
									JSONObject orgChildrenJSONObj = JSONFactoryUtil.createJSONObject();
									
									orgChildrenJSONObj.put("id", orgChildren.getOrganizationId());
									
									orgChildrenJSONObj.put("name", orgChildren.getName());
									
									jsonArray.put(orgChildrenJSONObj);
									
									childJsonArray1.put(orgChildrenJSONObj);								
									
									_log.info("orgJSONObj=="+orgChildrenJSONObj);
									
									childernMap.put(org.getOrganizationId(), childJsonArray1);
									
									
								}
							
							_log.info("childernMap=="+childernMap);
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}							
						
					}
					
					Set<Long> longKey = childernMap.keySet();
					
					ArrayList al = new ArrayList(); 
					
					//al.addAll(childernMap);
					
					System.out.println("longKey.size() = " + longKey.size());
					
					for(int i=longKey.size();i>1;i--){
						for(int j=i-1;j>0;j--){
							
						}	
						
					}					
					
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				
				orgParentJSONObj.put("jsonArray", jsonArray);
				
				responseJSON.put("json", orgParentJSONObj);
				
				response.getWriter().write(responseJSON.toString());
				
		}
	}
	
	
	
	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	private static Log _log = LogFactoryUtil.getLog(APIServlet.class);

}
