/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.AttachmentsLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class AttachmentsClp extends BaseModelImpl<Attachments>
	implements Attachments {
	public AttachmentsClp() {
	}

	public Class<?> getModelClass() {
		return Attachments.class;
	}

	public String getModelClassName() {
		return Attachments.class.getName();
	}

	public int getPrimaryKey() {
		return _attach_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setAttach_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_attach_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attach_id", getAttach_id());
		attributes.put("bug_id", getBug_id());
		attributes.put("creation_ts", getCreation_ts());
		attributes.put("description", getDescription());
		attributes.put("mimetype", getMimetype());
		attributes.put("ispatch", getIspatch());
		attributes.put("filename", getFilename());
		attributes.put("submitter_id", getSubmitter_id());
		attributes.put("isobsolete", getIsobsolete());
		attributes.put("isprivate", getIsprivate());
		attributes.put("isurl", getIsurl());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer attach_id = (Integer)attributes.get("attach_id");

		if (attach_id != null) {
			setAttach_id(attach_id);
		}

		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Date creation_ts = (Date)attributes.get("creation_ts");

		if (creation_ts != null) {
			setCreation_ts(creation_ts);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String mimetype = (String)attributes.get("mimetype");

		if (mimetype != null) {
			setMimetype(mimetype);
		}

		Integer ispatch = (Integer)attributes.get("ispatch");

		if (ispatch != null) {
			setIspatch(ispatch);
		}

		String filename = (String)attributes.get("filename");

		if (filename != null) {
			setFilename(filename);
		}

		Integer submitter_id = (Integer)attributes.get("submitter_id");

		if (submitter_id != null) {
			setSubmitter_id(submitter_id);
		}

		Integer isobsolete = (Integer)attributes.get("isobsolete");

		if (isobsolete != null) {
			setIsobsolete(isobsolete);
		}

		Integer isprivate = (Integer)attributes.get("isprivate");

		if (isprivate != null) {
			setIsprivate(isprivate);
		}

		Integer isurl = (Integer)attributes.get("isurl");

		if (isurl != null) {
			setIsurl(isurl);
		}
	}

	public int getAttach_id() {
		return _attach_id;
	}

	public void setAttach_id(int attach_id) {
		_attach_id = attach_id;
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public Date getCreation_ts() {
		return _creation_ts;
	}

	public void setCreation_ts(Date creation_ts) {
		_creation_ts = creation_ts;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getMimetype() {
		return _mimetype;
	}

	public void setMimetype(String mimetype) {
		_mimetype = mimetype;
	}

	public int getIspatch() {
		return _ispatch;
	}

	public void setIspatch(int ispatch) {
		_ispatch = ispatch;
	}

	public String getFilename() {
		return _filename;
	}

	public void setFilename(String filename) {
		_filename = filename;
	}

	public int getSubmitter_id() {
		return _submitter_id;
	}

	public void setSubmitter_id(int submitter_id) {
		_submitter_id = submitter_id;
	}

	public int getIsobsolete() {
		return _isobsolete;
	}

	public void setIsobsolete(int isobsolete) {
		_isobsolete = isobsolete;
	}

	public int getIsprivate() {
		return _isprivate;
	}

	public void setIsprivate(int isprivate) {
		_isprivate = isprivate;
	}

	public int getIsurl() {
		return _isurl;
	}

	public void setIsurl(int isurl) {
		_isurl = isurl;
	}

	public BaseModel<?> getAttachmentsRemoteModel() {
		return _attachmentsRemoteModel;
	}

	public void setAttachmentsRemoteModel(BaseModel<?> attachmentsRemoteModel) {
		_attachmentsRemoteModel = attachmentsRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			AttachmentsLocalServiceUtil.addAttachments(this);
		}
		else {
			AttachmentsLocalServiceUtil.updateAttachments(this);
		}
	}

	@Override
	public Attachments toEscapedModel() {
		return (Attachments)Proxy.newProxyInstance(Attachments.class.getClassLoader(),
			new Class[] { Attachments.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AttachmentsClp clone = new AttachmentsClp();

		clone.setAttach_id(getAttach_id());
		clone.setBug_id(getBug_id());
		clone.setCreation_ts(getCreation_ts());
		clone.setDescription(getDescription());
		clone.setMimetype(getMimetype());
		clone.setIspatch(getIspatch());
		clone.setFilename(getFilename());
		clone.setSubmitter_id(getSubmitter_id());
		clone.setIsobsolete(getIsobsolete());
		clone.setIsprivate(getIsprivate());
		clone.setIsurl(getIsurl());

		return clone;
	}

	public int compareTo(Attachments attachments) {
		int value = 0;

		value = DateUtil.compareTo(getCreation_ts(),
				attachments.getCreation_ts());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		AttachmentsClp attachments = null;

		try {
			attachments = (AttachmentsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = attachments.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{attach_id=");
		sb.append(getAttach_id());
		sb.append(", bug_id=");
		sb.append(getBug_id());
		sb.append(", creation_ts=");
		sb.append(getCreation_ts());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", mimetype=");
		sb.append(getMimetype());
		sb.append(", ispatch=");
		sb.append(getIspatch());
		sb.append(", filename=");
		sb.append(getFilename());
		sb.append(", submitter_id=");
		sb.append(getSubmitter_id());
		sb.append(", isobsolete=");
		sb.append(getIsobsolete());
		sb.append(", isprivate=");
		sb.append(getIsprivate());
		sb.append(", isurl=");
		sb.append(getIsurl());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Attachments");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>attach_id</column-name><column-value><![CDATA[");
		sb.append(getAttach_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>creation_ts</column-name><column-value><![CDATA[");
		sb.append(getCreation_ts());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mimetype</column-name><column-value><![CDATA[");
		sb.append(getMimetype());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ispatch</column-name><column-value><![CDATA[");
		sb.append(getIspatch());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>filename</column-name><column-value><![CDATA[");
		sb.append(getFilename());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>submitter_id</column-name><column-value><![CDATA[");
		sb.append(getSubmitter_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isobsolete</column-name><column-value><![CDATA[");
		sb.append(getIsobsolete());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isprivate</column-name><column-value><![CDATA[");
		sb.append(getIsprivate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isurl</column-name><column-value><![CDATA[");
		sb.append(getIsurl());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _attach_id;
	private int _bug_id;
	private Date _creation_ts;
	private String _description;
	private String _mimetype;
	private int _ispatch;
	private String _filename;
	private int _submitter_id;
	private int _isobsolete;
	private int _isprivate;
	private int _isurl;
	private BaseModel<?> _attachmentsRemoteModel;
}