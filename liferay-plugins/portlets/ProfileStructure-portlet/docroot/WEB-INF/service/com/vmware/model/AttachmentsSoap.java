/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.AttachmentsServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.AttachmentsServiceSoap
 * @generated
 */
public class AttachmentsSoap implements Serializable {
	public static AttachmentsSoap toSoapModel(Attachments model) {
		AttachmentsSoap soapModel = new AttachmentsSoap();

		soapModel.setAttach_id(model.getAttach_id());
		soapModel.setBug_id(model.getBug_id());
		soapModel.setCreation_ts(model.getCreation_ts());
		soapModel.setDescription(model.getDescription());
		soapModel.setMimetype(model.getMimetype());
		soapModel.setIspatch(model.getIspatch());
		soapModel.setFilename(model.getFilename());
		soapModel.setSubmitter_id(model.getSubmitter_id());
		soapModel.setIsobsolete(model.getIsobsolete());
		soapModel.setIsprivate(model.getIsprivate());
		soapModel.setIsurl(model.getIsurl());

		return soapModel;
	}

	public static AttachmentsSoap[] toSoapModels(Attachments[] models) {
		AttachmentsSoap[] soapModels = new AttachmentsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AttachmentsSoap[][] toSoapModels(Attachments[][] models) {
		AttachmentsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AttachmentsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AttachmentsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AttachmentsSoap[] toSoapModels(List<Attachments> models) {
		List<AttachmentsSoap> soapModels = new ArrayList<AttachmentsSoap>(models.size());

		for (Attachments model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AttachmentsSoap[soapModels.size()]);
	}

	public AttachmentsSoap() {
	}

	public int getPrimaryKey() {
		return _attach_id;
	}

	public void setPrimaryKey(int pk) {
		setAttach_id(pk);
	}

	public int getAttach_id() {
		return _attach_id;
	}

	public void setAttach_id(int attach_id) {
		_attach_id = attach_id;
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public Date getCreation_ts() {
		return _creation_ts;
	}

	public void setCreation_ts(Date creation_ts) {
		_creation_ts = creation_ts;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getMimetype() {
		return _mimetype;
	}

	public void setMimetype(String mimetype) {
		_mimetype = mimetype;
	}

	public int getIspatch() {
		return _ispatch;
	}

	public void setIspatch(int ispatch) {
		_ispatch = ispatch;
	}

	public String getFilename() {
		return _filename;
	}

	public void setFilename(String filename) {
		_filename = filename;
	}

	public int getSubmitter_id() {
		return _submitter_id;
	}

	public void setSubmitter_id(int submitter_id) {
		_submitter_id = submitter_id;
	}

	public int getIsobsolete() {
		return _isobsolete;
	}

	public void setIsobsolete(int isobsolete) {
		_isobsolete = isobsolete;
	}

	public int getIsprivate() {
		return _isprivate;
	}

	public void setIsprivate(int isprivate) {
		_isprivate = isprivate;
	}

	public int getIsurl() {
		return _isurl;
	}

	public void setIsurl(int isurl) {
		_isurl = isurl;
	}

	private int _attach_id;
	private int _bug_id;
	private Date _creation_ts;
	private String _description;
	private String _mimetype;
	private int _ispatch;
	private String _filename;
	private int _submitter_id;
	private int _isobsolete;
	private int _isprivate;
	private int _isurl;
}