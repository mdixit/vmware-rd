/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Attachments}.
 * </p>
 *
 * @author    iscisc
 * @see       Attachments
 * @generated
 */
public class AttachmentsWrapper implements Attachments,
	ModelWrapper<Attachments> {
	public AttachmentsWrapper(Attachments attachments) {
		_attachments = attachments;
	}

	public Class<?> getModelClass() {
		return Attachments.class;
	}

	public String getModelClassName() {
		return Attachments.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attach_id", getAttach_id());
		attributes.put("bug_id", getBug_id());
		attributes.put("creation_ts", getCreation_ts());
		attributes.put("description", getDescription());
		attributes.put("mimetype", getMimetype());
		attributes.put("ispatch", getIspatch());
		attributes.put("filename", getFilename());
		attributes.put("submitter_id", getSubmitter_id());
		attributes.put("isobsolete", getIsobsolete());
		attributes.put("isprivate", getIsprivate());
		attributes.put("isurl", getIsurl());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer attach_id = (Integer)attributes.get("attach_id");

		if (attach_id != null) {
			setAttach_id(attach_id);
		}

		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Date creation_ts = (Date)attributes.get("creation_ts");

		if (creation_ts != null) {
			setCreation_ts(creation_ts);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String mimetype = (String)attributes.get("mimetype");

		if (mimetype != null) {
			setMimetype(mimetype);
		}

		Integer ispatch = (Integer)attributes.get("ispatch");

		if (ispatch != null) {
			setIspatch(ispatch);
		}

		String filename = (String)attributes.get("filename");

		if (filename != null) {
			setFilename(filename);
		}

		Integer submitter_id = (Integer)attributes.get("submitter_id");

		if (submitter_id != null) {
			setSubmitter_id(submitter_id);
		}

		Integer isobsolete = (Integer)attributes.get("isobsolete");

		if (isobsolete != null) {
			setIsobsolete(isobsolete);
		}

		Integer isprivate = (Integer)attributes.get("isprivate");

		if (isprivate != null) {
			setIsprivate(isprivate);
		}

		Integer isurl = (Integer)attributes.get("isurl");

		if (isurl != null) {
			setIsurl(isurl);
		}
	}

	/**
	* Returns the primary key of this attachments.
	*
	* @return the primary key of this attachments
	*/
	public int getPrimaryKey() {
		return _attachments.getPrimaryKey();
	}

	/**
	* Sets the primary key of this attachments.
	*
	* @param primaryKey the primary key of this attachments
	*/
	public void setPrimaryKey(int primaryKey) {
		_attachments.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the attach_id of this attachments.
	*
	* @return the attach_id of this attachments
	*/
	public int getAttach_id() {
		return _attachments.getAttach_id();
	}

	/**
	* Sets the attach_id of this attachments.
	*
	* @param attach_id the attach_id of this attachments
	*/
	public void setAttach_id(int attach_id) {
		_attachments.setAttach_id(attach_id);
	}

	/**
	* Returns the bug_id of this attachments.
	*
	* @return the bug_id of this attachments
	*/
	public int getBug_id() {
		return _attachments.getBug_id();
	}

	/**
	* Sets the bug_id of this attachments.
	*
	* @param bug_id the bug_id of this attachments
	*/
	public void setBug_id(int bug_id) {
		_attachments.setBug_id(bug_id);
	}

	/**
	* Returns the creation_ts of this attachments.
	*
	* @return the creation_ts of this attachments
	*/
	public java.util.Date getCreation_ts() {
		return _attachments.getCreation_ts();
	}

	/**
	* Sets the creation_ts of this attachments.
	*
	* @param creation_ts the creation_ts of this attachments
	*/
	public void setCreation_ts(java.util.Date creation_ts) {
		_attachments.setCreation_ts(creation_ts);
	}

	/**
	* Returns the description of this attachments.
	*
	* @return the description of this attachments
	*/
	public java.lang.String getDescription() {
		return _attachments.getDescription();
	}

	/**
	* Sets the description of this attachments.
	*
	* @param description the description of this attachments
	*/
	public void setDescription(java.lang.String description) {
		_attachments.setDescription(description);
	}

	/**
	* Returns the mimetype of this attachments.
	*
	* @return the mimetype of this attachments
	*/
	public java.lang.String getMimetype() {
		return _attachments.getMimetype();
	}

	/**
	* Sets the mimetype of this attachments.
	*
	* @param mimetype the mimetype of this attachments
	*/
	public void setMimetype(java.lang.String mimetype) {
		_attachments.setMimetype(mimetype);
	}

	/**
	* Returns the ispatch of this attachments.
	*
	* @return the ispatch of this attachments
	*/
	public int getIspatch() {
		return _attachments.getIspatch();
	}

	/**
	* Sets the ispatch of this attachments.
	*
	* @param ispatch the ispatch of this attachments
	*/
	public void setIspatch(int ispatch) {
		_attachments.setIspatch(ispatch);
	}

	/**
	* Returns the filename of this attachments.
	*
	* @return the filename of this attachments
	*/
	public java.lang.String getFilename() {
		return _attachments.getFilename();
	}

	/**
	* Sets the filename of this attachments.
	*
	* @param filename the filename of this attachments
	*/
	public void setFilename(java.lang.String filename) {
		_attachments.setFilename(filename);
	}

	/**
	* Returns the submitter_id of this attachments.
	*
	* @return the submitter_id of this attachments
	*/
	public int getSubmitter_id() {
		return _attachments.getSubmitter_id();
	}

	/**
	* Sets the submitter_id of this attachments.
	*
	* @param submitter_id the submitter_id of this attachments
	*/
	public void setSubmitter_id(int submitter_id) {
		_attachments.setSubmitter_id(submitter_id);
	}

	/**
	* Returns the isobsolete of this attachments.
	*
	* @return the isobsolete of this attachments
	*/
	public int getIsobsolete() {
		return _attachments.getIsobsolete();
	}

	/**
	* Sets the isobsolete of this attachments.
	*
	* @param isobsolete the isobsolete of this attachments
	*/
	public void setIsobsolete(int isobsolete) {
		_attachments.setIsobsolete(isobsolete);
	}

	/**
	* Returns the isprivate of this attachments.
	*
	* @return the isprivate of this attachments
	*/
	public int getIsprivate() {
		return _attachments.getIsprivate();
	}

	/**
	* Sets the isprivate of this attachments.
	*
	* @param isprivate the isprivate of this attachments
	*/
	public void setIsprivate(int isprivate) {
		_attachments.setIsprivate(isprivate);
	}

	/**
	* Returns the isurl of this attachments.
	*
	* @return the isurl of this attachments
	*/
	public int getIsurl() {
		return _attachments.getIsurl();
	}

	/**
	* Sets the isurl of this attachments.
	*
	* @param isurl the isurl of this attachments
	*/
	public void setIsurl(int isurl) {
		_attachments.setIsurl(isurl);
	}

	public boolean isNew() {
		return _attachments.isNew();
	}

	public void setNew(boolean n) {
		_attachments.setNew(n);
	}

	public boolean isCachedModel() {
		return _attachments.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_attachments.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _attachments.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _attachments.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_attachments.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _attachments.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_attachments.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AttachmentsWrapper((Attachments)_attachments.clone());
	}

	public int compareTo(com.vmware.model.Attachments attachments) {
		return _attachments.compareTo(attachments);
	}

	@Override
	public int hashCode() {
		return _attachments.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Attachments> toCacheModel() {
		return _attachments.toCacheModel();
	}

	public com.vmware.model.Attachments toEscapedModel() {
		return new AttachmentsWrapper(_attachments.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _attachments.toString();
	}

	public java.lang.String toXmlString() {
		return _attachments.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_attachments.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Attachments getWrappedAttachments() {
		return _attachments;
	}

	public Attachments getWrappedModel() {
		return _attachments;
	}

	public void resetOriginalValues() {
		_attachments.resetOriginalValues();
	}

	private Attachments _attachments;
}