/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.BugLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class BugClp extends BaseModelImpl<Bug> implements Bug {
	public BugClp() {
	}

	public Class<?> getModelClass() {
		return Bug.class;
	}

	public String getModelClassName() {
		return Bug.class.getName();
	}

	public int getPrimaryKey() {
		return _bug_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setBug_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_bug_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("assigned_to", getAssigned_to());
		attributes.put("bug_file_loc", getBug_file_loc());
		attributes.put("bug_severity", getBug_severity());
		attributes.put("bug_status", getBug_status());
		attributes.put("creation_ts", getCreation_ts());
		attributes.put("delta_ts", getDelta_ts());
		attributes.put("short_desc", getShort_desc());
		attributes.put("host_op_sys", getHost_op_sys());
		attributes.put("guest_op_sys", getGuest_op_sys());
		attributes.put("priority", getPriority());
		attributes.put("rep_platform", getRep_platform());
		attributes.put("product_id", getProduct_id());
		attributes.put("component_id", getComponent_id());
		attributes.put("qa_contact", getQa_contact());
		attributes.put("reporter", getReporter());
		attributes.put("category_id", getCategory_id());
		attributes.put("resolution", getResolution());
		attributes.put("target_milestone", getTarget_milestone());
		attributes.put("status_whiteboard", getStatus_whiteboard());
		attributes.put("votes", getVotes());
		attributes.put("keywords", getKeywords());
		attributes.put("lastdiffed", getLastdiffed());
		attributes.put("everconfirmed", getEverconfirmed());
		attributes.put("reporter_accessible", getReporter_accessible());
		attributes.put("cclist_accessible", getCclist_accessible());
		attributes.put("estimated_time", getEstimated_time());
		attributes.put("remaining_time", getRemaining_time());
		attributes.put("deadline", getDeadline());
		attributes.put("bug_alias", getBug_alias());
		attributes.put("found_in_product_id", getFound_in_product_id());
		attributes.put("found_in_version_id", getFound_in_version_id());
		attributes.put("found_in_phase_id", getFound_in_phase_id());
		attributes.put("cf_type", getCf_type());
		attributes.put("cf_reported_by", getCf_reported_by());
		attributes.put("cf_attempted", getCf_attempted());
		attributes.put("cf_failed", getCf_failed());
		attributes.put("cf_public_summary", getCf_public_summary());
		attributes.put("cf_doc_impact", getCf_doc_impact());
		attributes.put("cf_security", getCf_security());
		attributes.put("cf_build", getCf_build());
		attributes.put("cf_branch", getCf_branch());
		attributes.put("cf_change", getCf_change());
		attributes.put("cf_test_id", getCf_test_id());
		attributes.put("cf_regression", getCf_regression());
		attributes.put("cf_reviewer", getCf_reviewer());
		attributes.put("cf_on_hold", getCf_on_hold());
		attributes.put("cf_public_severity", getCf_public_severity());
		attributes.put("cf_i18n_impact", getCf_i18n_impact());
		attributes.put("cf_eta", getCf_eta());
		attributes.put("cf_bug_source", getCf_bug_source());
		attributes.put("cf_viss", getCf_viss());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer assigned_to = (Integer)attributes.get("assigned_to");

		if (assigned_to != null) {
			setAssigned_to(assigned_to);
		}

		String bug_file_loc = (String)attributes.get("bug_file_loc");

		if (bug_file_loc != null) {
			setBug_file_loc(bug_file_loc);
		}

		String bug_severity = (String)attributes.get("bug_severity");

		if (bug_severity != null) {
			setBug_severity(bug_severity);
		}

		String bug_status = (String)attributes.get("bug_status");

		if (bug_status != null) {
			setBug_status(bug_status);
		}

		Date creation_ts = (Date)attributes.get("creation_ts");

		if (creation_ts != null) {
			setCreation_ts(creation_ts);
		}

		Date delta_ts = (Date)attributes.get("delta_ts");

		if (delta_ts != null) {
			setDelta_ts(delta_ts);
		}

		String short_desc = (String)attributes.get("short_desc");

		if (short_desc != null) {
			setShort_desc(short_desc);
		}

		String host_op_sys = (String)attributes.get("host_op_sys");

		if (host_op_sys != null) {
			setHost_op_sys(host_op_sys);
		}

		String guest_op_sys = (String)attributes.get("guest_op_sys");

		if (guest_op_sys != null) {
			setGuest_op_sys(guest_op_sys);
		}

		String priority = (String)attributes.get("priority");

		if (priority != null) {
			setPriority(priority);
		}

		String rep_platform = (String)attributes.get("rep_platform");

		if (rep_platform != null) {
			setRep_platform(rep_platform);
		}

		Integer product_id = (Integer)attributes.get("product_id");

		if (product_id != null) {
			setProduct_id(product_id);
		}

		Integer component_id = (Integer)attributes.get("component_id");

		if (component_id != null) {
			setComponent_id(component_id);
		}

		Integer qa_contact = (Integer)attributes.get("qa_contact");

		if (qa_contact != null) {
			setQa_contact(qa_contact);
		}

		Integer reporter = (Integer)attributes.get("reporter");

		if (reporter != null) {
			setReporter(reporter);
		}

		Integer category_id = (Integer)attributes.get("category_id");

		if (category_id != null) {
			setCategory_id(category_id);
		}

		String resolution = (String)attributes.get("resolution");

		if (resolution != null) {
			setResolution(resolution);
		}

		String target_milestone = (String)attributes.get("target_milestone");

		if (target_milestone != null) {
			setTarget_milestone(target_milestone);
		}

		String status_whiteboard = (String)attributes.get("status_whiteboard");

		if (status_whiteboard != null) {
			setStatus_whiteboard(status_whiteboard);
		}

		Integer votes = (Integer)attributes.get("votes");

		if (votes != null) {
			setVotes(votes);
		}

		String keywords = (String)attributes.get("keywords");

		if (keywords != null) {
			setKeywords(keywords);
		}

		Date lastdiffed = (Date)attributes.get("lastdiffed");

		if (lastdiffed != null) {
			setLastdiffed(lastdiffed);
		}

		Integer everconfirmed = (Integer)attributes.get("everconfirmed");

		if (everconfirmed != null) {
			setEverconfirmed(everconfirmed);
		}

		Integer reporter_accessible = (Integer)attributes.get(
				"reporter_accessible");

		if (reporter_accessible != null) {
			setReporter_accessible(reporter_accessible);
		}

		Integer cclist_accessible = (Integer)attributes.get("cclist_accessible");

		if (cclist_accessible != null) {
			setCclist_accessible(cclist_accessible);
		}

		Float estimated_time = (Float)attributes.get("estimated_time");

		if (estimated_time != null) {
			setEstimated_time(estimated_time);
		}

		Float remaining_time = (Float)attributes.get("remaining_time");

		if (remaining_time != null) {
			setRemaining_time(remaining_time);
		}

		Date deadline = (Date)attributes.get("deadline");

		if (deadline != null) {
			setDeadline(deadline);
		}

		String bug_alias = (String)attributes.get("bug_alias");

		if (bug_alias != null) {
			setBug_alias(bug_alias);
		}

		Integer found_in_product_id = (Integer)attributes.get(
				"found_in_product_id");

		if (found_in_product_id != null) {
			setFound_in_product_id(found_in_product_id);
		}

		Integer found_in_version_id = (Integer)attributes.get(
				"found_in_version_id");

		if (found_in_version_id != null) {
			setFound_in_version_id(found_in_version_id);
		}

		Integer found_in_phase_id = (Integer)attributes.get("found_in_phase_id");

		if (found_in_phase_id != null) {
			setFound_in_phase_id(found_in_phase_id);
		}

		String cf_type = (String)attributes.get("cf_type");

		if (cf_type != null) {
			setCf_type(cf_type);
		}

		String cf_reported_by = (String)attributes.get("cf_reported_by");

		if (cf_reported_by != null) {
			setCf_reported_by(cf_reported_by);
		}

		Integer cf_attempted = (Integer)attributes.get("cf_attempted");

		if (cf_attempted != null) {
			setCf_attempted(cf_attempted);
		}

		Integer cf_failed = (Integer)attributes.get("cf_failed");

		if (cf_failed != null) {
			setCf_failed(cf_failed);
		}

		String cf_public_summary = (String)attributes.get("cf_public_summary");

		if (cf_public_summary != null) {
			setCf_public_summary(cf_public_summary);
		}

		Integer cf_doc_impact = (Integer)attributes.get("cf_doc_impact");

		if (cf_doc_impact != null) {
			setCf_doc_impact(cf_doc_impact);
		}

		Integer cf_security = (Integer)attributes.get("cf_security");

		if (cf_security != null) {
			setCf_security(cf_security);
		}

		Integer cf_build = (Integer)attributes.get("cf_build");

		if (cf_build != null) {
			setCf_build(cf_build);
		}

		String cf_branch = (String)attributes.get("cf_branch");

		if (cf_branch != null) {
			setCf_branch(cf_branch);
		}

		Integer cf_change = (Integer)attributes.get("cf_change");

		if (cf_change != null) {
			setCf_change(cf_change);
		}

		Integer cf_test_id = (Integer)attributes.get("cf_test_id");

		if (cf_test_id != null) {
			setCf_test_id(cf_test_id);
		}

		String cf_regression = (String)attributes.get("cf_regression");

		if (cf_regression != null) {
			setCf_regression(cf_regression);
		}

		Integer cf_reviewer = (Integer)attributes.get("cf_reviewer");

		if (cf_reviewer != null) {
			setCf_reviewer(cf_reviewer);
		}

		String cf_on_hold = (String)attributes.get("cf_on_hold");

		if (cf_on_hold != null) {
			setCf_on_hold(cf_on_hold);
		}

		String cf_public_severity = (String)attributes.get("cf_public_severity");

		if (cf_public_severity != null) {
			setCf_public_severity(cf_public_severity);
		}

		Integer cf_i18n_impact = (Integer)attributes.get("cf_i18n_impact");

		if (cf_i18n_impact != null) {
			setCf_i18n_impact(cf_i18n_impact);
		}

		Date cf_eta = (Date)attributes.get("cf_eta");

		if (cf_eta != null) {
			setCf_eta(cf_eta);
		}

		String cf_bug_source = (String)attributes.get("cf_bug_source");

		if (cf_bug_source != null) {
			setCf_bug_source(cf_bug_source);
		}

		Float cf_viss = (Float)attributes.get("cf_viss");

		if (cf_viss != null) {
			setCf_viss(cf_viss);
		}
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getAssigned_to() {
		return _assigned_to;
	}

	public void setAssigned_to(int assigned_to) {
		_assigned_to = assigned_to;
	}

	public String getBug_file_loc() {
		return _bug_file_loc;
	}

	public void setBug_file_loc(String bug_file_loc) {
		_bug_file_loc = bug_file_loc;
	}

	public String getBug_severity() {
		return _bug_severity;
	}

	public void setBug_severity(String bug_severity) {
		_bug_severity = bug_severity;
	}

	public String getBug_status() {
		return _bug_status;
	}

	public void setBug_status(String bug_status) {
		_bug_status = bug_status;
	}

	public Date getCreation_ts() {
		return _creation_ts;
	}

	public void setCreation_ts(Date creation_ts) {
		_creation_ts = creation_ts;
	}

	public Date getDelta_ts() {
		return _delta_ts;
	}

	public void setDelta_ts(Date delta_ts) {
		_delta_ts = delta_ts;
	}

	public String getShort_desc() {
		return _short_desc;
	}

	public void setShort_desc(String short_desc) {
		_short_desc = short_desc;
	}

	public String getHost_op_sys() {
		return _host_op_sys;
	}

	public void setHost_op_sys(String host_op_sys) {
		_host_op_sys = host_op_sys;
	}

	public String getGuest_op_sys() {
		return _guest_op_sys;
	}

	public void setGuest_op_sys(String guest_op_sys) {
		_guest_op_sys = guest_op_sys;
	}

	public String getPriority() {
		return _priority;
	}

	public void setPriority(String priority) {
		_priority = priority;
	}

	public String getRep_platform() {
		return _rep_platform;
	}

	public void setRep_platform(String rep_platform) {
		_rep_platform = rep_platform;
	}

	public int getProduct_id() {
		return _product_id;
	}

	public void setProduct_id(int product_id) {
		_product_id = product_id;
	}

	public int getComponent_id() {
		return _component_id;
	}

	public void setComponent_id(int component_id) {
		_component_id = component_id;
	}

	public int getQa_contact() {
		return _qa_contact;
	}

	public void setQa_contact(int qa_contact) {
		_qa_contact = qa_contact;
	}

	public int getReporter() {
		return _reporter;
	}

	public void setReporter(int reporter) {
		_reporter = reporter;
	}

	public int getCategory_id() {
		return _category_id;
	}

	public void setCategory_id(int category_id) {
		_category_id = category_id;
	}

	public String getResolution() {
		return _resolution;
	}

	public void setResolution(String resolution) {
		_resolution = resolution;
	}

	public String getTarget_milestone() {
		return _target_milestone;
	}

	public void setTarget_milestone(String target_milestone) {
		_target_milestone = target_milestone;
	}

	public String getStatus_whiteboard() {
		return _status_whiteboard;
	}

	public void setStatus_whiteboard(String status_whiteboard) {
		_status_whiteboard = status_whiteboard;
	}

	public int getVotes() {
		return _votes;
	}

	public void setVotes(int votes) {
		_votes = votes;
	}

	public String getKeywords() {
		return _keywords;
	}

	public void setKeywords(String keywords) {
		_keywords = keywords;
	}

	public Date getLastdiffed() {
		return _lastdiffed;
	}

	public void setLastdiffed(Date lastdiffed) {
		_lastdiffed = lastdiffed;
	}

	public int getEverconfirmed() {
		return _everconfirmed;
	}

	public void setEverconfirmed(int everconfirmed) {
		_everconfirmed = everconfirmed;
	}

	public int getReporter_accessible() {
		return _reporter_accessible;
	}

	public void setReporter_accessible(int reporter_accessible) {
		_reporter_accessible = reporter_accessible;
	}

	public int getCclist_accessible() {
		return _cclist_accessible;
	}

	public void setCclist_accessible(int cclist_accessible) {
		_cclist_accessible = cclist_accessible;
	}

	public float getEstimated_time() {
		return _estimated_time;
	}

	public void setEstimated_time(float estimated_time) {
		_estimated_time = estimated_time;
	}

	public float getRemaining_time() {
		return _remaining_time;
	}

	public void setRemaining_time(float remaining_time) {
		_remaining_time = remaining_time;
	}

	public Date getDeadline() {
		return _deadline;
	}

	public void setDeadline(Date deadline) {
		_deadline = deadline;
	}

	public String getBug_alias() {
		return _bug_alias;
	}

	public void setBug_alias(String bug_alias) {
		_bug_alias = bug_alias;
	}

	public int getFound_in_product_id() {
		return _found_in_product_id;
	}

	public void setFound_in_product_id(int found_in_product_id) {
		_found_in_product_id = found_in_product_id;
	}

	public int getFound_in_version_id() {
		return _found_in_version_id;
	}

	public void setFound_in_version_id(int found_in_version_id) {
		_found_in_version_id = found_in_version_id;
	}

	public int getFound_in_phase_id() {
		return _found_in_phase_id;
	}

	public void setFound_in_phase_id(int found_in_phase_id) {
		_found_in_phase_id = found_in_phase_id;
	}

	public String getCf_type() {
		return _cf_type;
	}

	public void setCf_type(String cf_type) {
		_cf_type = cf_type;
	}

	public String getCf_reported_by() {
		return _cf_reported_by;
	}

	public void setCf_reported_by(String cf_reported_by) {
		_cf_reported_by = cf_reported_by;
	}

	public int getCf_attempted() {
		return _cf_attempted;
	}

	public void setCf_attempted(int cf_attempted) {
		_cf_attempted = cf_attempted;
	}

	public int getCf_failed() {
		return _cf_failed;
	}

	public void setCf_failed(int cf_failed) {
		_cf_failed = cf_failed;
	}

	public String getCf_public_summary() {
		return _cf_public_summary;
	}

	public void setCf_public_summary(String cf_public_summary) {
		_cf_public_summary = cf_public_summary;
	}

	public int getCf_doc_impact() {
		return _cf_doc_impact;
	}

	public void setCf_doc_impact(int cf_doc_impact) {
		_cf_doc_impact = cf_doc_impact;
	}

	public int getCf_security() {
		return _cf_security;
	}

	public void setCf_security(int cf_security) {
		_cf_security = cf_security;
	}

	public int getCf_build() {
		return _cf_build;
	}

	public void setCf_build(int cf_build) {
		_cf_build = cf_build;
	}

	public String getCf_branch() {
		return _cf_branch;
	}

	public void setCf_branch(String cf_branch) {
		_cf_branch = cf_branch;
	}

	public int getCf_change() {
		return _cf_change;
	}

	public void setCf_change(int cf_change) {
		_cf_change = cf_change;
	}

	public int getCf_test_id() {
		return _cf_test_id;
	}

	public void setCf_test_id(int cf_test_id) {
		_cf_test_id = cf_test_id;
	}

	public String getCf_regression() {
		return _cf_regression;
	}

	public void setCf_regression(String cf_regression) {
		_cf_regression = cf_regression;
	}

	public int getCf_reviewer() {
		return _cf_reviewer;
	}

	public void setCf_reviewer(int cf_reviewer) {
		_cf_reviewer = cf_reviewer;
	}

	public String getCf_on_hold() {
		return _cf_on_hold;
	}

	public void setCf_on_hold(String cf_on_hold) {
		_cf_on_hold = cf_on_hold;
	}

	public String getCf_public_severity() {
		return _cf_public_severity;
	}

	public void setCf_public_severity(String cf_public_severity) {
		_cf_public_severity = cf_public_severity;
	}

	public int getCf_i18n_impact() {
		return _cf_i18n_impact;
	}

	public void setCf_i18n_impact(int cf_i18n_impact) {
		_cf_i18n_impact = cf_i18n_impact;
	}

	public Date getCf_eta() {
		return _cf_eta;
	}

	public void setCf_eta(Date cf_eta) {
		_cf_eta = cf_eta;
	}

	public String getCf_bug_source() {
		return _cf_bug_source;
	}

	public void setCf_bug_source(String cf_bug_source) {
		_cf_bug_source = cf_bug_source;
	}

	public float getCf_viss() {
		return _cf_viss;
	}

	public void setCf_viss(float cf_viss) {
		_cf_viss = cf_viss;
	}

	public BaseModel<?> getBugRemoteModel() {
		return _bugRemoteModel;
	}

	public void setBugRemoteModel(BaseModel<?> bugRemoteModel) {
		_bugRemoteModel = bugRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			BugLocalServiceUtil.addBug(this);
		}
		else {
			BugLocalServiceUtil.updateBug(this);
		}
	}

	@Override
	public Bug toEscapedModel() {
		return (Bug)Proxy.newProxyInstance(Bug.class.getClassLoader(),
			new Class[] { Bug.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BugClp clone = new BugClp();

		clone.setBug_id(getBug_id());
		clone.setAssigned_to(getAssigned_to());
		clone.setBug_file_loc(getBug_file_loc());
		clone.setBug_severity(getBug_severity());
		clone.setBug_status(getBug_status());
		clone.setCreation_ts(getCreation_ts());
		clone.setDelta_ts(getDelta_ts());
		clone.setShort_desc(getShort_desc());
		clone.setHost_op_sys(getHost_op_sys());
		clone.setGuest_op_sys(getGuest_op_sys());
		clone.setPriority(getPriority());
		clone.setRep_platform(getRep_platform());
		clone.setProduct_id(getProduct_id());
		clone.setComponent_id(getComponent_id());
		clone.setQa_contact(getQa_contact());
		clone.setReporter(getReporter());
		clone.setCategory_id(getCategory_id());
		clone.setResolution(getResolution());
		clone.setTarget_milestone(getTarget_milestone());
		clone.setStatus_whiteboard(getStatus_whiteboard());
		clone.setVotes(getVotes());
		clone.setKeywords(getKeywords());
		clone.setLastdiffed(getLastdiffed());
		clone.setEverconfirmed(getEverconfirmed());
		clone.setReporter_accessible(getReporter_accessible());
		clone.setCclist_accessible(getCclist_accessible());
		clone.setEstimated_time(getEstimated_time());
		clone.setRemaining_time(getRemaining_time());
		clone.setDeadline(getDeadline());
		clone.setBug_alias(getBug_alias());
		clone.setFound_in_product_id(getFound_in_product_id());
		clone.setFound_in_version_id(getFound_in_version_id());
		clone.setFound_in_phase_id(getFound_in_phase_id());
		clone.setCf_type(getCf_type());
		clone.setCf_reported_by(getCf_reported_by());
		clone.setCf_attempted(getCf_attempted());
		clone.setCf_failed(getCf_failed());
		clone.setCf_public_summary(getCf_public_summary());
		clone.setCf_doc_impact(getCf_doc_impact());
		clone.setCf_security(getCf_security());
		clone.setCf_build(getCf_build());
		clone.setCf_branch(getCf_branch());
		clone.setCf_change(getCf_change());
		clone.setCf_test_id(getCf_test_id());
		clone.setCf_regression(getCf_regression());
		clone.setCf_reviewer(getCf_reviewer());
		clone.setCf_on_hold(getCf_on_hold());
		clone.setCf_public_severity(getCf_public_severity());
		clone.setCf_i18n_impact(getCf_i18n_impact());
		clone.setCf_eta(getCf_eta());
		clone.setCf_bug_source(getCf_bug_source());
		clone.setCf_viss(getCf_viss());

		return clone;
	}

	public int compareTo(Bug bug) {
		int value = 0;

		value = DateUtil.compareTo(getCreation_ts(), bug.getCreation_ts());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getVotes() < bug.getVotes()) {
			value = -1;
		}
		else if (getVotes() > bug.getVotes()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(getDeadline(), bug.getDeadline());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getEstimated_time() < bug.getEstimated_time()) {
			value = -1;
		}
		else if (getEstimated_time() > bug.getEstimated_time()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getRemaining_time() < bug.getRemaining_time()) {
			value = -1;
		}
		else if (getRemaining_time() > bug.getRemaining_time()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		BugClp bug = null;

		try {
			bug = (BugClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = bug.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(105);

		sb.append("{bug_id=");
		sb.append(getBug_id());
		sb.append(", assigned_to=");
		sb.append(getAssigned_to());
		sb.append(", bug_file_loc=");
		sb.append(getBug_file_loc());
		sb.append(", bug_severity=");
		sb.append(getBug_severity());
		sb.append(", bug_status=");
		sb.append(getBug_status());
		sb.append(", creation_ts=");
		sb.append(getCreation_ts());
		sb.append(", delta_ts=");
		sb.append(getDelta_ts());
		sb.append(", short_desc=");
		sb.append(getShort_desc());
		sb.append(", host_op_sys=");
		sb.append(getHost_op_sys());
		sb.append(", guest_op_sys=");
		sb.append(getGuest_op_sys());
		sb.append(", priority=");
		sb.append(getPriority());
		sb.append(", rep_platform=");
		sb.append(getRep_platform());
		sb.append(", product_id=");
		sb.append(getProduct_id());
		sb.append(", component_id=");
		sb.append(getComponent_id());
		sb.append(", qa_contact=");
		sb.append(getQa_contact());
		sb.append(", reporter=");
		sb.append(getReporter());
		sb.append(", category_id=");
		sb.append(getCategory_id());
		sb.append(", resolution=");
		sb.append(getResolution());
		sb.append(", target_milestone=");
		sb.append(getTarget_milestone());
		sb.append(", status_whiteboard=");
		sb.append(getStatus_whiteboard());
		sb.append(", votes=");
		sb.append(getVotes());
		sb.append(", keywords=");
		sb.append(getKeywords());
		sb.append(", lastdiffed=");
		sb.append(getLastdiffed());
		sb.append(", everconfirmed=");
		sb.append(getEverconfirmed());
		sb.append(", reporter_accessible=");
		sb.append(getReporter_accessible());
		sb.append(", cclist_accessible=");
		sb.append(getCclist_accessible());
		sb.append(", estimated_time=");
		sb.append(getEstimated_time());
		sb.append(", remaining_time=");
		sb.append(getRemaining_time());
		sb.append(", deadline=");
		sb.append(getDeadline());
		sb.append(", bug_alias=");
		sb.append(getBug_alias());
		sb.append(", found_in_product_id=");
		sb.append(getFound_in_product_id());
		sb.append(", found_in_version_id=");
		sb.append(getFound_in_version_id());
		sb.append(", found_in_phase_id=");
		sb.append(getFound_in_phase_id());
		sb.append(", cf_type=");
		sb.append(getCf_type());
		sb.append(", cf_reported_by=");
		sb.append(getCf_reported_by());
		sb.append(", cf_attempted=");
		sb.append(getCf_attempted());
		sb.append(", cf_failed=");
		sb.append(getCf_failed());
		sb.append(", cf_public_summary=");
		sb.append(getCf_public_summary());
		sb.append(", cf_doc_impact=");
		sb.append(getCf_doc_impact());
		sb.append(", cf_security=");
		sb.append(getCf_security());
		sb.append(", cf_build=");
		sb.append(getCf_build());
		sb.append(", cf_branch=");
		sb.append(getCf_branch());
		sb.append(", cf_change=");
		sb.append(getCf_change());
		sb.append(", cf_test_id=");
		sb.append(getCf_test_id());
		sb.append(", cf_regression=");
		sb.append(getCf_regression());
		sb.append(", cf_reviewer=");
		sb.append(getCf_reviewer());
		sb.append(", cf_on_hold=");
		sb.append(getCf_on_hold());
		sb.append(", cf_public_severity=");
		sb.append(getCf_public_severity());
		sb.append(", cf_i18n_impact=");
		sb.append(getCf_i18n_impact());
		sb.append(", cf_eta=");
		sb.append(getCf_eta());
		sb.append(", cf_bug_source=");
		sb.append(getCf_bug_source());
		sb.append(", cf_viss=");
		sb.append(getCf_viss());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(160);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Bug");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>assigned_to</column-name><column-value><![CDATA[");
		sb.append(getAssigned_to());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_file_loc</column-name><column-value><![CDATA[");
		sb.append(getBug_file_loc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_severity</column-name><column-value><![CDATA[");
		sb.append(getBug_severity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_status</column-name><column-value><![CDATA[");
		sb.append(getBug_status());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>creation_ts</column-name><column-value><![CDATA[");
		sb.append(getCreation_ts());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>delta_ts</column-name><column-value><![CDATA[");
		sb.append(getDelta_ts());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>short_desc</column-name><column-value><![CDATA[");
		sb.append(getShort_desc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>host_op_sys</column-name><column-value><![CDATA[");
		sb.append(getHost_op_sys());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>guest_op_sys</column-name><column-value><![CDATA[");
		sb.append(getGuest_op_sys());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>priority</column-name><column-value><![CDATA[");
		sb.append(getPriority());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rep_platform</column-name><column-value><![CDATA[");
		sb.append(getRep_platform());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>product_id</column-name><column-value><![CDATA[");
		sb.append(getProduct_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>component_id</column-name><column-value><![CDATA[");
		sb.append(getComponent_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>qa_contact</column-name><column-value><![CDATA[");
		sb.append(getQa_contact());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reporter</column-name><column-value><![CDATA[");
		sb.append(getReporter());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>category_id</column-name><column-value><![CDATA[");
		sb.append(getCategory_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>resolution</column-name><column-value><![CDATA[");
		sb.append(getResolution());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>target_milestone</column-name><column-value><![CDATA[");
		sb.append(getTarget_milestone());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status_whiteboard</column-name><column-value><![CDATA[");
		sb.append(getStatus_whiteboard());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>votes</column-name><column-value><![CDATA[");
		sb.append(getVotes());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>keywords</column-name><column-value><![CDATA[");
		sb.append(getKeywords());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastdiffed</column-name><column-value><![CDATA[");
		sb.append(getLastdiffed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>everconfirmed</column-name><column-value><![CDATA[");
		sb.append(getEverconfirmed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reporter_accessible</column-name><column-value><![CDATA[");
		sb.append(getReporter_accessible());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cclist_accessible</column-name><column-value><![CDATA[");
		sb.append(getCclist_accessible());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>estimated_time</column-name><column-value><![CDATA[");
		sb.append(getEstimated_time());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>remaining_time</column-name><column-value><![CDATA[");
		sb.append(getRemaining_time());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deadline</column-name><column-value><![CDATA[");
		sb.append(getDeadline());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_alias</column-name><column-value><![CDATA[");
		sb.append(getBug_alias());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>found_in_product_id</column-name><column-value><![CDATA[");
		sb.append(getFound_in_product_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>found_in_version_id</column-name><column-value><![CDATA[");
		sb.append(getFound_in_version_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>found_in_phase_id</column-name><column-value><![CDATA[");
		sb.append(getFound_in_phase_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_type</column-name><column-value><![CDATA[");
		sb.append(getCf_type());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_reported_by</column-name><column-value><![CDATA[");
		sb.append(getCf_reported_by());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_attempted</column-name><column-value><![CDATA[");
		sb.append(getCf_attempted());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_failed</column-name><column-value><![CDATA[");
		sb.append(getCf_failed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_public_summary</column-name><column-value><![CDATA[");
		sb.append(getCf_public_summary());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_doc_impact</column-name><column-value><![CDATA[");
		sb.append(getCf_doc_impact());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_security</column-name><column-value><![CDATA[");
		sb.append(getCf_security());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_build</column-name><column-value><![CDATA[");
		sb.append(getCf_build());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_branch</column-name><column-value><![CDATA[");
		sb.append(getCf_branch());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_change</column-name><column-value><![CDATA[");
		sb.append(getCf_change());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_test_id</column-name><column-value><![CDATA[");
		sb.append(getCf_test_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_regression</column-name><column-value><![CDATA[");
		sb.append(getCf_regression());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_reviewer</column-name><column-value><![CDATA[");
		sb.append(getCf_reviewer());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_on_hold</column-name><column-value><![CDATA[");
		sb.append(getCf_on_hold());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_public_severity</column-name><column-value><![CDATA[");
		sb.append(getCf_public_severity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_i18n_impact</column-name><column-value><![CDATA[");
		sb.append(getCf_i18n_impact());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_eta</column-name><column-value><![CDATA[");
		sb.append(getCf_eta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_bug_source</column-name><column-value><![CDATA[");
		sb.append(getCf_bug_source());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cf_viss</column-name><column-value><![CDATA[");
		sb.append(getCf_viss());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_id;
	private int _assigned_to;
	private String _bug_file_loc;
	private String _bug_severity;
	private String _bug_status;
	private Date _creation_ts;
	private Date _delta_ts;
	private String _short_desc;
	private String _host_op_sys;
	private String _guest_op_sys;
	private String _priority;
	private String _rep_platform;
	private int _product_id;
	private int _component_id;
	private int _qa_contact;
	private int _reporter;
	private int _category_id;
	private String _resolution;
	private String _target_milestone;
	private String _status_whiteboard;
	private int _votes;
	private String _keywords;
	private Date _lastdiffed;
	private int _everconfirmed;
	private int _reporter_accessible;
	private int _cclist_accessible;
	private float _estimated_time;
	private float _remaining_time;
	private Date _deadline;
	private String _bug_alias;
	private int _found_in_product_id;
	private int _found_in_version_id;
	private int _found_in_phase_id;
	private String _cf_type;
	private String _cf_reported_by;
	private int _cf_attempted;
	private int _cf_failed;
	private String _cf_public_summary;
	private int _cf_doc_impact;
	private int _cf_security;
	private int _cf_build;
	private String _cf_branch;
	private int _cf_change;
	private int _cf_test_id;
	private String _cf_regression;
	private int _cf_reviewer;
	private String _cf_on_hold;
	private String _cf_public_severity;
	private int _cf_i18n_impact;
	private Date _cf_eta;
	private String _cf_bug_source;
	private float _cf_viss;
	private BaseModel<?> _bugRemoteModel;
}