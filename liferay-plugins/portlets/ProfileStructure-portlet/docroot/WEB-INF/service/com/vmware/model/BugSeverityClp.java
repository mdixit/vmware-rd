/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.BugSeverityLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class BugSeverityClp extends BaseModelImpl<BugSeverity>
	implements BugSeverity {
	public BugSeverityClp() {
	}

	public Class<?> getModelClass() {
		return BugSeverity.class;
	}

	public String getModelClassName() {
		return BugSeverity.class.getName();
	}

	public int getPrimaryKey() {
		return _bug_severtiy_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setBug_severtiy_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_bug_severtiy_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_severtiy_id", getBug_severtiy_id());
		attributes.put("value", getValue());
		attributes.put("sortkey", getSortkey());
		attributes.put("isactive", getIsactive());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_severtiy_id = (Integer)attributes.get("bug_severtiy_id");

		if (bug_severtiy_id != null) {
			setBug_severtiy_id(bug_severtiy_id);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}
	}

	public int getBug_severtiy_id() {
		return _bug_severtiy_id;
	}

	public void setBug_severtiy_id(int bug_severtiy_id) {
		_bug_severtiy_id = bug_severtiy_id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	public BaseModel<?> getBugSeverityRemoteModel() {
		return _bugSeverityRemoteModel;
	}

	public void setBugSeverityRemoteModel(BaseModel<?> bugSeverityRemoteModel) {
		_bugSeverityRemoteModel = bugSeverityRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			BugSeverityLocalServiceUtil.addBugSeverity(this);
		}
		else {
			BugSeverityLocalServiceUtil.updateBugSeverity(this);
		}
	}

	@Override
	public BugSeverity toEscapedModel() {
		return (BugSeverity)Proxy.newProxyInstance(BugSeverity.class.getClassLoader(),
			new Class[] { BugSeverity.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BugSeverityClp clone = new BugSeverityClp();

		clone.setBug_severtiy_id(getBug_severtiy_id());
		clone.setValue(getValue());
		clone.setSortkey(getSortkey());
		clone.setIsactive(getIsactive());

		return clone;
	}

	public int compareTo(BugSeverity bugSeverity) {
		int primaryKey = bugSeverity.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		BugSeverityClp bugSeverity = null;

		try {
			bugSeverity = (BugSeverityClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = bugSeverity.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{bug_severtiy_id=");
		sb.append(getBug_severtiy_id());
		sb.append(", value=");
		sb.append(getValue());
		sb.append(", sortkey=");
		sb.append(getSortkey());
		sb.append(", isactive=");
		sb.append(getIsactive());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.BugSeverity");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_severtiy_id</column-name><column-value><![CDATA[");
		sb.append(getBug_severtiy_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>value</column-name><column-value><![CDATA[");
		sb.append(getValue());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sortkey</column-name><column-value><![CDATA[");
		sb.append(getSortkey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isactive</column-name><column-value><![CDATA[");
		sb.append(getIsactive());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_severtiy_id;
	private String _value;
	private int _sortkey;
	private int _isactive;
	private BaseModel<?> _bugSeverityRemoteModel;
}