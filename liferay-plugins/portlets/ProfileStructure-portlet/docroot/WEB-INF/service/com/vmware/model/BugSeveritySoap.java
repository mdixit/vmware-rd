/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.BugSeverityServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.BugSeverityServiceSoap
 * @generated
 */
public class BugSeveritySoap implements Serializable {
	public static BugSeveritySoap toSoapModel(BugSeverity model) {
		BugSeveritySoap soapModel = new BugSeveritySoap();

		soapModel.setBug_severtiy_id(model.getBug_severtiy_id());
		soapModel.setValue(model.getValue());
		soapModel.setSortkey(model.getSortkey());
		soapModel.setIsactive(model.getIsactive());

		return soapModel;
	}

	public static BugSeveritySoap[] toSoapModels(BugSeverity[] models) {
		BugSeveritySoap[] soapModels = new BugSeveritySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BugSeveritySoap[][] toSoapModels(BugSeverity[][] models) {
		BugSeveritySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BugSeveritySoap[models.length][models[0].length];
		}
		else {
			soapModels = new BugSeveritySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BugSeveritySoap[] toSoapModels(List<BugSeverity> models) {
		List<BugSeveritySoap> soapModels = new ArrayList<BugSeveritySoap>(models.size());

		for (BugSeverity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BugSeveritySoap[soapModels.size()]);
	}

	public BugSeveritySoap() {
	}

	public int getPrimaryKey() {
		return _bug_severtiy_id;
	}

	public void setPrimaryKey(int pk) {
		setBug_severtiy_id(pk);
	}

	public int getBug_severtiy_id() {
		return _bug_severtiy_id;
	}

	public void setBug_severtiy_id(int bug_severtiy_id) {
		_bug_severtiy_id = bug_severtiy_id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	private int _bug_severtiy_id;
	private String _value;
	private int _sortkey;
	private int _isactive;
}