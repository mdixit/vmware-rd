/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BugSeverity}.
 * </p>
 *
 * @author    iscisc
 * @see       BugSeverity
 * @generated
 */
public class BugSeverityWrapper implements BugSeverity,
	ModelWrapper<BugSeverity> {
	public BugSeverityWrapper(BugSeverity bugSeverity) {
		_bugSeverity = bugSeverity;
	}

	public Class<?> getModelClass() {
		return BugSeverity.class;
	}

	public String getModelClassName() {
		return BugSeverity.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_severtiy_id", getBug_severtiy_id());
		attributes.put("value", getValue());
		attributes.put("sortkey", getSortkey());
		attributes.put("isactive", getIsactive());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_severtiy_id = (Integer)attributes.get("bug_severtiy_id");

		if (bug_severtiy_id != null) {
			setBug_severtiy_id(bug_severtiy_id);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}
	}

	/**
	* Returns the primary key of this bug severity.
	*
	* @return the primary key of this bug severity
	*/
	public int getPrimaryKey() {
		return _bugSeverity.getPrimaryKey();
	}

	/**
	* Sets the primary key of this bug severity.
	*
	* @param primaryKey the primary key of this bug severity
	*/
	public void setPrimaryKey(int primaryKey) {
		_bugSeverity.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_severtiy_id of this bug severity.
	*
	* @return the bug_severtiy_id of this bug severity
	*/
	public int getBug_severtiy_id() {
		return _bugSeverity.getBug_severtiy_id();
	}

	/**
	* Sets the bug_severtiy_id of this bug severity.
	*
	* @param bug_severtiy_id the bug_severtiy_id of this bug severity
	*/
	public void setBug_severtiy_id(int bug_severtiy_id) {
		_bugSeverity.setBug_severtiy_id(bug_severtiy_id);
	}

	/**
	* Returns the value of this bug severity.
	*
	* @return the value of this bug severity
	*/
	public java.lang.String getValue() {
		return _bugSeverity.getValue();
	}

	/**
	* Sets the value of this bug severity.
	*
	* @param value the value of this bug severity
	*/
	public void setValue(java.lang.String value) {
		_bugSeverity.setValue(value);
	}

	/**
	* Returns the sortkey of this bug severity.
	*
	* @return the sortkey of this bug severity
	*/
	public int getSortkey() {
		return _bugSeverity.getSortkey();
	}

	/**
	* Sets the sortkey of this bug severity.
	*
	* @param sortkey the sortkey of this bug severity
	*/
	public void setSortkey(int sortkey) {
		_bugSeverity.setSortkey(sortkey);
	}

	/**
	* Returns the isactive of this bug severity.
	*
	* @return the isactive of this bug severity
	*/
	public int getIsactive() {
		return _bugSeverity.getIsactive();
	}

	/**
	* Sets the isactive of this bug severity.
	*
	* @param isactive the isactive of this bug severity
	*/
	public void setIsactive(int isactive) {
		_bugSeverity.setIsactive(isactive);
	}

	public boolean isNew() {
		return _bugSeverity.isNew();
	}

	public void setNew(boolean n) {
		_bugSeverity.setNew(n);
	}

	public boolean isCachedModel() {
		return _bugSeverity.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_bugSeverity.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _bugSeverity.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _bugSeverity.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_bugSeverity.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _bugSeverity.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_bugSeverity.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BugSeverityWrapper((BugSeverity)_bugSeverity.clone());
	}

	public int compareTo(com.vmware.model.BugSeverity bugSeverity) {
		return _bugSeverity.compareTo(bugSeverity);
	}

	@Override
	public int hashCode() {
		return _bugSeverity.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.BugSeverity> toCacheModel() {
		return _bugSeverity.toCacheModel();
	}

	public com.vmware.model.BugSeverity toEscapedModel() {
		return new BugSeverityWrapper(_bugSeverity.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _bugSeverity.toString();
	}

	public java.lang.String toXmlString() {
		return _bugSeverity.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_bugSeverity.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BugSeverity getWrappedBugSeverity() {
		return _bugSeverity;
	}

	public BugSeverity getWrappedModel() {
		return _bugSeverity;
	}

	public void resetOriginalValues() {
		_bugSeverity.resetOriginalValues();
	}

	private BugSeverity _bugSeverity;
}