/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.BugServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.BugServiceSoap
 * @generated
 */
public class BugSoap implements Serializable {
	public static BugSoap toSoapModel(Bug model) {
		BugSoap soapModel = new BugSoap();

		soapModel.setBug_id(model.getBug_id());
		soapModel.setAssigned_to(model.getAssigned_to());
		soapModel.setBug_file_loc(model.getBug_file_loc());
		soapModel.setBug_severity(model.getBug_severity());
		soapModel.setBug_status(model.getBug_status());
		soapModel.setCreation_ts(model.getCreation_ts());
		soapModel.setDelta_ts(model.getDelta_ts());
		soapModel.setShort_desc(model.getShort_desc());
		soapModel.setHost_op_sys(model.getHost_op_sys());
		soapModel.setGuest_op_sys(model.getGuest_op_sys());
		soapModel.setPriority(model.getPriority());
		soapModel.setRep_platform(model.getRep_platform());
		soapModel.setProduct_id(model.getProduct_id());
		soapModel.setComponent_id(model.getComponent_id());
		soapModel.setQa_contact(model.getQa_contact());
		soapModel.setReporter(model.getReporter());
		soapModel.setCategory_id(model.getCategory_id());
		soapModel.setResolution(model.getResolution());
		soapModel.setTarget_milestone(model.getTarget_milestone());
		soapModel.setStatus_whiteboard(model.getStatus_whiteboard());
		soapModel.setVotes(model.getVotes());
		soapModel.setKeywords(model.getKeywords());
		soapModel.setLastdiffed(model.getLastdiffed());
		soapModel.setEverconfirmed(model.getEverconfirmed());
		soapModel.setReporter_accessible(model.getReporter_accessible());
		soapModel.setCclist_accessible(model.getCclist_accessible());
		soapModel.setEstimated_time(model.getEstimated_time());
		soapModel.setRemaining_time(model.getRemaining_time());
		soapModel.setDeadline(model.getDeadline());
		soapModel.setBug_alias(model.getBug_alias());
		soapModel.setFound_in_product_id(model.getFound_in_product_id());
		soapModel.setFound_in_version_id(model.getFound_in_version_id());
		soapModel.setFound_in_phase_id(model.getFound_in_phase_id());
		soapModel.setCf_type(model.getCf_type());
		soapModel.setCf_reported_by(model.getCf_reported_by());
		soapModel.setCf_attempted(model.getCf_attempted());
		soapModel.setCf_failed(model.getCf_failed());
		soapModel.setCf_public_summary(model.getCf_public_summary());
		soapModel.setCf_doc_impact(model.getCf_doc_impact());
		soapModel.setCf_security(model.getCf_security());
		soapModel.setCf_build(model.getCf_build());
		soapModel.setCf_branch(model.getCf_branch());
		soapModel.setCf_change(model.getCf_change());
		soapModel.setCf_test_id(model.getCf_test_id());
		soapModel.setCf_regression(model.getCf_regression());
		soapModel.setCf_reviewer(model.getCf_reviewer());
		soapModel.setCf_on_hold(model.getCf_on_hold());
		soapModel.setCf_public_severity(model.getCf_public_severity());
		soapModel.setCf_i18n_impact(model.getCf_i18n_impact());
		soapModel.setCf_eta(model.getCf_eta());
		soapModel.setCf_bug_source(model.getCf_bug_source());
		soapModel.setCf_viss(model.getCf_viss());

		return soapModel;
	}

	public static BugSoap[] toSoapModels(Bug[] models) {
		BugSoap[] soapModels = new BugSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BugSoap[][] toSoapModels(Bug[][] models) {
		BugSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BugSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BugSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BugSoap[] toSoapModels(List<Bug> models) {
		List<BugSoap> soapModels = new ArrayList<BugSoap>(models.size());

		for (Bug model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BugSoap[soapModels.size()]);
	}

	public BugSoap() {
	}

	public int getPrimaryKey() {
		return _bug_id;
	}

	public void setPrimaryKey(int pk) {
		setBug_id(pk);
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getAssigned_to() {
		return _assigned_to;
	}

	public void setAssigned_to(int assigned_to) {
		_assigned_to = assigned_to;
	}

	public String getBug_file_loc() {
		return _bug_file_loc;
	}

	public void setBug_file_loc(String bug_file_loc) {
		_bug_file_loc = bug_file_loc;
	}

	public String getBug_severity() {
		return _bug_severity;
	}

	public void setBug_severity(String bug_severity) {
		_bug_severity = bug_severity;
	}

	public String getBug_status() {
		return _bug_status;
	}

	public void setBug_status(String bug_status) {
		_bug_status = bug_status;
	}

	public Date getCreation_ts() {
		return _creation_ts;
	}

	public void setCreation_ts(Date creation_ts) {
		_creation_ts = creation_ts;
	}

	public Date getDelta_ts() {
		return _delta_ts;
	}

	public void setDelta_ts(Date delta_ts) {
		_delta_ts = delta_ts;
	}

	public String getShort_desc() {
		return _short_desc;
	}

	public void setShort_desc(String short_desc) {
		_short_desc = short_desc;
	}

	public String getHost_op_sys() {
		return _host_op_sys;
	}

	public void setHost_op_sys(String host_op_sys) {
		_host_op_sys = host_op_sys;
	}

	public String getGuest_op_sys() {
		return _guest_op_sys;
	}

	public void setGuest_op_sys(String guest_op_sys) {
		_guest_op_sys = guest_op_sys;
	}

	public String getPriority() {
		return _priority;
	}

	public void setPriority(String priority) {
		_priority = priority;
	}

	public String getRep_platform() {
		return _rep_platform;
	}

	public void setRep_platform(String rep_platform) {
		_rep_platform = rep_platform;
	}

	public int getProduct_id() {
		return _product_id;
	}

	public void setProduct_id(int product_id) {
		_product_id = product_id;
	}

	public int getComponent_id() {
		return _component_id;
	}

	public void setComponent_id(int component_id) {
		_component_id = component_id;
	}

	public int getQa_contact() {
		return _qa_contact;
	}

	public void setQa_contact(int qa_contact) {
		_qa_contact = qa_contact;
	}

	public int getReporter() {
		return _reporter;
	}

	public void setReporter(int reporter) {
		_reporter = reporter;
	}

	public int getCategory_id() {
		return _category_id;
	}

	public void setCategory_id(int category_id) {
		_category_id = category_id;
	}

	public String getResolution() {
		return _resolution;
	}

	public void setResolution(String resolution) {
		_resolution = resolution;
	}

	public String getTarget_milestone() {
		return _target_milestone;
	}

	public void setTarget_milestone(String target_milestone) {
		_target_milestone = target_milestone;
	}

	public String getStatus_whiteboard() {
		return _status_whiteboard;
	}

	public void setStatus_whiteboard(String status_whiteboard) {
		_status_whiteboard = status_whiteboard;
	}

	public int getVotes() {
		return _votes;
	}

	public void setVotes(int votes) {
		_votes = votes;
	}

	public String getKeywords() {
		return _keywords;
	}

	public void setKeywords(String keywords) {
		_keywords = keywords;
	}

	public Date getLastdiffed() {
		return _lastdiffed;
	}

	public void setLastdiffed(Date lastdiffed) {
		_lastdiffed = lastdiffed;
	}

	public int getEverconfirmed() {
		return _everconfirmed;
	}

	public void setEverconfirmed(int everconfirmed) {
		_everconfirmed = everconfirmed;
	}

	public int getReporter_accessible() {
		return _reporter_accessible;
	}

	public void setReporter_accessible(int reporter_accessible) {
		_reporter_accessible = reporter_accessible;
	}

	public int getCclist_accessible() {
		return _cclist_accessible;
	}

	public void setCclist_accessible(int cclist_accessible) {
		_cclist_accessible = cclist_accessible;
	}

	public float getEstimated_time() {
		return _estimated_time;
	}

	public void setEstimated_time(float estimated_time) {
		_estimated_time = estimated_time;
	}

	public float getRemaining_time() {
		return _remaining_time;
	}

	public void setRemaining_time(float remaining_time) {
		_remaining_time = remaining_time;
	}

	public Date getDeadline() {
		return _deadline;
	}

	public void setDeadline(Date deadline) {
		_deadline = deadline;
	}

	public String getBug_alias() {
		return _bug_alias;
	}

	public void setBug_alias(String bug_alias) {
		_bug_alias = bug_alias;
	}

	public int getFound_in_product_id() {
		return _found_in_product_id;
	}

	public void setFound_in_product_id(int found_in_product_id) {
		_found_in_product_id = found_in_product_id;
	}

	public int getFound_in_version_id() {
		return _found_in_version_id;
	}

	public void setFound_in_version_id(int found_in_version_id) {
		_found_in_version_id = found_in_version_id;
	}

	public int getFound_in_phase_id() {
		return _found_in_phase_id;
	}

	public void setFound_in_phase_id(int found_in_phase_id) {
		_found_in_phase_id = found_in_phase_id;
	}

	public String getCf_type() {
		return _cf_type;
	}

	public void setCf_type(String cf_type) {
		_cf_type = cf_type;
	}

	public String getCf_reported_by() {
		return _cf_reported_by;
	}

	public void setCf_reported_by(String cf_reported_by) {
		_cf_reported_by = cf_reported_by;
	}

	public int getCf_attempted() {
		return _cf_attempted;
	}

	public void setCf_attempted(int cf_attempted) {
		_cf_attempted = cf_attempted;
	}

	public int getCf_failed() {
		return _cf_failed;
	}

	public void setCf_failed(int cf_failed) {
		_cf_failed = cf_failed;
	}

	public String getCf_public_summary() {
		return _cf_public_summary;
	}

	public void setCf_public_summary(String cf_public_summary) {
		_cf_public_summary = cf_public_summary;
	}

	public int getCf_doc_impact() {
		return _cf_doc_impact;
	}

	public void setCf_doc_impact(int cf_doc_impact) {
		_cf_doc_impact = cf_doc_impact;
	}

	public int getCf_security() {
		return _cf_security;
	}

	public void setCf_security(int cf_security) {
		_cf_security = cf_security;
	}

	public int getCf_build() {
		return _cf_build;
	}

	public void setCf_build(int cf_build) {
		_cf_build = cf_build;
	}

	public String getCf_branch() {
		return _cf_branch;
	}

	public void setCf_branch(String cf_branch) {
		_cf_branch = cf_branch;
	}

	public int getCf_change() {
		return _cf_change;
	}

	public void setCf_change(int cf_change) {
		_cf_change = cf_change;
	}

	public int getCf_test_id() {
		return _cf_test_id;
	}

	public void setCf_test_id(int cf_test_id) {
		_cf_test_id = cf_test_id;
	}

	public String getCf_regression() {
		return _cf_regression;
	}

	public void setCf_regression(String cf_regression) {
		_cf_regression = cf_regression;
	}

	public int getCf_reviewer() {
		return _cf_reviewer;
	}

	public void setCf_reviewer(int cf_reviewer) {
		_cf_reviewer = cf_reviewer;
	}

	public String getCf_on_hold() {
		return _cf_on_hold;
	}

	public void setCf_on_hold(String cf_on_hold) {
		_cf_on_hold = cf_on_hold;
	}

	public String getCf_public_severity() {
		return _cf_public_severity;
	}

	public void setCf_public_severity(String cf_public_severity) {
		_cf_public_severity = cf_public_severity;
	}

	public int getCf_i18n_impact() {
		return _cf_i18n_impact;
	}

	public void setCf_i18n_impact(int cf_i18n_impact) {
		_cf_i18n_impact = cf_i18n_impact;
	}

	public Date getCf_eta() {
		return _cf_eta;
	}

	public void setCf_eta(Date cf_eta) {
		_cf_eta = cf_eta;
	}

	public String getCf_bug_source() {
		return _cf_bug_source;
	}

	public void setCf_bug_source(String cf_bug_source) {
		_cf_bug_source = cf_bug_source;
	}

	public float getCf_viss() {
		return _cf_viss;
	}

	public void setCf_viss(float cf_viss) {
		_cf_viss = cf_viss;
	}

	private int _bug_id;
	private int _assigned_to;
	private String _bug_file_loc;
	private String _bug_severity;
	private String _bug_status;
	private Date _creation_ts;
	private Date _delta_ts;
	private String _short_desc;
	private String _host_op_sys;
	private String _guest_op_sys;
	private String _priority;
	private String _rep_platform;
	private int _product_id;
	private int _component_id;
	private int _qa_contact;
	private int _reporter;
	private int _category_id;
	private String _resolution;
	private String _target_milestone;
	private String _status_whiteboard;
	private int _votes;
	private String _keywords;
	private Date _lastdiffed;
	private int _everconfirmed;
	private int _reporter_accessible;
	private int _cclist_accessible;
	private float _estimated_time;
	private float _remaining_time;
	private Date _deadline;
	private String _bug_alias;
	private int _found_in_product_id;
	private int _found_in_version_id;
	private int _found_in_phase_id;
	private String _cf_type;
	private String _cf_reported_by;
	private int _cf_attempted;
	private int _cf_failed;
	private String _cf_public_summary;
	private int _cf_doc_impact;
	private int _cf_security;
	private int _cf_build;
	private String _cf_branch;
	private int _cf_change;
	private int _cf_test_id;
	private String _cf_regression;
	private int _cf_reviewer;
	private String _cf_on_hold;
	private String _cf_public_severity;
	private int _cf_i18n_impact;
	private Date _cf_eta;
	private String _cf_bug_source;
	private float _cf_viss;
}