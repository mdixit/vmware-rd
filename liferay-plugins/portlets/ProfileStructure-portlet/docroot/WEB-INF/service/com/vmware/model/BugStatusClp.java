/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.BugStatusLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class BugStatusClp extends BaseModelImpl<BugStatus> implements BugStatus {
	public BugStatusClp() {
	}

	public Class<?> getModelClass() {
		return BugStatus.class;
	}

	public String getModelClassName() {
		return BugStatus.class.getName();
	}

	public int getPrimaryKey() {
		return _bug_status_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setBug_status_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_bug_status_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_status_id", getBug_status_id());
		attributes.put("value", getValue());
		attributes.put("sortkey", getSortkey());
		attributes.put("isactive", getIsactive());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_status_id = (Integer)attributes.get("bug_status_id");

		if (bug_status_id != null) {
			setBug_status_id(bug_status_id);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}
	}

	public int getBug_status_id() {
		return _bug_status_id;
	}

	public void setBug_status_id(int bug_status_id) {
		_bug_status_id = bug_status_id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	public BaseModel<?> getBugStatusRemoteModel() {
		return _bugStatusRemoteModel;
	}

	public void setBugStatusRemoteModel(BaseModel<?> bugStatusRemoteModel) {
		_bugStatusRemoteModel = bugStatusRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			BugStatusLocalServiceUtil.addBugStatus(this);
		}
		else {
			BugStatusLocalServiceUtil.updateBugStatus(this);
		}
	}

	@Override
	public BugStatus toEscapedModel() {
		return (BugStatus)Proxy.newProxyInstance(BugStatus.class.getClassLoader(),
			new Class[] { BugStatus.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BugStatusClp clone = new BugStatusClp();

		clone.setBug_status_id(getBug_status_id());
		clone.setValue(getValue());
		clone.setSortkey(getSortkey());
		clone.setIsactive(getIsactive());

		return clone;
	}

	public int compareTo(BugStatus bugStatus) {
		int primaryKey = bugStatus.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		BugStatusClp bugStatus = null;

		try {
			bugStatus = (BugStatusClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = bugStatus.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{bug_status_id=");
		sb.append(getBug_status_id());
		sb.append(", value=");
		sb.append(getValue());
		sb.append(", sortkey=");
		sb.append(getSortkey());
		sb.append(", isactive=");
		sb.append(getIsactive());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.BugStatus");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_status_id</column-name><column-value><![CDATA[");
		sb.append(getBug_status_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>value</column-name><column-value><![CDATA[");
		sb.append(getValue());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sortkey</column-name><column-value><![CDATA[");
		sb.append(getSortkey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isactive</column-name><column-value><![CDATA[");
		sb.append(getIsactive());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_status_id;
	private String _value;
	private int _sortkey;
	private int _isactive;
	private BaseModel<?> _bugStatusRemoteModel;
}