/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.BugStatusServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.BugStatusServiceSoap
 * @generated
 */
public class BugStatusSoap implements Serializable {
	public static BugStatusSoap toSoapModel(BugStatus model) {
		BugStatusSoap soapModel = new BugStatusSoap();

		soapModel.setBug_status_id(model.getBug_status_id());
		soapModel.setValue(model.getValue());
		soapModel.setSortkey(model.getSortkey());
		soapModel.setIsactive(model.getIsactive());

		return soapModel;
	}

	public static BugStatusSoap[] toSoapModels(BugStatus[] models) {
		BugStatusSoap[] soapModels = new BugStatusSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BugStatusSoap[][] toSoapModels(BugStatus[][] models) {
		BugStatusSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BugStatusSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BugStatusSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BugStatusSoap[] toSoapModels(List<BugStatus> models) {
		List<BugStatusSoap> soapModels = new ArrayList<BugStatusSoap>(models.size());

		for (BugStatus model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BugStatusSoap[soapModels.size()]);
	}

	public BugStatusSoap() {
	}

	public int getPrimaryKey() {
		return _bug_status_id;
	}

	public void setPrimaryKey(int pk) {
		setBug_status_id(pk);
	}

	public int getBug_status_id() {
		return _bug_status_id;
	}

	public void setBug_status_id(int bug_status_id) {
		_bug_status_id = bug_status_id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	private int _bug_status_id;
	private String _value;
	private int _sortkey;
	private int _isactive;
}