/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BugStatus}.
 * </p>
 *
 * @author    iscisc
 * @see       BugStatus
 * @generated
 */
public class BugStatusWrapper implements BugStatus, ModelWrapper<BugStatus> {
	public BugStatusWrapper(BugStatus bugStatus) {
		_bugStatus = bugStatus;
	}

	public Class<?> getModelClass() {
		return BugStatus.class;
	}

	public String getModelClassName() {
		return BugStatus.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_status_id", getBug_status_id());
		attributes.put("value", getValue());
		attributes.put("sortkey", getSortkey());
		attributes.put("isactive", getIsactive());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_status_id = (Integer)attributes.get("bug_status_id");

		if (bug_status_id != null) {
			setBug_status_id(bug_status_id);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}
	}

	/**
	* Returns the primary key of this bug status.
	*
	* @return the primary key of this bug status
	*/
	public int getPrimaryKey() {
		return _bugStatus.getPrimaryKey();
	}

	/**
	* Sets the primary key of this bug status.
	*
	* @param primaryKey the primary key of this bug status
	*/
	public void setPrimaryKey(int primaryKey) {
		_bugStatus.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_status_id of this bug status.
	*
	* @return the bug_status_id of this bug status
	*/
	public int getBug_status_id() {
		return _bugStatus.getBug_status_id();
	}

	/**
	* Sets the bug_status_id of this bug status.
	*
	* @param bug_status_id the bug_status_id of this bug status
	*/
	public void setBug_status_id(int bug_status_id) {
		_bugStatus.setBug_status_id(bug_status_id);
	}

	/**
	* Returns the value of this bug status.
	*
	* @return the value of this bug status
	*/
	public java.lang.String getValue() {
		return _bugStatus.getValue();
	}

	/**
	* Sets the value of this bug status.
	*
	* @param value the value of this bug status
	*/
	public void setValue(java.lang.String value) {
		_bugStatus.setValue(value);
	}

	/**
	* Returns the sortkey of this bug status.
	*
	* @return the sortkey of this bug status
	*/
	public int getSortkey() {
		return _bugStatus.getSortkey();
	}

	/**
	* Sets the sortkey of this bug status.
	*
	* @param sortkey the sortkey of this bug status
	*/
	public void setSortkey(int sortkey) {
		_bugStatus.setSortkey(sortkey);
	}

	/**
	* Returns the isactive of this bug status.
	*
	* @return the isactive of this bug status
	*/
	public int getIsactive() {
		return _bugStatus.getIsactive();
	}

	/**
	* Sets the isactive of this bug status.
	*
	* @param isactive the isactive of this bug status
	*/
	public void setIsactive(int isactive) {
		_bugStatus.setIsactive(isactive);
	}

	public boolean isNew() {
		return _bugStatus.isNew();
	}

	public void setNew(boolean n) {
		_bugStatus.setNew(n);
	}

	public boolean isCachedModel() {
		return _bugStatus.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_bugStatus.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _bugStatus.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _bugStatus.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_bugStatus.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _bugStatus.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_bugStatus.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BugStatusWrapper((BugStatus)_bugStatus.clone());
	}

	public int compareTo(com.vmware.model.BugStatus bugStatus) {
		return _bugStatus.compareTo(bugStatus);
	}

	@Override
	public int hashCode() {
		return _bugStatus.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.BugStatus> toCacheModel() {
		return _bugStatus.toCacheModel();
	}

	public com.vmware.model.BugStatus toEscapedModel() {
		return new BugStatusWrapper(_bugStatus.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _bugStatus.toString();
	}

	public java.lang.String toXmlString() {
		return _bugStatus.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_bugStatus.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BugStatus getWrappedBugStatus() {
		return _bugStatus;
	}

	public BugStatus getWrappedModel() {
		return _bugStatus;
	}

	public void resetOriginalValues() {
		_bugStatus.resetOriginalValues();
	}

	private BugStatus _bugStatus;
}