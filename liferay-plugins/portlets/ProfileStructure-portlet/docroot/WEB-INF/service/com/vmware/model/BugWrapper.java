/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Bug}.
 * </p>
 *
 * @author    iscisc
 * @see       Bug
 * @generated
 */
public class BugWrapper implements Bug, ModelWrapper<Bug> {
	public BugWrapper(Bug bug) {
		_bug = bug;
	}

	public Class<?> getModelClass() {
		return Bug.class;
	}

	public String getModelClassName() {
		return Bug.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("assigned_to", getAssigned_to());
		attributes.put("bug_file_loc", getBug_file_loc());
		attributes.put("bug_severity", getBug_severity());
		attributes.put("bug_status", getBug_status());
		attributes.put("creation_ts", getCreation_ts());
		attributes.put("delta_ts", getDelta_ts());
		attributes.put("short_desc", getShort_desc());
		attributes.put("host_op_sys", getHost_op_sys());
		attributes.put("guest_op_sys", getGuest_op_sys());
		attributes.put("priority", getPriority());
		attributes.put("rep_platform", getRep_platform());
		attributes.put("product_id", getProduct_id());
		attributes.put("component_id", getComponent_id());
		attributes.put("qa_contact", getQa_contact());
		attributes.put("reporter", getReporter());
		attributes.put("category_id", getCategory_id());
		attributes.put("resolution", getResolution());
		attributes.put("target_milestone", getTarget_milestone());
		attributes.put("status_whiteboard", getStatus_whiteboard());
		attributes.put("votes", getVotes());
		attributes.put("keywords", getKeywords());
		attributes.put("lastdiffed", getLastdiffed());
		attributes.put("everconfirmed", getEverconfirmed());
		attributes.put("reporter_accessible", getReporter_accessible());
		attributes.put("cclist_accessible", getCclist_accessible());
		attributes.put("estimated_time", getEstimated_time());
		attributes.put("remaining_time", getRemaining_time());
		attributes.put("deadline", getDeadline());
		attributes.put("bug_alias", getBug_alias());
		attributes.put("found_in_product_id", getFound_in_product_id());
		attributes.put("found_in_version_id", getFound_in_version_id());
		attributes.put("found_in_phase_id", getFound_in_phase_id());
		attributes.put("cf_type", getCf_type());
		attributes.put("cf_reported_by", getCf_reported_by());
		attributes.put("cf_attempted", getCf_attempted());
		attributes.put("cf_failed", getCf_failed());
		attributes.put("cf_public_summary", getCf_public_summary());
		attributes.put("cf_doc_impact", getCf_doc_impact());
		attributes.put("cf_security", getCf_security());
		attributes.put("cf_build", getCf_build());
		attributes.put("cf_branch", getCf_branch());
		attributes.put("cf_change", getCf_change());
		attributes.put("cf_test_id", getCf_test_id());
		attributes.put("cf_regression", getCf_regression());
		attributes.put("cf_reviewer", getCf_reviewer());
		attributes.put("cf_on_hold", getCf_on_hold());
		attributes.put("cf_public_severity", getCf_public_severity());
		attributes.put("cf_i18n_impact", getCf_i18n_impact());
		attributes.put("cf_eta", getCf_eta());
		attributes.put("cf_bug_source", getCf_bug_source());
		attributes.put("cf_viss", getCf_viss());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer assigned_to = (Integer)attributes.get("assigned_to");

		if (assigned_to != null) {
			setAssigned_to(assigned_to);
		}

		String bug_file_loc = (String)attributes.get("bug_file_loc");

		if (bug_file_loc != null) {
			setBug_file_loc(bug_file_loc);
		}

		String bug_severity = (String)attributes.get("bug_severity");

		if (bug_severity != null) {
			setBug_severity(bug_severity);
		}

		String bug_status = (String)attributes.get("bug_status");

		if (bug_status != null) {
			setBug_status(bug_status);
		}

		Date creation_ts = (Date)attributes.get("creation_ts");

		if (creation_ts != null) {
			setCreation_ts(creation_ts);
		}

		Date delta_ts = (Date)attributes.get("delta_ts");

		if (delta_ts != null) {
			setDelta_ts(delta_ts);
		}

		String short_desc = (String)attributes.get("short_desc");

		if (short_desc != null) {
			setShort_desc(short_desc);
		}

		String host_op_sys = (String)attributes.get("host_op_sys");

		if (host_op_sys != null) {
			setHost_op_sys(host_op_sys);
		}

		String guest_op_sys = (String)attributes.get("guest_op_sys");

		if (guest_op_sys != null) {
			setGuest_op_sys(guest_op_sys);
		}

		String priority = (String)attributes.get("priority");

		if (priority != null) {
			setPriority(priority);
		}

		String rep_platform = (String)attributes.get("rep_platform");

		if (rep_platform != null) {
			setRep_platform(rep_platform);
		}

		Integer product_id = (Integer)attributes.get("product_id");

		if (product_id != null) {
			setProduct_id(product_id);
		}

		Integer component_id = (Integer)attributes.get("component_id");

		if (component_id != null) {
			setComponent_id(component_id);
		}

		Integer qa_contact = (Integer)attributes.get("qa_contact");

		if (qa_contact != null) {
			setQa_contact(qa_contact);
		}

		Integer reporter = (Integer)attributes.get("reporter");

		if (reporter != null) {
			setReporter(reporter);
		}

		Integer category_id = (Integer)attributes.get("category_id");

		if (category_id != null) {
			setCategory_id(category_id);
		}

		String resolution = (String)attributes.get("resolution");

		if (resolution != null) {
			setResolution(resolution);
		}

		String target_milestone = (String)attributes.get("target_milestone");

		if (target_milestone != null) {
			setTarget_milestone(target_milestone);
		}

		String status_whiteboard = (String)attributes.get("status_whiteboard");

		if (status_whiteboard != null) {
			setStatus_whiteboard(status_whiteboard);
		}

		Integer votes = (Integer)attributes.get("votes");

		if (votes != null) {
			setVotes(votes);
		}

		String keywords = (String)attributes.get("keywords");

		if (keywords != null) {
			setKeywords(keywords);
		}

		Date lastdiffed = (Date)attributes.get("lastdiffed");

		if (lastdiffed != null) {
			setLastdiffed(lastdiffed);
		}

		Integer everconfirmed = (Integer)attributes.get("everconfirmed");

		if (everconfirmed != null) {
			setEverconfirmed(everconfirmed);
		}

		Integer reporter_accessible = (Integer)attributes.get(
				"reporter_accessible");

		if (reporter_accessible != null) {
			setReporter_accessible(reporter_accessible);
		}

		Integer cclist_accessible = (Integer)attributes.get("cclist_accessible");

		if (cclist_accessible != null) {
			setCclist_accessible(cclist_accessible);
		}

		Float estimated_time = (Float)attributes.get("estimated_time");

		if (estimated_time != null) {
			setEstimated_time(estimated_time);
		}

		Float remaining_time = (Float)attributes.get("remaining_time");

		if (remaining_time != null) {
			setRemaining_time(remaining_time);
		}

		Date deadline = (Date)attributes.get("deadline");

		if (deadline != null) {
			setDeadline(deadline);
		}

		String bug_alias = (String)attributes.get("bug_alias");

		if (bug_alias != null) {
			setBug_alias(bug_alias);
		}

		Integer found_in_product_id = (Integer)attributes.get(
				"found_in_product_id");

		if (found_in_product_id != null) {
			setFound_in_product_id(found_in_product_id);
		}

		Integer found_in_version_id = (Integer)attributes.get(
				"found_in_version_id");

		if (found_in_version_id != null) {
			setFound_in_version_id(found_in_version_id);
		}

		Integer found_in_phase_id = (Integer)attributes.get("found_in_phase_id");

		if (found_in_phase_id != null) {
			setFound_in_phase_id(found_in_phase_id);
		}

		String cf_type = (String)attributes.get("cf_type");

		if (cf_type != null) {
			setCf_type(cf_type);
		}

		String cf_reported_by = (String)attributes.get("cf_reported_by");

		if (cf_reported_by != null) {
			setCf_reported_by(cf_reported_by);
		}

		Integer cf_attempted = (Integer)attributes.get("cf_attempted");

		if (cf_attempted != null) {
			setCf_attempted(cf_attempted);
		}

		Integer cf_failed = (Integer)attributes.get("cf_failed");

		if (cf_failed != null) {
			setCf_failed(cf_failed);
		}

		String cf_public_summary = (String)attributes.get("cf_public_summary");

		if (cf_public_summary != null) {
			setCf_public_summary(cf_public_summary);
		}

		Integer cf_doc_impact = (Integer)attributes.get("cf_doc_impact");

		if (cf_doc_impact != null) {
			setCf_doc_impact(cf_doc_impact);
		}

		Integer cf_security = (Integer)attributes.get("cf_security");

		if (cf_security != null) {
			setCf_security(cf_security);
		}

		Integer cf_build = (Integer)attributes.get("cf_build");

		if (cf_build != null) {
			setCf_build(cf_build);
		}

		String cf_branch = (String)attributes.get("cf_branch");

		if (cf_branch != null) {
			setCf_branch(cf_branch);
		}

		Integer cf_change = (Integer)attributes.get("cf_change");

		if (cf_change != null) {
			setCf_change(cf_change);
		}

		Integer cf_test_id = (Integer)attributes.get("cf_test_id");

		if (cf_test_id != null) {
			setCf_test_id(cf_test_id);
		}

		String cf_regression = (String)attributes.get("cf_regression");

		if (cf_regression != null) {
			setCf_regression(cf_regression);
		}

		Integer cf_reviewer = (Integer)attributes.get("cf_reviewer");

		if (cf_reviewer != null) {
			setCf_reviewer(cf_reviewer);
		}

		String cf_on_hold = (String)attributes.get("cf_on_hold");

		if (cf_on_hold != null) {
			setCf_on_hold(cf_on_hold);
		}

		String cf_public_severity = (String)attributes.get("cf_public_severity");

		if (cf_public_severity != null) {
			setCf_public_severity(cf_public_severity);
		}

		Integer cf_i18n_impact = (Integer)attributes.get("cf_i18n_impact");

		if (cf_i18n_impact != null) {
			setCf_i18n_impact(cf_i18n_impact);
		}

		Date cf_eta = (Date)attributes.get("cf_eta");

		if (cf_eta != null) {
			setCf_eta(cf_eta);
		}

		String cf_bug_source = (String)attributes.get("cf_bug_source");

		if (cf_bug_source != null) {
			setCf_bug_source(cf_bug_source);
		}

		Float cf_viss = (Float)attributes.get("cf_viss");

		if (cf_viss != null) {
			setCf_viss(cf_viss);
		}
	}

	/**
	* Returns the primary key of this bug.
	*
	* @return the primary key of this bug
	*/
	public int getPrimaryKey() {
		return _bug.getPrimaryKey();
	}

	/**
	* Sets the primary key of this bug.
	*
	* @param primaryKey the primary key of this bug
	*/
	public void setPrimaryKey(int primaryKey) {
		_bug.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_id of this bug.
	*
	* @return the bug_id of this bug
	*/
	public int getBug_id() {
		return _bug.getBug_id();
	}

	/**
	* Sets the bug_id of this bug.
	*
	* @param bug_id the bug_id of this bug
	*/
	public void setBug_id(int bug_id) {
		_bug.setBug_id(bug_id);
	}

	/**
	* Returns the assigned_to of this bug.
	*
	* @return the assigned_to of this bug
	*/
	public int getAssigned_to() {
		return _bug.getAssigned_to();
	}

	/**
	* Sets the assigned_to of this bug.
	*
	* @param assigned_to the assigned_to of this bug
	*/
	public void setAssigned_to(int assigned_to) {
		_bug.setAssigned_to(assigned_to);
	}

	/**
	* Returns the bug_file_loc of this bug.
	*
	* @return the bug_file_loc of this bug
	*/
	public java.lang.String getBug_file_loc() {
		return _bug.getBug_file_loc();
	}

	/**
	* Sets the bug_file_loc of this bug.
	*
	* @param bug_file_loc the bug_file_loc of this bug
	*/
	public void setBug_file_loc(java.lang.String bug_file_loc) {
		_bug.setBug_file_loc(bug_file_loc);
	}

	/**
	* Returns the bug_severity of this bug.
	*
	* @return the bug_severity of this bug
	*/
	public java.lang.String getBug_severity() {
		return _bug.getBug_severity();
	}

	/**
	* Sets the bug_severity of this bug.
	*
	* @param bug_severity the bug_severity of this bug
	*/
	public void setBug_severity(java.lang.String bug_severity) {
		_bug.setBug_severity(bug_severity);
	}

	/**
	* Returns the bug_status of this bug.
	*
	* @return the bug_status of this bug
	*/
	public java.lang.String getBug_status() {
		return _bug.getBug_status();
	}

	/**
	* Sets the bug_status of this bug.
	*
	* @param bug_status the bug_status of this bug
	*/
	public void setBug_status(java.lang.String bug_status) {
		_bug.setBug_status(bug_status);
	}

	/**
	* Returns the creation_ts of this bug.
	*
	* @return the creation_ts of this bug
	*/
	public java.util.Date getCreation_ts() {
		return _bug.getCreation_ts();
	}

	/**
	* Sets the creation_ts of this bug.
	*
	* @param creation_ts the creation_ts of this bug
	*/
	public void setCreation_ts(java.util.Date creation_ts) {
		_bug.setCreation_ts(creation_ts);
	}

	/**
	* Returns the delta_ts of this bug.
	*
	* @return the delta_ts of this bug
	*/
	public java.util.Date getDelta_ts() {
		return _bug.getDelta_ts();
	}

	/**
	* Sets the delta_ts of this bug.
	*
	* @param delta_ts the delta_ts of this bug
	*/
	public void setDelta_ts(java.util.Date delta_ts) {
		_bug.setDelta_ts(delta_ts);
	}

	/**
	* Returns the short_desc of this bug.
	*
	* @return the short_desc of this bug
	*/
	public java.lang.String getShort_desc() {
		return _bug.getShort_desc();
	}

	/**
	* Sets the short_desc of this bug.
	*
	* @param short_desc the short_desc of this bug
	*/
	public void setShort_desc(java.lang.String short_desc) {
		_bug.setShort_desc(short_desc);
	}

	/**
	* Returns the host_op_sys of this bug.
	*
	* @return the host_op_sys of this bug
	*/
	public java.lang.String getHost_op_sys() {
		return _bug.getHost_op_sys();
	}

	/**
	* Sets the host_op_sys of this bug.
	*
	* @param host_op_sys the host_op_sys of this bug
	*/
	public void setHost_op_sys(java.lang.String host_op_sys) {
		_bug.setHost_op_sys(host_op_sys);
	}

	/**
	* Returns the guest_op_sys of this bug.
	*
	* @return the guest_op_sys of this bug
	*/
	public java.lang.String getGuest_op_sys() {
		return _bug.getGuest_op_sys();
	}

	/**
	* Sets the guest_op_sys of this bug.
	*
	* @param guest_op_sys the guest_op_sys of this bug
	*/
	public void setGuest_op_sys(java.lang.String guest_op_sys) {
		_bug.setGuest_op_sys(guest_op_sys);
	}

	/**
	* Returns the priority of this bug.
	*
	* @return the priority of this bug
	*/
	public java.lang.String getPriority() {
		return _bug.getPriority();
	}

	/**
	* Sets the priority of this bug.
	*
	* @param priority the priority of this bug
	*/
	public void setPriority(java.lang.String priority) {
		_bug.setPriority(priority);
	}

	/**
	* Returns the rep_platform of this bug.
	*
	* @return the rep_platform of this bug
	*/
	public java.lang.String getRep_platform() {
		return _bug.getRep_platform();
	}

	/**
	* Sets the rep_platform of this bug.
	*
	* @param rep_platform the rep_platform of this bug
	*/
	public void setRep_platform(java.lang.String rep_platform) {
		_bug.setRep_platform(rep_platform);
	}

	/**
	* Returns the product_id of this bug.
	*
	* @return the product_id of this bug
	*/
	public int getProduct_id() {
		return _bug.getProduct_id();
	}

	/**
	* Sets the product_id of this bug.
	*
	* @param product_id the product_id of this bug
	*/
	public void setProduct_id(int product_id) {
		_bug.setProduct_id(product_id);
	}

	/**
	* Returns the component_id of this bug.
	*
	* @return the component_id of this bug
	*/
	public int getComponent_id() {
		return _bug.getComponent_id();
	}

	/**
	* Sets the component_id of this bug.
	*
	* @param component_id the component_id of this bug
	*/
	public void setComponent_id(int component_id) {
		_bug.setComponent_id(component_id);
	}

	/**
	* Returns the qa_contact of this bug.
	*
	* @return the qa_contact of this bug
	*/
	public int getQa_contact() {
		return _bug.getQa_contact();
	}

	/**
	* Sets the qa_contact of this bug.
	*
	* @param qa_contact the qa_contact of this bug
	*/
	public void setQa_contact(int qa_contact) {
		_bug.setQa_contact(qa_contact);
	}

	/**
	* Returns the reporter of this bug.
	*
	* @return the reporter of this bug
	*/
	public int getReporter() {
		return _bug.getReporter();
	}

	/**
	* Sets the reporter of this bug.
	*
	* @param reporter the reporter of this bug
	*/
	public void setReporter(int reporter) {
		_bug.setReporter(reporter);
	}

	/**
	* Returns the category_id of this bug.
	*
	* @return the category_id of this bug
	*/
	public int getCategory_id() {
		return _bug.getCategory_id();
	}

	/**
	* Sets the category_id of this bug.
	*
	* @param category_id the category_id of this bug
	*/
	public void setCategory_id(int category_id) {
		_bug.setCategory_id(category_id);
	}

	/**
	* Returns the resolution of this bug.
	*
	* @return the resolution of this bug
	*/
	public java.lang.String getResolution() {
		return _bug.getResolution();
	}

	/**
	* Sets the resolution of this bug.
	*
	* @param resolution the resolution of this bug
	*/
	public void setResolution(java.lang.String resolution) {
		_bug.setResolution(resolution);
	}

	/**
	* Returns the target_milestone of this bug.
	*
	* @return the target_milestone of this bug
	*/
	public java.lang.String getTarget_milestone() {
		return _bug.getTarget_milestone();
	}

	/**
	* Sets the target_milestone of this bug.
	*
	* @param target_milestone the target_milestone of this bug
	*/
	public void setTarget_milestone(java.lang.String target_milestone) {
		_bug.setTarget_milestone(target_milestone);
	}

	/**
	* Returns the status_whiteboard of this bug.
	*
	* @return the status_whiteboard of this bug
	*/
	public java.lang.String getStatus_whiteboard() {
		return _bug.getStatus_whiteboard();
	}

	/**
	* Sets the status_whiteboard of this bug.
	*
	* @param status_whiteboard the status_whiteboard of this bug
	*/
	public void setStatus_whiteboard(java.lang.String status_whiteboard) {
		_bug.setStatus_whiteboard(status_whiteboard);
	}

	/**
	* Returns the votes of this bug.
	*
	* @return the votes of this bug
	*/
	public int getVotes() {
		return _bug.getVotes();
	}

	/**
	* Sets the votes of this bug.
	*
	* @param votes the votes of this bug
	*/
	public void setVotes(int votes) {
		_bug.setVotes(votes);
	}

	/**
	* Returns the keywords of this bug.
	*
	* @return the keywords of this bug
	*/
	public java.lang.String getKeywords() {
		return _bug.getKeywords();
	}

	/**
	* Sets the keywords of this bug.
	*
	* @param keywords the keywords of this bug
	*/
	public void setKeywords(java.lang.String keywords) {
		_bug.setKeywords(keywords);
	}

	/**
	* Returns the lastdiffed of this bug.
	*
	* @return the lastdiffed of this bug
	*/
	public java.util.Date getLastdiffed() {
		return _bug.getLastdiffed();
	}

	/**
	* Sets the lastdiffed of this bug.
	*
	* @param lastdiffed the lastdiffed of this bug
	*/
	public void setLastdiffed(java.util.Date lastdiffed) {
		_bug.setLastdiffed(lastdiffed);
	}

	/**
	* Returns the everconfirmed of this bug.
	*
	* @return the everconfirmed of this bug
	*/
	public int getEverconfirmed() {
		return _bug.getEverconfirmed();
	}

	/**
	* Sets the everconfirmed of this bug.
	*
	* @param everconfirmed the everconfirmed of this bug
	*/
	public void setEverconfirmed(int everconfirmed) {
		_bug.setEverconfirmed(everconfirmed);
	}

	/**
	* Returns the reporter_accessible of this bug.
	*
	* @return the reporter_accessible of this bug
	*/
	public int getReporter_accessible() {
		return _bug.getReporter_accessible();
	}

	/**
	* Sets the reporter_accessible of this bug.
	*
	* @param reporter_accessible the reporter_accessible of this bug
	*/
	public void setReporter_accessible(int reporter_accessible) {
		_bug.setReporter_accessible(reporter_accessible);
	}

	/**
	* Returns the cclist_accessible of this bug.
	*
	* @return the cclist_accessible of this bug
	*/
	public int getCclist_accessible() {
		return _bug.getCclist_accessible();
	}

	/**
	* Sets the cclist_accessible of this bug.
	*
	* @param cclist_accessible the cclist_accessible of this bug
	*/
	public void setCclist_accessible(int cclist_accessible) {
		_bug.setCclist_accessible(cclist_accessible);
	}

	/**
	* Returns the estimated_time of this bug.
	*
	* @return the estimated_time of this bug
	*/
	public float getEstimated_time() {
		return _bug.getEstimated_time();
	}

	/**
	* Sets the estimated_time of this bug.
	*
	* @param estimated_time the estimated_time of this bug
	*/
	public void setEstimated_time(float estimated_time) {
		_bug.setEstimated_time(estimated_time);
	}

	/**
	* Returns the remaining_time of this bug.
	*
	* @return the remaining_time of this bug
	*/
	public float getRemaining_time() {
		return _bug.getRemaining_time();
	}

	/**
	* Sets the remaining_time of this bug.
	*
	* @param remaining_time the remaining_time of this bug
	*/
	public void setRemaining_time(float remaining_time) {
		_bug.setRemaining_time(remaining_time);
	}

	/**
	* Returns the deadline of this bug.
	*
	* @return the deadline of this bug
	*/
	public java.util.Date getDeadline() {
		return _bug.getDeadline();
	}

	/**
	* Sets the deadline of this bug.
	*
	* @param deadline the deadline of this bug
	*/
	public void setDeadline(java.util.Date deadline) {
		_bug.setDeadline(deadline);
	}

	/**
	* Returns the bug_alias of this bug.
	*
	* @return the bug_alias of this bug
	*/
	public java.lang.String getBug_alias() {
		return _bug.getBug_alias();
	}

	/**
	* Sets the bug_alias of this bug.
	*
	* @param bug_alias the bug_alias of this bug
	*/
	public void setBug_alias(java.lang.String bug_alias) {
		_bug.setBug_alias(bug_alias);
	}

	/**
	* Returns the found_in_product_id of this bug.
	*
	* @return the found_in_product_id of this bug
	*/
	public int getFound_in_product_id() {
		return _bug.getFound_in_product_id();
	}

	/**
	* Sets the found_in_product_id of this bug.
	*
	* @param found_in_product_id the found_in_product_id of this bug
	*/
	public void setFound_in_product_id(int found_in_product_id) {
		_bug.setFound_in_product_id(found_in_product_id);
	}

	/**
	* Returns the found_in_version_id of this bug.
	*
	* @return the found_in_version_id of this bug
	*/
	public int getFound_in_version_id() {
		return _bug.getFound_in_version_id();
	}

	/**
	* Sets the found_in_version_id of this bug.
	*
	* @param found_in_version_id the found_in_version_id of this bug
	*/
	public void setFound_in_version_id(int found_in_version_id) {
		_bug.setFound_in_version_id(found_in_version_id);
	}

	/**
	* Returns the found_in_phase_id of this bug.
	*
	* @return the found_in_phase_id of this bug
	*/
	public int getFound_in_phase_id() {
		return _bug.getFound_in_phase_id();
	}

	/**
	* Sets the found_in_phase_id of this bug.
	*
	* @param found_in_phase_id the found_in_phase_id of this bug
	*/
	public void setFound_in_phase_id(int found_in_phase_id) {
		_bug.setFound_in_phase_id(found_in_phase_id);
	}

	/**
	* Returns the cf_type of this bug.
	*
	* @return the cf_type of this bug
	*/
	public java.lang.String getCf_type() {
		return _bug.getCf_type();
	}

	/**
	* Sets the cf_type of this bug.
	*
	* @param cf_type the cf_type of this bug
	*/
	public void setCf_type(java.lang.String cf_type) {
		_bug.setCf_type(cf_type);
	}

	/**
	* Returns the cf_reported_by of this bug.
	*
	* @return the cf_reported_by of this bug
	*/
	public java.lang.String getCf_reported_by() {
		return _bug.getCf_reported_by();
	}

	/**
	* Sets the cf_reported_by of this bug.
	*
	* @param cf_reported_by the cf_reported_by of this bug
	*/
	public void setCf_reported_by(java.lang.String cf_reported_by) {
		_bug.setCf_reported_by(cf_reported_by);
	}

	/**
	* Returns the cf_attempted of this bug.
	*
	* @return the cf_attempted of this bug
	*/
	public int getCf_attempted() {
		return _bug.getCf_attempted();
	}

	/**
	* Sets the cf_attempted of this bug.
	*
	* @param cf_attempted the cf_attempted of this bug
	*/
	public void setCf_attempted(int cf_attempted) {
		_bug.setCf_attempted(cf_attempted);
	}

	/**
	* Returns the cf_failed of this bug.
	*
	* @return the cf_failed of this bug
	*/
	public int getCf_failed() {
		return _bug.getCf_failed();
	}

	/**
	* Sets the cf_failed of this bug.
	*
	* @param cf_failed the cf_failed of this bug
	*/
	public void setCf_failed(int cf_failed) {
		_bug.setCf_failed(cf_failed);
	}

	/**
	* Returns the cf_public_summary of this bug.
	*
	* @return the cf_public_summary of this bug
	*/
	public java.lang.String getCf_public_summary() {
		return _bug.getCf_public_summary();
	}

	/**
	* Sets the cf_public_summary of this bug.
	*
	* @param cf_public_summary the cf_public_summary of this bug
	*/
	public void setCf_public_summary(java.lang.String cf_public_summary) {
		_bug.setCf_public_summary(cf_public_summary);
	}

	/**
	* Returns the cf_doc_impact of this bug.
	*
	* @return the cf_doc_impact of this bug
	*/
	public int getCf_doc_impact() {
		return _bug.getCf_doc_impact();
	}

	/**
	* Sets the cf_doc_impact of this bug.
	*
	* @param cf_doc_impact the cf_doc_impact of this bug
	*/
	public void setCf_doc_impact(int cf_doc_impact) {
		_bug.setCf_doc_impact(cf_doc_impact);
	}

	/**
	* Returns the cf_security of this bug.
	*
	* @return the cf_security of this bug
	*/
	public int getCf_security() {
		return _bug.getCf_security();
	}

	/**
	* Sets the cf_security of this bug.
	*
	* @param cf_security the cf_security of this bug
	*/
	public void setCf_security(int cf_security) {
		_bug.setCf_security(cf_security);
	}

	/**
	* Returns the cf_build of this bug.
	*
	* @return the cf_build of this bug
	*/
	public int getCf_build() {
		return _bug.getCf_build();
	}

	/**
	* Sets the cf_build of this bug.
	*
	* @param cf_build the cf_build of this bug
	*/
	public void setCf_build(int cf_build) {
		_bug.setCf_build(cf_build);
	}

	/**
	* Returns the cf_branch of this bug.
	*
	* @return the cf_branch of this bug
	*/
	public java.lang.String getCf_branch() {
		return _bug.getCf_branch();
	}

	/**
	* Sets the cf_branch of this bug.
	*
	* @param cf_branch the cf_branch of this bug
	*/
	public void setCf_branch(java.lang.String cf_branch) {
		_bug.setCf_branch(cf_branch);
	}

	/**
	* Returns the cf_change of this bug.
	*
	* @return the cf_change of this bug
	*/
	public int getCf_change() {
		return _bug.getCf_change();
	}

	/**
	* Sets the cf_change of this bug.
	*
	* @param cf_change the cf_change of this bug
	*/
	public void setCf_change(int cf_change) {
		_bug.setCf_change(cf_change);
	}

	/**
	* Returns the cf_test_id of this bug.
	*
	* @return the cf_test_id of this bug
	*/
	public int getCf_test_id() {
		return _bug.getCf_test_id();
	}

	/**
	* Sets the cf_test_id of this bug.
	*
	* @param cf_test_id the cf_test_id of this bug
	*/
	public void setCf_test_id(int cf_test_id) {
		_bug.setCf_test_id(cf_test_id);
	}

	/**
	* Returns the cf_regression of this bug.
	*
	* @return the cf_regression of this bug
	*/
	public java.lang.String getCf_regression() {
		return _bug.getCf_regression();
	}

	/**
	* Sets the cf_regression of this bug.
	*
	* @param cf_regression the cf_regression of this bug
	*/
	public void setCf_regression(java.lang.String cf_regression) {
		_bug.setCf_regression(cf_regression);
	}

	/**
	* Returns the cf_reviewer of this bug.
	*
	* @return the cf_reviewer of this bug
	*/
	public int getCf_reviewer() {
		return _bug.getCf_reviewer();
	}

	/**
	* Sets the cf_reviewer of this bug.
	*
	* @param cf_reviewer the cf_reviewer of this bug
	*/
	public void setCf_reviewer(int cf_reviewer) {
		_bug.setCf_reviewer(cf_reviewer);
	}

	/**
	* Returns the cf_on_hold of this bug.
	*
	* @return the cf_on_hold of this bug
	*/
	public java.lang.String getCf_on_hold() {
		return _bug.getCf_on_hold();
	}

	/**
	* Sets the cf_on_hold of this bug.
	*
	* @param cf_on_hold the cf_on_hold of this bug
	*/
	public void setCf_on_hold(java.lang.String cf_on_hold) {
		_bug.setCf_on_hold(cf_on_hold);
	}

	/**
	* Returns the cf_public_severity of this bug.
	*
	* @return the cf_public_severity of this bug
	*/
	public java.lang.String getCf_public_severity() {
		return _bug.getCf_public_severity();
	}

	/**
	* Sets the cf_public_severity of this bug.
	*
	* @param cf_public_severity the cf_public_severity of this bug
	*/
	public void setCf_public_severity(java.lang.String cf_public_severity) {
		_bug.setCf_public_severity(cf_public_severity);
	}

	/**
	* Returns the cf_i18n_impact of this bug.
	*
	* @return the cf_i18n_impact of this bug
	*/
	public int getCf_i18n_impact() {
		return _bug.getCf_i18n_impact();
	}

	/**
	* Sets the cf_i18n_impact of this bug.
	*
	* @param cf_i18n_impact the cf_i18n_impact of this bug
	*/
	public void setCf_i18n_impact(int cf_i18n_impact) {
		_bug.setCf_i18n_impact(cf_i18n_impact);
	}

	/**
	* Returns the cf_eta of this bug.
	*
	* @return the cf_eta of this bug
	*/
	public java.util.Date getCf_eta() {
		return _bug.getCf_eta();
	}

	/**
	* Sets the cf_eta of this bug.
	*
	* @param cf_eta the cf_eta of this bug
	*/
	public void setCf_eta(java.util.Date cf_eta) {
		_bug.setCf_eta(cf_eta);
	}

	/**
	* Returns the cf_bug_source of this bug.
	*
	* @return the cf_bug_source of this bug
	*/
	public java.lang.String getCf_bug_source() {
		return _bug.getCf_bug_source();
	}

	/**
	* Sets the cf_bug_source of this bug.
	*
	* @param cf_bug_source the cf_bug_source of this bug
	*/
	public void setCf_bug_source(java.lang.String cf_bug_source) {
		_bug.setCf_bug_source(cf_bug_source);
	}

	/**
	* Returns the cf_viss of this bug.
	*
	* @return the cf_viss of this bug
	*/
	public float getCf_viss() {
		return _bug.getCf_viss();
	}

	/**
	* Sets the cf_viss of this bug.
	*
	* @param cf_viss the cf_viss of this bug
	*/
	public void setCf_viss(float cf_viss) {
		_bug.setCf_viss(cf_viss);
	}

	public boolean isNew() {
		return _bug.isNew();
	}

	public void setNew(boolean n) {
		_bug.setNew(n);
	}

	public boolean isCachedModel() {
		return _bug.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_bug.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _bug.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _bug.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_bug.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _bug.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_bug.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BugWrapper((Bug)_bug.clone());
	}

	public int compareTo(com.vmware.model.Bug bug) {
		return _bug.compareTo(bug);
	}

	@Override
	public int hashCode() {
		return _bug.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Bug> toCacheModel() {
		return _bug.toCacheModel();
	}

	public com.vmware.model.Bug toEscapedModel() {
		return new BugWrapper(_bug.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _bug.toString();
	}

	public java.lang.String toXmlString() {
		return _bug.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_bug.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Bug getWrappedBug() {
		return _bug;
	}

	public Bug getWrappedModel() {
		return _bug;
	}

	public void resetOriginalValues() {
		_bug.resetOriginalValues();
	}

	private Bug _bug;
}