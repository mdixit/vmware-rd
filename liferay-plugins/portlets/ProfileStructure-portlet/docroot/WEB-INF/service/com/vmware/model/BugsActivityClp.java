/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.BugsActivityLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class BugsActivityClp extends BaseModelImpl<BugsActivity>
	implements BugsActivity {
	public BugsActivityClp() {
	}

	public Class<?> getModelClass() {
		return BugsActivity.class;
	}

	public String getModelClassName() {
		return BugsActivity.class.getName();
	}

	public int getPrimaryKey() {
		return _bug_activity_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setBug_activity_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_bug_activity_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_activity_id", getBug_activity_id());
		attributes.put("bug_id", getBug_id());
		attributes.put("attach_id", getAttach_id());
		attributes.put("who", getWho());
		attributes.put("bug_when", getBug_when());
		attributes.put("fieldid", getFieldid());
		attributes.put("added", getAdded());
		attributes.put("removed", getRemoved());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_activity_id = (Integer)attributes.get("bug_activity_id");

		if (bug_activity_id != null) {
			setBug_activity_id(bug_activity_id);
		}

		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer attach_id = (Integer)attributes.get("attach_id");

		if (attach_id != null) {
			setAttach_id(attach_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Date bug_when = (Date)attributes.get("bug_when");

		if (bug_when != null) {
			setBug_when(bug_when);
		}

		Integer fieldid = (Integer)attributes.get("fieldid");

		if (fieldid != null) {
			setFieldid(fieldid);
		}

		String added = (String)attributes.get("added");

		if (added != null) {
			setAdded(added);
		}

		String removed = (String)attributes.get("removed");

		if (removed != null) {
			setRemoved(removed);
		}
	}

	public int getBug_activity_id() {
		return _bug_activity_id;
	}

	public void setBug_activity_id(int bug_activity_id) {
		_bug_activity_id = bug_activity_id;
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getAttach_id() {
		return _attach_id;
	}

	public void setAttach_id(int attach_id) {
		_attach_id = attach_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public Date getBug_when() {
		return _bug_when;
	}

	public void setBug_when(Date bug_when) {
		_bug_when = bug_when;
	}

	public int getFieldid() {
		return _fieldid;
	}

	public void setFieldid(int fieldid) {
		_fieldid = fieldid;
	}

	public String getAdded() {
		return _added;
	}

	public void setAdded(String added) {
		_added = added;
	}

	public String getRemoved() {
		return _removed;
	}

	public void setRemoved(String removed) {
		_removed = removed;
	}

	public BaseModel<?> getBugsActivityRemoteModel() {
		return _bugsActivityRemoteModel;
	}

	public void setBugsActivityRemoteModel(BaseModel<?> bugsActivityRemoteModel) {
		_bugsActivityRemoteModel = bugsActivityRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			BugsActivityLocalServiceUtil.addBugsActivity(this);
		}
		else {
			BugsActivityLocalServiceUtil.updateBugsActivity(this);
		}
	}

	@Override
	public BugsActivity toEscapedModel() {
		return (BugsActivity)Proxy.newProxyInstance(BugsActivity.class.getClassLoader(),
			new Class[] { BugsActivity.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BugsActivityClp clone = new BugsActivityClp();

		clone.setBug_activity_id(getBug_activity_id());
		clone.setBug_id(getBug_id());
		clone.setAttach_id(getAttach_id());
		clone.setWho(getWho());
		clone.setBug_when(getBug_when());
		clone.setFieldid(getFieldid());
		clone.setAdded(getAdded());
		clone.setRemoved(getRemoved());

		return clone;
	}

	public int compareTo(BugsActivity bugsActivity) {
		int value = 0;

		value = DateUtil.compareTo(getBug_when(), bugsActivity.getBug_when());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		BugsActivityClp bugsActivity = null;

		try {
			bugsActivity = (BugsActivityClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = bugsActivity.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{bug_activity_id=");
		sb.append(getBug_activity_id());
		sb.append(", bug_id=");
		sb.append(getBug_id());
		sb.append(", attach_id=");
		sb.append(getAttach_id());
		sb.append(", who=");
		sb.append(getWho());
		sb.append(", bug_when=");
		sb.append(getBug_when());
		sb.append(", fieldid=");
		sb.append(getFieldid());
		sb.append(", added=");
		sb.append(getAdded());
		sb.append(", removed=");
		sb.append(getRemoved());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(28);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.BugsActivity");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_activity_id</column-name><column-value><![CDATA[");
		sb.append(getBug_activity_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>attach_id</column-name><column-value><![CDATA[");
		sb.append(getAttach_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>who</column-name><column-value><![CDATA[");
		sb.append(getWho());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_when</column-name><column-value><![CDATA[");
		sb.append(getBug_when());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fieldid</column-name><column-value><![CDATA[");
		sb.append(getFieldid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>added</column-name><column-value><![CDATA[");
		sb.append(getAdded());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>removed</column-name><column-value><![CDATA[");
		sb.append(getRemoved());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_activity_id;
	private int _bug_id;
	private int _attach_id;
	private int _who;
	private Date _bug_when;
	private int _fieldid;
	private String _added;
	private String _removed;
	private BaseModel<?> _bugsActivityRemoteModel;
}