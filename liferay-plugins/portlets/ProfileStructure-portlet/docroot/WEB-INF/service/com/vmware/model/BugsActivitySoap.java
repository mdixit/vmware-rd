/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.BugsActivityServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.BugsActivityServiceSoap
 * @generated
 */
public class BugsActivitySoap implements Serializable {
	public static BugsActivitySoap toSoapModel(BugsActivity model) {
		BugsActivitySoap soapModel = new BugsActivitySoap();

		soapModel.setBug_activity_id(model.getBug_activity_id());
		soapModel.setBug_id(model.getBug_id());
		soapModel.setAttach_id(model.getAttach_id());
		soapModel.setWho(model.getWho());
		soapModel.setBug_when(model.getBug_when());
		soapModel.setFieldid(model.getFieldid());
		soapModel.setAdded(model.getAdded());
		soapModel.setRemoved(model.getRemoved());

		return soapModel;
	}

	public static BugsActivitySoap[] toSoapModels(BugsActivity[] models) {
		BugsActivitySoap[] soapModels = new BugsActivitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BugsActivitySoap[][] toSoapModels(BugsActivity[][] models) {
		BugsActivitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BugsActivitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new BugsActivitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BugsActivitySoap[] toSoapModels(List<BugsActivity> models) {
		List<BugsActivitySoap> soapModels = new ArrayList<BugsActivitySoap>(models.size());

		for (BugsActivity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BugsActivitySoap[soapModels.size()]);
	}

	public BugsActivitySoap() {
	}

	public int getPrimaryKey() {
		return _bug_activity_id;
	}

	public void setPrimaryKey(int pk) {
		setBug_activity_id(pk);
	}

	public int getBug_activity_id() {
		return _bug_activity_id;
	}

	public void setBug_activity_id(int bug_activity_id) {
		_bug_activity_id = bug_activity_id;
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getAttach_id() {
		return _attach_id;
	}

	public void setAttach_id(int attach_id) {
		_attach_id = attach_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public Date getBug_when() {
		return _bug_when;
	}

	public void setBug_when(Date bug_when) {
		_bug_when = bug_when;
	}

	public int getFieldid() {
		return _fieldid;
	}

	public void setFieldid(int fieldid) {
		_fieldid = fieldid;
	}

	public String getAdded() {
		return _added;
	}

	public void setAdded(String added) {
		_added = added;
	}

	public String getRemoved() {
		return _removed;
	}

	public void setRemoved(String removed) {
		_removed = removed;
	}

	private int _bug_activity_id;
	private int _bug_id;
	private int _attach_id;
	private int _who;
	private Date _bug_when;
	private int _fieldid;
	private String _added;
	private String _removed;
}