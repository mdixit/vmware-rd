/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BugsActivity}.
 * </p>
 *
 * @author    iscisc
 * @see       BugsActivity
 * @generated
 */
public class BugsActivityWrapper implements BugsActivity,
	ModelWrapper<BugsActivity> {
	public BugsActivityWrapper(BugsActivity bugsActivity) {
		_bugsActivity = bugsActivity;
	}

	public Class<?> getModelClass() {
		return BugsActivity.class;
	}

	public String getModelClassName() {
		return BugsActivity.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_activity_id", getBug_activity_id());
		attributes.put("bug_id", getBug_id());
		attributes.put("attach_id", getAttach_id());
		attributes.put("who", getWho());
		attributes.put("bug_when", getBug_when());
		attributes.put("fieldid", getFieldid());
		attributes.put("added", getAdded());
		attributes.put("removed", getRemoved());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_activity_id = (Integer)attributes.get("bug_activity_id");

		if (bug_activity_id != null) {
			setBug_activity_id(bug_activity_id);
		}

		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer attach_id = (Integer)attributes.get("attach_id");

		if (attach_id != null) {
			setAttach_id(attach_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Date bug_when = (Date)attributes.get("bug_when");

		if (bug_when != null) {
			setBug_when(bug_when);
		}

		Integer fieldid = (Integer)attributes.get("fieldid");

		if (fieldid != null) {
			setFieldid(fieldid);
		}

		String added = (String)attributes.get("added");

		if (added != null) {
			setAdded(added);
		}

		String removed = (String)attributes.get("removed");

		if (removed != null) {
			setRemoved(removed);
		}
	}

	/**
	* Returns the primary key of this bugs activity.
	*
	* @return the primary key of this bugs activity
	*/
	public int getPrimaryKey() {
		return _bugsActivity.getPrimaryKey();
	}

	/**
	* Sets the primary key of this bugs activity.
	*
	* @param primaryKey the primary key of this bugs activity
	*/
	public void setPrimaryKey(int primaryKey) {
		_bugsActivity.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_activity_id of this bugs activity.
	*
	* @return the bug_activity_id of this bugs activity
	*/
	public int getBug_activity_id() {
		return _bugsActivity.getBug_activity_id();
	}

	/**
	* Sets the bug_activity_id of this bugs activity.
	*
	* @param bug_activity_id the bug_activity_id of this bugs activity
	*/
	public void setBug_activity_id(int bug_activity_id) {
		_bugsActivity.setBug_activity_id(bug_activity_id);
	}

	/**
	* Returns the bug_id of this bugs activity.
	*
	* @return the bug_id of this bugs activity
	*/
	public int getBug_id() {
		return _bugsActivity.getBug_id();
	}

	/**
	* Sets the bug_id of this bugs activity.
	*
	* @param bug_id the bug_id of this bugs activity
	*/
	public void setBug_id(int bug_id) {
		_bugsActivity.setBug_id(bug_id);
	}

	/**
	* Returns the attach_id of this bugs activity.
	*
	* @return the attach_id of this bugs activity
	*/
	public int getAttach_id() {
		return _bugsActivity.getAttach_id();
	}

	/**
	* Sets the attach_id of this bugs activity.
	*
	* @param attach_id the attach_id of this bugs activity
	*/
	public void setAttach_id(int attach_id) {
		_bugsActivity.setAttach_id(attach_id);
	}

	/**
	* Returns the who of this bugs activity.
	*
	* @return the who of this bugs activity
	*/
	public int getWho() {
		return _bugsActivity.getWho();
	}

	/**
	* Sets the who of this bugs activity.
	*
	* @param who the who of this bugs activity
	*/
	public void setWho(int who) {
		_bugsActivity.setWho(who);
	}

	/**
	* Returns the bug_when of this bugs activity.
	*
	* @return the bug_when of this bugs activity
	*/
	public java.util.Date getBug_when() {
		return _bugsActivity.getBug_when();
	}

	/**
	* Sets the bug_when of this bugs activity.
	*
	* @param bug_when the bug_when of this bugs activity
	*/
	public void setBug_when(java.util.Date bug_when) {
		_bugsActivity.setBug_when(bug_when);
	}

	/**
	* Returns the fieldid of this bugs activity.
	*
	* @return the fieldid of this bugs activity
	*/
	public int getFieldid() {
		return _bugsActivity.getFieldid();
	}

	/**
	* Sets the fieldid of this bugs activity.
	*
	* @param fieldid the fieldid of this bugs activity
	*/
	public void setFieldid(int fieldid) {
		_bugsActivity.setFieldid(fieldid);
	}

	/**
	* Returns the added of this bugs activity.
	*
	* @return the added of this bugs activity
	*/
	public java.lang.String getAdded() {
		return _bugsActivity.getAdded();
	}

	/**
	* Sets the added of this bugs activity.
	*
	* @param added the added of this bugs activity
	*/
	public void setAdded(java.lang.String added) {
		_bugsActivity.setAdded(added);
	}

	/**
	* Returns the removed of this bugs activity.
	*
	* @return the removed of this bugs activity
	*/
	public java.lang.String getRemoved() {
		return _bugsActivity.getRemoved();
	}

	/**
	* Sets the removed of this bugs activity.
	*
	* @param removed the removed of this bugs activity
	*/
	public void setRemoved(java.lang.String removed) {
		_bugsActivity.setRemoved(removed);
	}

	public boolean isNew() {
		return _bugsActivity.isNew();
	}

	public void setNew(boolean n) {
		_bugsActivity.setNew(n);
	}

	public boolean isCachedModel() {
		return _bugsActivity.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_bugsActivity.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _bugsActivity.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _bugsActivity.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_bugsActivity.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _bugsActivity.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_bugsActivity.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BugsActivityWrapper((BugsActivity)_bugsActivity.clone());
	}

	public int compareTo(com.vmware.model.BugsActivity bugsActivity) {
		return _bugsActivity.compareTo(bugsActivity);
	}

	@Override
	public int hashCode() {
		return _bugsActivity.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.BugsActivity> toCacheModel() {
		return _bugsActivity.toCacheModel();
	}

	public com.vmware.model.BugsActivity toEscapedModel() {
		return new BugsActivityWrapper(_bugsActivity.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _bugsActivity.toString();
	}

	public java.lang.String toXmlString() {
		return _bugsActivity.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_bugsActivity.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BugsActivity getWrappedBugsActivity() {
		return _bugsActivity;
	}

	public BugsActivity getWrappedModel() {
		return _bugsActivity;
	}

	public void resetOriginalValues() {
		_bugsActivity.resetOriginalValues();
	}

	private BugsActivity _bugsActivity;
}