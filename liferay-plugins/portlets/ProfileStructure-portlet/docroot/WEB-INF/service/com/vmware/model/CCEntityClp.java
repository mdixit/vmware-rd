/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.CCEntityLocalServiceUtil;
import com.vmware.service.persistence.CCEntityPK;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class CCEntityClp extends BaseModelImpl<CCEntity> implements CCEntity {
	public CCEntityClp() {
	}

	public Class<?> getModelClass() {
		return CCEntity.class;
	}

	public String getModelClassName() {
		return CCEntity.class.getName();
	}

	public CCEntityPK getPrimaryKey() {
		return new CCEntityPK(_bug_id, _who);
	}

	public void setPrimaryKey(CCEntityPK primaryKey) {
		setBug_id(primaryKey.bug_id);
		setWho(primaryKey.who);
	}

	public Serializable getPrimaryKeyObj() {
		return new CCEntityPK(_bug_id, _who);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((CCEntityPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("who", getWho());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public BaseModel<?> getCCEntityRemoteModel() {
		return _ccEntityRemoteModel;
	}

	public void setCCEntityRemoteModel(BaseModel<?> ccEntityRemoteModel) {
		_ccEntityRemoteModel = ccEntityRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			CCEntityLocalServiceUtil.addCCEntity(this);
		}
		else {
			CCEntityLocalServiceUtil.updateCCEntity(this);
		}
	}

	@Override
	public CCEntity toEscapedModel() {
		return (CCEntity)Proxy.newProxyInstance(CCEntity.class.getClassLoader(),
			new Class[] { CCEntity.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CCEntityClp clone = new CCEntityClp();

		clone.setBug_id(getBug_id());
		clone.setWho(getWho());

		return clone;
	}

	public int compareTo(CCEntity ccEntity) {
		CCEntityPK primaryKey = ccEntity.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CCEntityClp ccEntity = null;

		try {
			ccEntity = (CCEntityClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		CCEntityPK primaryKey = ccEntity.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{bug_id=");
		sb.append(getBug_id());
		sb.append(", who=");
		sb.append(getWho());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.CCEntity");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>who</column-name><column-value><![CDATA[");
		sb.append(getWho());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_id;
	private int _who;
	private BaseModel<?> _ccEntityRemoteModel;
}