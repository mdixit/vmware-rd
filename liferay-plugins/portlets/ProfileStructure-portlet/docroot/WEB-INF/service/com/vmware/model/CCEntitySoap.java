/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.vmware.service.persistence.CCEntityPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.CCEntityServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.CCEntityServiceSoap
 * @generated
 */
public class CCEntitySoap implements Serializable {
	public static CCEntitySoap toSoapModel(CCEntity model) {
		CCEntitySoap soapModel = new CCEntitySoap();

		soapModel.setBug_id(model.getBug_id());
		soapModel.setWho(model.getWho());

		return soapModel;
	}

	public static CCEntitySoap[] toSoapModels(CCEntity[] models) {
		CCEntitySoap[] soapModels = new CCEntitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CCEntitySoap[][] toSoapModels(CCEntity[][] models) {
		CCEntitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CCEntitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new CCEntitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CCEntitySoap[] toSoapModels(List<CCEntity> models) {
		List<CCEntitySoap> soapModels = new ArrayList<CCEntitySoap>(models.size());

		for (CCEntity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CCEntitySoap[soapModels.size()]);
	}

	public CCEntitySoap() {
	}

	public CCEntityPK getPrimaryKey() {
		return new CCEntityPK(_bug_id, _who);
	}

	public void setPrimaryKey(CCEntityPK pk) {
		setBug_id(pk.bug_id);
		setWho(pk.who);
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	private int _bug_id;
	private int _who;
}