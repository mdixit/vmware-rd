/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CCEntity}.
 * </p>
 *
 * @author    iscisc
 * @see       CCEntity
 * @generated
 */
public class CCEntityWrapper implements CCEntity, ModelWrapper<CCEntity> {
	public CCEntityWrapper(CCEntity ccEntity) {
		_ccEntity = ccEntity;
	}

	public Class<?> getModelClass() {
		return CCEntity.class;
	}

	public String getModelClassName() {
		return CCEntity.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("who", getWho());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}
	}

	/**
	* Returns the primary key of this c c entity.
	*
	* @return the primary key of this c c entity
	*/
	public com.vmware.service.persistence.CCEntityPK getPrimaryKey() {
		return _ccEntity.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c c entity.
	*
	* @param primaryKey the primary key of this c c entity
	*/
	public void setPrimaryKey(
		com.vmware.service.persistence.CCEntityPK primaryKey) {
		_ccEntity.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_id of this c c entity.
	*
	* @return the bug_id of this c c entity
	*/
	public int getBug_id() {
		return _ccEntity.getBug_id();
	}

	/**
	* Sets the bug_id of this c c entity.
	*
	* @param bug_id the bug_id of this c c entity
	*/
	public void setBug_id(int bug_id) {
		_ccEntity.setBug_id(bug_id);
	}

	/**
	* Returns the who of this c c entity.
	*
	* @return the who of this c c entity
	*/
	public int getWho() {
		return _ccEntity.getWho();
	}

	/**
	* Sets the who of this c c entity.
	*
	* @param who the who of this c c entity
	*/
	public void setWho(int who) {
		_ccEntity.setWho(who);
	}

	public boolean isNew() {
		return _ccEntity.isNew();
	}

	public void setNew(boolean n) {
		_ccEntity.setNew(n);
	}

	public boolean isCachedModel() {
		return _ccEntity.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_ccEntity.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _ccEntity.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _ccEntity.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ccEntity.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ccEntity.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ccEntity.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CCEntityWrapper((CCEntity)_ccEntity.clone());
	}

	public int compareTo(com.vmware.model.CCEntity ccEntity) {
		return _ccEntity.compareTo(ccEntity);
	}

	@Override
	public int hashCode() {
		return _ccEntity.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.CCEntity> toCacheModel() {
		return _ccEntity.toCacheModel();
	}

	public com.vmware.model.CCEntity toEscapedModel() {
		return new CCEntityWrapper(_ccEntity.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ccEntity.toString();
	}

	public java.lang.String toXmlString() {
		return _ccEntity.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ccEntity.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CCEntity getWrappedCCEntity() {
		return _ccEntity;
	}

	public CCEntity getWrappedModel() {
		return _ccEntity;
	}

	public void resetOriginalValues() {
		_ccEntity.resetOriginalValues();
	}

	private CCEntity _ccEntity;
}