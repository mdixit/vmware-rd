/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.ComponentsLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class ComponentsClp extends BaseModelImpl<Components>
	implements Components {
	public ComponentsClp() {
	}

	public Class<?> getModelClass() {
		return Components.class;
	}

	public String getModelClassName() {
		return Components.class.getName();
	}

	public int getPrimaryKey() {
		return _comp_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setComp_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_comp_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("comp_id", getComp_id());
		attributes.put("category_id", getCategory_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("template", getTemplate());
		attributes.put("initialowner", getInitialowner());
		attributes.put("initialqacontact", getInitialqacontact());
		attributes.put("manager", getManager());
		attributes.put("qa_manager", getQa_manager());
		attributes.put("disallownew", getDisallownew());
		attributes.put("disable_template", getDisable_template());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer comp_id = (Integer)attributes.get("comp_id");

		if (comp_id != null) {
			setComp_id(comp_id);
		}

		Integer category_id = (Integer)attributes.get("category_id");

		if (category_id != null) {
			setCategory_id(category_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String template = (String)attributes.get("template");

		if (template != null) {
			setTemplate(template);
		}

		Integer initialowner = (Integer)attributes.get("initialowner");

		if (initialowner != null) {
			setInitialowner(initialowner);
		}

		Integer initialqacontact = (Integer)attributes.get("initialqacontact");

		if (initialqacontact != null) {
			setInitialqacontact(initialqacontact);
		}

		Integer manager = (Integer)attributes.get("manager");

		if (manager != null) {
			setManager(manager);
		}

		Integer qa_manager = (Integer)attributes.get("qa_manager");

		if (qa_manager != null) {
			setQa_manager(qa_manager);
		}

		Integer disallownew = (Integer)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}

		Integer disable_template = (Integer)attributes.get("disable_template");

		if (disable_template != null) {
			setDisable_template(disable_template);
		}
	}

	public int getComp_id() {
		return _comp_id;
	}

	public void setComp_id(int comp_id) {
		_comp_id = comp_id;
	}

	public int getCategory_id() {
		return _category_id;
	}

	public void setCategory_id(int category_id) {
		_category_id = category_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getTemplate() {
		return _template;
	}

	public void setTemplate(String template) {
		_template = template;
	}

	public int getInitialowner() {
		return _initialowner;
	}

	public void setInitialowner(int initialowner) {
		_initialowner = initialowner;
	}

	public int getInitialqacontact() {
		return _initialqacontact;
	}

	public void setInitialqacontact(int initialqacontact) {
		_initialqacontact = initialqacontact;
	}

	public int getManager() {
		return _manager;
	}

	public void setManager(int manager) {
		_manager = manager;
	}

	public int getQa_manager() {
		return _qa_manager;
	}

	public void setQa_manager(int qa_manager) {
		_qa_manager = qa_manager;
	}

	public int getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(int disallownew) {
		_disallownew = disallownew;
	}

	public int getDisable_template() {
		return _disable_template;
	}

	public void setDisable_template(int disable_template) {
		_disable_template = disable_template;
	}

	public BaseModel<?> getComponentsRemoteModel() {
		return _componentsRemoteModel;
	}

	public void setComponentsRemoteModel(BaseModel<?> componentsRemoteModel) {
		_componentsRemoteModel = componentsRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ComponentsLocalServiceUtil.addComponents(this);
		}
		else {
			ComponentsLocalServiceUtil.updateComponents(this);
		}
	}

	@Override
	public Components toEscapedModel() {
		return (Components)Proxy.newProxyInstance(Components.class.getClassLoader(),
			new Class[] { Components.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ComponentsClp clone = new ComponentsClp();

		clone.setComp_id(getComp_id());
		clone.setCategory_id(getCategory_id());
		clone.setName(getName());
		clone.setDescription(getDescription());
		clone.setTemplate(getTemplate());
		clone.setInitialowner(getInitialowner());
		clone.setInitialqacontact(getInitialqacontact());
		clone.setManager(getManager());
		clone.setQa_manager(getQa_manager());
		clone.setDisallownew(getDisallownew());
		clone.setDisable_template(getDisable_template());

		return clone;
	}

	public int compareTo(Components components) {
		int primaryKey = components.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ComponentsClp components = null;

		try {
			components = (ComponentsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = components.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{comp_id=");
		sb.append(getComp_id());
		sb.append(", category_id=");
		sb.append(getCategory_id());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", template=");
		sb.append(getTemplate());
		sb.append(", initialowner=");
		sb.append(getInitialowner());
		sb.append(", initialqacontact=");
		sb.append(getInitialqacontact());
		sb.append(", manager=");
		sb.append(getManager());
		sb.append(", qa_manager=");
		sb.append(getQa_manager());
		sb.append(", disallownew=");
		sb.append(getDisallownew());
		sb.append(", disable_template=");
		sb.append(getDisable_template());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Components");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>comp_id</column-name><column-value><![CDATA[");
		sb.append(getComp_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>category_id</column-name><column-value><![CDATA[");
		sb.append(getCategory_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>template</column-name><column-value><![CDATA[");
		sb.append(getTemplate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>initialowner</column-name><column-value><![CDATA[");
		sb.append(getInitialowner());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>initialqacontact</column-name><column-value><![CDATA[");
		sb.append(getInitialqacontact());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>manager</column-name><column-value><![CDATA[");
		sb.append(getManager());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>qa_manager</column-name><column-value><![CDATA[");
		sb.append(getQa_manager());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disallownew</column-name><column-value><![CDATA[");
		sb.append(getDisallownew());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disable_template</column-name><column-value><![CDATA[");
		sb.append(getDisable_template());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _comp_id;
	private int _category_id;
	private String _name;
	private String _description;
	private String _template;
	private int _initialowner;
	private int _initialqacontact;
	private int _manager;
	private int _qa_manager;
	private int _disallownew;
	private int _disable_template;
	private BaseModel<?> _componentsRemoteModel;
}