/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.ComponentsServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.ComponentsServiceSoap
 * @generated
 */
public class ComponentsSoap implements Serializable {
	public static ComponentsSoap toSoapModel(Components model) {
		ComponentsSoap soapModel = new ComponentsSoap();

		soapModel.setComp_id(model.getComp_id());
		soapModel.setCategory_id(model.getCategory_id());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());
		soapModel.setTemplate(model.getTemplate());
		soapModel.setInitialowner(model.getInitialowner());
		soapModel.setInitialqacontact(model.getInitialqacontact());
		soapModel.setManager(model.getManager());
		soapModel.setQa_manager(model.getQa_manager());
		soapModel.setDisallownew(model.getDisallownew());
		soapModel.setDisable_template(model.getDisable_template());

		return soapModel;
	}

	public static ComponentsSoap[] toSoapModels(Components[] models) {
		ComponentsSoap[] soapModels = new ComponentsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ComponentsSoap[][] toSoapModels(Components[][] models) {
		ComponentsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ComponentsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ComponentsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ComponentsSoap[] toSoapModels(List<Components> models) {
		List<ComponentsSoap> soapModels = new ArrayList<ComponentsSoap>(models.size());

		for (Components model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ComponentsSoap[soapModels.size()]);
	}

	public ComponentsSoap() {
	}

	public int getPrimaryKey() {
		return _comp_id;
	}

	public void setPrimaryKey(int pk) {
		setComp_id(pk);
	}

	public int getComp_id() {
		return _comp_id;
	}

	public void setComp_id(int comp_id) {
		_comp_id = comp_id;
	}

	public int getCategory_id() {
		return _category_id;
	}

	public void setCategory_id(int category_id) {
		_category_id = category_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getTemplate() {
		return _template;
	}

	public void setTemplate(String template) {
		_template = template;
	}

	public int getInitialowner() {
		return _initialowner;
	}

	public void setInitialowner(int initialowner) {
		_initialowner = initialowner;
	}

	public int getInitialqacontact() {
		return _initialqacontact;
	}

	public void setInitialqacontact(int initialqacontact) {
		_initialqacontact = initialqacontact;
	}

	public int getManager() {
		return _manager;
	}

	public void setManager(int manager) {
		_manager = manager;
	}

	public int getQa_manager() {
		return _qa_manager;
	}

	public void setQa_manager(int qa_manager) {
		_qa_manager = qa_manager;
	}

	public int getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(int disallownew) {
		_disallownew = disallownew;
	}

	public int getDisable_template() {
		return _disable_template;
	}

	public void setDisable_template(int disable_template) {
		_disable_template = disable_template;
	}

	private int _comp_id;
	private int _category_id;
	private String _name;
	private String _description;
	private String _template;
	private int _initialowner;
	private int _initialqacontact;
	private int _manager;
	private int _qa_manager;
	private int _disallownew;
	private int _disable_template;
}