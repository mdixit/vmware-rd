/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Components}.
 * </p>
 *
 * @author    iscisc
 * @see       Components
 * @generated
 */
public class ComponentsWrapper implements Components, ModelWrapper<Components> {
	public ComponentsWrapper(Components components) {
		_components = components;
	}

	public Class<?> getModelClass() {
		return Components.class;
	}

	public String getModelClassName() {
		return Components.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("comp_id", getComp_id());
		attributes.put("category_id", getCategory_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("template", getTemplate());
		attributes.put("initialowner", getInitialowner());
		attributes.put("initialqacontact", getInitialqacontact());
		attributes.put("manager", getManager());
		attributes.put("qa_manager", getQa_manager());
		attributes.put("disallownew", getDisallownew());
		attributes.put("disable_template", getDisable_template());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer comp_id = (Integer)attributes.get("comp_id");

		if (comp_id != null) {
			setComp_id(comp_id);
		}

		Integer category_id = (Integer)attributes.get("category_id");

		if (category_id != null) {
			setCategory_id(category_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String template = (String)attributes.get("template");

		if (template != null) {
			setTemplate(template);
		}

		Integer initialowner = (Integer)attributes.get("initialowner");

		if (initialowner != null) {
			setInitialowner(initialowner);
		}

		Integer initialqacontact = (Integer)attributes.get("initialqacontact");

		if (initialqacontact != null) {
			setInitialqacontact(initialqacontact);
		}

		Integer manager = (Integer)attributes.get("manager");

		if (manager != null) {
			setManager(manager);
		}

		Integer qa_manager = (Integer)attributes.get("qa_manager");

		if (qa_manager != null) {
			setQa_manager(qa_manager);
		}

		Integer disallownew = (Integer)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}

		Integer disable_template = (Integer)attributes.get("disable_template");

		if (disable_template != null) {
			setDisable_template(disable_template);
		}
	}

	/**
	* Returns the primary key of this components.
	*
	* @return the primary key of this components
	*/
	public int getPrimaryKey() {
		return _components.getPrimaryKey();
	}

	/**
	* Sets the primary key of this components.
	*
	* @param primaryKey the primary key of this components
	*/
	public void setPrimaryKey(int primaryKey) {
		_components.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the comp_id of this components.
	*
	* @return the comp_id of this components
	*/
	public int getComp_id() {
		return _components.getComp_id();
	}

	/**
	* Sets the comp_id of this components.
	*
	* @param comp_id the comp_id of this components
	*/
	public void setComp_id(int comp_id) {
		_components.setComp_id(comp_id);
	}

	/**
	* Returns the category_id of this components.
	*
	* @return the category_id of this components
	*/
	public int getCategory_id() {
		return _components.getCategory_id();
	}

	/**
	* Sets the category_id of this components.
	*
	* @param category_id the category_id of this components
	*/
	public void setCategory_id(int category_id) {
		_components.setCategory_id(category_id);
	}

	/**
	* Returns the name of this components.
	*
	* @return the name of this components
	*/
	public java.lang.String getName() {
		return _components.getName();
	}

	/**
	* Sets the name of this components.
	*
	* @param name the name of this components
	*/
	public void setName(java.lang.String name) {
		_components.setName(name);
	}

	/**
	* Returns the description of this components.
	*
	* @return the description of this components
	*/
	public java.lang.String getDescription() {
		return _components.getDescription();
	}

	/**
	* Sets the description of this components.
	*
	* @param description the description of this components
	*/
	public void setDescription(java.lang.String description) {
		_components.setDescription(description);
	}

	/**
	* Returns the template of this components.
	*
	* @return the template of this components
	*/
	public java.lang.String getTemplate() {
		return _components.getTemplate();
	}

	/**
	* Sets the template of this components.
	*
	* @param template the template of this components
	*/
	public void setTemplate(java.lang.String template) {
		_components.setTemplate(template);
	}

	/**
	* Returns the initialowner of this components.
	*
	* @return the initialowner of this components
	*/
	public int getInitialowner() {
		return _components.getInitialowner();
	}

	/**
	* Sets the initialowner of this components.
	*
	* @param initialowner the initialowner of this components
	*/
	public void setInitialowner(int initialowner) {
		_components.setInitialowner(initialowner);
	}

	/**
	* Returns the initialqacontact of this components.
	*
	* @return the initialqacontact of this components
	*/
	public int getInitialqacontact() {
		return _components.getInitialqacontact();
	}

	/**
	* Sets the initialqacontact of this components.
	*
	* @param initialqacontact the initialqacontact of this components
	*/
	public void setInitialqacontact(int initialqacontact) {
		_components.setInitialqacontact(initialqacontact);
	}

	/**
	* Returns the manager of this components.
	*
	* @return the manager of this components
	*/
	public int getManager() {
		return _components.getManager();
	}

	/**
	* Sets the manager of this components.
	*
	* @param manager the manager of this components
	*/
	public void setManager(int manager) {
		_components.setManager(manager);
	}

	/**
	* Returns the qa_manager of this components.
	*
	* @return the qa_manager of this components
	*/
	public int getQa_manager() {
		return _components.getQa_manager();
	}

	/**
	* Sets the qa_manager of this components.
	*
	* @param qa_manager the qa_manager of this components
	*/
	public void setQa_manager(int qa_manager) {
		_components.setQa_manager(qa_manager);
	}

	/**
	* Returns the disallownew of this components.
	*
	* @return the disallownew of this components
	*/
	public int getDisallownew() {
		return _components.getDisallownew();
	}

	/**
	* Sets the disallownew of this components.
	*
	* @param disallownew the disallownew of this components
	*/
	public void setDisallownew(int disallownew) {
		_components.setDisallownew(disallownew);
	}

	/**
	* Returns the disable_template of this components.
	*
	* @return the disable_template of this components
	*/
	public int getDisable_template() {
		return _components.getDisable_template();
	}

	/**
	* Sets the disable_template of this components.
	*
	* @param disable_template the disable_template of this components
	*/
	public void setDisable_template(int disable_template) {
		_components.setDisable_template(disable_template);
	}

	public boolean isNew() {
		return _components.isNew();
	}

	public void setNew(boolean n) {
		_components.setNew(n);
	}

	public boolean isCachedModel() {
		return _components.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_components.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _components.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _components.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_components.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _components.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_components.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ComponentsWrapper((Components)_components.clone());
	}

	public int compareTo(com.vmware.model.Components components) {
		return _components.compareTo(components);
	}

	@Override
	public int hashCode() {
		return _components.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Components> toCacheModel() {
		return _components.toCacheModel();
	}

	public com.vmware.model.Components toEscapedModel() {
		return new ComponentsWrapper(_components.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _components.toString();
	}

	public java.lang.String toXmlString() {
		return _components.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_components.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Components getWrappedComponents() {
		return _components;
	}

	public Components getWrappedModel() {
		return _components;
	}

	public void resetOriginalValues() {
		_components.resetOriginalValues();
	}

	private Components _components;
}