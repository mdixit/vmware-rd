/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.DuplicateLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class DuplicateClp extends BaseModelImpl<Duplicate> implements Duplicate {
	public DuplicateClp() {
	}

	public Class<?> getModelClass() {
		return Duplicate.class;
	}

	public String getModelClassName() {
		return Duplicate.class.getName();
	}

	public int getPrimaryKey() {
		return _dupe;
	}

	public void setPrimaryKey(int primaryKey) {
		setDupe(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_dupe);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dupe", getDupe());
		attributes.put("dupe_of", getDupe_of());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer dupe = (Integer)attributes.get("dupe");

		if (dupe != null) {
			setDupe(dupe);
		}

		Integer dupe_of = (Integer)attributes.get("dupe_of");

		if (dupe_of != null) {
			setDupe_of(dupe_of);
		}
	}

	public int getDupe() {
		return _dupe;
	}

	public void setDupe(int dupe) {
		_dupe = dupe;
	}

	public int getDupe_of() {
		return _dupe_of;
	}

	public void setDupe_of(int dupe_of) {
		_dupe_of = dupe_of;
	}

	public BaseModel<?> getDuplicateRemoteModel() {
		return _duplicateRemoteModel;
	}

	public void setDuplicateRemoteModel(BaseModel<?> duplicateRemoteModel) {
		_duplicateRemoteModel = duplicateRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			DuplicateLocalServiceUtil.addDuplicate(this);
		}
		else {
			DuplicateLocalServiceUtil.updateDuplicate(this);
		}
	}

	@Override
	public Duplicate toEscapedModel() {
		return (Duplicate)Proxy.newProxyInstance(Duplicate.class.getClassLoader(),
			new Class[] { Duplicate.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DuplicateClp clone = new DuplicateClp();

		clone.setDupe(getDupe());
		clone.setDupe_of(getDupe_of());

		return clone;
	}

	public int compareTo(Duplicate duplicate) {
		int primaryKey = duplicate.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		DuplicateClp duplicate = null;

		try {
			duplicate = (DuplicateClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = duplicate.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{dupe=");
		sb.append(getDupe());
		sb.append(", dupe_of=");
		sb.append(getDupe_of());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Duplicate");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>dupe</column-name><column-value><![CDATA[");
		sb.append(getDupe());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dupe_of</column-name><column-value><![CDATA[");
		sb.append(getDupe_of());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _dupe;
	private int _dupe_of;
	private BaseModel<?> _duplicateRemoteModel;
}