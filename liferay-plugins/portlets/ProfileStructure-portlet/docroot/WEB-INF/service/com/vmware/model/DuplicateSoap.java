/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.DuplicateServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.DuplicateServiceSoap
 * @generated
 */
public class DuplicateSoap implements Serializable {
	public static DuplicateSoap toSoapModel(Duplicate model) {
		DuplicateSoap soapModel = new DuplicateSoap();

		soapModel.setDupe(model.getDupe());
		soapModel.setDupe_of(model.getDupe_of());

		return soapModel;
	}

	public static DuplicateSoap[] toSoapModels(Duplicate[] models) {
		DuplicateSoap[] soapModels = new DuplicateSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DuplicateSoap[][] toSoapModels(Duplicate[][] models) {
		DuplicateSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DuplicateSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DuplicateSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DuplicateSoap[] toSoapModels(List<Duplicate> models) {
		List<DuplicateSoap> soapModels = new ArrayList<DuplicateSoap>(models.size());

		for (Duplicate model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DuplicateSoap[soapModels.size()]);
	}

	public DuplicateSoap() {
	}

	public int getPrimaryKey() {
		return _dupe;
	}

	public void setPrimaryKey(int pk) {
		setDupe(pk);
	}

	public int getDupe() {
		return _dupe;
	}

	public void setDupe(int dupe) {
		_dupe = dupe;
	}

	public int getDupe_of() {
		return _dupe_of;
	}

	public void setDupe_of(int dupe_of) {
		_dupe_of = dupe_of;
	}

	private int _dupe;
	private int _dupe_of;
}