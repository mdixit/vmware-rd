/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Duplicate}.
 * </p>
 *
 * @author    iscisc
 * @see       Duplicate
 * @generated
 */
public class DuplicateWrapper implements Duplicate, ModelWrapper<Duplicate> {
	public DuplicateWrapper(Duplicate duplicate) {
		_duplicate = duplicate;
	}

	public Class<?> getModelClass() {
		return Duplicate.class;
	}

	public String getModelClassName() {
		return Duplicate.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dupe", getDupe());
		attributes.put("dupe_of", getDupe_of());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer dupe = (Integer)attributes.get("dupe");

		if (dupe != null) {
			setDupe(dupe);
		}

		Integer dupe_of = (Integer)attributes.get("dupe_of");

		if (dupe_of != null) {
			setDupe_of(dupe_of);
		}
	}

	/**
	* Returns the primary key of this duplicate.
	*
	* @return the primary key of this duplicate
	*/
	public int getPrimaryKey() {
		return _duplicate.getPrimaryKey();
	}

	/**
	* Sets the primary key of this duplicate.
	*
	* @param primaryKey the primary key of this duplicate
	*/
	public void setPrimaryKey(int primaryKey) {
		_duplicate.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the dupe of this duplicate.
	*
	* @return the dupe of this duplicate
	*/
	public int getDupe() {
		return _duplicate.getDupe();
	}

	/**
	* Sets the dupe of this duplicate.
	*
	* @param dupe the dupe of this duplicate
	*/
	public void setDupe(int dupe) {
		_duplicate.setDupe(dupe);
	}

	/**
	* Returns the dupe_of of this duplicate.
	*
	* @return the dupe_of of this duplicate
	*/
	public int getDupe_of() {
		return _duplicate.getDupe_of();
	}

	/**
	* Sets the dupe_of of this duplicate.
	*
	* @param dupe_of the dupe_of of this duplicate
	*/
	public void setDupe_of(int dupe_of) {
		_duplicate.setDupe_of(dupe_of);
	}

	public boolean isNew() {
		return _duplicate.isNew();
	}

	public void setNew(boolean n) {
		_duplicate.setNew(n);
	}

	public boolean isCachedModel() {
		return _duplicate.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_duplicate.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _duplicate.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _duplicate.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_duplicate.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _duplicate.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_duplicate.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DuplicateWrapper((Duplicate)_duplicate.clone());
	}

	public int compareTo(com.vmware.model.Duplicate duplicate) {
		return _duplicate.compareTo(duplicate);
	}

	@Override
	public int hashCode() {
		return _duplicate.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Duplicate> toCacheModel() {
		return _duplicate.toCacheModel();
	}

	public com.vmware.model.Duplicate toEscapedModel() {
		return new DuplicateWrapper(_duplicate.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _duplicate.toString();
	}

	public java.lang.String toXmlString() {
		return _duplicate.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_duplicate.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Duplicate getWrappedDuplicate() {
		return _duplicate;
	}

	public Duplicate getWrappedModel() {
		return _duplicate;
	}

	public void resetOriginalValues() {
		_duplicate.resetOriginalValues();
	}

	private Duplicate _duplicate;
}