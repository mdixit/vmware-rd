/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.FieldDefsLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class FieldDefsClp extends BaseModelImpl<FieldDefs> implements FieldDefs {
	public FieldDefsClp() {
	}

	public Class<?> getModelClass() {
		return FieldDefs.class;
	}

	public String getModelClassName() {
		return FieldDefs.class.getName();
	}

	public int getPrimaryKey() {
		return _fielddef_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setFielddef_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_fielddef_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("fielddef_id", getFielddef_id());
		attributes.put("name", getName());
		attributes.put("type", getType());
		attributes.put("custom", getCustom());
		attributes.put("description", getDescription());
		attributes.put("mailhead", getMailhead());
		attributes.put("sortkey", getSortkey());
		attributes.put("obsolete", getObsolete());
		attributes.put("enter_bug", getEnter_bug());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer fielddef_id = (Integer)attributes.get("fielddef_id");

		if (fielddef_id != null) {
			setFielddef_id(fielddef_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Integer custom = (Integer)attributes.get("custom");

		if (custom != null) {
			setCustom(custom);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Integer mailhead = (Integer)attributes.get("mailhead");

		if (mailhead != null) {
			setMailhead(mailhead);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer obsolete = (Integer)attributes.get("obsolete");

		if (obsolete != null) {
			setObsolete(obsolete);
		}

		Integer enter_bug = (Integer)attributes.get("enter_bug");

		if (enter_bug != null) {
			setEnter_bug(enter_bug);
		}
	}

	public int getFielddef_id() {
		return _fielddef_id;
	}

	public void setFielddef_id(int fielddef_id) {
		_fielddef_id = fielddef_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public int getCustom() {
		return _custom;
	}

	public void setCustom(int custom) {
		_custom = custom;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public int getMailhead() {
		return _mailhead;
	}

	public void setMailhead(int mailhead) {
		_mailhead = mailhead;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getObsolete() {
		return _obsolete;
	}

	public void setObsolete(int obsolete) {
		_obsolete = obsolete;
	}

	public int getEnter_bug() {
		return _enter_bug;
	}

	public void setEnter_bug(int enter_bug) {
		_enter_bug = enter_bug;
	}

	public BaseModel<?> getFieldDefsRemoteModel() {
		return _fieldDefsRemoteModel;
	}

	public void setFieldDefsRemoteModel(BaseModel<?> fieldDefsRemoteModel) {
		_fieldDefsRemoteModel = fieldDefsRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			FieldDefsLocalServiceUtil.addFieldDefs(this);
		}
		else {
			FieldDefsLocalServiceUtil.updateFieldDefs(this);
		}
	}

	@Override
	public FieldDefs toEscapedModel() {
		return (FieldDefs)Proxy.newProxyInstance(FieldDefs.class.getClassLoader(),
			new Class[] { FieldDefs.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		FieldDefsClp clone = new FieldDefsClp();

		clone.setFielddef_id(getFielddef_id());
		clone.setName(getName());
		clone.setType(getType());
		clone.setCustom(getCustom());
		clone.setDescription(getDescription());
		clone.setMailhead(getMailhead());
		clone.setSortkey(getSortkey());
		clone.setObsolete(getObsolete());
		clone.setEnter_bug(getEnter_bug());

		return clone;
	}

	public int compareTo(FieldDefs fieldDefs) {
		int primaryKey = fieldDefs.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		FieldDefsClp fieldDefs = null;

		try {
			fieldDefs = (FieldDefsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = fieldDefs.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{fielddef_id=");
		sb.append(getFielddef_id());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", type=");
		sb.append(getType());
		sb.append(", custom=");
		sb.append(getCustom());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", mailhead=");
		sb.append(getMailhead());
		sb.append(", sortkey=");
		sb.append(getSortkey());
		sb.append(", obsolete=");
		sb.append(getObsolete());
		sb.append(", enter_bug=");
		sb.append(getEnter_bug());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.FieldDefs");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>fielddef_id</column-name><column-value><![CDATA[");
		sb.append(getFielddef_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>custom</column-name><column-value><![CDATA[");
		sb.append(getCustom());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailhead</column-name><column-value><![CDATA[");
		sb.append(getMailhead());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sortkey</column-name><column-value><![CDATA[");
		sb.append(getSortkey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>obsolete</column-name><column-value><![CDATA[");
		sb.append(getObsolete());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>enter_bug</column-name><column-value><![CDATA[");
		sb.append(getEnter_bug());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _fielddef_id;
	private String _name;
	private int _type;
	private int _custom;
	private String _description;
	private int _mailhead;
	private int _sortkey;
	private int _obsolete;
	private int _enter_bug;
	private BaseModel<?> _fieldDefsRemoteModel;
}