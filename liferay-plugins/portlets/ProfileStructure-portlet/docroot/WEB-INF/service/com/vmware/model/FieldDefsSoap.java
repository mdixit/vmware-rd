/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.FieldDefsServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.FieldDefsServiceSoap
 * @generated
 */
public class FieldDefsSoap implements Serializable {
	public static FieldDefsSoap toSoapModel(FieldDefs model) {
		FieldDefsSoap soapModel = new FieldDefsSoap();

		soapModel.setFielddef_id(model.getFielddef_id());
		soapModel.setName(model.getName());
		soapModel.setType(model.getType());
		soapModel.setCustom(model.getCustom());
		soapModel.setDescription(model.getDescription());
		soapModel.setMailhead(model.getMailhead());
		soapModel.setSortkey(model.getSortkey());
		soapModel.setObsolete(model.getObsolete());
		soapModel.setEnter_bug(model.getEnter_bug());

		return soapModel;
	}

	public static FieldDefsSoap[] toSoapModels(FieldDefs[] models) {
		FieldDefsSoap[] soapModels = new FieldDefsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static FieldDefsSoap[][] toSoapModels(FieldDefs[][] models) {
		FieldDefsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new FieldDefsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new FieldDefsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static FieldDefsSoap[] toSoapModels(List<FieldDefs> models) {
		List<FieldDefsSoap> soapModels = new ArrayList<FieldDefsSoap>(models.size());

		for (FieldDefs model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new FieldDefsSoap[soapModels.size()]);
	}

	public FieldDefsSoap() {
	}

	public int getPrimaryKey() {
		return _fielddef_id;
	}

	public void setPrimaryKey(int pk) {
		setFielddef_id(pk);
	}

	public int getFielddef_id() {
		return _fielddef_id;
	}

	public void setFielddef_id(int fielddef_id) {
		_fielddef_id = fielddef_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public int getCustom() {
		return _custom;
	}

	public void setCustom(int custom) {
		_custom = custom;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public int getMailhead() {
		return _mailhead;
	}

	public void setMailhead(int mailhead) {
		_mailhead = mailhead;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getObsolete() {
		return _obsolete;
	}

	public void setObsolete(int obsolete) {
		_obsolete = obsolete;
	}

	public int getEnter_bug() {
		return _enter_bug;
	}

	public void setEnter_bug(int enter_bug) {
		_enter_bug = enter_bug;
	}

	private int _fielddef_id;
	private String _name;
	private int _type;
	private int _custom;
	private String _description;
	private int _mailhead;
	private int _sortkey;
	private int _obsolete;
	private int _enter_bug;
}