/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FieldDefs}.
 * </p>
 *
 * @author    iscisc
 * @see       FieldDefs
 * @generated
 */
public class FieldDefsWrapper implements FieldDefs, ModelWrapper<FieldDefs> {
	public FieldDefsWrapper(FieldDefs fieldDefs) {
		_fieldDefs = fieldDefs;
	}

	public Class<?> getModelClass() {
		return FieldDefs.class;
	}

	public String getModelClassName() {
		return FieldDefs.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("fielddef_id", getFielddef_id());
		attributes.put("name", getName());
		attributes.put("type", getType());
		attributes.put("custom", getCustom());
		attributes.put("description", getDescription());
		attributes.put("mailhead", getMailhead());
		attributes.put("sortkey", getSortkey());
		attributes.put("obsolete", getObsolete());
		attributes.put("enter_bug", getEnter_bug());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer fielddef_id = (Integer)attributes.get("fielddef_id");

		if (fielddef_id != null) {
			setFielddef_id(fielddef_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Integer custom = (Integer)attributes.get("custom");

		if (custom != null) {
			setCustom(custom);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Integer mailhead = (Integer)attributes.get("mailhead");

		if (mailhead != null) {
			setMailhead(mailhead);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer obsolete = (Integer)attributes.get("obsolete");

		if (obsolete != null) {
			setObsolete(obsolete);
		}

		Integer enter_bug = (Integer)attributes.get("enter_bug");

		if (enter_bug != null) {
			setEnter_bug(enter_bug);
		}
	}

	/**
	* Returns the primary key of this field defs.
	*
	* @return the primary key of this field defs
	*/
	public int getPrimaryKey() {
		return _fieldDefs.getPrimaryKey();
	}

	/**
	* Sets the primary key of this field defs.
	*
	* @param primaryKey the primary key of this field defs
	*/
	public void setPrimaryKey(int primaryKey) {
		_fieldDefs.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the fielddef_id of this field defs.
	*
	* @return the fielddef_id of this field defs
	*/
	public int getFielddef_id() {
		return _fieldDefs.getFielddef_id();
	}

	/**
	* Sets the fielddef_id of this field defs.
	*
	* @param fielddef_id the fielddef_id of this field defs
	*/
	public void setFielddef_id(int fielddef_id) {
		_fieldDefs.setFielddef_id(fielddef_id);
	}

	/**
	* Returns the name of this field defs.
	*
	* @return the name of this field defs
	*/
	public java.lang.String getName() {
		return _fieldDefs.getName();
	}

	/**
	* Sets the name of this field defs.
	*
	* @param name the name of this field defs
	*/
	public void setName(java.lang.String name) {
		_fieldDefs.setName(name);
	}

	/**
	* Returns the type of this field defs.
	*
	* @return the type of this field defs
	*/
	public int getType() {
		return _fieldDefs.getType();
	}

	/**
	* Sets the type of this field defs.
	*
	* @param type the type of this field defs
	*/
	public void setType(int type) {
		_fieldDefs.setType(type);
	}

	/**
	* Returns the custom of this field defs.
	*
	* @return the custom of this field defs
	*/
	public int getCustom() {
		return _fieldDefs.getCustom();
	}

	/**
	* Sets the custom of this field defs.
	*
	* @param custom the custom of this field defs
	*/
	public void setCustom(int custom) {
		_fieldDefs.setCustom(custom);
	}

	/**
	* Returns the description of this field defs.
	*
	* @return the description of this field defs
	*/
	public java.lang.String getDescription() {
		return _fieldDefs.getDescription();
	}

	/**
	* Sets the description of this field defs.
	*
	* @param description the description of this field defs
	*/
	public void setDescription(java.lang.String description) {
		_fieldDefs.setDescription(description);
	}

	/**
	* Returns the mailhead of this field defs.
	*
	* @return the mailhead of this field defs
	*/
	public int getMailhead() {
		return _fieldDefs.getMailhead();
	}

	/**
	* Sets the mailhead of this field defs.
	*
	* @param mailhead the mailhead of this field defs
	*/
	public void setMailhead(int mailhead) {
		_fieldDefs.setMailhead(mailhead);
	}

	/**
	* Returns the sortkey of this field defs.
	*
	* @return the sortkey of this field defs
	*/
	public int getSortkey() {
		return _fieldDefs.getSortkey();
	}

	/**
	* Sets the sortkey of this field defs.
	*
	* @param sortkey the sortkey of this field defs
	*/
	public void setSortkey(int sortkey) {
		_fieldDefs.setSortkey(sortkey);
	}

	/**
	* Returns the obsolete of this field defs.
	*
	* @return the obsolete of this field defs
	*/
	public int getObsolete() {
		return _fieldDefs.getObsolete();
	}

	/**
	* Sets the obsolete of this field defs.
	*
	* @param obsolete the obsolete of this field defs
	*/
	public void setObsolete(int obsolete) {
		_fieldDefs.setObsolete(obsolete);
	}

	/**
	* Returns the enter_bug of this field defs.
	*
	* @return the enter_bug of this field defs
	*/
	public int getEnter_bug() {
		return _fieldDefs.getEnter_bug();
	}

	/**
	* Sets the enter_bug of this field defs.
	*
	* @param enter_bug the enter_bug of this field defs
	*/
	public void setEnter_bug(int enter_bug) {
		_fieldDefs.setEnter_bug(enter_bug);
	}

	public boolean isNew() {
		return _fieldDefs.isNew();
	}

	public void setNew(boolean n) {
		_fieldDefs.setNew(n);
	}

	public boolean isCachedModel() {
		return _fieldDefs.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_fieldDefs.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _fieldDefs.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _fieldDefs.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_fieldDefs.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _fieldDefs.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_fieldDefs.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FieldDefsWrapper((FieldDefs)_fieldDefs.clone());
	}

	public int compareTo(com.vmware.model.FieldDefs fieldDefs) {
		return _fieldDefs.compareTo(fieldDefs);
	}

	@Override
	public int hashCode() {
		return _fieldDefs.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.FieldDefs> toCacheModel() {
		return _fieldDefs.toCacheModel();
	}

	public com.vmware.model.FieldDefs toEscapedModel() {
		return new FieldDefsWrapper(_fieldDefs.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _fieldDefs.toString();
	}

	public java.lang.String toXmlString() {
		return _fieldDefs.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_fieldDefs.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public FieldDefs getWrappedFieldDefs() {
		return _fieldDefs;
	}

	public FieldDefs getWrappedModel() {
		return _fieldDefs;
	}

	public void resetOriginalValues() {
		_fieldDefs.resetOriginalValues();
	}

	private FieldDefs _fieldDefs;
}