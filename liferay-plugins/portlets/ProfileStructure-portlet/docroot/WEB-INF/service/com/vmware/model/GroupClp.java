/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.GroupLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class GroupClp extends BaseModelImpl<Group> implements Group {
	public GroupClp() {
	}

	public Class<?> getModelClass() {
		return Group.class;
	}

	public String getModelClassName() {
		return Group.class.getName();
	}

	public int getPrimaryKey() {
		return _group_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setGroup_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_group_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("group_id", getGroup_id());
		attributes.put("name", getName());
		attributes.put("isbuggroup", getIsbuggroup());
		attributes.put("isactive", getIsactive());
		attributes.put("description", getDescription());
		attributes.put("userregexp", getUserregexp());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer group_id = (Integer)attributes.get("group_id");

		if (group_id != null) {
			setGroup_id(group_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer isbuggroup = (Integer)attributes.get("isbuggroup");

		if (isbuggroup != null) {
			setIsbuggroup(isbuggroup);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String userregexp = (String)attributes.get("userregexp");

		if (userregexp != null) {
			setUserregexp(userregexp);
		}
	}

	public int getGroup_id() {
		return _group_id;
	}

	public void setGroup_id(int group_id) {
		_group_id = group_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getIsbuggroup() {
		return _isbuggroup;
	}

	public void setIsbuggroup(int isbuggroup) {
		_isbuggroup = isbuggroup;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getUserregexp() {
		return _userregexp;
	}

	public void setUserregexp(String userregexp) {
		_userregexp = userregexp;
	}

	public BaseModel<?> getGroupRemoteModel() {
		return _groupRemoteModel;
	}

	public void setGroupRemoteModel(BaseModel<?> groupRemoteModel) {
		_groupRemoteModel = groupRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			GroupLocalServiceUtil.addGroup(this);
		}
		else {
			GroupLocalServiceUtil.updateGroup(this);
		}
	}

	@Override
	public Group toEscapedModel() {
		return (Group)Proxy.newProxyInstance(Group.class.getClassLoader(),
			new Class[] { Group.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		GroupClp clone = new GroupClp();

		clone.setGroup_id(getGroup_id());
		clone.setName(getName());
		clone.setIsbuggroup(getIsbuggroup());
		clone.setIsactive(getIsactive());
		clone.setDescription(getDescription());
		clone.setUserregexp(getUserregexp());

		return clone;
	}

	public int compareTo(Group group) {
		int primaryKey = group.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		GroupClp group = null;

		try {
			group = (GroupClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = group.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{group_id=");
		sb.append(getGroup_id());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", isbuggroup=");
		sb.append(getIsbuggroup());
		sb.append(", isactive=");
		sb.append(getIsactive());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", userregexp=");
		sb.append(getUserregexp());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Group");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>group_id</column-name><column-value><![CDATA[");
		sb.append(getGroup_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isbuggroup</column-name><column-value><![CDATA[");
		sb.append(getIsbuggroup());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isactive</column-name><column-value><![CDATA[");
		sb.append(getIsactive());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userregexp</column-name><column-value><![CDATA[");
		sb.append(getUserregexp());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _group_id;
	private String _name;
	private int _isbuggroup;
	private int _isactive;
	private String _description;
	private String _userregexp;
	private BaseModel<?> _groupRemoteModel;
}