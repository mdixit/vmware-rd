/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.GroupServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.GroupServiceSoap
 * @generated
 */
public class GroupSoap implements Serializable {
	public static GroupSoap toSoapModel(Group model) {
		GroupSoap soapModel = new GroupSoap();

		soapModel.setGroup_id(model.getGroup_id());
		soapModel.setName(model.getName());
		soapModel.setIsbuggroup(model.getIsbuggroup());
		soapModel.setIsactive(model.getIsactive());
		soapModel.setDescription(model.getDescription());
		soapModel.setUserregexp(model.getUserregexp());

		return soapModel;
	}

	public static GroupSoap[] toSoapModels(Group[] models) {
		GroupSoap[] soapModels = new GroupSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static GroupSoap[][] toSoapModels(Group[][] models) {
		GroupSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new GroupSoap[models.length][models[0].length];
		}
		else {
			soapModels = new GroupSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static GroupSoap[] toSoapModels(List<Group> models) {
		List<GroupSoap> soapModels = new ArrayList<GroupSoap>(models.size());

		for (Group model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new GroupSoap[soapModels.size()]);
	}

	public GroupSoap() {
	}

	public int getPrimaryKey() {
		return _group_id;
	}

	public void setPrimaryKey(int pk) {
		setGroup_id(pk);
	}

	public int getGroup_id() {
		return _group_id;
	}

	public void setGroup_id(int group_id) {
		_group_id = group_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getIsbuggroup() {
		return _isbuggroup;
	}

	public void setIsbuggroup(int isbuggroup) {
		_isbuggroup = isbuggroup;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getUserregexp() {
		return _userregexp;
	}

	public void setUserregexp(String userregexp) {
		_userregexp = userregexp;
	}

	private int _group_id;
	private String _name;
	private int _isbuggroup;
	private int _isactive;
	private String _description;
	private String _userregexp;
}