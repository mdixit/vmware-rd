/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Group}.
 * </p>
 *
 * @author    iscisc
 * @see       Group
 * @generated
 */
public class GroupWrapper implements Group, ModelWrapper<Group> {
	public GroupWrapper(Group group) {
		_group = group;
	}

	public Class<?> getModelClass() {
		return Group.class;
	}

	public String getModelClassName() {
		return Group.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("group_id", getGroup_id());
		attributes.put("name", getName());
		attributes.put("isbuggroup", getIsbuggroup());
		attributes.put("isactive", getIsactive());
		attributes.put("description", getDescription());
		attributes.put("userregexp", getUserregexp());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer group_id = (Integer)attributes.get("group_id");

		if (group_id != null) {
			setGroup_id(group_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer isbuggroup = (Integer)attributes.get("isbuggroup");

		if (isbuggroup != null) {
			setIsbuggroup(isbuggroup);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String userregexp = (String)attributes.get("userregexp");

		if (userregexp != null) {
			setUserregexp(userregexp);
		}
	}

	/**
	* Returns the primary key of this group.
	*
	* @return the primary key of this group
	*/
	public int getPrimaryKey() {
		return _group.getPrimaryKey();
	}

	/**
	* Sets the primary key of this group.
	*
	* @param primaryKey the primary key of this group
	*/
	public void setPrimaryKey(int primaryKey) {
		_group.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the group_id of this group.
	*
	* @return the group_id of this group
	*/
	public int getGroup_id() {
		return _group.getGroup_id();
	}

	/**
	* Sets the group_id of this group.
	*
	* @param group_id the group_id of this group
	*/
	public void setGroup_id(int group_id) {
		_group.setGroup_id(group_id);
	}

	/**
	* Returns the name of this group.
	*
	* @return the name of this group
	*/
	public java.lang.String getName() {
		return _group.getName();
	}

	/**
	* Sets the name of this group.
	*
	* @param name the name of this group
	*/
	public void setName(java.lang.String name) {
		_group.setName(name);
	}

	/**
	* Returns the isbuggroup of this group.
	*
	* @return the isbuggroup of this group
	*/
	public int getIsbuggroup() {
		return _group.getIsbuggroup();
	}

	/**
	* Sets the isbuggroup of this group.
	*
	* @param isbuggroup the isbuggroup of this group
	*/
	public void setIsbuggroup(int isbuggroup) {
		_group.setIsbuggroup(isbuggroup);
	}

	/**
	* Returns the isactive of this group.
	*
	* @return the isactive of this group
	*/
	public int getIsactive() {
		return _group.getIsactive();
	}

	/**
	* Sets the isactive of this group.
	*
	* @param isactive the isactive of this group
	*/
	public void setIsactive(int isactive) {
		_group.setIsactive(isactive);
	}

	/**
	* Returns the description of this group.
	*
	* @return the description of this group
	*/
	public java.lang.String getDescription() {
		return _group.getDescription();
	}

	/**
	* Sets the description of this group.
	*
	* @param description the description of this group
	*/
	public void setDescription(java.lang.String description) {
		_group.setDescription(description);
	}

	/**
	* Returns the userregexp of this group.
	*
	* @return the userregexp of this group
	*/
	public java.lang.String getUserregexp() {
		return _group.getUserregexp();
	}

	/**
	* Sets the userregexp of this group.
	*
	* @param userregexp the userregexp of this group
	*/
	public void setUserregexp(java.lang.String userregexp) {
		_group.setUserregexp(userregexp);
	}

	public boolean isNew() {
		return _group.isNew();
	}

	public void setNew(boolean n) {
		_group.setNew(n);
	}

	public boolean isCachedModel() {
		return _group.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_group.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _group.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _group.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_group.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _group.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_group.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new GroupWrapper((Group)_group.clone());
	}

	public int compareTo(com.vmware.model.Group group) {
		return _group.compareTo(group);
	}

	@Override
	public int hashCode() {
		return _group.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Group> toCacheModel() {
		return _group.toCacheModel();
	}

	public com.vmware.model.Group toEscapedModel() {
		return new GroupWrapper(_group.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _group.toString();
	}

	public java.lang.String toXmlString() {
		return _group.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_group.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Group getWrappedGroup() {
		return _group;
	}

	public Group getWrappedModel() {
		return _group;
	}

	public void resetOriginalValues() {
		_group.resetOriginalValues();
	}

	private Group _group;
}