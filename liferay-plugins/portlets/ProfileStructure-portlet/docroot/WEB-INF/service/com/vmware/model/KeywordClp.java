/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.KeywordLocalServiceUtil;
import com.vmware.service.persistence.KeywordPK;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class KeywordClp extends BaseModelImpl<Keyword> implements Keyword {
	public KeywordClp() {
	}

	public Class<?> getModelClass() {
		return Keyword.class;
	}

	public String getModelClassName() {
		return Keyword.class.getName();
	}

	public KeywordPK getPrimaryKey() {
		return new KeywordPK(_bug_id, _keywordid);
	}

	public void setPrimaryKey(KeywordPK primaryKey) {
		setBug_id(primaryKey.bug_id);
		setKeywordid(primaryKey.keywordid);
	}

	public Serializable getPrimaryKeyObj() {
		return new KeywordPK(_bug_id, _keywordid);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((KeywordPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("keywordid", getKeywordid());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer keywordid = (Integer)attributes.get("keywordid");

		if (keywordid != null) {
			setKeywordid(keywordid);
		}
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getKeywordid() {
		return _keywordid;
	}

	public void setKeywordid(int keywordid) {
		_keywordid = keywordid;
	}

	public BaseModel<?> getKeywordRemoteModel() {
		return _keywordRemoteModel;
	}

	public void setKeywordRemoteModel(BaseModel<?> keywordRemoteModel) {
		_keywordRemoteModel = keywordRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			KeywordLocalServiceUtil.addKeyword(this);
		}
		else {
			KeywordLocalServiceUtil.updateKeyword(this);
		}
	}

	@Override
	public Keyword toEscapedModel() {
		return (Keyword)Proxy.newProxyInstance(Keyword.class.getClassLoader(),
			new Class[] { Keyword.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		KeywordClp clone = new KeywordClp();

		clone.setBug_id(getBug_id());
		clone.setKeywordid(getKeywordid());

		return clone;
	}

	public int compareTo(Keyword keyword) {
		KeywordPK primaryKey = keyword.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		KeywordClp keyword = null;

		try {
			keyword = (KeywordClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		KeywordPK primaryKey = keyword.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{bug_id=");
		sb.append(getBug_id());
		sb.append(", keywordid=");
		sb.append(getKeywordid());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Keyword");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>keywordid</column-name><column-value><![CDATA[");
		sb.append(getKeywordid());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_id;
	private int _keywordid;
	private BaseModel<?> _keywordRemoteModel;
}