/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.KeywordDefsLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class KeywordDefsClp extends BaseModelImpl<KeywordDefs>
	implements KeywordDefs {
	public KeywordDefsClp() {
	}

	public Class<?> getModelClass() {
		return KeywordDefs.class;
	}

	public String getModelClassName() {
		return KeywordDefs.class.getName();
	}

	public int getPrimaryKey() {
		return _key_def_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setKey_def_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_key_def_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("key_def_id", getKey_def_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("disallownew", getDisallownew());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer key_def_id = (Integer)attributes.get("key_def_id");

		if (key_def_id != null) {
			setKey_def_id(key_def_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String disallownew = (String)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}
	}

	public int getKey_def_id() {
		return _key_def_id;
	}

	public void setKey_def_id(int key_def_id) {
		_key_def_id = key_def_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(String disallownew) {
		_disallownew = disallownew;
	}

	public BaseModel<?> getKeywordDefsRemoteModel() {
		return _keywordDefsRemoteModel;
	}

	public void setKeywordDefsRemoteModel(BaseModel<?> keywordDefsRemoteModel) {
		_keywordDefsRemoteModel = keywordDefsRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			KeywordDefsLocalServiceUtil.addKeywordDefs(this);
		}
		else {
			KeywordDefsLocalServiceUtil.updateKeywordDefs(this);
		}
	}

	@Override
	public KeywordDefs toEscapedModel() {
		return (KeywordDefs)Proxy.newProxyInstance(KeywordDefs.class.getClassLoader(),
			new Class[] { KeywordDefs.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		KeywordDefsClp clone = new KeywordDefsClp();

		clone.setKey_def_id(getKey_def_id());
		clone.setName(getName());
		clone.setDescription(getDescription());
		clone.setDisallownew(getDisallownew());

		return clone;
	}

	public int compareTo(KeywordDefs keywordDefs) {
		int primaryKey = keywordDefs.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		KeywordDefsClp keywordDefs = null;

		try {
			keywordDefs = (KeywordDefsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = keywordDefs.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{key_def_id=");
		sb.append(getKey_def_id());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", disallownew=");
		sb.append(getDisallownew());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.KeywordDefs");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>key_def_id</column-name><column-value><![CDATA[");
		sb.append(getKey_def_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disallownew</column-name><column-value><![CDATA[");
		sb.append(getDisallownew());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _key_def_id;
	private String _name;
	private String _description;
	private String _disallownew;
	private BaseModel<?> _keywordDefsRemoteModel;
}