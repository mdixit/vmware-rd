/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.KeywordDefsServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.KeywordDefsServiceSoap
 * @generated
 */
public class KeywordDefsSoap implements Serializable {
	public static KeywordDefsSoap toSoapModel(KeywordDefs model) {
		KeywordDefsSoap soapModel = new KeywordDefsSoap();

		soapModel.setKey_def_id(model.getKey_def_id());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());
		soapModel.setDisallownew(model.getDisallownew());

		return soapModel;
	}

	public static KeywordDefsSoap[] toSoapModels(KeywordDefs[] models) {
		KeywordDefsSoap[] soapModels = new KeywordDefsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static KeywordDefsSoap[][] toSoapModels(KeywordDefs[][] models) {
		KeywordDefsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new KeywordDefsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new KeywordDefsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static KeywordDefsSoap[] toSoapModels(List<KeywordDefs> models) {
		List<KeywordDefsSoap> soapModels = new ArrayList<KeywordDefsSoap>(models.size());

		for (KeywordDefs model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new KeywordDefsSoap[soapModels.size()]);
	}

	public KeywordDefsSoap() {
	}

	public int getPrimaryKey() {
		return _key_def_id;
	}

	public void setPrimaryKey(int pk) {
		setKey_def_id(pk);
	}

	public int getKey_def_id() {
		return _key_def_id;
	}

	public void setKey_def_id(int key_def_id) {
		_key_def_id = key_def_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(String disallownew) {
		_disallownew = disallownew;
	}

	private int _key_def_id;
	private String _name;
	private String _description;
	private String _disallownew;
}