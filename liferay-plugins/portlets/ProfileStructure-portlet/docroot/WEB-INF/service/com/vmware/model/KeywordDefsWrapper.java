/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link KeywordDefs}.
 * </p>
 *
 * @author    iscisc
 * @see       KeywordDefs
 * @generated
 */
public class KeywordDefsWrapper implements KeywordDefs,
	ModelWrapper<KeywordDefs> {
	public KeywordDefsWrapper(KeywordDefs keywordDefs) {
		_keywordDefs = keywordDefs;
	}

	public Class<?> getModelClass() {
		return KeywordDefs.class;
	}

	public String getModelClassName() {
		return KeywordDefs.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("key_def_id", getKey_def_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("disallownew", getDisallownew());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer key_def_id = (Integer)attributes.get("key_def_id");

		if (key_def_id != null) {
			setKey_def_id(key_def_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String disallownew = (String)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}
	}

	/**
	* Returns the primary key of this keyword defs.
	*
	* @return the primary key of this keyword defs
	*/
	public int getPrimaryKey() {
		return _keywordDefs.getPrimaryKey();
	}

	/**
	* Sets the primary key of this keyword defs.
	*
	* @param primaryKey the primary key of this keyword defs
	*/
	public void setPrimaryKey(int primaryKey) {
		_keywordDefs.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the key_def_id of this keyword defs.
	*
	* @return the key_def_id of this keyword defs
	*/
	public int getKey_def_id() {
		return _keywordDefs.getKey_def_id();
	}

	/**
	* Sets the key_def_id of this keyword defs.
	*
	* @param key_def_id the key_def_id of this keyword defs
	*/
	public void setKey_def_id(int key_def_id) {
		_keywordDefs.setKey_def_id(key_def_id);
	}

	/**
	* Returns the name of this keyword defs.
	*
	* @return the name of this keyword defs
	*/
	public java.lang.String getName() {
		return _keywordDefs.getName();
	}

	/**
	* Sets the name of this keyword defs.
	*
	* @param name the name of this keyword defs
	*/
	public void setName(java.lang.String name) {
		_keywordDefs.setName(name);
	}

	/**
	* Returns the description of this keyword defs.
	*
	* @return the description of this keyword defs
	*/
	public java.lang.String getDescription() {
		return _keywordDefs.getDescription();
	}

	/**
	* Sets the description of this keyword defs.
	*
	* @param description the description of this keyword defs
	*/
	public void setDescription(java.lang.String description) {
		_keywordDefs.setDescription(description);
	}

	/**
	* Returns the disallownew of this keyword defs.
	*
	* @return the disallownew of this keyword defs
	*/
	public java.lang.String getDisallownew() {
		return _keywordDefs.getDisallownew();
	}

	/**
	* Sets the disallownew of this keyword defs.
	*
	* @param disallownew the disallownew of this keyword defs
	*/
	public void setDisallownew(java.lang.String disallownew) {
		_keywordDefs.setDisallownew(disallownew);
	}

	public boolean isNew() {
		return _keywordDefs.isNew();
	}

	public void setNew(boolean n) {
		_keywordDefs.setNew(n);
	}

	public boolean isCachedModel() {
		return _keywordDefs.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_keywordDefs.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _keywordDefs.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _keywordDefs.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_keywordDefs.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _keywordDefs.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_keywordDefs.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new KeywordDefsWrapper((KeywordDefs)_keywordDefs.clone());
	}

	public int compareTo(com.vmware.model.KeywordDefs keywordDefs) {
		return _keywordDefs.compareTo(keywordDefs);
	}

	@Override
	public int hashCode() {
		return _keywordDefs.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.KeywordDefs> toCacheModel() {
		return _keywordDefs.toCacheModel();
	}

	public com.vmware.model.KeywordDefs toEscapedModel() {
		return new KeywordDefsWrapper(_keywordDefs.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _keywordDefs.toString();
	}

	public java.lang.String toXmlString() {
		return _keywordDefs.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_keywordDefs.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public KeywordDefs getWrappedKeywordDefs() {
		return _keywordDefs;
	}

	public KeywordDefs getWrappedModel() {
		return _keywordDefs;
	}

	public void resetOriginalValues() {
		_keywordDefs.resetOriginalValues();
	}

	private KeywordDefs _keywordDefs;
}