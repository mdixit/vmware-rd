/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.vmware.service.persistence.KeywordPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.KeywordServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.KeywordServiceSoap
 * @generated
 */
public class KeywordSoap implements Serializable {
	public static KeywordSoap toSoapModel(Keyword model) {
		KeywordSoap soapModel = new KeywordSoap();

		soapModel.setBug_id(model.getBug_id());
		soapModel.setKeywordid(model.getKeywordid());

		return soapModel;
	}

	public static KeywordSoap[] toSoapModels(Keyword[] models) {
		KeywordSoap[] soapModels = new KeywordSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static KeywordSoap[][] toSoapModels(Keyword[][] models) {
		KeywordSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new KeywordSoap[models.length][models[0].length];
		}
		else {
			soapModels = new KeywordSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static KeywordSoap[] toSoapModels(List<Keyword> models) {
		List<KeywordSoap> soapModels = new ArrayList<KeywordSoap>(models.size());

		for (Keyword model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new KeywordSoap[soapModels.size()]);
	}

	public KeywordSoap() {
	}

	public KeywordPK getPrimaryKey() {
		return new KeywordPK(_bug_id, _keywordid);
	}

	public void setPrimaryKey(KeywordPK pk) {
		setBug_id(pk.bug_id);
		setKeywordid(pk.keywordid);
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getKeywordid() {
		return _keywordid;
	}

	public void setKeywordid(int keywordid) {
		_keywordid = keywordid;
	}

	private int _bug_id;
	private int _keywordid;
}