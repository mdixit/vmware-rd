/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Keyword}.
 * </p>
 *
 * @author    iscisc
 * @see       Keyword
 * @generated
 */
public class KeywordWrapper implements Keyword, ModelWrapper<Keyword> {
	public KeywordWrapper(Keyword keyword) {
		_keyword = keyword;
	}

	public Class<?> getModelClass() {
		return Keyword.class;
	}

	public String getModelClassName() {
		return Keyword.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("keywordid", getKeywordid());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer keywordid = (Integer)attributes.get("keywordid");

		if (keywordid != null) {
			setKeywordid(keywordid);
		}
	}

	/**
	* Returns the primary key of this keyword.
	*
	* @return the primary key of this keyword
	*/
	public com.vmware.service.persistence.KeywordPK getPrimaryKey() {
		return _keyword.getPrimaryKey();
	}

	/**
	* Sets the primary key of this keyword.
	*
	* @param primaryKey the primary key of this keyword
	*/
	public void setPrimaryKey(
		com.vmware.service.persistence.KeywordPK primaryKey) {
		_keyword.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_id of this keyword.
	*
	* @return the bug_id of this keyword
	*/
	public int getBug_id() {
		return _keyword.getBug_id();
	}

	/**
	* Sets the bug_id of this keyword.
	*
	* @param bug_id the bug_id of this keyword
	*/
	public void setBug_id(int bug_id) {
		_keyword.setBug_id(bug_id);
	}

	/**
	* Returns the keywordid of this keyword.
	*
	* @return the keywordid of this keyword
	*/
	public int getKeywordid() {
		return _keyword.getKeywordid();
	}

	/**
	* Sets the keywordid of this keyword.
	*
	* @param keywordid the keywordid of this keyword
	*/
	public void setKeywordid(int keywordid) {
		_keyword.setKeywordid(keywordid);
	}

	public boolean isNew() {
		return _keyword.isNew();
	}

	public void setNew(boolean n) {
		_keyword.setNew(n);
	}

	public boolean isCachedModel() {
		return _keyword.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_keyword.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _keyword.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _keyword.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_keyword.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _keyword.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_keyword.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new KeywordWrapper((Keyword)_keyword.clone());
	}

	public int compareTo(com.vmware.model.Keyword keyword) {
		return _keyword.compareTo(keyword);
	}

	@Override
	public int hashCode() {
		return _keyword.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Keyword> toCacheModel() {
		return _keyword.toCacheModel();
	}

	public com.vmware.model.Keyword toEscapedModel() {
		return new KeywordWrapper(_keyword.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _keyword.toString();
	}

	public java.lang.String toXmlString() {
		return _keyword.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_keyword.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Keyword getWrappedKeyword() {
		return _keyword;
	}

	public Keyword getWrappedModel() {
		return _keyword;
	}

	public void resetOriginalValues() {
		_keyword.resetOriginalValues();
	}

	private Keyword _keyword;
}