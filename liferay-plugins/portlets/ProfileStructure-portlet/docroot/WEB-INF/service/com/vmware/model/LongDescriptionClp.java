/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.LongDescriptionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class LongDescriptionClp extends BaseModelImpl<LongDescription>
	implements LongDescription {
	public LongDescriptionClp() {
	}

	public Class<?> getModelClass() {
		return LongDescription.class;
	}

	public String getModelClassName() {
		return LongDescription.class.getName();
	}

	public int getPrimaryKey() {
		return _comment_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setComment_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_comment_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("comment_id", getComment_id());
		attributes.put("bug_id", getBug_id());
		attributes.put("who", getWho());
		attributes.put("bug_when", getBug_when());
		attributes.put("work_time", getWork_time());
		attributes.put("thetext", getThetext());
		attributes.put("isprivate", getIsprivate());
		attributes.put("already_wrapped", getAlready_wrapped());
		attributes.put("type", getType());
		attributes.put("extra_data", getExtra_data());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer comment_id = (Integer)attributes.get("comment_id");

		if (comment_id != null) {
			setComment_id(comment_id);
		}

		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Date bug_when = (Date)attributes.get("bug_when");

		if (bug_when != null) {
			setBug_when(bug_when);
		}

		Float work_time = (Float)attributes.get("work_time");

		if (work_time != null) {
			setWork_time(work_time);
		}

		String thetext = (String)attributes.get("thetext");

		if (thetext != null) {
			setThetext(thetext);
		}

		Integer isprivate = (Integer)attributes.get("isprivate");

		if (isprivate != null) {
			setIsprivate(isprivate);
		}

		Integer already_wrapped = (Integer)attributes.get("already_wrapped");

		if (already_wrapped != null) {
			setAlready_wrapped(already_wrapped);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String extra_data = (String)attributes.get("extra_data");

		if (extra_data != null) {
			setExtra_data(extra_data);
		}
	}

	public int getComment_id() {
		return _comment_id;
	}

	public void setComment_id(int comment_id) {
		_comment_id = comment_id;
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public Date getBug_when() {
		return _bug_when;
	}

	public void setBug_when(Date bug_when) {
		_bug_when = bug_when;
	}

	public Float getWork_time() {
		return _work_time;
	}

	public void setWork_time(Float work_time) {
		_work_time = work_time;
	}

	public String getThetext() {
		return _thetext;
	}

	public void setThetext(String thetext) {
		_thetext = thetext;
	}

	public int getIsprivate() {
		return _isprivate;
	}

	public void setIsprivate(int isprivate) {
		_isprivate = isprivate;
	}

	public int getAlready_wrapped() {
		return _already_wrapped;
	}

	public void setAlready_wrapped(int already_wrapped) {
		_already_wrapped = already_wrapped;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public String getExtra_data() {
		return _extra_data;
	}

	public void setExtra_data(String extra_data) {
		_extra_data = extra_data;
	}

	public BaseModel<?> getLongDescriptionRemoteModel() {
		return _longDescriptionRemoteModel;
	}

	public void setLongDescriptionRemoteModel(
		BaseModel<?> longDescriptionRemoteModel) {
		_longDescriptionRemoteModel = longDescriptionRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LongDescriptionLocalServiceUtil.addLongDescription(this);
		}
		else {
			LongDescriptionLocalServiceUtil.updateLongDescription(this);
		}
	}

	@Override
	public LongDescription toEscapedModel() {
		return (LongDescription)Proxy.newProxyInstance(LongDescription.class.getClassLoader(),
			new Class[] { LongDescription.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LongDescriptionClp clone = new LongDescriptionClp();

		clone.setComment_id(getComment_id());
		clone.setBug_id(getBug_id());
		clone.setWho(getWho());
		clone.setBug_when(getBug_when());
		clone.setWork_time(getWork_time());
		clone.setThetext(getThetext());
		clone.setIsprivate(getIsprivate());
		clone.setAlready_wrapped(getAlready_wrapped());
		clone.setType(getType());
		clone.setExtra_data(getExtra_data());

		return clone;
	}

	public int compareTo(LongDescription longDescription) {
		int value = 0;

		value = DateUtil.compareTo(getBug_when(), longDescription.getBug_when());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		LongDescriptionClp longDescription = null;

		try {
			longDescription = (LongDescriptionClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = longDescription.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{comment_id=");
		sb.append(getComment_id());
		sb.append(", bug_id=");
		sb.append(getBug_id());
		sb.append(", who=");
		sb.append(getWho());
		sb.append(", bug_when=");
		sb.append(getBug_when());
		sb.append(", work_time=");
		sb.append(getWork_time());
		sb.append(", thetext=");
		sb.append(getThetext());
		sb.append(", isprivate=");
		sb.append(getIsprivate());
		sb.append(", already_wrapped=");
		sb.append(getAlready_wrapped());
		sb.append(", type=");
		sb.append(getType());
		sb.append(", extra_data=");
		sb.append(getExtra_data());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.LongDescription");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>comment_id</column-name><column-value><![CDATA[");
		sb.append(getComment_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>who</column-name><column-value><![CDATA[");
		sb.append(getWho());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>bug_when</column-name><column-value><![CDATA[");
		sb.append(getBug_when());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>work_time</column-name><column-value><![CDATA[");
		sb.append(getWork_time());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>thetext</column-name><column-value><![CDATA[");
		sb.append(getThetext());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isprivate</column-name><column-value><![CDATA[");
		sb.append(getIsprivate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>already_wrapped</column-name><column-value><![CDATA[");
		sb.append(getAlready_wrapped());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>extra_data</column-name><column-value><![CDATA[");
		sb.append(getExtra_data());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _comment_id;
	private int _bug_id;
	private int _who;
	private Date _bug_when;
	private Float _work_time;
	private String _thetext;
	private int _isprivate;
	private int _already_wrapped;
	private int _type;
	private String _extra_data;
	private BaseModel<?> _longDescriptionRemoteModel;
}