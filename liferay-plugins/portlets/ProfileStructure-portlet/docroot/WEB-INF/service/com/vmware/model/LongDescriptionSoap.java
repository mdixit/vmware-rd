/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.LongDescriptionServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.LongDescriptionServiceSoap
 * @generated
 */
public class LongDescriptionSoap implements Serializable {
	public static LongDescriptionSoap toSoapModel(LongDescription model) {
		LongDescriptionSoap soapModel = new LongDescriptionSoap();

		soapModel.setComment_id(model.getComment_id());
		soapModel.setBug_id(model.getBug_id());
		soapModel.setWho(model.getWho());
		soapModel.setBug_when(model.getBug_when());
		soapModel.setWork_time(model.getWork_time());
		soapModel.setThetext(model.getThetext());
		soapModel.setIsprivate(model.getIsprivate());
		soapModel.setAlready_wrapped(model.getAlready_wrapped());
		soapModel.setType(model.getType());
		soapModel.setExtra_data(model.getExtra_data());

		return soapModel;
	}

	public static LongDescriptionSoap[] toSoapModels(LongDescription[] models) {
		LongDescriptionSoap[] soapModels = new LongDescriptionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LongDescriptionSoap[][] toSoapModels(
		LongDescription[][] models) {
		LongDescriptionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LongDescriptionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LongDescriptionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LongDescriptionSoap[] toSoapModels(
		List<LongDescription> models) {
		List<LongDescriptionSoap> soapModels = new ArrayList<LongDescriptionSoap>(models.size());

		for (LongDescription model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LongDescriptionSoap[soapModels.size()]);
	}

	public LongDescriptionSoap() {
	}

	public int getPrimaryKey() {
		return _comment_id;
	}

	public void setPrimaryKey(int pk) {
		setComment_id(pk);
	}

	public int getComment_id() {
		return _comment_id;
	}

	public void setComment_id(int comment_id) {
		_comment_id = comment_id;
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public Date getBug_when() {
		return _bug_when;
	}

	public void setBug_when(Date bug_when) {
		_bug_when = bug_when;
	}

	public Float getWork_time() {
		return _work_time;
	}

	public void setWork_time(Float work_time) {
		_work_time = work_time;
	}

	public String getThetext() {
		return _thetext;
	}

	public void setThetext(String thetext) {
		_thetext = thetext;
	}

	public int getIsprivate() {
		return _isprivate;
	}

	public void setIsprivate(int isprivate) {
		_isprivate = isprivate;
	}

	public int getAlready_wrapped() {
		return _already_wrapped;
	}

	public void setAlready_wrapped(int already_wrapped) {
		_already_wrapped = already_wrapped;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public String getExtra_data() {
		return _extra_data;
	}

	public void setExtra_data(String extra_data) {
		_extra_data = extra_data;
	}

	private int _comment_id;
	private int _bug_id;
	private int _who;
	private Date _bug_when;
	private Float _work_time;
	private String _thetext;
	private int _isprivate;
	private int _already_wrapped;
	private int _type;
	private String _extra_data;
}