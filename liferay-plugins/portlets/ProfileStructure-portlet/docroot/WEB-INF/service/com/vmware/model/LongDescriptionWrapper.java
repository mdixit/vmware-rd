/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LongDescription}.
 * </p>
 *
 * @author    iscisc
 * @see       LongDescription
 * @generated
 */
public class LongDescriptionWrapper implements LongDescription,
	ModelWrapper<LongDescription> {
	public LongDescriptionWrapper(LongDescription longDescription) {
		_longDescription = longDescription;
	}

	public Class<?> getModelClass() {
		return LongDescription.class;
	}

	public String getModelClassName() {
		return LongDescription.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("comment_id", getComment_id());
		attributes.put("bug_id", getBug_id());
		attributes.put("who", getWho());
		attributes.put("bug_when", getBug_when());
		attributes.put("work_time", getWork_time());
		attributes.put("thetext", getThetext());
		attributes.put("isprivate", getIsprivate());
		attributes.put("already_wrapped", getAlready_wrapped());
		attributes.put("type", getType());
		attributes.put("extra_data", getExtra_data());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer comment_id = (Integer)attributes.get("comment_id");

		if (comment_id != null) {
			setComment_id(comment_id);
		}

		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Date bug_when = (Date)attributes.get("bug_when");

		if (bug_when != null) {
			setBug_when(bug_when);
		}

		Float work_time = (Float)attributes.get("work_time");

		if (work_time != null) {
			setWork_time(work_time);
		}

		String thetext = (String)attributes.get("thetext");

		if (thetext != null) {
			setThetext(thetext);
		}

		Integer isprivate = (Integer)attributes.get("isprivate");

		if (isprivate != null) {
			setIsprivate(isprivate);
		}

		Integer already_wrapped = (Integer)attributes.get("already_wrapped");

		if (already_wrapped != null) {
			setAlready_wrapped(already_wrapped);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String extra_data = (String)attributes.get("extra_data");

		if (extra_data != null) {
			setExtra_data(extra_data);
		}
	}

	/**
	* Returns the primary key of this long description.
	*
	* @return the primary key of this long description
	*/
	public int getPrimaryKey() {
		return _longDescription.getPrimaryKey();
	}

	/**
	* Sets the primary key of this long description.
	*
	* @param primaryKey the primary key of this long description
	*/
	public void setPrimaryKey(int primaryKey) {
		_longDescription.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the comment_id of this long description.
	*
	* @return the comment_id of this long description
	*/
	public int getComment_id() {
		return _longDescription.getComment_id();
	}

	/**
	* Sets the comment_id of this long description.
	*
	* @param comment_id the comment_id of this long description
	*/
	public void setComment_id(int comment_id) {
		_longDescription.setComment_id(comment_id);
	}

	/**
	* Returns the bug_id of this long description.
	*
	* @return the bug_id of this long description
	*/
	public int getBug_id() {
		return _longDescription.getBug_id();
	}

	/**
	* Sets the bug_id of this long description.
	*
	* @param bug_id the bug_id of this long description
	*/
	public void setBug_id(int bug_id) {
		_longDescription.setBug_id(bug_id);
	}

	/**
	* Returns the who of this long description.
	*
	* @return the who of this long description
	*/
	public int getWho() {
		return _longDescription.getWho();
	}

	/**
	* Sets the who of this long description.
	*
	* @param who the who of this long description
	*/
	public void setWho(int who) {
		_longDescription.setWho(who);
	}

	/**
	* Returns the bug_when of this long description.
	*
	* @return the bug_when of this long description
	*/
	public java.util.Date getBug_when() {
		return _longDescription.getBug_when();
	}

	/**
	* Sets the bug_when of this long description.
	*
	* @param bug_when the bug_when of this long description
	*/
	public void setBug_when(java.util.Date bug_when) {
		_longDescription.setBug_when(bug_when);
	}

	/**
	* Returns the work_time of this long description.
	*
	* @return the work_time of this long description
	*/
	public java.lang.Float getWork_time() {
		return _longDescription.getWork_time();
	}

	/**
	* Sets the work_time of this long description.
	*
	* @param work_time the work_time of this long description
	*/
	public void setWork_time(java.lang.Float work_time) {
		_longDescription.setWork_time(work_time);
	}

	/**
	* Returns the thetext of this long description.
	*
	* @return the thetext of this long description
	*/
	public java.lang.String getThetext() {
		return _longDescription.getThetext();
	}

	/**
	* Sets the thetext of this long description.
	*
	* @param thetext the thetext of this long description
	*/
	public void setThetext(java.lang.String thetext) {
		_longDescription.setThetext(thetext);
	}

	/**
	* Returns the isprivate of this long description.
	*
	* @return the isprivate of this long description
	*/
	public int getIsprivate() {
		return _longDescription.getIsprivate();
	}

	/**
	* Sets the isprivate of this long description.
	*
	* @param isprivate the isprivate of this long description
	*/
	public void setIsprivate(int isprivate) {
		_longDescription.setIsprivate(isprivate);
	}

	/**
	* Returns the already_wrapped of this long description.
	*
	* @return the already_wrapped of this long description
	*/
	public int getAlready_wrapped() {
		return _longDescription.getAlready_wrapped();
	}

	/**
	* Sets the already_wrapped of this long description.
	*
	* @param already_wrapped the already_wrapped of this long description
	*/
	public void setAlready_wrapped(int already_wrapped) {
		_longDescription.setAlready_wrapped(already_wrapped);
	}

	/**
	* Returns the type of this long description.
	*
	* @return the type of this long description
	*/
	public int getType() {
		return _longDescription.getType();
	}

	/**
	* Sets the type of this long description.
	*
	* @param type the type of this long description
	*/
	public void setType(int type) {
		_longDescription.setType(type);
	}

	/**
	* Returns the extra_data of this long description.
	*
	* @return the extra_data of this long description
	*/
	public java.lang.String getExtra_data() {
		return _longDescription.getExtra_data();
	}

	/**
	* Sets the extra_data of this long description.
	*
	* @param extra_data the extra_data of this long description
	*/
	public void setExtra_data(java.lang.String extra_data) {
		_longDescription.setExtra_data(extra_data);
	}

	public boolean isNew() {
		return _longDescription.isNew();
	}

	public void setNew(boolean n) {
		_longDescription.setNew(n);
	}

	public boolean isCachedModel() {
		return _longDescription.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_longDescription.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _longDescription.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _longDescription.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_longDescription.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _longDescription.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_longDescription.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LongDescriptionWrapper((LongDescription)_longDescription.clone());
	}

	public int compareTo(com.vmware.model.LongDescription longDescription) {
		return _longDescription.compareTo(longDescription);
	}

	@Override
	public int hashCode() {
		return _longDescription.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.LongDescription> toCacheModel() {
		return _longDescription.toCacheModel();
	}

	public com.vmware.model.LongDescription toEscapedModel() {
		return new LongDescriptionWrapper(_longDescription.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _longDescription.toString();
	}

	public java.lang.String toXmlString() {
		return _longDescription.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_longDescription.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LongDescription getWrappedLongDescription() {
		return _longDescription;
	}

	public LongDescription getWrappedModel() {
		return _longDescription;
	}

	public void resetOriginalValues() {
		_longDescription.resetOriginalValues();
	}

	private LongDescription _longDescription;
}