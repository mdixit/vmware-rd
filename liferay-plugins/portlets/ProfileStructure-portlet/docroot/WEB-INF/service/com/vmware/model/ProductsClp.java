/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.ProductsLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class ProductsClp extends BaseModelImpl<Products> implements Products {
	public ProductsClp() {
	}

	public Class<?> getModelClass() {
		return Products.class;
	}

	public String getModelClassName() {
		return Products.class.getName();
	}

	public int getPrimaryKey() {
		return _prod_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setProd_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_prod_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("prod_id", getProd_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("template", getTemplate());
		attributes.put("sortkey", getSortkey());
		attributes.put("defaultcategory", getDefaultcategory());
		attributes.put("disallownew", getDisallownew());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer prod_id = (Integer)attributes.get("prod_id");

		if (prod_id != null) {
			setProd_id(prod_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String template = (String)attributes.get("template");

		if (template != null) {
			setTemplate(template);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer defaultcategory = (Integer)attributes.get("defaultcategory");

		if (defaultcategory != null) {
			setDefaultcategory(defaultcategory);
		}

		Integer disallownew = (Integer)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}
	}

	public int getProd_id() {
		return _prod_id;
	}

	public void setProd_id(int prod_id) {
		_prod_id = prod_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getTemplate() {
		return _template;
	}

	public void setTemplate(String template) {
		_template = template;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getDefaultcategory() {
		return _defaultcategory;
	}

	public void setDefaultcategory(int defaultcategory) {
		_defaultcategory = defaultcategory;
	}

	public int getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(int disallownew) {
		_disallownew = disallownew;
	}

	public BaseModel<?> getProductsRemoteModel() {
		return _productsRemoteModel;
	}

	public void setProductsRemoteModel(BaseModel<?> productsRemoteModel) {
		_productsRemoteModel = productsRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ProductsLocalServiceUtil.addProducts(this);
		}
		else {
			ProductsLocalServiceUtil.updateProducts(this);
		}
	}

	@Override
	public Products toEscapedModel() {
		return (Products)Proxy.newProxyInstance(Products.class.getClassLoader(),
			new Class[] { Products.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ProductsClp clone = new ProductsClp();

		clone.setProd_id(getProd_id());
		clone.setName(getName());
		clone.setDescription(getDescription());
		clone.setTemplate(getTemplate());
		clone.setSortkey(getSortkey());
		clone.setDefaultcategory(getDefaultcategory());
		clone.setDisallownew(getDisallownew());

		return clone;
	}

	public int compareTo(Products products) {
		int primaryKey = products.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ProductsClp products = null;

		try {
			products = (ProductsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = products.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{prod_id=");
		sb.append(getProd_id());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", template=");
		sb.append(getTemplate());
		sb.append(", sortkey=");
		sb.append(getSortkey());
		sb.append(", defaultcategory=");
		sb.append(getDefaultcategory());
		sb.append(", disallownew=");
		sb.append(getDisallownew());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Products");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>prod_id</column-name><column-value><![CDATA[");
		sb.append(getProd_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>template</column-name><column-value><![CDATA[");
		sb.append(getTemplate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sortkey</column-name><column-value><![CDATA[");
		sb.append(getSortkey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>defaultcategory</column-name><column-value><![CDATA[");
		sb.append(getDefaultcategory());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disallownew</column-name><column-value><![CDATA[");
		sb.append(getDisallownew());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _prod_id;
	private String _name;
	private String _description;
	private String _template;
	private int _sortkey;
	private int _defaultcategory;
	private int _disallownew;
	private BaseModel<?> _productsRemoteModel;
}