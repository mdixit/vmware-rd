/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.ProductsServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.ProductsServiceSoap
 * @generated
 */
public class ProductsSoap implements Serializable {
	public static ProductsSoap toSoapModel(Products model) {
		ProductsSoap soapModel = new ProductsSoap();

		soapModel.setProd_id(model.getProd_id());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());
		soapModel.setTemplate(model.getTemplate());
		soapModel.setSortkey(model.getSortkey());
		soapModel.setDefaultcategory(model.getDefaultcategory());
		soapModel.setDisallownew(model.getDisallownew());

		return soapModel;
	}

	public static ProductsSoap[] toSoapModels(Products[] models) {
		ProductsSoap[] soapModels = new ProductsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProductsSoap[][] toSoapModels(Products[][] models) {
		ProductsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProductsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProductsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProductsSoap[] toSoapModels(List<Products> models) {
		List<ProductsSoap> soapModels = new ArrayList<ProductsSoap>(models.size());

		for (Products model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProductsSoap[soapModels.size()]);
	}

	public ProductsSoap() {
	}

	public int getPrimaryKey() {
		return _prod_id;
	}

	public void setPrimaryKey(int pk) {
		setProd_id(pk);
	}

	public int getProd_id() {
		return _prod_id;
	}

	public void setProd_id(int prod_id) {
		_prod_id = prod_id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getTemplate() {
		return _template;
	}

	public void setTemplate(String template) {
		_template = template;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getDefaultcategory() {
		return _defaultcategory;
	}

	public void setDefaultcategory(int defaultcategory) {
		_defaultcategory = defaultcategory;
	}

	public int getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(int disallownew) {
		_disallownew = disallownew;
	}

	private int _prod_id;
	private String _name;
	private String _description;
	private String _template;
	private int _sortkey;
	private int _defaultcategory;
	private int _disallownew;
}