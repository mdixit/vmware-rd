/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Products}.
 * </p>
 *
 * @author    iscisc
 * @see       Products
 * @generated
 */
public class ProductsWrapper implements Products, ModelWrapper<Products> {
	public ProductsWrapper(Products products) {
		_products = products;
	}

	public Class<?> getModelClass() {
		return Products.class;
	}

	public String getModelClassName() {
		return Products.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("prod_id", getProd_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("template", getTemplate());
		attributes.put("sortkey", getSortkey());
		attributes.put("defaultcategory", getDefaultcategory());
		attributes.put("disallownew", getDisallownew());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer prod_id = (Integer)attributes.get("prod_id");

		if (prod_id != null) {
			setProd_id(prod_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String template = (String)attributes.get("template");

		if (template != null) {
			setTemplate(template);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer defaultcategory = (Integer)attributes.get("defaultcategory");

		if (defaultcategory != null) {
			setDefaultcategory(defaultcategory);
		}

		Integer disallownew = (Integer)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}
	}

	/**
	* Returns the primary key of this products.
	*
	* @return the primary key of this products
	*/
	public int getPrimaryKey() {
		return _products.getPrimaryKey();
	}

	/**
	* Sets the primary key of this products.
	*
	* @param primaryKey the primary key of this products
	*/
	public void setPrimaryKey(int primaryKey) {
		_products.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the prod_id of this products.
	*
	* @return the prod_id of this products
	*/
	public int getProd_id() {
		return _products.getProd_id();
	}

	/**
	* Sets the prod_id of this products.
	*
	* @param prod_id the prod_id of this products
	*/
	public void setProd_id(int prod_id) {
		_products.setProd_id(prod_id);
	}

	/**
	* Returns the name of this products.
	*
	* @return the name of this products
	*/
	public java.lang.String getName() {
		return _products.getName();
	}

	/**
	* Sets the name of this products.
	*
	* @param name the name of this products
	*/
	public void setName(java.lang.String name) {
		_products.setName(name);
	}

	/**
	* Returns the description of this products.
	*
	* @return the description of this products
	*/
	public java.lang.String getDescription() {
		return _products.getDescription();
	}

	/**
	* Sets the description of this products.
	*
	* @param description the description of this products
	*/
	public void setDescription(java.lang.String description) {
		_products.setDescription(description);
	}

	/**
	* Returns the template of this products.
	*
	* @return the template of this products
	*/
	public java.lang.String getTemplate() {
		return _products.getTemplate();
	}

	/**
	* Sets the template of this products.
	*
	* @param template the template of this products
	*/
	public void setTemplate(java.lang.String template) {
		_products.setTemplate(template);
	}

	/**
	* Returns the sortkey of this products.
	*
	* @return the sortkey of this products
	*/
	public int getSortkey() {
		return _products.getSortkey();
	}

	/**
	* Sets the sortkey of this products.
	*
	* @param sortkey the sortkey of this products
	*/
	public void setSortkey(int sortkey) {
		_products.setSortkey(sortkey);
	}

	/**
	* Returns the defaultcategory of this products.
	*
	* @return the defaultcategory of this products
	*/
	public int getDefaultcategory() {
		return _products.getDefaultcategory();
	}

	/**
	* Sets the defaultcategory of this products.
	*
	* @param defaultcategory the defaultcategory of this products
	*/
	public void setDefaultcategory(int defaultcategory) {
		_products.setDefaultcategory(defaultcategory);
	}

	/**
	* Returns the disallownew of this products.
	*
	* @return the disallownew of this products
	*/
	public int getDisallownew() {
		return _products.getDisallownew();
	}

	/**
	* Sets the disallownew of this products.
	*
	* @param disallownew the disallownew of this products
	*/
	public void setDisallownew(int disallownew) {
		_products.setDisallownew(disallownew);
	}

	public boolean isNew() {
		return _products.isNew();
	}

	public void setNew(boolean n) {
		_products.setNew(n);
	}

	public boolean isCachedModel() {
		return _products.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_products.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _products.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _products.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_products.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _products.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_products.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProductsWrapper((Products)_products.clone());
	}

	public int compareTo(com.vmware.model.Products products) {
		return _products.compareTo(products);
	}

	@Override
	public int hashCode() {
		return _products.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Products> toCacheModel() {
		return _products.toCacheModel();
	}

	public com.vmware.model.Products toEscapedModel() {
		return new ProductsWrapper(_products.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _products.toString();
	}

	public java.lang.String toXmlString() {
		return _products.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_products.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Products getWrappedProducts() {
		return _products;
	}

	public Products getWrappedModel() {
		return _products;
	}

	public void resetOriginalValues() {
		_products.resetOriginalValues();
	}

	private Products _products;
}