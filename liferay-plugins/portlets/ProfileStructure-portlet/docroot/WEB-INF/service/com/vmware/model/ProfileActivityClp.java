/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.ProfileActivityLocalServiceUtil;
import com.vmware.service.persistence.ProfileActivityPK;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class ProfileActivityClp extends BaseModelImpl<ProfileActivity>
	implements ProfileActivity {
	public ProfileActivityClp() {
	}

	public Class<?> getModelClass() {
		return ProfileActivity.class;
	}

	public String getModelClassName() {
		return ProfileActivity.class.getName();
	}

	public ProfileActivityPK getPrimaryKey() {
		return new ProfileActivityPK(_userid, _who);
	}

	public void setPrimaryKey(ProfileActivityPK primaryKey) {
		setUserid(primaryKey.userid);
		setWho(primaryKey.who);
	}

	public Serializable getPrimaryKeyObj() {
		return new ProfileActivityPK(_userid, _who);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((ProfileActivityPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userid", getUserid());
		attributes.put("who", getWho());
		attributes.put("profiles_when", getProfiles_when());
		attributes.put("fieldid", getFieldid());
		attributes.put("oldvalue", getOldvalue());
		attributes.put("newvalue", getNewvalue());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer userid = (Integer)attributes.get("userid");

		if (userid != null) {
			setUserid(userid);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Date profiles_when = (Date)attributes.get("profiles_when");

		if (profiles_when != null) {
			setProfiles_when(profiles_when);
		}

		Integer fieldid = (Integer)attributes.get("fieldid");

		if (fieldid != null) {
			setFieldid(fieldid);
		}

		String oldvalue = (String)attributes.get("oldvalue");

		if (oldvalue != null) {
			setOldvalue(oldvalue);
		}

		String newvalue = (String)attributes.get("newvalue");

		if (newvalue != null) {
			setNewvalue(newvalue);
		}
	}

	public int getUserid() {
		return _userid;
	}

	public void setUserid(int userid) {
		_userid = userid;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public Date getProfiles_when() {
		return _profiles_when;
	}

	public void setProfiles_when(Date profiles_when) {
		_profiles_when = profiles_when;
	}

	public int getFieldid() {
		return _fieldid;
	}

	public void setFieldid(int fieldid) {
		_fieldid = fieldid;
	}

	public String getOldvalue() {
		return _oldvalue;
	}

	public void setOldvalue(String oldvalue) {
		_oldvalue = oldvalue;
	}

	public String getNewvalue() {
		return _newvalue;
	}

	public void setNewvalue(String newvalue) {
		_newvalue = newvalue;
	}

	public BaseModel<?> getProfileActivityRemoteModel() {
		return _profileActivityRemoteModel;
	}

	public void setProfileActivityRemoteModel(
		BaseModel<?> profileActivityRemoteModel) {
		_profileActivityRemoteModel = profileActivityRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ProfileActivityLocalServiceUtil.addProfileActivity(this);
		}
		else {
			ProfileActivityLocalServiceUtil.updateProfileActivity(this);
		}
	}

	@Override
	public ProfileActivity toEscapedModel() {
		return (ProfileActivity)Proxy.newProxyInstance(ProfileActivity.class.getClassLoader(),
			new Class[] { ProfileActivity.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ProfileActivityClp clone = new ProfileActivityClp();

		clone.setUserid(getUserid());
		clone.setWho(getWho());
		clone.setProfiles_when(getProfiles_when());
		clone.setFieldid(getFieldid());
		clone.setOldvalue(getOldvalue());
		clone.setNewvalue(getNewvalue());

		return clone;
	}

	public int compareTo(ProfileActivity profileActivity) {
		int value = 0;

		value = DateUtil.compareTo(getProfiles_when(),
				profileActivity.getProfiles_when());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ProfileActivityClp profileActivity = null;

		try {
			profileActivity = (ProfileActivityClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		ProfileActivityPK primaryKey = profileActivity.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{userid=");
		sb.append(getUserid());
		sb.append(", who=");
		sb.append(getWho());
		sb.append(", profiles_when=");
		sb.append(getProfiles_when());
		sb.append(", fieldid=");
		sb.append(getFieldid());
		sb.append(", oldvalue=");
		sb.append(getOldvalue());
		sb.append(", newvalue=");
		sb.append(getNewvalue());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.ProfileActivity");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>userid</column-name><column-value><![CDATA[");
		sb.append(getUserid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>who</column-name><column-value><![CDATA[");
		sb.append(getWho());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>profiles_when</column-name><column-value><![CDATA[");
		sb.append(getProfiles_when());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fieldid</column-name><column-value><![CDATA[");
		sb.append(getFieldid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>oldvalue</column-name><column-value><![CDATA[");
		sb.append(getOldvalue());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>newvalue</column-name><column-value><![CDATA[");
		sb.append(getNewvalue());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _userid;
	private int _who;
	private Date _profiles_when;
	private int _fieldid;
	private String _oldvalue;
	private String _newvalue;
	private BaseModel<?> _profileActivityRemoteModel;
}