/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.vmware.service.persistence.ProfileActivityPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.ProfileActivityServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.ProfileActivityServiceSoap
 * @generated
 */
public class ProfileActivitySoap implements Serializable {
	public static ProfileActivitySoap toSoapModel(ProfileActivity model) {
		ProfileActivitySoap soapModel = new ProfileActivitySoap();

		soapModel.setUserid(model.getUserid());
		soapModel.setWho(model.getWho());
		soapModel.setProfiles_when(model.getProfiles_when());
		soapModel.setFieldid(model.getFieldid());
		soapModel.setOldvalue(model.getOldvalue());
		soapModel.setNewvalue(model.getNewvalue());

		return soapModel;
	}

	public static ProfileActivitySoap[] toSoapModels(ProfileActivity[] models) {
		ProfileActivitySoap[] soapModels = new ProfileActivitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProfileActivitySoap[][] toSoapModels(
		ProfileActivity[][] models) {
		ProfileActivitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProfileActivitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProfileActivitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProfileActivitySoap[] toSoapModels(
		List<ProfileActivity> models) {
		List<ProfileActivitySoap> soapModels = new ArrayList<ProfileActivitySoap>(models.size());

		for (ProfileActivity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProfileActivitySoap[soapModels.size()]);
	}

	public ProfileActivitySoap() {
	}

	public ProfileActivityPK getPrimaryKey() {
		return new ProfileActivityPK(_userid, _who);
	}

	public void setPrimaryKey(ProfileActivityPK pk) {
		setUserid(pk.userid);
		setWho(pk.who);
	}

	public int getUserid() {
		return _userid;
	}

	public void setUserid(int userid) {
		_userid = userid;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public Date getProfiles_when() {
		return _profiles_when;
	}

	public void setProfiles_when(Date profiles_when) {
		_profiles_when = profiles_when;
	}

	public int getFieldid() {
		return _fieldid;
	}

	public void setFieldid(int fieldid) {
		_fieldid = fieldid;
	}

	public String getOldvalue() {
		return _oldvalue;
	}

	public void setOldvalue(String oldvalue) {
		_oldvalue = oldvalue;
	}

	public String getNewvalue() {
		return _newvalue;
	}

	public void setNewvalue(String newvalue) {
		_newvalue = newvalue;
	}

	private int _userid;
	private int _who;
	private Date _profiles_when;
	private int _fieldid;
	private String _oldvalue;
	private String _newvalue;
}