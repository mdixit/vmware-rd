/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ProfileActivity}.
 * </p>
 *
 * @author    iscisc
 * @see       ProfileActivity
 * @generated
 */
public class ProfileActivityWrapper implements ProfileActivity,
	ModelWrapper<ProfileActivity> {
	public ProfileActivityWrapper(ProfileActivity profileActivity) {
		_profileActivity = profileActivity;
	}

	public Class<?> getModelClass() {
		return ProfileActivity.class;
	}

	public String getModelClassName() {
		return ProfileActivity.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userid", getUserid());
		attributes.put("who", getWho());
		attributes.put("profiles_when", getProfiles_when());
		attributes.put("fieldid", getFieldid());
		attributes.put("oldvalue", getOldvalue());
		attributes.put("newvalue", getNewvalue());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer userid = (Integer)attributes.get("userid");

		if (userid != null) {
			setUserid(userid);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Date profiles_when = (Date)attributes.get("profiles_when");

		if (profiles_when != null) {
			setProfiles_when(profiles_when);
		}

		Integer fieldid = (Integer)attributes.get("fieldid");

		if (fieldid != null) {
			setFieldid(fieldid);
		}

		String oldvalue = (String)attributes.get("oldvalue");

		if (oldvalue != null) {
			setOldvalue(oldvalue);
		}

		String newvalue = (String)attributes.get("newvalue");

		if (newvalue != null) {
			setNewvalue(newvalue);
		}
	}

	/**
	* Returns the primary key of this profile activity.
	*
	* @return the primary key of this profile activity
	*/
	public com.vmware.service.persistence.ProfileActivityPK getPrimaryKey() {
		return _profileActivity.getPrimaryKey();
	}

	/**
	* Sets the primary key of this profile activity.
	*
	* @param primaryKey the primary key of this profile activity
	*/
	public void setPrimaryKey(
		com.vmware.service.persistence.ProfileActivityPK primaryKey) {
		_profileActivity.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the userid of this profile activity.
	*
	* @return the userid of this profile activity
	*/
	public int getUserid() {
		return _profileActivity.getUserid();
	}

	/**
	* Sets the userid of this profile activity.
	*
	* @param userid the userid of this profile activity
	*/
	public void setUserid(int userid) {
		_profileActivity.setUserid(userid);
	}

	/**
	* Returns the who of this profile activity.
	*
	* @return the who of this profile activity
	*/
	public int getWho() {
		return _profileActivity.getWho();
	}

	/**
	* Sets the who of this profile activity.
	*
	* @param who the who of this profile activity
	*/
	public void setWho(int who) {
		_profileActivity.setWho(who);
	}

	/**
	* Returns the profiles_when of this profile activity.
	*
	* @return the profiles_when of this profile activity
	*/
	public java.util.Date getProfiles_when() {
		return _profileActivity.getProfiles_when();
	}

	/**
	* Sets the profiles_when of this profile activity.
	*
	* @param profiles_when the profiles_when of this profile activity
	*/
	public void setProfiles_when(java.util.Date profiles_when) {
		_profileActivity.setProfiles_when(profiles_when);
	}

	/**
	* Returns the fieldid of this profile activity.
	*
	* @return the fieldid of this profile activity
	*/
	public int getFieldid() {
		return _profileActivity.getFieldid();
	}

	/**
	* Sets the fieldid of this profile activity.
	*
	* @param fieldid the fieldid of this profile activity
	*/
	public void setFieldid(int fieldid) {
		_profileActivity.setFieldid(fieldid);
	}

	/**
	* Returns the oldvalue of this profile activity.
	*
	* @return the oldvalue of this profile activity
	*/
	public java.lang.String getOldvalue() {
		return _profileActivity.getOldvalue();
	}

	/**
	* Sets the oldvalue of this profile activity.
	*
	* @param oldvalue the oldvalue of this profile activity
	*/
	public void setOldvalue(java.lang.String oldvalue) {
		_profileActivity.setOldvalue(oldvalue);
	}

	/**
	* Returns the newvalue of this profile activity.
	*
	* @return the newvalue of this profile activity
	*/
	public java.lang.String getNewvalue() {
		return _profileActivity.getNewvalue();
	}

	/**
	* Sets the newvalue of this profile activity.
	*
	* @param newvalue the newvalue of this profile activity
	*/
	public void setNewvalue(java.lang.String newvalue) {
		_profileActivity.setNewvalue(newvalue);
	}

	public boolean isNew() {
		return _profileActivity.isNew();
	}

	public void setNew(boolean n) {
		_profileActivity.setNew(n);
	}

	public boolean isCachedModel() {
		return _profileActivity.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_profileActivity.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _profileActivity.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _profileActivity.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_profileActivity.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _profileActivity.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_profileActivity.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProfileActivityWrapper((ProfileActivity)_profileActivity.clone());
	}

	public int compareTo(com.vmware.model.ProfileActivity profileActivity) {
		return _profileActivity.compareTo(profileActivity);
	}

	@Override
	public int hashCode() {
		return _profileActivity.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.ProfileActivity> toCacheModel() {
		return _profileActivity.toCacheModel();
	}

	public com.vmware.model.ProfileActivity toEscapedModel() {
		return new ProfileActivityWrapper(_profileActivity.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _profileActivity.toString();
	}

	public java.lang.String toXmlString() {
		return _profileActivity.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_profileActivity.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public ProfileActivity getWrappedProfileActivity() {
		return _profileActivity;
	}

	public ProfileActivity getWrappedModel() {
		return _profileActivity;
	}

	public void resetOriginalValues() {
		_profileActivity.resetOriginalValues();
	}

	private ProfileActivity _profileActivity;
}