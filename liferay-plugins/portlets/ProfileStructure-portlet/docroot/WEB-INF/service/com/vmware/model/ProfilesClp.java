/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.ProfilesLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class ProfilesClp extends BaseModelImpl<Profiles> implements Profiles {
	public ProfilesClp() {
	}

	public Class<?> getModelClass() {
		return Profiles.class;
	}

	public String getModelClassName() {
		return Profiles.class.getName();
	}

	public int getPrimaryKey() {
		return _userid;
	}

	public void setPrimaryKey(int primaryKey) {
		setUserid(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_userid);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userid", getUserid());
		attributes.put("login_name", getLogin_name());
		attributes.put("all_login_names", getAll_login_names());
		attributes.put("cryptpassword", getCryptpassword());
		attributes.put("realname", getRealname());
		attributes.put("disabledtext", getDisabledtext());
		attributes.put("disable_mail", getDisable_mail());
		attributes.put("mybugslink", getMybugslink());
		attributes.put("manager_id", getManager_id());
		attributes.put("extern_id", getExtern_id());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer userid = (Integer)attributes.get("userid");

		if (userid != null) {
			setUserid(userid);
		}

		String login_name = (String)attributes.get("login_name");

		if (login_name != null) {
			setLogin_name(login_name);
		}

		String all_login_names = (String)attributes.get("all_login_names");

		if (all_login_names != null) {
			setAll_login_names(all_login_names);
		}

		String cryptpassword = (String)attributes.get("cryptpassword");

		if (cryptpassword != null) {
			setCryptpassword(cryptpassword);
		}

		String realname = (String)attributes.get("realname");

		if (realname != null) {
			setRealname(realname);
		}

		String disabledtext = (String)attributes.get("disabledtext");

		if (disabledtext != null) {
			setDisabledtext(disabledtext);
		}

		Integer disable_mail = (Integer)attributes.get("disable_mail");

		if (disable_mail != null) {
			setDisable_mail(disable_mail);
		}

		Integer mybugslink = (Integer)attributes.get("mybugslink");

		if (mybugslink != null) {
			setMybugslink(mybugslink);
		}

		Integer manager_id = (Integer)attributes.get("manager_id");

		if (manager_id != null) {
			setManager_id(manager_id);
		}

		String extern_id = (String)attributes.get("extern_id");

		if (extern_id != null) {
			setExtern_id(extern_id);
		}
	}

	public int getUserid() {
		return _userid;
	}

	public void setUserid(int userid) {
		_userid = userid;
	}

	public String getLogin_name() {
		return _login_name;
	}

	public void setLogin_name(String login_name) {
		_login_name = login_name;
	}

	public String getAll_login_names() {
		return _all_login_names;
	}

	public void setAll_login_names(String all_login_names) {
		_all_login_names = all_login_names;
	}

	public String getCryptpassword() {
		return _cryptpassword;
	}

	public void setCryptpassword(String cryptpassword) {
		_cryptpassword = cryptpassword;
	}

	public String getRealname() {
		return _realname;
	}

	public void setRealname(String realname) {
		_realname = realname;
	}

	public String getDisabledtext() {
		return _disabledtext;
	}

	public void setDisabledtext(String disabledtext) {
		_disabledtext = disabledtext;
	}

	public int getDisable_mail() {
		return _disable_mail;
	}

	public void setDisable_mail(int disable_mail) {
		_disable_mail = disable_mail;
	}

	public int getMybugslink() {
		return _mybugslink;
	}

	public void setMybugslink(int mybugslink) {
		_mybugslink = mybugslink;
	}

	public int getManager_id() {
		return _manager_id;
	}

	public void setManager_id(int manager_id) {
		_manager_id = manager_id;
	}

	public String getExtern_id() {
		return _extern_id;
	}

	public void setExtern_id(String extern_id) {
		_extern_id = extern_id;
	}

	public BaseModel<?> getProfilesRemoteModel() {
		return _profilesRemoteModel;
	}

	public void setProfilesRemoteModel(BaseModel<?> profilesRemoteModel) {
		_profilesRemoteModel = profilesRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ProfilesLocalServiceUtil.addProfiles(this);
		}
		else {
			ProfilesLocalServiceUtil.updateProfiles(this);
		}
	}

	@Override
	public Profiles toEscapedModel() {
		return (Profiles)Proxy.newProxyInstance(Profiles.class.getClassLoader(),
			new Class[] { Profiles.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ProfilesClp clone = new ProfilesClp();

		clone.setUserid(getUserid());
		clone.setLogin_name(getLogin_name());
		clone.setAll_login_names(getAll_login_names());
		clone.setCryptpassword(getCryptpassword());
		clone.setRealname(getRealname());
		clone.setDisabledtext(getDisabledtext());
		clone.setDisable_mail(getDisable_mail());
		clone.setMybugslink(getMybugslink());
		clone.setManager_id(getManager_id());
		clone.setExtern_id(getExtern_id());

		return clone;
	}

	public int compareTo(Profiles profiles) {
		int primaryKey = profiles.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ProfilesClp profiles = null;

		try {
			profiles = (ProfilesClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = profiles.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{userid=");
		sb.append(getUserid());
		sb.append(", login_name=");
		sb.append(getLogin_name());
		sb.append(", all_login_names=");
		sb.append(getAll_login_names());
		sb.append(", cryptpassword=");
		sb.append(getCryptpassword());
		sb.append(", realname=");
		sb.append(getRealname());
		sb.append(", disabledtext=");
		sb.append(getDisabledtext());
		sb.append(", disable_mail=");
		sb.append(getDisable_mail());
		sb.append(", mybugslink=");
		sb.append(getMybugslink());
		sb.append(", manager_id=");
		sb.append(getManager_id());
		sb.append(", extern_id=");
		sb.append(getExtern_id());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Profiles");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>userid</column-name><column-value><![CDATA[");
		sb.append(getUserid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>login_name</column-name><column-value><![CDATA[");
		sb.append(getLogin_name());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>all_login_names</column-name><column-value><![CDATA[");
		sb.append(getAll_login_names());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cryptpassword</column-name><column-value><![CDATA[");
		sb.append(getCryptpassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realname</column-name><column-value><![CDATA[");
		sb.append(getRealname());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disabledtext</column-name><column-value><![CDATA[");
		sb.append(getDisabledtext());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disable_mail</column-name><column-value><![CDATA[");
		sb.append(getDisable_mail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mybugslink</column-name><column-value><![CDATA[");
		sb.append(getMybugslink());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>manager_id</column-name><column-value><![CDATA[");
		sb.append(getManager_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>extern_id</column-name><column-value><![CDATA[");
		sb.append(getExtern_id());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _userid;
	private String _login_name;
	private String _all_login_names;
	private String _cryptpassword;
	private String _realname;
	private String _disabledtext;
	private int _disable_mail;
	private int _mybugslink;
	private int _manager_id;
	private String _extern_id;
	private BaseModel<?> _profilesRemoteModel;
}