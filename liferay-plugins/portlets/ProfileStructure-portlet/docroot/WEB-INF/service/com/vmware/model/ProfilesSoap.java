/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.ProfilesServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.ProfilesServiceSoap
 * @generated
 */
public class ProfilesSoap implements Serializable {
	public static ProfilesSoap toSoapModel(Profiles model) {
		ProfilesSoap soapModel = new ProfilesSoap();

		soapModel.setUserid(model.getUserid());
		soapModel.setLogin_name(model.getLogin_name());
		soapModel.setAll_login_names(model.getAll_login_names());
		soapModel.setCryptpassword(model.getCryptpassword());
		soapModel.setRealname(model.getRealname());
		soapModel.setDisabledtext(model.getDisabledtext());
		soapModel.setDisable_mail(model.getDisable_mail());
		soapModel.setMybugslink(model.getMybugslink());
		soapModel.setManager_id(model.getManager_id());
		soapModel.setExtern_id(model.getExtern_id());

		return soapModel;
	}

	public static ProfilesSoap[] toSoapModels(Profiles[] models) {
		ProfilesSoap[] soapModels = new ProfilesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProfilesSoap[][] toSoapModels(Profiles[][] models) {
		ProfilesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProfilesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProfilesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProfilesSoap[] toSoapModels(List<Profiles> models) {
		List<ProfilesSoap> soapModels = new ArrayList<ProfilesSoap>(models.size());

		for (Profiles model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProfilesSoap[soapModels.size()]);
	}

	public ProfilesSoap() {
	}

	public int getPrimaryKey() {
		return _userid;
	}

	public void setPrimaryKey(int pk) {
		setUserid(pk);
	}

	public int getUserid() {
		return _userid;
	}

	public void setUserid(int userid) {
		_userid = userid;
	}

	public String getLogin_name() {
		return _login_name;
	}

	public void setLogin_name(String login_name) {
		_login_name = login_name;
	}

	public String getAll_login_names() {
		return _all_login_names;
	}

	public void setAll_login_names(String all_login_names) {
		_all_login_names = all_login_names;
	}

	public String getCryptpassword() {
		return _cryptpassword;
	}

	public void setCryptpassword(String cryptpassword) {
		_cryptpassword = cryptpassword;
	}

	public String getRealname() {
		return _realname;
	}

	public void setRealname(String realname) {
		_realname = realname;
	}

	public String getDisabledtext() {
		return _disabledtext;
	}

	public void setDisabledtext(String disabledtext) {
		_disabledtext = disabledtext;
	}

	public int getDisable_mail() {
		return _disable_mail;
	}

	public void setDisable_mail(int disable_mail) {
		_disable_mail = disable_mail;
	}

	public int getMybugslink() {
		return _mybugslink;
	}

	public void setMybugslink(int mybugslink) {
		_mybugslink = mybugslink;
	}

	public int getManager_id() {
		return _manager_id;
	}

	public void setManager_id(int manager_id) {
		_manager_id = manager_id;
	}

	public String getExtern_id() {
		return _extern_id;
	}

	public void setExtern_id(String extern_id) {
		_extern_id = extern_id;
	}

	private int _userid;
	private String _login_name;
	private String _all_login_names;
	private String _cryptpassword;
	private String _realname;
	private String _disabledtext;
	private int _disable_mail;
	private int _mybugslink;
	private int _manager_id;
	private String _extern_id;
}