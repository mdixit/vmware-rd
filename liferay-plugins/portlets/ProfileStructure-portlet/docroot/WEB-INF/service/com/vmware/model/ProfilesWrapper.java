/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Profiles}.
 * </p>
 *
 * @author    iscisc
 * @see       Profiles
 * @generated
 */
public class ProfilesWrapper implements Profiles, ModelWrapper<Profiles> {
	public ProfilesWrapper(Profiles profiles) {
		_profiles = profiles;
	}

	public Class<?> getModelClass() {
		return Profiles.class;
	}

	public String getModelClassName() {
		return Profiles.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userid", getUserid());
		attributes.put("login_name", getLogin_name());
		attributes.put("all_login_names", getAll_login_names());
		attributes.put("cryptpassword", getCryptpassword());
		attributes.put("realname", getRealname());
		attributes.put("disabledtext", getDisabledtext());
		attributes.put("disable_mail", getDisable_mail());
		attributes.put("mybugslink", getMybugslink());
		attributes.put("manager_id", getManager_id());
		attributes.put("extern_id", getExtern_id());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer userid = (Integer)attributes.get("userid");

		if (userid != null) {
			setUserid(userid);
		}

		String login_name = (String)attributes.get("login_name");

		if (login_name != null) {
			setLogin_name(login_name);
		}

		String all_login_names = (String)attributes.get("all_login_names");

		if (all_login_names != null) {
			setAll_login_names(all_login_names);
		}

		String cryptpassword = (String)attributes.get("cryptpassword");

		if (cryptpassword != null) {
			setCryptpassword(cryptpassword);
		}

		String realname = (String)attributes.get("realname");

		if (realname != null) {
			setRealname(realname);
		}

		String disabledtext = (String)attributes.get("disabledtext");

		if (disabledtext != null) {
			setDisabledtext(disabledtext);
		}

		Integer disable_mail = (Integer)attributes.get("disable_mail");

		if (disable_mail != null) {
			setDisable_mail(disable_mail);
		}

		Integer mybugslink = (Integer)attributes.get("mybugslink");

		if (mybugslink != null) {
			setMybugslink(mybugslink);
		}

		Integer manager_id = (Integer)attributes.get("manager_id");

		if (manager_id != null) {
			setManager_id(manager_id);
		}

		String extern_id = (String)attributes.get("extern_id");

		if (extern_id != null) {
			setExtern_id(extern_id);
		}
	}

	/**
	* Returns the primary key of this profiles.
	*
	* @return the primary key of this profiles
	*/
	public int getPrimaryKey() {
		return _profiles.getPrimaryKey();
	}

	/**
	* Sets the primary key of this profiles.
	*
	* @param primaryKey the primary key of this profiles
	*/
	public void setPrimaryKey(int primaryKey) {
		_profiles.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the userid of this profiles.
	*
	* @return the userid of this profiles
	*/
	public int getUserid() {
		return _profiles.getUserid();
	}

	/**
	* Sets the userid of this profiles.
	*
	* @param userid the userid of this profiles
	*/
	public void setUserid(int userid) {
		_profiles.setUserid(userid);
	}

	/**
	* Returns the login_name of this profiles.
	*
	* @return the login_name of this profiles
	*/
	public java.lang.String getLogin_name() {
		return _profiles.getLogin_name();
	}

	/**
	* Sets the login_name of this profiles.
	*
	* @param login_name the login_name of this profiles
	*/
	public void setLogin_name(java.lang.String login_name) {
		_profiles.setLogin_name(login_name);
	}

	/**
	* Returns the all_login_names of this profiles.
	*
	* @return the all_login_names of this profiles
	*/
	public java.lang.String getAll_login_names() {
		return _profiles.getAll_login_names();
	}

	/**
	* Sets the all_login_names of this profiles.
	*
	* @param all_login_names the all_login_names of this profiles
	*/
	public void setAll_login_names(java.lang.String all_login_names) {
		_profiles.setAll_login_names(all_login_names);
	}

	/**
	* Returns the cryptpassword of this profiles.
	*
	* @return the cryptpassword of this profiles
	*/
	public java.lang.String getCryptpassword() {
		return _profiles.getCryptpassword();
	}

	/**
	* Sets the cryptpassword of this profiles.
	*
	* @param cryptpassword the cryptpassword of this profiles
	*/
	public void setCryptpassword(java.lang.String cryptpassword) {
		_profiles.setCryptpassword(cryptpassword);
	}

	/**
	* Returns the realname of this profiles.
	*
	* @return the realname of this profiles
	*/
	public java.lang.String getRealname() {
		return _profiles.getRealname();
	}

	/**
	* Sets the realname of this profiles.
	*
	* @param realname the realname of this profiles
	*/
	public void setRealname(java.lang.String realname) {
		_profiles.setRealname(realname);
	}

	/**
	* Returns the disabledtext of this profiles.
	*
	* @return the disabledtext of this profiles
	*/
	public java.lang.String getDisabledtext() {
		return _profiles.getDisabledtext();
	}

	/**
	* Sets the disabledtext of this profiles.
	*
	* @param disabledtext the disabledtext of this profiles
	*/
	public void setDisabledtext(java.lang.String disabledtext) {
		_profiles.setDisabledtext(disabledtext);
	}

	/**
	* Returns the disable_mail of this profiles.
	*
	* @return the disable_mail of this profiles
	*/
	public int getDisable_mail() {
		return _profiles.getDisable_mail();
	}

	/**
	* Sets the disable_mail of this profiles.
	*
	* @param disable_mail the disable_mail of this profiles
	*/
	public void setDisable_mail(int disable_mail) {
		_profiles.setDisable_mail(disable_mail);
	}

	/**
	* Returns the mybugslink of this profiles.
	*
	* @return the mybugslink of this profiles
	*/
	public int getMybugslink() {
		return _profiles.getMybugslink();
	}

	/**
	* Sets the mybugslink of this profiles.
	*
	* @param mybugslink the mybugslink of this profiles
	*/
	public void setMybugslink(int mybugslink) {
		_profiles.setMybugslink(mybugslink);
	}

	/**
	* Returns the manager_id of this profiles.
	*
	* @return the manager_id of this profiles
	*/
	public int getManager_id() {
		return _profiles.getManager_id();
	}

	/**
	* Sets the manager_id of this profiles.
	*
	* @param manager_id the manager_id of this profiles
	*/
	public void setManager_id(int manager_id) {
		_profiles.setManager_id(manager_id);
	}

	/**
	* Returns the extern_id of this profiles.
	*
	* @return the extern_id of this profiles
	*/
	public java.lang.String getExtern_id() {
		return _profiles.getExtern_id();
	}

	/**
	* Sets the extern_id of this profiles.
	*
	* @param extern_id the extern_id of this profiles
	*/
	public void setExtern_id(java.lang.String extern_id) {
		_profiles.setExtern_id(extern_id);
	}

	public boolean isNew() {
		return _profiles.isNew();
	}

	public void setNew(boolean n) {
		_profiles.setNew(n);
	}

	public boolean isCachedModel() {
		return _profiles.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_profiles.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _profiles.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _profiles.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_profiles.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _profiles.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_profiles.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProfilesWrapper((Profiles)_profiles.clone());
	}

	public int compareTo(com.vmware.model.Profiles profiles) {
		return _profiles.compareTo(profiles);
	}

	@Override
	public int hashCode() {
		return _profiles.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Profiles> toCacheModel() {
		return _profiles.toCacheModel();
	}

	public com.vmware.model.Profiles toEscapedModel() {
		return new ProfilesWrapper(_profiles.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _profiles.toString();
	}

	public java.lang.String toXmlString() {
		return _profiles.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_profiles.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Profiles getWrappedProfiles() {
		return _profiles;
	}

	public Profiles getWrappedModel() {
		return _profiles;
	}

	public void resetOriginalValues() {
		_profiles.resetOriginalValues();
	}

	private Profiles _profiles;
}