/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.ResolutionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class ResolutionClp extends BaseModelImpl<Resolution>
	implements Resolution {
	public ResolutionClp() {
	}

	public Class<?> getModelClass() {
		return Resolution.class;
	}

	public String getModelClassName() {
		return Resolution.class.getName();
	}

	public int getPrimaryKey() {
		return _res_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setRes_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_res_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("res_id", getRes_id());
		attributes.put("value", getValue());
		attributes.put("sortkey", getSortkey());
		attributes.put("isactive", getIsactive());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer res_id = (Integer)attributes.get("res_id");

		if (res_id != null) {
			setRes_id(res_id);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}
	}

	public int getRes_id() {
		return _res_id;
	}

	public void setRes_id(int res_id) {
		_res_id = res_id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	public BaseModel<?> getResolutionRemoteModel() {
		return _resolutionRemoteModel;
	}

	public void setResolutionRemoteModel(BaseModel<?> resolutionRemoteModel) {
		_resolutionRemoteModel = resolutionRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ResolutionLocalServiceUtil.addResolution(this);
		}
		else {
			ResolutionLocalServiceUtil.updateResolution(this);
		}
	}

	@Override
	public Resolution toEscapedModel() {
		return (Resolution)Proxy.newProxyInstance(Resolution.class.getClassLoader(),
			new Class[] { Resolution.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ResolutionClp clone = new ResolutionClp();

		clone.setRes_id(getRes_id());
		clone.setValue(getValue());
		clone.setSortkey(getSortkey());
		clone.setIsactive(getIsactive());

		return clone;
	}

	public int compareTo(Resolution resolution) {
		int primaryKey = resolution.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ResolutionClp resolution = null;

		try {
			resolution = (ResolutionClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = resolution.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{res_id=");
		sb.append(getRes_id());
		sb.append(", value=");
		sb.append(getValue());
		sb.append(", sortkey=");
		sb.append(getSortkey());
		sb.append(", isactive=");
		sb.append(getIsactive());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Resolution");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>res_id</column-name><column-value><![CDATA[");
		sb.append(getRes_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>value</column-name><column-value><![CDATA[");
		sb.append(getValue());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sortkey</column-name><column-value><![CDATA[");
		sb.append(getSortkey());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isactive</column-name><column-value><![CDATA[");
		sb.append(getIsactive());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _res_id;
	private String _value;
	private int _sortkey;
	private int _isactive;
	private BaseModel<?> _resolutionRemoteModel;
}