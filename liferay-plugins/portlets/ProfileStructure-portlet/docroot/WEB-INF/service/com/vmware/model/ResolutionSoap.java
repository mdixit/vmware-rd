/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.ResolutionServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.ResolutionServiceSoap
 * @generated
 */
public class ResolutionSoap implements Serializable {
	public static ResolutionSoap toSoapModel(Resolution model) {
		ResolutionSoap soapModel = new ResolutionSoap();

		soapModel.setRes_id(model.getRes_id());
		soapModel.setValue(model.getValue());
		soapModel.setSortkey(model.getSortkey());
		soapModel.setIsactive(model.getIsactive());

		return soapModel;
	}

	public static ResolutionSoap[] toSoapModels(Resolution[] models) {
		ResolutionSoap[] soapModels = new ResolutionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ResolutionSoap[][] toSoapModels(Resolution[][] models) {
		ResolutionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ResolutionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ResolutionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ResolutionSoap[] toSoapModels(List<Resolution> models) {
		List<ResolutionSoap> soapModels = new ArrayList<ResolutionSoap>(models.size());

		for (Resolution model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ResolutionSoap[soapModels.size()]);
	}

	public ResolutionSoap() {
	}

	public int getPrimaryKey() {
		return _res_id;
	}

	public void setPrimaryKey(int pk) {
		setRes_id(pk);
	}

	public int getRes_id() {
		return _res_id;
	}

	public void setRes_id(int res_id) {
		_res_id = res_id;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		_value = value;
	}

	public int getSortkey() {
		return _sortkey;
	}

	public void setSortkey(int sortkey) {
		_sortkey = sortkey;
	}

	public int getIsactive() {
		return _isactive;
	}

	public void setIsactive(int isactive) {
		_isactive = isactive;
	}

	private int _res_id;
	private String _value;
	private int _sortkey;
	private int _isactive;
}