/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Resolution}.
 * </p>
 *
 * @author    iscisc
 * @see       Resolution
 * @generated
 */
public class ResolutionWrapper implements Resolution, ModelWrapper<Resolution> {
	public ResolutionWrapper(Resolution resolution) {
		_resolution = resolution;
	}

	public Class<?> getModelClass() {
		return Resolution.class;
	}

	public String getModelClassName() {
		return Resolution.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("res_id", getRes_id());
		attributes.put("value", getValue());
		attributes.put("sortkey", getSortkey());
		attributes.put("isactive", getIsactive());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer res_id = (Integer)attributes.get("res_id");

		if (res_id != null) {
			setRes_id(res_id);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}

		Integer sortkey = (Integer)attributes.get("sortkey");

		if (sortkey != null) {
			setSortkey(sortkey);
		}

		Integer isactive = (Integer)attributes.get("isactive");

		if (isactive != null) {
			setIsactive(isactive);
		}
	}

	/**
	* Returns the primary key of this resolution.
	*
	* @return the primary key of this resolution
	*/
	public int getPrimaryKey() {
		return _resolution.getPrimaryKey();
	}

	/**
	* Sets the primary key of this resolution.
	*
	* @param primaryKey the primary key of this resolution
	*/
	public void setPrimaryKey(int primaryKey) {
		_resolution.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the res_id of this resolution.
	*
	* @return the res_id of this resolution
	*/
	public int getRes_id() {
		return _resolution.getRes_id();
	}

	/**
	* Sets the res_id of this resolution.
	*
	* @param res_id the res_id of this resolution
	*/
	public void setRes_id(int res_id) {
		_resolution.setRes_id(res_id);
	}

	/**
	* Returns the value of this resolution.
	*
	* @return the value of this resolution
	*/
	public java.lang.String getValue() {
		return _resolution.getValue();
	}

	/**
	* Sets the value of this resolution.
	*
	* @param value the value of this resolution
	*/
	public void setValue(java.lang.String value) {
		_resolution.setValue(value);
	}

	/**
	* Returns the sortkey of this resolution.
	*
	* @return the sortkey of this resolution
	*/
	public int getSortkey() {
		return _resolution.getSortkey();
	}

	/**
	* Sets the sortkey of this resolution.
	*
	* @param sortkey the sortkey of this resolution
	*/
	public void setSortkey(int sortkey) {
		_resolution.setSortkey(sortkey);
	}

	/**
	* Returns the isactive of this resolution.
	*
	* @return the isactive of this resolution
	*/
	public int getIsactive() {
		return _resolution.getIsactive();
	}

	/**
	* Sets the isactive of this resolution.
	*
	* @param isactive the isactive of this resolution
	*/
	public void setIsactive(int isactive) {
		_resolution.setIsactive(isactive);
	}

	public boolean isNew() {
		return _resolution.isNew();
	}

	public void setNew(boolean n) {
		_resolution.setNew(n);
	}

	public boolean isCachedModel() {
		return _resolution.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_resolution.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _resolution.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _resolution.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_resolution.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _resolution.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_resolution.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ResolutionWrapper((Resolution)_resolution.clone());
	}

	public int compareTo(com.vmware.model.Resolution resolution) {
		return _resolution.compareTo(resolution);
	}

	@Override
	public int hashCode() {
		return _resolution.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Resolution> toCacheModel() {
		return _resolution.toCacheModel();
	}

	public com.vmware.model.Resolution toEscapedModel() {
		return new ResolutionWrapper(_resolution.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _resolution.toString();
	}

	public java.lang.String toXmlString() {
		return _resolution.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_resolution.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Resolution getWrappedResolution() {
		return _resolution;
	}

	public Resolution getWrappedModel() {
		return _resolution;
	}

	public void resetOriginalValues() {
		_resolution.resetOriginalValues();
	}

	private Resolution _resolution;
}