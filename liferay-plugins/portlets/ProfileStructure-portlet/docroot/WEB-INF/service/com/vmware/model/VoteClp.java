/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.service.VoteLocalServiceUtil;
import com.vmware.service.persistence.VotePK;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author iscisc
 */
public class VoteClp extends BaseModelImpl<Vote> implements Vote {
	public VoteClp() {
	}

	public Class<?> getModelClass() {
		return Vote.class;
	}

	public String getModelClassName() {
		return Vote.class.getName();
	}

	public VotePK getPrimaryKey() {
		return new VotePK(_bug_id, _who);
	}

	public void setPrimaryKey(VotePK primaryKey) {
		setBug_id(primaryKey.bug_id);
		setWho(primaryKey.who);
	}

	public Serializable getPrimaryKeyObj() {
		return new VotePK(_bug_id, _who);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((VotePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("who", getWho());
		attributes.put("vote_count", getVote_count());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Integer vote_count = (Integer)attributes.get("vote_count");

		if (vote_count != null) {
			setVote_count(vote_count);
		}
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public int getVote_count() {
		return _vote_count;
	}

	public void setVote_count(int vote_count) {
		_vote_count = vote_count;
	}

	public BaseModel<?> getVoteRemoteModel() {
		return _voteRemoteModel;
	}

	public void setVoteRemoteModel(BaseModel<?> voteRemoteModel) {
		_voteRemoteModel = voteRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			VoteLocalServiceUtil.addVote(this);
		}
		else {
			VoteLocalServiceUtil.updateVote(this);
		}
	}

	@Override
	public Vote toEscapedModel() {
		return (Vote)Proxy.newProxyInstance(Vote.class.getClassLoader(),
			new Class[] { Vote.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		VoteClp clone = new VoteClp();

		clone.setBug_id(getBug_id());
		clone.setWho(getWho());
		clone.setVote_count(getVote_count());

		return clone;
	}

	public int compareTo(Vote vote) {
		VotePK primaryKey = vote.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		VoteClp vote = null;

		try {
			vote = (VoteClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		VotePK primaryKey = vote.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{bug_id=");
		sb.append(getBug_id());
		sb.append(", who=");
		sb.append(getWho());
		sb.append(", vote_count=");
		sb.append(getVote_count());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Vote");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>who</column-name><column-value><![CDATA[");
		sb.append(getWho());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vote_count</column-name><column-value><![CDATA[");
		sb.append(getVote_count());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _bug_id;
	private int _who;
	private int _vote_count;
	private BaseModel<?> _voteRemoteModel;
}