/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import com.vmware.service.persistence.VotePK;

import java.io.Serializable;

/**
 * The base model interface for the Vote service. Represents a row in the &quot;votes&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.vmware.model.impl.VoteModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.vmware.model.impl.VoteImpl}.
 * </p>
 *
 * @author iscisc
 * @see Vote
 * @see com.vmware.model.impl.VoteImpl
 * @see com.vmware.model.impl.VoteModelImpl
 * @generated
 */
public interface VoteModel extends BaseModel<Vote> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a vote model instance should use the {@link Vote} interface instead.
	 */

	/**
	 * Returns the primary key of this vote.
	 *
	 * @return the primary key of this vote
	 */
	public VotePK getPrimaryKey();

	/**
	 * Sets the primary key of this vote.
	 *
	 * @param primaryKey the primary key of this vote
	 */
	public void setPrimaryKey(VotePK primaryKey);

	/**
	 * Returns the bug_id of this vote.
	 *
	 * @return the bug_id of this vote
	 */
	public int getBug_id();

	/**
	 * Sets the bug_id of this vote.
	 *
	 * @param bug_id the bug_id of this vote
	 */
	public void setBug_id(int bug_id);

	/**
	 * Returns the who of this vote.
	 *
	 * @return the who of this vote
	 */
	public int getWho();

	/**
	 * Sets the who of this vote.
	 *
	 * @param who the who of this vote
	 */
	public void setWho(int who);

	/**
	 * Returns the vote_count of this vote.
	 *
	 * @return the vote_count of this vote
	 */
	public int getVote_count();

	/**
	 * Sets the vote_count of this vote.
	 *
	 * @param vote_count the vote_count of this vote
	 */
	public void setVote_count(int vote_count);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(Vote vote);

	public int hashCode();

	public CacheModel<Vote> toCacheModel();

	public Vote toEscapedModel();

	public String toString();

	public String toXmlString();
}