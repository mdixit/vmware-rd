/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.vmware.service.persistence.VotePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.vmware.service.http.VoteServiceSoap}.
 *
 * @author    iscisc
 * @see       com.vmware.service.http.VoteServiceSoap
 * @generated
 */
public class VoteSoap implements Serializable {
	public static VoteSoap toSoapModel(Vote model) {
		VoteSoap soapModel = new VoteSoap();

		soapModel.setBug_id(model.getBug_id());
		soapModel.setWho(model.getWho());
		soapModel.setVote_count(model.getVote_count());

		return soapModel;
	}

	public static VoteSoap[] toSoapModels(Vote[] models) {
		VoteSoap[] soapModels = new VoteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VoteSoap[][] toSoapModels(Vote[][] models) {
		VoteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VoteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VoteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VoteSoap[] toSoapModels(List<Vote> models) {
		List<VoteSoap> soapModels = new ArrayList<VoteSoap>(models.size());

		for (Vote model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VoteSoap[soapModels.size()]);
	}

	public VoteSoap() {
	}

	public VotePK getPrimaryKey() {
		return new VotePK(_bug_id, _who);
	}

	public void setPrimaryKey(VotePK pk) {
		setBug_id(pk.bug_id);
		setWho(pk.who);
	}

	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	public int getWho() {
		return _who;
	}

	public void setWho(int who) {
		_who = who;
	}

	public int getVote_count() {
		return _vote_count;
	}

	public void setVote_count(int vote_count) {
		_vote_count = vote_count;
	}

	private int _bug_id;
	private int _who;
	private int _vote_count;
}