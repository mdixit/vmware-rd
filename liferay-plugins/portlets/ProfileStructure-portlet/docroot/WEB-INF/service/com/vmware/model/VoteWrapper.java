/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Vote}.
 * </p>
 *
 * @author    iscisc
 * @see       Vote
 * @generated
 */
public class VoteWrapper implements Vote, ModelWrapper<Vote> {
	public VoteWrapper(Vote vote) {
		_vote = vote;
	}

	public Class<?> getModelClass() {
		return Vote.class;
	}

	public String getModelClassName() {
		return Vote.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("who", getWho());
		attributes.put("vote_count", getVote_count());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer who = (Integer)attributes.get("who");

		if (who != null) {
			setWho(who);
		}

		Integer vote_count = (Integer)attributes.get("vote_count");

		if (vote_count != null) {
			setVote_count(vote_count);
		}
	}

	/**
	* Returns the primary key of this vote.
	*
	* @return the primary key of this vote
	*/
	public com.vmware.service.persistence.VotePK getPrimaryKey() {
		return _vote.getPrimaryKey();
	}

	/**
	* Sets the primary key of this vote.
	*
	* @param primaryKey the primary key of this vote
	*/
	public void setPrimaryKey(com.vmware.service.persistence.VotePK primaryKey) {
		_vote.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the bug_id of this vote.
	*
	* @return the bug_id of this vote
	*/
	public int getBug_id() {
		return _vote.getBug_id();
	}

	/**
	* Sets the bug_id of this vote.
	*
	* @param bug_id the bug_id of this vote
	*/
	public void setBug_id(int bug_id) {
		_vote.setBug_id(bug_id);
	}

	/**
	* Returns the who of this vote.
	*
	* @return the who of this vote
	*/
	public int getWho() {
		return _vote.getWho();
	}

	/**
	* Sets the who of this vote.
	*
	* @param who the who of this vote
	*/
	public void setWho(int who) {
		_vote.setWho(who);
	}

	/**
	* Returns the vote_count of this vote.
	*
	* @return the vote_count of this vote
	*/
	public int getVote_count() {
		return _vote.getVote_count();
	}

	/**
	* Sets the vote_count of this vote.
	*
	* @param vote_count the vote_count of this vote
	*/
	public void setVote_count(int vote_count) {
		_vote.setVote_count(vote_count);
	}

	public boolean isNew() {
		return _vote.isNew();
	}

	public void setNew(boolean n) {
		_vote.setNew(n);
	}

	public boolean isCachedModel() {
		return _vote.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_vote.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _vote.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _vote.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_vote.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _vote.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_vote.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new VoteWrapper((Vote)_vote.clone());
	}

	public int compareTo(com.vmware.model.Vote vote) {
		return _vote.compareTo(vote);
	}

	@Override
	public int hashCode() {
		return _vote.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.vmware.model.Vote> toCacheModel() {
		return _vote.toCacheModel();
	}

	public com.vmware.model.Vote toEscapedModel() {
		return new VoteWrapper(_vote.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _vote.toString();
	}

	public java.lang.String toXmlString() {
		return _vote.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_vote.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Vote getWrappedVote() {
		return _vote;
	}

	public Vote getWrappedModel() {
		return _vote;
	}

	public void resetOriginalValues() {
		_vote.resetOriginalValues();
	}

	private Vote _vote;
}