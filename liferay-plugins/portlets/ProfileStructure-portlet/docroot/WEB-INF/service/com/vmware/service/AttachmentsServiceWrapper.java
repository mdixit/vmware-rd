/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link AttachmentsService}.
 * </p>
 *
 * @author    iscisc
 * @see       AttachmentsService
 * @generated
 */
public class AttachmentsServiceWrapper implements AttachmentsService,
	ServiceWrapper<AttachmentsService> {
	public AttachmentsServiceWrapper(AttachmentsService attachmentsService) {
		_attachmentsService = attachmentsService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _attachmentsService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_attachmentsService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _attachmentsService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public AttachmentsService getWrappedAttachmentsService() {
		return _attachmentsService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedAttachmentsService(
		AttachmentsService attachmentsService) {
		_attachmentsService = attachmentsService;
	}

	public AttachmentsService getWrappedService() {
		return _attachmentsService;
	}

	public void setWrappedService(AttachmentsService attachmentsService) {
		_attachmentsService = attachmentsService;
	}

	private AttachmentsService _attachmentsService;
}