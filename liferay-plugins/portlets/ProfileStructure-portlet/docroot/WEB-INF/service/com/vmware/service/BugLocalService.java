/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * The interface for the bug local service.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see BugLocalServiceUtil
 * @see com.vmware.service.base.BugLocalServiceBaseImpl
 * @see com.vmware.service.impl.BugLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface BugLocalService extends BaseLocalService, InvokableLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BugLocalServiceUtil} to access the bug local service. Add custom service methods to {@link com.vmware.service.impl.BugLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Adds the bug to the database. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @return the bug that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug addBug(com.vmware.model.Bug bug)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Creates a new bug with the primary key. Does not add the bug to the database.
	*
	* @param bug_id the primary key for the new bug
	* @return the new bug
	*/
	public com.vmware.model.Bug createBug(int bug_id);

	/**
	* Deletes the bug with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_id the primary key of the bug
	* @return the bug that was removed
	* @throws PortalException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug deleteBug(int bug_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Deletes the bug from the database. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @return the bug that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug deleteBug(com.vmware.model.Bug bug)
		throws com.liferay.portal.kernel.exception.SystemException;

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Bug fetchBug(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug with the primary key.
	*
	* @param bug_id the primary key of the bug
	* @return the bug
	* @throws PortalException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Bug getBug(int bug_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of bugs
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugs(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs.
	*
	* @return the number of bugs
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getBugsCount()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Updates the bug in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @return the bug that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug updateBug(com.vmware.model.Bug bug)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Updates the bug in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @param merge whether to merge the bug with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the bug that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug updateBug(com.vmware.model.Bug bug,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugByAssignToId(
		int assigned_to);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugPriorityByUserId(
		int assigned_to, java.lang.String priority);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugsByReporter(int reporter);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugVotesByUserId(
		int assigned_to, int votes);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugPriority(
		java.lang.String priority);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Profiles getBugReporterByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Profiles getBugQAContactByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Components getBugComponentsByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Products getBugProductsByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.BugSeverity getBugSeverityByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.BugStatus getBugStatus(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Profiles getBugAssignedByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Resolution getBugResolutionByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.KeywordDefs> getKeywordDefByBugId(
		int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugByShortDesc(
		java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Bug getDuplicateByDupeofId(int bug_id);
}