/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the bug local service. This utility wraps {@link com.vmware.service.impl.BugLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see BugLocalService
 * @see com.vmware.service.base.BugLocalServiceBaseImpl
 * @see com.vmware.service.impl.BugLocalServiceImpl
 * @generated
 */
public class BugLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.vmware.service.impl.BugLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the bug to the database. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @return the bug that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug addBug(com.vmware.model.Bug bug)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addBug(bug);
	}

	/**
	* Creates a new bug with the primary key. Does not add the bug to the database.
	*
	* @param bug_id the primary key for the new bug
	* @return the new bug
	*/
	public static com.vmware.model.Bug createBug(int bug_id) {
		return getService().createBug(bug_id);
	}

	/**
	* Deletes the bug with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_id the primary key of the bug
	* @return the bug that was removed
	* @throws PortalException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug deleteBug(int bug_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBug(bug_id);
	}

	/**
	* Deletes the bug from the database. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @return the bug that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug deleteBug(com.vmware.model.Bug bug)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBug(bug);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.vmware.model.Bug fetchBug(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchBug(bug_id);
	}

	/**
	* Returns the bug with the primary key.
	*
	* @param bug_id the primary key of the bug
	* @return the bug
	* @throws PortalException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug getBug(int bug_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getBug(bug_id);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the bugs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBugs(start, end);
	}

	/**
	* Returns the number of bugs.
	*
	* @return the number of bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int getBugsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBugsCount();
	}

	/**
	* Updates the bug in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @return the bug that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug updateBug(com.vmware.model.Bug bug)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBug(bug);
	}

	/**
	* Updates the bug in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bug the bug
	* @param merge whether to merge the bug with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the bug that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug updateBug(com.vmware.model.Bug bug,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBug(bug, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.vmware.model.Bug> getBugByAssignToId(
		int assigned_to) {
		return getService().getBugByAssignToId(assigned_to);
	}

	public static java.util.List<com.vmware.model.Bug> getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity) {
		return getService().getBugSeverityByUserId(assigned_to, bug_severity);
	}

	public static java.util.List<com.vmware.model.Bug> getBugPriorityByUserId(
		int assigned_to, java.lang.String priority) {
		return getService().getBugPriorityByUserId(assigned_to, priority);
	}

	public static java.util.List<com.vmware.model.Bug> getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status) {
		return getService().getBugStatusByUserId(assigned_to, bug_status);
	}

	public static java.util.List<com.vmware.model.Bug> getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status) {
		return getService().getBugKeywordsByUserId(assigned_to, bug_status);
	}

	public static java.util.List<com.vmware.model.Bug> getBugsByReporter(
		int reporter) {
		return getService().getBugsByReporter(reporter);
	}

	public static java.util.List<com.vmware.model.Bug> getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc) {
		return getService().getBugShortDescByUserId(assigned_to, short_desc);
	}

	public static java.util.List<com.vmware.model.Bug> getBugVotesByUserId(
		int assigned_to, int votes) {
		return getService().getBugVotesByUserId(assigned_to, votes);
	}

	public static java.util.List<com.vmware.model.Bug> getBugPriority(
		java.lang.String priority) {
		return getService().getBugPriority(priority);
	}

	public static java.util.List<com.vmware.model.Bug> getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution) {
		return getService().getBugResolutionByUserId(assigned_to, resolution);
	}

	public static com.vmware.model.Profiles getBugReporterByBugId(int bug_id) {
		return getService().getBugReporterByBugId(bug_id);
	}

	public static com.vmware.model.Profiles getBugQAContactByBugId(int bug_id) {
		return getService().getBugQAContactByBugId(bug_id);
	}

	public static com.vmware.model.Components getBugComponentsByBugId(
		int bug_id) {
		return getService().getBugComponentsByBugId(bug_id);
	}

	public static com.vmware.model.Products getBugProductsByBugId(int bug_id) {
		return getService().getBugProductsByBugId(bug_id);
	}

	public static com.vmware.model.BugSeverity getBugSeverityByBugId(int bug_id) {
		return getService().getBugSeverityByBugId(bug_id);
	}

	public static com.vmware.model.BugStatus getBugStatus(int bug_id) {
		return getService().getBugStatus(bug_id);
	}

	public static com.vmware.model.Profiles getBugAssignedByBugId(int bug_id) {
		return getService().getBugAssignedByBugId(bug_id);
	}

	public static com.vmware.model.Resolution getBugResolutionByBugId(
		int bug_id) {
		return getService().getBugResolutionByBugId(bug_id);
	}

	public static java.util.List<com.vmware.model.KeywordDefs> getKeywordDefByBugId(
		int bug_id) {
		return getService().getKeywordDefByBugId(bug_id);
	}

	public static java.util.List<com.vmware.model.Bug> getBugByShortDesc(
		java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBugByShortDesc(short_desc);
	}

	public static com.vmware.model.Bug getDuplicateByDupeofId(int bug_id) {
		return getService().getDuplicateByDupeofId(bug_id);
	}

	public static void clearService() {
		_service = null;
	}

	public static BugLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BugLocalService.class.getName());

			if (invokableLocalService instanceof BugLocalService) {
				_service = (BugLocalService)invokableLocalService;
			}
			else {
				_service = new BugLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(BugLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(BugLocalService service) {
	}

	private static BugLocalService _service;
}