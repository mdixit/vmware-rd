/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseService;
import com.liferay.portal.service.InvokableService;

/**
 * The interface for the bug remote service.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author iscisc
 * @see BugServiceUtil
 * @see com.vmware.service.base.BugServiceBaseImpl
 * @see com.vmware.service.impl.BugServiceImpl
 * @generated
 */
@JSONWebService
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface BugService extends BaseService, InvokableService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BugServiceUtil} to access the bug remote service. Add custom service methods to {@link com.vmware.service.impl.BugServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugByAssignToId(
		int assigned_to);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugPriorityByUserId(
		int assigned_to, java.lang.String priority);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugsByReporter(int reporter);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugVotesByUserId(
		int assigned_to, int votes);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugPriority(
		java.lang.String priority);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<com.vmware.model.Bug> getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Profiles getBugReporterByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Profiles getBugQAContactByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Components getBugComponentsByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Products getBugProductsByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.BugSeverity getBugSeverityByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.BugStatus getBugStatus(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Profiles getBugAssignedByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Resolution getBugResolutionByBugId(int bug_id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.vmware.model.Bug getDuplicateByDupeofId(int bug_id);
}