/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.InvokableService;

/**
 * @author iscisc
 */
public class BugServiceClp implements BugService {
	public BugServiceClp(InvokableService invokableService) {
		_invokableService = invokableService;

		_methodName0 = "getBeanIdentifier";

		_methodParameterTypes0 = new String[] {  };

		_methodName1 = "setBeanIdentifier";

		_methodParameterTypes1 = new String[] { "java.lang.String" };

		_methodName3 = "getBugByAssignToId";

		_methodParameterTypes3 = new String[] { "int" };

		_methodName4 = "getBugSeverityByUserId";

		_methodParameterTypes4 = new String[] { "int", "java.lang.String" };

		_methodName5 = "getBugPriorityByUserId";

		_methodParameterTypes5 = new String[] { "int", "java.lang.String" };

		_methodName6 = "getBugStatusByUserId";

		_methodParameterTypes6 = new String[] { "int", "java.lang.String" };

		_methodName7 = "getBugKeywordsByUserId";

		_methodParameterTypes7 = new String[] { "int", "java.lang.String" };

		_methodName8 = "getBugsByReporter";

		_methodParameterTypes8 = new String[] { "int" };

		_methodName9 = "getBugShortDescByUserId";

		_methodParameterTypes9 = new String[] { "int", "java.lang.String" };

		_methodName10 = "getBugVotesByUserId";

		_methodParameterTypes10 = new String[] { "int", "int" };

		_methodName11 = "getBugPriority";

		_methodParameterTypes11 = new String[] { "java.lang.String" };

		_methodName12 = "getBugResolutionByUserId";

		_methodParameterTypes12 = new String[] { "int", "java.lang.String" };

		_methodName13 = "getBugReporterByBugId";

		_methodParameterTypes13 = new String[] { "int" };

		_methodName14 = "getBugQAContactByBugId";

		_methodParameterTypes14 = new String[] { "int" };

		_methodName15 = "getBugComponentsByBugId";

		_methodParameterTypes15 = new String[] { "int" };

		_methodName16 = "getBugProductsByBugId";

		_methodParameterTypes16 = new String[] { "int" };

		_methodName17 = "getBugSeverityByBugId";

		_methodParameterTypes17 = new String[] { "int" };

		_methodName18 = "getBugStatus";

		_methodParameterTypes18 = new String[] { "int" };

		_methodName19 = "getBugAssignedByBugId";

		_methodParameterTypes19 = new String[] { "int" };

		_methodName20 = "getBugResolutionByBugId";

		_methodParameterTypes20 = new String[] { "int" };

		_methodName21 = "getDuplicateByDupeofId";

		_methodParameterTypes21 = new String[] { "int" };
	}

	public java.lang.String getBeanIdentifier() {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName0,
					_methodParameterTypes0, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.lang.String)ClpSerializer.translateOutput(returnObj);
	}

	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		try {
			_invokableService.invokeMethod(_methodName1,
				_methodParameterTypes1,
				new Object[] { ClpSerializer.translateInput(beanIdentifier) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		throw new UnsupportedOperationException();
	}

	public java.util.List<com.vmware.model.Bug> getBugByAssignToId(
		int assigned_to) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName3,
					_methodParameterTypes3, new Object[] { assigned_to });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName4,
					_methodParameterTypes4,
					new Object[] {
						assigned_to,
						
					ClpSerializer.translateInput(bug_severity)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugPriorityByUserId(
		int assigned_to, java.lang.String priority) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName5,
					_methodParameterTypes5,
					new Object[] {
						assigned_to,
						
					ClpSerializer.translateInput(priority)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName6,
					_methodParameterTypes6,
					new Object[] {
						assigned_to,
						
					ClpSerializer.translateInput(bug_status)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName7,
					_methodParameterTypes7,
					new Object[] {
						assigned_to,
						
					ClpSerializer.translateInput(bug_status)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugsByReporter(int reporter) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName8,
					_methodParameterTypes8, new Object[] { reporter });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName9,
					_methodParameterTypes9,
					new Object[] {
						assigned_to,
						
					ClpSerializer.translateInput(short_desc)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugVotesByUserId(
		int assigned_to, int votes) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName10,
					_methodParameterTypes10, new Object[] { assigned_to, votes });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugPriority(
		java.lang.String priority) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName11,
					_methodParameterTypes11,
					new Object[] { ClpSerializer.translateInput(priority) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.vmware.model.Bug> getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName12,
					_methodParameterTypes12,
					new Object[] {
						assigned_to,
						
					ClpSerializer.translateInput(resolution)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.util.List<com.vmware.model.Bug>)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Profiles getBugReporterByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName13,
					_methodParameterTypes13, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Profiles)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Profiles getBugQAContactByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName14,
					_methodParameterTypes14, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Profiles)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Components getBugComponentsByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName15,
					_methodParameterTypes15, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Components)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Products getBugProductsByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName16,
					_methodParameterTypes16, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Products)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.BugSeverity getBugSeverityByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName17,
					_methodParameterTypes17, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.BugSeverity)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.BugStatus getBugStatus(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName18,
					_methodParameterTypes18, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.BugStatus)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Profiles getBugAssignedByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName19,
					_methodParameterTypes19, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Profiles)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Resolution getBugResolutionByBugId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName20,
					_methodParameterTypes20, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Resolution)ClpSerializer.translateOutput(returnObj);
	}

	public com.vmware.model.Bug getDuplicateByDupeofId(int bug_id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName21,
					_methodParameterTypes21, new Object[] { bug_id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.vmware.model.Bug)ClpSerializer.translateOutput(returnObj);
	}

	private InvokableService _invokableService;
	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName18;
	private String[] _methodParameterTypes18;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName20;
	private String[] _methodParameterTypes20;
	private String _methodName21;
	private String[] _methodParameterTypes21;
}