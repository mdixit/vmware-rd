/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * The utility for the bug remote service. This utility wraps {@link com.vmware.service.impl.BugServiceImpl} and is the primary access point for service operations in application layer code running on a remote server.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author iscisc
 * @see BugService
 * @see com.vmware.service.base.BugServiceBaseImpl
 * @see com.vmware.service.impl.BugServiceImpl
 * @generated
 */
public class BugServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.vmware.service.impl.BugServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.vmware.model.Bug> getBugByAssignToId(
		int assigned_to) {
		return getService().getBugByAssignToId(assigned_to);
	}

	public static java.util.List<com.vmware.model.Bug> getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity) {
		return getService().getBugSeverityByUserId(assigned_to, bug_severity);
	}

	public static java.util.List<com.vmware.model.Bug> getBugPriorityByUserId(
		int assigned_to, java.lang.String priority) {
		return getService().getBugPriorityByUserId(assigned_to, priority);
	}

	public static java.util.List<com.vmware.model.Bug> getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status) {
		return getService().getBugStatusByUserId(assigned_to, bug_status);
	}

	public static java.util.List<com.vmware.model.Bug> getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status) {
		return getService().getBugKeywordsByUserId(assigned_to, bug_status);
	}

	public static java.util.List<com.vmware.model.Bug> getBugsByReporter(
		int reporter) {
		return getService().getBugsByReporter(reporter);
	}

	public static java.util.List<com.vmware.model.Bug> getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc) {
		return getService().getBugShortDescByUserId(assigned_to, short_desc);
	}

	public static java.util.List<com.vmware.model.Bug> getBugVotesByUserId(
		int assigned_to, int votes) {
		return getService().getBugVotesByUserId(assigned_to, votes);
	}

	public static java.util.List<com.vmware.model.Bug> getBugPriority(
		java.lang.String priority) {
		return getService().getBugPriority(priority);
	}

	public static java.util.List<com.vmware.model.Bug> getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution) {
		return getService().getBugResolutionByUserId(assigned_to, resolution);
	}

	public static com.vmware.model.Profiles getBugReporterByBugId(int bug_id) {
		return getService().getBugReporterByBugId(bug_id);
	}

	public static com.vmware.model.Profiles getBugQAContactByBugId(int bug_id) {
		return getService().getBugQAContactByBugId(bug_id);
	}

	public static com.vmware.model.Components getBugComponentsByBugId(
		int bug_id) {
		return getService().getBugComponentsByBugId(bug_id);
	}

	public static com.vmware.model.Products getBugProductsByBugId(int bug_id) {
		return getService().getBugProductsByBugId(bug_id);
	}

	public static com.vmware.model.BugSeverity getBugSeverityByBugId(int bug_id) {
		return getService().getBugSeverityByBugId(bug_id);
	}

	public static com.vmware.model.BugStatus getBugStatus(int bug_id) {
		return getService().getBugStatus(bug_id);
	}

	public static com.vmware.model.Profiles getBugAssignedByBugId(int bug_id) {
		return getService().getBugAssignedByBugId(bug_id);
	}

	public static com.vmware.model.Resolution getBugResolutionByBugId(
		int bug_id) {
		return getService().getBugResolutionByBugId(bug_id);
	}

	public static com.vmware.model.Bug getDuplicateByDupeofId(int bug_id) {
		return getService().getDuplicateByDupeofId(bug_id);
	}

	public static void clearService() {
		_service = null;
	}

	public static BugService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BugService.class.getName());

			if (invokableService instanceof BugService) {
				_service = (BugService)invokableService;
			}
			else {
				_service = new BugServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(BugServiceUtil.class, "_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(BugService service) {
	}

	private static BugService _service;
}