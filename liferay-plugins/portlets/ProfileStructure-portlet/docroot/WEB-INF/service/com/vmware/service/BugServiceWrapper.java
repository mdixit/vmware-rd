/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BugService}.
 * </p>
 *
 * @author    iscisc
 * @see       BugService
 * @generated
 */
public class BugServiceWrapper implements BugService,
	ServiceWrapper<BugService> {
	public BugServiceWrapper(BugService bugService) {
		_bugService = bugService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _bugService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_bugService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _bugService.invokeMethod(name, parameterTypes, arguments);
	}

	public java.util.List<com.vmware.model.Bug> getBugByAssignToId(
		int assigned_to) {
		return _bugService.getBugByAssignToId(assigned_to);
	}

	public java.util.List<com.vmware.model.Bug> getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity) {
		return _bugService.getBugSeverityByUserId(assigned_to, bug_severity);
	}

	public java.util.List<com.vmware.model.Bug> getBugPriorityByUserId(
		int assigned_to, java.lang.String priority) {
		return _bugService.getBugPriorityByUserId(assigned_to, priority);
	}

	public java.util.List<com.vmware.model.Bug> getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status) {
		return _bugService.getBugStatusByUserId(assigned_to, bug_status);
	}

	public java.util.List<com.vmware.model.Bug> getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status) {
		return _bugService.getBugKeywordsByUserId(assigned_to, bug_status);
	}

	public java.util.List<com.vmware.model.Bug> getBugsByReporter(int reporter) {
		return _bugService.getBugsByReporter(reporter);
	}

	public java.util.List<com.vmware.model.Bug> getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc) {
		return _bugService.getBugShortDescByUserId(assigned_to, short_desc);
	}

	public java.util.List<com.vmware.model.Bug> getBugVotesByUserId(
		int assigned_to, int votes) {
		return _bugService.getBugVotesByUserId(assigned_to, votes);
	}

	public java.util.List<com.vmware.model.Bug> getBugPriority(
		java.lang.String priority) {
		return _bugService.getBugPriority(priority);
	}

	public java.util.List<com.vmware.model.Bug> getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution) {
		return _bugService.getBugResolutionByUserId(assigned_to, resolution);
	}

	public com.vmware.model.Profiles getBugReporterByBugId(int bug_id) {
		return _bugService.getBugReporterByBugId(bug_id);
	}

	public com.vmware.model.Profiles getBugQAContactByBugId(int bug_id) {
		return _bugService.getBugQAContactByBugId(bug_id);
	}

	public com.vmware.model.Components getBugComponentsByBugId(int bug_id) {
		return _bugService.getBugComponentsByBugId(bug_id);
	}

	public com.vmware.model.Products getBugProductsByBugId(int bug_id) {
		return _bugService.getBugProductsByBugId(bug_id);
	}

	public com.vmware.model.BugSeverity getBugSeverityByBugId(int bug_id) {
		return _bugService.getBugSeverityByBugId(bug_id);
	}

	public com.vmware.model.BugStatus getBugStatus(int bug_id) {
		return _bugService.getBugStatus(bug_id);
	}

	public com.vmware.model.Profiles getBugAssignedByBugId(int bug_id) {
		return _bugService.getBugAssignedByBugId(bug_id);
	}

	public com.vmware.model.Resolution getBugResolutionByBugId(int bug_id) {
		return _bugService.getBugResolutionByBugId(bug_id);
	}

	public com.vmware.model.Bug getDuplicateByDupeofId(int bug_id) {
		return _bugService.getDuplicateByDupeofId(bug_id);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BugService getWrappedBugService() {
		return _bugService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBugService(BugService bugService) {
		_bugService = bugService;
	}

	public BugService getWrappedService() {
		return _bugService;
	}

	public void setWrappedService(BugService bugService) {
		_bugService = bugService;
	}

	private BugService _bugService;
}