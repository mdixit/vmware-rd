/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BugSeverityLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       BugSeverityLocalService
 * @generated
 */
public class BugSeverityLocalServiceWrapper implements BugSeverityLocalService,
	ServiceWrapper<BugSeverityLocalService> {
	public BugSeverityLocalServiceWrapper(
		BugSeverityLocalService bugSeverityLocalService) {
		_bugSeverityLocalService = bugSeverityLocalService;
	}

	/**
	* Adds the bug severity to the database. Also notifies the appropriate model listeners.
	*
	* @param bugSeverity the bug severity
	* @return the bug severity that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity addBugSeverity(
		com.vmware.model.BugSeverity bugSeverity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.addBugSeverity(bugSeverity);
	}

	/**
	* Creates a new bug severity with the primary key. Does not add the bug severity to the database.
	*
	* @param bug_severtiy_id the primary key for the new bug severity
	* @return the new bug severity
	*/
	public com.vmware.model.BugSeverity createBugSeverity(int bug_severtiy_id) {
		return _bugSeverityLocalService.createBugSeverity(bug_severtiy_id);
	}

	/**
	* Deletes the bug severity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_severtiy_id the primary key of the bug severity
	* @return the bug severity that was removed
	* @throws PortalException if a bug severity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity deleteBugSeverity(int bug_severtiy_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.deleteBugSeverity(bug_severtiy_id);
	}

	/**
	* Deletes the bug severity from the database. Also notifies the appropriate model listeners.
	*
	* @param bugSeverity the bug severity
	* @return the bug severity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity deleteBugSeverity(
		com.vmware.model.BugSeverity bugSeverity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.deleteBugSeverity(bugSeverity);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _bugSeverityLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.BugSeverity fetchBugSeverity(int bug_severtiy_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.fetchBugSeverity(bug_severtiy_id);
	}

	/**
	* Returns the bug severity with the primary key.
	*
	* @param bug_severtiy_id the primary key of the bug severity
	* @return the bug severity
	* @throws PortalException if a bug severity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity getBugSeverity(int bug_severtiy_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.getBugSeverity(bug_severtiy_id);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the bug severities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bug severities
	* @param end the upper bound of the range of bug severities (not inclusive)
	* @return the range of bug severities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugSeverity> getBugSeverities(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.getBugSeverities(start, end);
	}

	/**
	* Returns the number of bug severities.
	*
	* @return the number of bug severities
	* @throws SystemException if a system exception occurred
	*/
	public int getBugSeveritiesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.getBugSeveritiesCount();
	}

	/**
	* Updates the bug severity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bugSeverity the bug severity
	* @return the bug severity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity updateBugSeverity(
		com.vmware.model.BugSeverity bugSeverity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.updateBugSeverity(bugSeverity);
	}

	/**
	* Updates the bug severity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bugSeverity the bug severity
	* @param merge whether to merge the bug severity with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the bug severity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity updateBugSeverity(
		com.vmware.model.BugSeverity bugSeverity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugSeverityLocalService.updateBugSeverity(bugSeverity, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _bugSeverityLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_bugSeverityLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _bugSeverityLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BugSeverityLocalService getWrappedBugSeverityLocalService() {
		return _bugSeverityLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBugSeverityLocalService(
		BugSeverityLocalService bugSeverityLocalService) {
		_bugSeverityLocalService = bugSeverityLocalService;
	}

	public BugSeverityLocalService getWrappedService() {
		return _bugSeverityLocalService;
	}

	public void setWrappedService(
		BugSeverityLocalService bugSeverityLocalService) {
		_bugSeverityLocalService = bugSeverityLocalService;
	}

	private BugSeverityLocalService _bugSeverityLocalService;
}