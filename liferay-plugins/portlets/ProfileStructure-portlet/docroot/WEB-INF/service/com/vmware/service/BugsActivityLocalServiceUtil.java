/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the bugs activity local service. This utility wraps {@link com.vmware.service.impl.BugsActivityLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see BugsActivityLocalService
 * @see com.vmware.service.base.BugsActivityLocalServiceBaseImpl
 * @see com.vmware.service.impl.BugsActivityLocalServiceImpl
 * @generated
 */
public class BugsActivityLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.vmware.service.impl.BugsActivityLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the bugs activity to the database. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @return the bugs activity that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity addBugsActivity(
		com.vmware.model.BugsActivity bugsActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addBugsActivity(bugsActivity);
	}

	/**
	* Creates a new bugs activity with the primary key. Does not add the bugs activity to the database.
	*
	* @param bug_activity_id the primary key for the new bugs activity
	* @return the new bugs activity
	*/
	public static com.vmware.model.BugsActivity createBugsActivity(
		int bug_activity_id) {
		return getService().createBugsActivity(bug_activity_id);
	}

	/**
	* Deletes the bugs activity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity that was removed
	* @throws PortalException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity deleteBugsActivity(
		int bug_activity_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBugsActivity(bug_activity_id);
	}

	/**
	* Deletes the bugs activity from the database. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @return the bugs activity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity deleteBugsActivity(
		com.vmware.model.BugsActivity bugsActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBugsActivity(bugsActivity);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.vmware.model.BugsActivity fetchBugsActivity(
		int bug_activity_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchBugsActivity(bug_activity_id);
	}

	/**
	* Returns the bugs activity with the primary key.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity
	* @throws PortalException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity getBugsActivity(
		int bug_activity_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getBugsActivity(bug_activity_id);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the bugs activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> getBugsActivities(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBugsActivities(start, end);
	}

	/**
	* Returns the number of bugs activities.
	*
	* @return the number of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static int getBugsActivitiesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBugsActivitiesCount();
	}

	/**
	* Updates the bugs activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @return the bugs activity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity updateBugsActivity(
		com.vmware.model.BugsActivity bugsActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBugsActivity(bugsActivity);
	}

	/**
	* Updates the bugs activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @param merge whether to merge the bugs activity with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the bugs activity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity updateBugsActivity(
		com.vmware.model.BugsActivity bugsActivity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBugsActivity(bugsActivity, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserId(
		int who) {
		return getService().getBugActivityByUserId(who);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugId(
		int bugId) {
		return getService().getBugActivityByBugId(bugId);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserIdAndFieldId(
		int userId, int fieldId) {
		return getService().getBugActivityByUserIdAndFieldId(userId, fieldId);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugActivityByAttachId(
		int attachId) {
		return getService().getBugActivityByBugActivityByAttachId(attachId);
	}

	public static void clearService() {
		_service = null;
	}

	public static BugsActivityLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BugsActivityLocalService.class.getName());

			if (invokableLocalService instanceof BugsActivityLocalService) {
				_service = (BugsActivityLocalService)invokableLocalService;
			}
			else {
				_service = new BugsActivityLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(BugsActivityLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(BugsActivityLocalService service) {
	}

	private static BugsActivityLocalService _service;
}