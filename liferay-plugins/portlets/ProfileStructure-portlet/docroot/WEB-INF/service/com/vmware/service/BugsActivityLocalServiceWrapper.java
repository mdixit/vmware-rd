/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BugsActivityLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       BugsActivityLocalService
 * @generated
 */
public class BugsActivityLocalServiceWrapper implements BugsActivityLocalService,
	ServiceWrapper<BugsActivityLocalService> {
	public BugsActivityLocalServiceWrapper(
		BugsActivityLocalService bugsActivityLocalService) {
		_bugsActivityLocalService = bugsActivityLocalService;
	}

	/**
	* Adds the bugs activity to the database. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @return the bugs activity that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugsActivity addBugsActivity(
		com.vmware.model.BugsActivity bugsActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.addBugsActivity(bugsActivity);
	}

	/**
	* Creates a new bugs activity with the primary key. Does not add the bugs activity to the database.
	*
	* @param bug_activity_id the primary key for the new bugs activity
	* @return the new bugs activity
	*/
	public com.vmware.model.BugsActivity createBugsActivity(int bug_activity_id) {
		return _bugsActivityLocalService.createBugsActivity(bug_activity_id);
	}

	/**
	* Deletes the bugs activity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity that was removed
	* @throws PortalException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugsActivity deleteBugsActivity(int bug_activity_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.deleteBugsActivity(bug_activity_id);
	}

	/**
	* Deletes the bugs activity from the database. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @return the bugs activity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugsActivity deleteBugsActivity(
		com.vmware.model.BugsActivity bugsActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.deleteBugsActivity(bugsActivity);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _bugsActivityLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.BugsActivity fetchBugsActivity(int bug_activity_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.fetchBugsActivity(bug_activity_id);
	}

	/**
	* Returns the bugs activity with the primary key.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity
	* @throws PortalException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugsActivity getBugsActivity(int bug_activity_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.getBugsActivity(bug_activity_id);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the bugs activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugsActivity> getBugsActivities(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.getBugsActivities(start, end);
	}

	/**
	* Returns the number of bugs activities.
	*
	* @return the number of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public int getBugsActivitiesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.getBugsActivitiesCount();
	}

	/**
	* Updates the bugs activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @return the bugs activity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugsActivity updateBugsActivity(
		com.vmware.model.BugsActivity bugsActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.updateBugsActivity(bugsActivity);
	}

	/**
	* Updates the bugs activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param bugsActivity the bugs activity
	* @param merge whether to merge the bugs activity with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the bugs activity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugsActivity updateBugsActivity(
		com.vmware.model.BugsActivity bugsActivity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _bugsActivityLocalService.updateBugsActivity(bugsActivity, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _bugsActivityLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_bugsActivityLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _bugsActivityLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserId(
		int who) {
		return _bugsActivityLocalService.getBugActivityByUserId(who);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugId(
		int bugId) {
		return _bugsActivityLocalService.getBugActivityByBugId(bugId);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserIdAndFieldId(
		int userId, int fieldId) {
		return _bugsActivityLocalService.getBugActivityByUserIdAndFieldId(userId,
			fieldId);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugActivityByAttachId(
		int attachId) {
		return _bugsActivityLocalService.getBugActivityByBugActivityByAttachId(attachId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BugsActivityLocalService getWrappedBugsActivityLocalService() {
		return _bugsActivityLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBugsActivityLocalService(
		BugsActivityLocalService bugsActivityLocalService) {
		_bugsActivityLocalService = bugsActivityLocalService;
	}

	public BugsActivityLocalService getWrappedService() {
		return _bugsActivityLocalService;
	}

	public void setWrappedService(
		BugsActivityLocalService bugsActivityLocalService) {
		_bugsActivityLocalService = bugsActivityLocalService;
	}

	private BugsActivityLocalService _bugsActivityLocalService;
}