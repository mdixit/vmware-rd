/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * The utility for the bugs activity remote service. This utility wraps {@link com.vmware.service.impl.BugsActivityServiceImpl} and is the primary access point for service operations in application layer code running on a remote server.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author iscisc
 * @see BugsActivityService
 * @see com.vmware.service.base.BugsActivityServiceBaseImpl
 * @see com.vmware.service.impl.BugsActivityServiceImpl
 * @generated
 */
public class BugsActivityServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.vmware.service.impl.BugsActivityServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserId(
		int who) {
		return getService().getBugActivityByUserId(who);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugId(
		int bugId) {
		return getService().getBugActivityByBugId(bugId);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserIdAndFieldId(
		int userId, int fieldId) {
		return getService().getBugActivityByUserIdAndFieldId(userId, fieldId);
	}

	public static java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugActivityByAttachId(
		int attachId) {
		return getService().getBugActivityByBugActivityByAttachId(attachId);
	}

	public static void clearService() {
		_service = null;
	}

	public static BugsActivityService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BugsActivityService.class.getName());

			if (invokableService instanceof BugsActivityService) {
				_service = (BugsActivityService)invokableService;
			}
			else {
				_service = new BugsActivityServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(BugsActivityServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(BugsActivityService service) {
	}

	private static BugsActivityService _service;
}