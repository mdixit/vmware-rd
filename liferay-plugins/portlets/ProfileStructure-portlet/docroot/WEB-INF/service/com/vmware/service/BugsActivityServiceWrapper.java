/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BugsActivityService}.
 * </p>
 *
 * @author    iscisc
 * @see       BugsActivityService
 * @generated
 */
public class BugsActivityServiceWrapper implements BugsActivityService,
	ServiceWrapper<BugsActivityService> {
	public BugsActivityServiceWrapper(BugsActivityService bugsActivityService) {
		_bugsActivityService = bugsActivityService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _bugsActivityService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_bugsActivityService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _bugsActivityService.invokeMethod(name, parameterTypes, arguments);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserId(
		int who) {
		return _bugsActivityService.getBugActivityByUserId(who);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugId(
		int bugId) {
		return _bugsActivityService.getBugActivityByBugId(bugId);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByUserIdAndFieldId(
		int userId, int fieldId) {
		return _bugsActivityService.getBugActivityByUserIdAndFieldId(userId,
			fieldId);
	}

	public java.util.List<com.vmware.model.BugsActivity> getBugActivityByBugActivityByAttachId(
		int attachId) {
		return _bugsActivityService.getBugActivityByBugActivityByAttachId(attachId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BugsActivityService getWrappedBugsActivityService() {
		return _bugsActivityService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBugsActivityService(
		BugsActivityService bugsActivityService) {
		_bugsActivityService = bugsActivityService;
	}

	public BugsActivityService getWrappedService() {
		return _bugsActivityService;
	}

	public void setWrappedService(BugsActivityService bugsActivityService) {
		_bugsActivityService = bugsActivityService;
	}

	private BugsActivityService _bugsActivityService;
}