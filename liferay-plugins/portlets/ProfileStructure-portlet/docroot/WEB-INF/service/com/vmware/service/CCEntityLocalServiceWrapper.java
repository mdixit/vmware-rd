/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CCEntityLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       CCEntityLocalService
 * @generated
 */
public class CCEntityLocalServiceWrapper implements CCEntityLocalService,
	ServiceWrapper<CCEntityLocalService> {
	public CCEntityLocalServiceWrapper(
		CCEntityLocalService ccEntityLocalService) {
		_ccEntityLocalService = ccEntityLocalService;
	}

	/**
	* Adds the c c entity to the database. Also notifies the appropriate model listeners.
	*
	* @param ccEntity the c c entity
	* @return the c c entity that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity addCCEntity(
		com.vmware.model.CCEntity ccEntity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.addCCEntity(ccEntity);
	}

	/**
	* Creates a new c c entity with the primary key. Does not add the c c entity to the database.
	*
	* @param ccEntityPK the primary key for the new c c entity
	* @return the new c c entity
	*/
	public com.vmware.model.CCEntity createCCEntity(
		com.vmware.service.persistence.CCEntityPK ccEntityPK) {
		return _ccEntityLocalService.createCCEntity(ccEntityPK);
	}

	/**
	* Deletes the c c entity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity that was removed
	* @throws PortalException if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity deleteCCEntity(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.deleteCCEntity(ccEntityPK);
	}

	/**
	* Deletes the c c entity from the database. Also notifies the appropriate model listeners.
	*
	* @param ccEntity the c c entity
	* @return the c c entity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity deleteCCEntity(
		com.vmware.model.CCEntity ccEntity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.deleteCCEntity(ccEntity);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ccEntityLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.CCEntity fetchCCEntity(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.fetchCCEntity(ccEntityPK);
	}

	/**
	* Returns the c c entity with the primary key.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity
	* @throws PortalException if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity getCCEntity(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.getCCEntity(ccEntityPK);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c c entities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of c c entities
	* @param end the upper bound of the range of c c entities (not inclusive)
	* @return the range of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.CCEntity> getCCEntities(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.getCCEntities(start, end);
	}

	/**
	* Returns the number of c c entities.
	*
	* @return the number of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public int getCCEntitiesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.getCCEntitiesCount();
	}

	/**
	* Updates the c c entity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ccEntity the c c entity
	* @return the c c entity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity updateCCEntity(
		com.vmware.model.CCEntity ccEntity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.updateCCEntity(ccEntity);
	}

	/**
	* Updates the c c entity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ccEntity the c c entity
	* @param merge whether to merge the c c entity with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the c c entity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity updateCCEntity(
		com.vmware.model.CCEntity ccEntity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ccEntityLocalService.updateCCEntity(ccEntity, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _ccEntityLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ccEntityLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ccEntityLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CCEntityLocalService getWrappedCCEntityLocalService() {
		return _ccEntityLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCCEntityLocalService(
		CCEntityLocalService ccEntityLocalService) {
		_ccEntityLocalService = ccEntityLocalService;
	}

	public CCEntityLocalService getWrappedService() {
		return _ccEntityLocalService;
	}

	public void setWrappedService(CCEntityLocalService ccEntityLocalService) {
		_ccEntityLocalService = ccEntityLocalService;
	}

	private CCEntityLocalService _ccEntityLocalService;
}