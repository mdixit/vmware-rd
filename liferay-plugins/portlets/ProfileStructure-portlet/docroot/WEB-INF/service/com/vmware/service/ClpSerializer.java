/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import com.vmware.model.AttachmentsClp;
import com.vmware.model.BugClp;
import com.vmware.model.BugSeverityClp;
import com.vmware.model.BugStatusClp;
import com.vmware.model.BugsActivityClp;
import com.vmware.model.CCEntityClp;
import com.vmware.model.ComponentsClp;
import com.vmware.model.DuplicateClp;
import com.vmware.model.FieldDefsClp;
import com.vmware.model.GroupClp;
import com.vmware.model.KeywordClp;
import com.vmware.model.KeywordDefsClp;
import com.vmware.model.LongDescriptionClp;
import com.vmware.model.ProductsClp;
import com.vmware.model.ProfileActivityClp;
import com.vmware.model.ProfilesClp;
import com.vmware.model.ResolutionClp;
import com.vmware.model.VoteClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"ProfileStructure-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"ProfileStructure-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "ProfileStructure-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AttachmentsClp.class.getName())) {
			return translateInputAttachments(oldModel);
		}

		if (oldModelClassName.equals(BugClp.class.getName())) {
			return translateInputBug(oldModel);
		}

		if (oldModelClassName.equals(BugsActivityClp.class.getName())) {
			return translateInputBugsActivity(oldModel);
		}

		if (oldModelClassName.equals(BugSeverityClp.class.getName())) {
			return translateInputBugSeverity(oldModel);
		}

		if (oldModelClassName.equals(BugStatusClp.class.getName())) {
			return translateInputBugStatus(oldModel);
		}

		if (oldModelClassName.equals(CCEntityClp.class.getName())) {
			return translateInputCCEntity(oldModel);
		}

		if (oldModelClassName.equals(ComponentsClp.class.getName())) {
			return translateInputComponents(oldModel);
		}

		if (oldModelClassName.equals(DuplicateClp.class.getName())) {
			return translateInputDuplicate(oldModel);
		}

		if (oldModelClassName.equals(FieldDefsClp.class.getName())) {
			return translateInputFieldDefs(oldModel);
		}

		if (oldModelClassName.equals(GroupClp.class.getName())) {
			return translateInputGroup(oldModel);
		}

		if (oldModelClassName.equals(KeywordClp.class.getName())) {
			return translateInputKeyword(oldModel);
		}

		if (oldModelClassName.equals(KeywordDefsClp.class.getName())) {
			return translateInputKeywordDefs(oldModel);
		}

		if (oldModelClassName.equals(LongDescriptionClp.class.getName())) {
			return translateInputLongDescription(oldModel);
		}

		if (oldModelClassName.equals(ProductsClp.class.getName())) {
			return translateInputProducts(oldModel);
		}

		if (oldModelClassName.equals(ProfileActivityClp.class.getName())) {
			return translateInputProfileActivity(oldModel);
		}

		if (oldModelClassName.equals(ProfilesClp.class.getName())) {
			return translateInputProfiles(oldModel);
		}

		if (oldModelClassName.equals(ResolutionClp.class.getName())) {
			return translateInputResolution(oldModel);
		}

		if (oldModelClassName.equals(VoteClp.class.getName())) {
			return translateInputVote(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAttachments(BaseModel<?> oldModel) {
		AttachmentsClp oldClpModel = (AttachmentsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAttachmentsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBug(BaseModel<?> oldModel) {
		BugClp oldClpModel = (BugClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBugRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBugsActivity(BaseModel<?> oldModel) {
		BugsActivityClp oldClpModel = (BugsActivityClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBugsActivityRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBugSeverity(BaseModel<?> oldModel) {
		BugSeverityClp oldClpModel = (BugSeverityClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBugSeverityRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBugStatus(BaseModel<?> oldModel) {
		BugStatusClp oldClpModel = (BugStatusClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBugStatusRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCCEntity(BaseModel<?> oldModel) {
		CCEntityClp oldClpModel = (CCEntityClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCCEntityRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputComponents(BaseModel<?> oldModel) {
		ComponentsClp oldClpModel = (ComponentsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getComponentsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDuplicate(BaseModel<?> oldModel) {
		DuplicateClp oldClpModel = (DuplicateClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDuplicateRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputFieldDefs(BaseModel<?> oldModel) {
		FieldDefsClp oldClpModel = (FieldDefsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getFieldDefsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputGroup(BaseModel<?> oldModel) {
		GroupClp oldClpModel = (GroupClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getGroupRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputKeyword(BaseModel<?> oldModel) {
		KeywordClp oldClpModel = (KeywordClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getKeywordRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputKeywordDefs(BaseModel<?> oldModel) {
		KeywordDefsClp oldClpModel = (KeywordDefsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getKeywordDefsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLongDescription(BaseModel<?> oldModel) {
		LongDescriptionClp oldClpModel = (LongDescriptionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLongDescriptionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputProducts(BaseModel<?> oldModel) {
		ProductsClp oldClpModel = (ProductsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getProductsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputProfileActivity(BaseModel<?> oldModel) {
		ProfileActivityClp oldClpModel = (ProfileActivityClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getProfileActivityRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputProfiles(BaseModel<?> oldModel) {
		ProfilesClp oldClpModel = (ProfilesClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getProfilesRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputResolution(BaseModel<?> oldModel) {
		ResolutionClp oldClpModel = (ResolutionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getResolutionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputVote(BaseModel<?> oldModel) {
		VoteClp oldClpModel = (VoteClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getVoteRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals("com.vmware.model.impl.AttachmentsImpl")) {
			return translateOutputAttachments(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.BugImpl")) {
			return translateOutputBug(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.BugsActivityImpl")) {
			return translateOutputBugsActivity(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.BugSeverityImpl")) {
			return translateOutputBugSeverity(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.BugStatusImpl")) {
			return translateOutputBugStatus(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.CCEntityImpl")) {
			return translateOutputCCEntity(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.ComponentsImpl")) {
			return translateOutputComponents(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.DuplicateImpl")) {
			return translateOutputDuplicate(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.FieldDefsImpl")) {
			return translateOutputFieldDefs(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.GroupImpl")) {
			return translateOutputGroup(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.KeywordImpl")) {
			return translateOutputKeyword(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.KeywordDefsImpl")) {
			return translateOutputKeywordDefs(oldModel);
		}

		if (oldModelClassName.equals(
					"com.vmware.model.impl.LongDescriptionImpl")) {
			return translateOutputLongDescription(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.ProductsImpl")) {
			return translateOutputProducts(oldModel);
		}

		if (oldModelClassName.equals(
					"com.vmware.model.impl.ProfileActivityImpl")) {
			return translateOutputProfileActivity(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.ProfilesImpl")) {
			return translateOutputProfiles(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.ResolutionImpl")) {
			return translateOutputResolution(oldModel);
		}

		if (oldModelClassName.equals("com.vmware.model.impl.VoteImpl")) {
			return translateOutputVote(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals("com.vmware.NoSuchAttachmentsException")) {
			return new com.vmware.NoSuchAttachmentsException();
		}

		if (className.equals("com.vmware.NoSuchBugException")) {
			return new com.vmware.NoSuchBugException();
		}

		if (className.equals("com.vmware.NoSuchBugsActivityException")) {
			return new com.vmware.NoSuchBugsActivityException();
		}

		if (className.equals("com.vmware.NoSuchBugSeverityException")) {
			return new com.vmware.NoSuchBugSeverityException();
		}

		if (className.equals("com.vmware.NoSuchBugStatusException")) {
			return new com.vmware.NoSuchBugStatusException();
		}

		if (className.equals("com.vmware.NoSuchCCEntityException")) {
			return new com.vmware.NoSuchCCEntityException();
		}

		if (className.equals("com.vmware.NoSuchComponentsException")) {
			return new com.vmware.NoSuchComponentsException();
		}

		if (className.equals("com.vmware.NoSuchDuplicateException")) {
			return new com.vmware.NoSuchDuplicateException();
		}

		if (className.equals("com.vmware.NoSuchFieldDefsException")) {
			return new com.vmware.NoSuchFieldDefsException();
		}

		if (className.equals("com.vmware.NoSuchGroupException")) {
			return new com.vmware.NoSuchGroupException();
		}

		if (className.equals("com.vmware.NoSuchKeywordException")) {
			return new com.vmware.NoSuchKeywordException();
		}

		if (className.equals("com.vmware.NoSuchKeywordDefsException")) {
			return new com.vmware.NoSuchKeywordDefsException();
		}

		if (className.equals("com.vmware.NoSuchLongDescriptionException")) {
			return new com.vmware.NoSuchLongDescriptionException();
		}

		if (className.equals("com.vmware.NoSuchProductsException")) {
			return new com.vmware.NoSuchProductsException();
		}

		if (className.equals("com.vmware.NoSuchProfileActivityException")) {
			return new com.vmware.NoSuchProfileActivityException();
		}

		if (className.equals("com.vmware.NoSuchProfilesException")) {
			return new com.vmware.NoSuchProfilesException();
		}

		if (className.equals("com.vmware.NoSuchResolutionException")) {
			return new com.vmware.NoSuchResolutionException();
		}

		if (className.equals("com.vmware.NoSuchVoteException")) {
			return new com.vmware.NoSuchVoteException();
		}

		return throwable;
	}

	public static Object translateOutputAttachments(BaseModel<?> oldModel) {
		AttachmentsClp newModel = new AttachmentsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAttachmentsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBug(BaseModel<?> oldModel) {
		BugClp newModel = new BugClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBugRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBugsActivity(BaseModel<?> oldModel) {
		BugsActivityClp newModel = new BugsActivityClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBugsActivityRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBugSeverity(BaseModel<?> oldModel) {
		BugSeverityClp newModel = new BugSeverityClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBugSeverityRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBugStatus(BaseModel<?> oldModel) {
		BugStatusClp newModel = new BugStatusClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBugStatusRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCCEntity(BaseModel<?> oldModel) {
		CCEntityClp newModel = new CCEntityClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCCEntityRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputComponents(BaseModel<?> oldModel) {
		ComponentsClp newModel = new ComponentsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setComponentsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDuplicate(BaseModel<?> oldModel) {
		DuplicateClp newModel = new DuplicateClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDuplicateRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputFieldDefs(BaseModel<?> oldModel) {
		FieldDefsClp newModel = new FieldDefsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setFieldDefsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputGroup(BaseModel<?> oldModel) {
		GroupClp newModel = new GroupClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setGroupRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputKeyword(BaseModel<?> oldModel) {
		KeywordClp newModel = new KeywordClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setKeywordRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputKeywordDefs(BaseModel<?> oldModel) {
		KeywordDefsClp newModel = new KeywordDefsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setKeywordDefsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLongDescription(BaseModel<?> oldModel) {
		LongDescriptionClp newModel = new LongDescriptionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLongDescriptionRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputProducts(BaseModel<?> oldModel) {
		ProductsClp newModel = new ProductsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setProductsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputProfileActivity(BaseModel<?> oldModel) {
		ProfileActivityClp newModel = new ProfileActivityClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setProfileActivityRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputProfiles(BaseModel<?> oldModel) {
		ProfilesClp newModel = new ProfilesClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setProfilesRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputResolution(BaseModel<?> oldModel) {
		ResolutionClp newModel = new ResolutionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setResolutionRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputVote(BaseModel<?> oldModel) {
		VoteClp newModel = new VoteClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setVoteRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}