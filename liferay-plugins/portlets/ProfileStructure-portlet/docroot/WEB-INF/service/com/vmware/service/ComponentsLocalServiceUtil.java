/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the components local service. This utility wraps {@link com.vmware.service.impl.ComponentsLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see ComponentsLocalService
 * @see com.vmware.service.base.ComponentsLocalServiceBaseImpl
 * @see com.vmware.service.impl.ComponentsLocalServiceImpl
 * @generated
 */
public class ComponentsLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.vmware.service.impl.ComponentsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the components to the database. Also notifies the appropriate model listeners.
	*
	* @param components the components
	* @return the components that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components addComponents(
		com.vmware.model.Components components)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addComponents(components);
	}

	/**
	* Creates a new components with the primary key. Does not add the components to the database.
	*
	* @param comp_id the primary key for the new components
	* @return the new components
	*/
	public static com.vmware.model.Components createComponents(int comp_id) {
		return getService().createComponents(comp_id);
	}

	/**
	* Deletes the components with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param comp_id the primary key of the components
	* @return the components that was removed
	* @throws PortalException if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components deleteComponents(int comp_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteComponents(comp_id);
	}

	/**
	* Deletes the components from the database. Also notifies the appropriate model listeners.
	*
	* @param components the components
	* @return the components that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components deleteComponents(
		com.vmware.model.Components components)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteComponents(components);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.vmware.model.Components fetchComponents(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchComponents(comp_id);
	}

	/**
	* Returns the components with the primary key.
	*
	* @param comp_id the primary key of the components
	* @return the components
	* @throws PortalException if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components getComponents(int comp_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getComponents(comp_id);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the componentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @return the range of componentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Components> getComponentses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getComponentses(start, end);
	}

	/**
	* Returns the number of componentses.
	*
	* @return the number of componentses
	* @throws SystemException if a system exception occurred
	*/
	public static int getComponentsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getComponentsesCount();
	}

	/**
	* Updates the components in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param components the components
	* @return the components that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components updateComponents(
		com.vmware.model.Components components)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateComponents(components);
	}

	/**
	* Updates the components in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param components the components
	* @param merge whether to merge the components with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the components that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components updateComponents(
		com.vmware.model.Components components, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateComponents(components, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void clearService() {
		_service = null;
	}

	public static ComponentsLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ComponentsLocalService.class.getName());

			if (invokableLocalService instanceof ComponentsLocalService) {
				_service = (ComponentsLocalService)invokableLocalService;
			}
			else {
				_service = new ComponentsLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ComponentsLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(ComponentsLocalService service) {
	}

	private static ComponentsLocalService _service;
}