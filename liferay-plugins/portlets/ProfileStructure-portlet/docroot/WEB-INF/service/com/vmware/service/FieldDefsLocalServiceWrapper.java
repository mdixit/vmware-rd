/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link FieldDefsLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       FieldDefsLocalService
 * @generated
 */
public class FieldDefsLocalServiceWrapper implements FieldDefsLocalService,
	ServiceWrapper<FieldDefsLocalService> {
	public FieldDefsLocalServiceWrapper(
		FieldDefsLocalService fieldDefsLocalService) {
		_fieldDefsLocalService = fieldDefsLocalService;
	}

	/**
	* Adds the field defs to the database. Also notifies the appropriate model listeners.
	*
	* @param fieldDefs the field defs
	* @return the field defs that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs addFieldDefs(
		com.vmware.model.FieldDefs fieldDefs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.addFieldDefs(fieldDefs);
	}

	/**
	* Creates a new field defs with the primary key. Does not add the field defs to the database.
	*
	* @param fielddef_id the primary key for the new field defs
	* @return the new field defs
	*/
	public com.vmware.model.FieldDefs createFieldDefs(int fielddef_id) {
		return _fieldDefsLocalService.createFieldDefs(fielddef_id);
	}

	/**
	* Deletes the field defs with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param fielddef_id the primary key of the field defs
	* @return the field defs that was removed
	* @throws PortalException if a field defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs deleteFieldDefs(int fielddef_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.deleteFieldDefs(fielddef_id);
	}

	/**
	* Deletes the field defs from the database. Also notifies the appropriate model listeners.
	*
	* @param fieldDefs the field defs
	* @return the field defs that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs deleteFieldDefs(
		com.vmware.model.FieldDefs fieldDefs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.deleteFieldDefs(fieldDefs);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _fieldDefsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.FieldDefs fetchFieldDefs(int fielddef_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.fetchFieldDefs(fielddef_id);
	}

	/**
	* Returns the field defs with the primary key.
	*
	* @param fielddef_id the primary key of the field defs
	* @return the field defs
	* @throws PortalException if a field defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs getFieldDefs(int fielddef_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.getFieldDefs(fielddef_id);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the field defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of field defses
	* @param end the upper bound of the range of field defses (not inclusive)
	* @return the range of field defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.FieldDefs> getFieldDefses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.getFieldDefses(start, end);
	}

	/**
	* Returns the number of field defses.
	*
	* @return the number of field defses
	* @throws SystemException if a system exception occurred
	*/
	public int getFieldDefsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.getFieldDefsesCount();
	}

	/**
	* Updates the field defs in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param fieldDefs the field defs
	* @return the field defs that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs updateFieldDefs(
		com.vmware.model.FieldDefs fieldDefs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.updateFieldDefs(fieldDefs);
	}

	/**
	* Updates the field defs in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param fieldDefs the field defs
	* @param merge whether to merge the field defs with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the field defs that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs updateFieldDefs(
		com.vmware.model.FieldDefs fieldDefs, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _fieldDefsLocalService.updateFieldDefs(fieldDefs, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _fieldDefsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_fieldDefsLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _fieldDefsLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public FieldDefsLocalService getWrappedFieldDefsLocalService() {
		return _fieldDefsLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedFieldDefsLocalService(
		FieldDefsLocalService fieldDefsLocalService) {
		_fieldDefsLocalService = fieldDefsLocalService;
	}

	public FieldDefsLocalService getWrappedService() {
		return _fieldDefsLocalService;
	}

	public void setWrappedService(FieldDefsLocalService fieldDefsLocalService) {
		_fieldDefsLocalService = fieldDefsLocalService;
	}

	private FieldDefsLocalService _fieldDefsLocalService;
}