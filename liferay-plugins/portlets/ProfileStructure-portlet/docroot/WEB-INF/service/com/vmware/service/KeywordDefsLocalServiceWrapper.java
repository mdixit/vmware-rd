/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link KeywordDefsLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       KeywordDefsLocalService
 * @generated
 */
public class KeywordDefsLocalServiceWrapper implements KeywordDefsLocalService,
	ServiceWrapper<KeywordDefsLocalService> {
	public KeywordDefsLocalServiceWrapper(
		KeywordDefsLocalService keywordDefsLocalService) {
		_keywordDefsLocalService = keywordDefsLocalService;
	}

	/**
	* Adds the keyword defs to the database. Also notifies the appropriate model listeners.
	*
	* @param keywordDefs the keyword defs
	* @return the keyword defs that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs addKeywordDefs(
		com.vmware.model.KeywordDefs keywordDefs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.addKeywordDefs(keywordDefs);
	}

	/**
	* Creates a new keyword defs with the primary key. Does not add the keyword defs to the database.
	*
	* @param key_def_id the primary key for the new keyword defs
	* @return the new keyword defs
	*/
	public com.vmware.model.KeywordDefs createKeywordDefs(int key_def_id) {
		return _keywordDefsLocalService.createKeywordDefs(key_def_id);
	}

	/**
	* Deletes the keyword defs with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs that was removed
	* @throws PortalException if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs deleteKeywordDefs(int key_def_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.deleteKeywordDefs(key_def_id);
	}

	/**
	* Deletes the keyword defs from the database. Also notifies the appropriate model listeners.
	*
	* @param keywordDefs the keyword defs
	* @return the keyword defs that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs deleteKeywordDefs(
		com.vmware.model.KeywordDefs keywordDefs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.deleteKeywordDefs(keywordDefs);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _keywordDefsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.KeywordDefs fetchKeywordDefs(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.fetchKeywordDefs(key_def_id);
	}

	/**
	* Returns the keyword defs with the primary key.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs
	* @throws PortalException if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs getKeywordDefs(int key_def_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.getKeywordDefs(key_def_id);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the keyword defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keyword defses
	* @param end the upper bound of the range of keyword defses (not inclusive)
	* @return the range of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.KeywordDefs> getKeywordDefses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.getKeywordDefses(start, end);
	}

	/**
	* Returns the number of keyword defses.
	*
	* @return the number of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public int getKeywordDefsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.getKeywordDefsesCount();
	}

	/**
	* Updates the keyword defs in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param keywordDefs the keyword defs
	* @return the keyword defs that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs updateKeywordDefs(
		com.vmware.model.KeywordDefs keywordDefs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.updateKeywordDefs(keywordDefs);
	}

	/**
	* Updates the keyword defs in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param keywordDefs the keyword defs
	* @param merge whether to merge the keyword defs with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the keyword defs that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs updateKeywordDefs(
		com.vmware.model.KeywordDefs keywordDefs, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordDefsLocalService.updateKeywordDefs(keywordDefs, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _keywordDefsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_keywordDefsLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _keywordDefsLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public KeywordDefsLocalService getWrappedKeywordDefsLocalService() {
		return _keywordDefsLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedKeywordDefsLocalService(
		KeywordDefsLocalService keywordDefsLocalService) {
		_keywordDefsLocalService = keywordDefsLocalService;
	}

	public KeywordDefsLocalService getWrappedService() {
		return _keywordDefsLocalService;
	}

	public void setWrappedService(
		KeywordDefsLocalService keywordDefsLocalService) {
		_keywordDefsLocalService = keywordDefsLocalService;
	}

	private KeywordDefsLocalService _keywordDefsLocalService;
}