/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the keyword local service. This utility wraps {@link com.vmware.service.impl.KeywordLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see KeywordLocalService
 * @see com.vmware.service.base.KeywordLocalServiceBaseImpl
 * @see com.vmware.service.impl.KeywordLocalServiceImpl
 * @generated
 */
public class KeywordLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.vmware.service.impl.KeywordLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the keyword to the database. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @return the keyword that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword addKeyword(
		com.vmware.model.Keyword keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addKeyword(keyword);
	}

	/**
	* Creates a new keyword with the primary key. Does not add the keyword to the database.
	*
	* @param keywordPK the primary key for the new keyword
	* @return the new keyword
	*/
	public static com.vmware.model.Keyword createKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK) {
		return getService().createKeyword(keywordPK);
	}

	/**
	* Deletes the keyword with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword that was removed
	* @throws PortalException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword deleteKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteKeyword(keywordPK);
	}

	/**
	* Deletes the keyword from the database. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @return the keyword that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword deleteKeyword(
		com.vmware.model.Keyword keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteKeyword(keyword);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.vmware.model.Keyword fetchKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchKeyword(keywordPK);
	}

	/**
	* Returns the keyword with the primary key.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword
	* @throws PortalException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword getKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getKeyword(keywordPK);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the keywords.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keywords
	* @param end the upper bound of the range of keywords (not inclusive)
	* @return the range of keywords
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Keyword> getKeywords(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getKeywords(start, end);
	}

	/**
	* Returns the number of keywords.
	*
	* @return the number of keywords
	* @throws SystemException if a system exception occurred
	*/
	public static int getKeywordsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getKeywordsCount();
	}

	/**
	* Updates the keyword in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @return the keyword that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword updateKeyword(
		com.vmware.model.Keyword keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateKeyword(keyword);
	}

	/**
	* Updates the keyword in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @param merge whether to merge the keyword with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the keyword that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword updateKeyword(
		com.vmware.model.Keyword keyword, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateKeyword(keyword, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void clearService() {
		_service = null;
	}

	public static KeywordLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					KeywordLocalService.class.getName());

			if (invokableLocalService instanceof KeywordLocalService) {
				_service = (KeywordLocalService)invokableLocalService;
			}
			else {
				_service = new KeywordLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(KeywordLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(KeywordLocalService service) {
	}

	private static KeywordLocalService _service;
}