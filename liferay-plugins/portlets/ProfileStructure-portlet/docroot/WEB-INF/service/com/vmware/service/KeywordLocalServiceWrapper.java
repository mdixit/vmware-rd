/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link KeywordLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       KeywordLocalService
 * @generated
 */
public class KeywordLocalServiceWrapper implements KeywordLocalService,
	ServiceWrapper<KeywordLocalService> {
	public KeywordLocalServiceWrapper(KeywordLocalService keywordLocalService) {
		_keywordLocalService = keywordLocalService;
	}

	/**
	* Adds the keyword to the database. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @return the keyword that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword addKeyword(com.vmware.model.Keyword keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.addKeyword(keyword);
	}

	/**
	* Creates a new keyword with the primary key. Does not add the keyword to the database.
	*
	* @param keywordPK the primary key for the new keyword
	* @return the new keyword
	*/
	public com.vmware.model.Keyword createKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK) {
		return _keywordLocalService.createKeyword(keywordPK);
	}

	/**
	* Deletes the keyword with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword that was removed
	* @throws PortalException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword deleteKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.deleteKeyword(keywordPK);
	}

	/**
	* Deletes the keyword from the database. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @return the keyword that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword deleteKeyword(
		com.vmware.model.Keyword keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.deleteKeyword(keyword);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _keywordLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.Keyword fetchKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.fetchKeyword(keywordPK);
	}

	/**
	* Returns the keyword with the primary key.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword
	* @throws PortalException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword getKeyword(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.getKeyword(keywordPK);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the keywords.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keywords
	* @param end the upper bound of the range of keywords (not inclusive)
	* @return the range of keywords
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Keyword> getKeywords(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.getKeywords(start, end);
	}

	/**
	* Returns the number of keywords.
	*
	* @return the number of keywords
	* @throws SystemException if a system exception occurred
	*/
	public int getKeywordsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.getKeywordsCount();
	}

	/**
	* Updates the keyword in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @return the keyword that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword updateKeyword(
		com.vmware.model.Keyword keyword)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.updateKeyword(keyword);
	}

	/**
	* Updates the keyword in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param keyword the keyword
	* @param merge whether to merge the keyword with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the keyword that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword updateKeyword(
		com.vmware.model.Keyword keyword, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _keywordLocalService.updateKeyword(keyword, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _keywordLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_keywordLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _keywordLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public KeywordLocalService getWrappedKeywordLocalService() {
		return _keywordLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedKeywordLocalService(
		KeywordLocalService keywordLocalService) {
		_keywordLocalService = keywordLocalService;
	}

	public KeywordLocalService getWrappedService() {
		return _keywordLocalService;
	}

	public void setWrappedService(KeywordLocalService keywordLocalService) {
		_keywordLocalService = keywordLocalService;
	}

	private KeywordLocalService _keywordLocalService;
}