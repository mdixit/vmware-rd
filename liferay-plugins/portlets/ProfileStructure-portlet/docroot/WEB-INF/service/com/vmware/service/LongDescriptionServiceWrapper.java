/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LongDescriptionService}.
 * </p>
 *
 * @author    iscisc
 * @see       LongDescriptionService
 * @generated
 */
public class LongDescriptionServiceWrapper implements LongDescriptionService,
	ServiceWrapper<LongDescriptionService> {
	public LongDescriptionServiceWrapper(
		LongDescriptionService longDescriptionService) {
		_longDescriptionService = longDescriptionService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _longDescriptionService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_longDescriptionService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _longDescriptionService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<com.vmware.model.LongDescription> getLongDescByBugId(
		int bug_id) {
		return _longDescriptionService.getLongDescByBugId(bug_id);
	}

	public java.util.List<com.vmware.model.LongDescription> getLongDescByUserId(
		int who) {
		return _longDescriptionService.getLongDescByUserId(who);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LongDescriptionService getWrappedLongDescriptionService() {
		return _longDescriptionService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLongDescriptionService(
		LongDescriptionService longDescriptionService) {
		_longDescriptionService = longDescriptionService;
	}

	public LongDescriptionService getWrappedService() {
		return _longDescriptionService;
	}

	public void setWrappedService(LongDescriptionService longDescriptionService) {
		_longDescriptionService = longDescriptionService;
	}

	private LongDescriptionService _longDescriptionService;
}