/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ProfileActivityLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       ProfileActivityLocalService
 * @generated
 */
public class ProfileActivityLocalServiceWrapper
	implements ProfileActivityLocalService,
		ServiceWrapper<ProfileActivityLocalService> {
	public ProfileActivityLocalServiceWrapper(
		ProfileActivityLocalService profileActivityLocalService) {
		_profileActivityLocalService = profileActivityLocalService;
	}

	/**
	* Adds the profile activity to the database. Also notifies the appropriate model listeners.
	*
	* @param profileActivity the profile activity
	* @return the profile activity that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity addProfileActivity(
		com.vmware.model.ProfileActivity profileActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.addProfileActivity(profileActivity);
	}

	/**
	* Creates a new profile activity with the primary key. Does not add the profile activity to the database.
	*
	* @param profileActivityPK the primary key for the new profile activity
	* @return the new profile activity
	*/
	public com.vmware.model.ProfileActivity createProfileActivity(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK) {
		return _profileActivityLocalService.createProfileActivity(profileActivityPK);
	}

	/**
	* Deletes the profile activity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param profileActivityPK the primary key of the profile activity
	* @return the profile activity that was removed
	* @throws PortalException if a profile activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity deleteProfileActivity(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.deleteProfileActivity(profileActivityPK);
	}

	/**
	* Deletes the profile activity from the database. Also notifies the appropriate model listeners.
	*
	* @param profileActivity the profile activity
	* @return the profile activity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity deleteProfileActivity(
		com.vmware.model.ProfileActivity profileActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.deleteProfileActivity(profileActivity);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _profileActivityLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.ProfileActivity fetchProfileActivity(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.fetchProfileActivity(profileActivityPK);
	}

	/**
	* Returns the profile activity with the primary key.
	*
	* @param profileActivityPK the primary key of the profile activity
	* @return the profile activity
	* @throws PortalException if a profile activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity getProfileActivity(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.getProfileActivity(profileActivityPK);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the profile activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profile activities
	* @param end the upper bound of the range of profile activities (not inclusive)
	* @return the range of profile activities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.ProfileActivity> getProfileActivities(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.getProfileActivities(start, end);
	}

	/**
	* Returns the number of profile activities.
	*
	* @return the number of profile activities
	* @throws SystemException if a system exception occurred
	*/
	public int getProfileActivitiesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.getProfileActivitiesCount();
	}

	/**
	* Updates the profile activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param profileActivity the profile activity
	* @return the profile activity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity updateProfileActivity(
		com.vmware.model.ProfileActivity profileActivity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.updateProfileActivity(profileActivity);
	}

	/**
	* Updates the profile activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param profileActivity the profile activity
	* @param merge whether to merge the profile activity with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the profile activity that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity updateProfileActivity(
		com.vmware.model.ProfileActivity profileActivity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _profileActivityLocalService.updateProfileActivity(profileActivity,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _profileActivityLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_profileActivityLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _profileActivityLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public com.vmware.model.ProfileActivity getProfileActivityByUserId(
		int userId) {
		return _profileActivityLocalService.getProfileActivityByUserId(userId);
	}

	public com.vmware.model.FieldDefs getFieldDefsByFieldId(int fieldId) {
		return _profileActivityLocalService.getFieldDefsByFieldId(fieldId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public ProfileActivityLocalService getWrappedProfileActivityLocalService() {
		return _profileActivityLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedProfileActivityLocalService(
		ProfileActivityLocalService profileActivityLocalService) {
		_profileActivityLocalService = profileActivityLocalService;
	}

	public ProfileActivityLocalService getWrappedService() {
		return _profileActivityLocalService;
	}

	public void setWrappedService(
		ProfileActivityLocalService profileActivityLocalService) {
		_profileActivityLocalService = profileActivityLocalService;
	}

	private ProfileActivityLocalService _profileActivityLocalService;
}