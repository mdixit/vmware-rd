/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ResolutionLocalService}.
 * </p>
 *
 * @author    iscisc
 * @see       ResolutionLocalService
 * @generated
 */
public class ResolutionLocalServiceWrapper implements ResolutionLocalService,
	ServiceWrapper<ResolutionLocalService> {
	public ResolutionLocalServiceWrapper(
		ResolutionLocalService resolutionLocalService) {
		_resolutionLocalService = resolutionLocalService;
	}

	/**
	* Adds the resolution to the database. Also notifies the appropriate model listeners.
	*
	* @param resolution the resolution
	* @return the resolution that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution addResolution(
		com.vmware.model.Resolution resolution)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.addResolution(resolution);
	}

	/**
	* Creates a new resolution with the primary key. Does not add the resolution to the database.
	*
	* @param res_id the primary key for the new resolution
	* @return the new resolution
	*/
	public com.vmware.model.Resolution createResolution(int res_id) {
		return _resolutionLocalService.createResolution(res_id);
	}

	/**
	* Deletes the resolution with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution that was removed
	* @throws PortalException if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution deleteResolution(int res_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.deleteResolution(res_id);
	}

	/**
	* Deletes the resolution from the database. Also notifies the appropriate model listeners.
	*
	* @param resolution the resolution
	* @return the resolution that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution deleteResolution(
		com.vmware.model.Resolution resolution)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.deleteResolution(resolution);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _resolutionLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.vmware.model.Resolution fetchResolution(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.fetchResolution(res_id);
	}

	/**
	* Returns the resolution with the primary key.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution
	* @throws PortalException if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution getResolution(int res_id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.getResolution(res_id);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the resolutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of resolutions
	* @param end the upper bound of the range of resolutions (not inclusive)
	* @return the range of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Resolution> getResolutions(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.getResolutions(start, end);
	}

	/**
	* Returns the number of resolutions.
	*
	* @return the number of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public int getResolutionsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.getResolutionsCount();
	}

	/**
	* Updates the resolution in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param resolution the resolution
	* @return the resolution that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution updateResolution(
		com.vmware.model.Resolution resolution)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.updateResolution(resolution);
	}

	/**
	* Updates the resolution in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param resolution the resolution
	* @param merge whether to merge the resolution with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the resolution that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution updateResolution(
		com.vmware.model.Resolution resolution, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _resolutionLocalService.updateResolution(resolution, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _resolutionLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_resolutionLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _resolutionLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public ResolutionLocalService getWrappedResolutionLocalService() {
		return _resolutionLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedResolutionLocalService(
		ResolutionLocalService resolutionLocalService) {
		_resolutionLocalService = resolutionLocalService;
	}

	public ResolutionLocalService getWrappedService() {
		return _resolutionLocalService;
	}

	public void setWrappedService(ResolutionLocalService resolutionLocalService) {
		_resolutionLocalService = resolutionLocalService;
	}

	private ResolutionLocalService _resolutionLocalService;
}