/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import com.vmware.service.AttachmentsLocalServiceUtil;
import com.vmware.service.AttachmentsServiceUtil;
import com.vmware.service.BugLocalServiceUtil;
import com.vmware.service.BugServiceUtil;
import com.vmware.service.BugSeverityLocalServiceUtil;
import com.vmware.service.BugSeverityServiceUtil;
import com.vmware.service.BugStatusLocalServiceUtil;
import com.vmware.service.BugStatusServiceUtil;
import com.vmware.service.BugsActivityLocalServiceUtil;
import com.vmware.service.BugsActivityServiceUtil;
import com.vmware.service.CCEntityLocalServiceUtil;
import com.vmware.service.CCEntityServiceUtil;
import com.vmware.service.ClpSerializer;
import com.vmware.service.ComponentsLocalServiceUtil;
import com.vmware.service.ComponentsServiceUtil;
import com.vmware.service.DuplicateLocalServiceUtil;
import com.vmware.service.DuplicateServiceUtil;
import com.vmware.service.FieldDefsLocalServiceUtil;
import com.vmware.service.FieldDefsServiceUtil;
import com.vmware.service.GroupLocalServiceUtil;
import com.vmware.service.GroupServiceUtil;
import com.vmware.service.KeywordDefsLocalServiceUtil;
import com.vmware.service.KeywordDefsServiceUtil;
import com.vmware.service.KeywordLocalServiceUtil;
import com.vmware.service.KeywordServiceUtil;
import com.vmware.service.LongDescriptionLocalServiceUtil;
import com.vmware.service.LongDescriptionServiceUtil;
import com.vmware.service.ProductsLocalServiceUtil;
import com.vmware.service.ProductsServiceUtil;
import com.vmware.service.ProfileActivityLocalServiceUtil;
import com.vmware.service.ProfileActivityServiceUtil;
import com.vmware.service.ProfilesLocalServiceUtil;
import com.vmware.service.ProfilesServiceUtil;
import com.vmware.service.ResolutionLocalServiceUtil;
import com.vmware.service.ResolutionServiceUtil;
import com.vmware.service.VoteLocalServiceUtil;
import com.vmware.service.VoteServiceUtil;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			AttachmentsLocalServiceUtil.clearService();

			AttachmentsServiceUtil.clearService();
			BugLocalServiceUtil.clearService();

			BugServiceUtil.clearService();
			BugsActivityLocalServiceUtil.clearService();

			BugsActivityServiceUtil.clearService();
			BugSeverityLocalServiceUtil.clearService();

			BugSeverityServiceUtil.clearService();
			BugStatusLocalServiceUtil.clearService();

			BugStatusServiceUtil.clearService();
			CCEntityLocalServiceUtil.clearService();

			CCEntityServiceUtil.clearService();
			ComponentsLocalServiceUtil.clearService();

			ComponentsServiceUtil.clearService();
			DuplicateLocalServiceUtil.clearService();

			DuplicateServiceUtil.clearService();
			FieldDefsLocalServiceUtil.clearService();

			FieldDefsServiceUtil.clearService();
			GroupLocalServiceUtil.clearService();

			GroupServiceUtil.clearService();
			KeywordLocalServiceUtil.clearService();

			KeywordServiceUtil.clearService();
			KeywordDefsLocalServiceUtil.clearService();

			KeywordDefsServiceUtil.clearService();
			LongDescriptionLocalServiceUtil.clearService();

			LongDescriptionServiceUtil.clearService();
			ProductsLocalServiceUtil.clearService();

			ProductsServiceUtil.clearService();
			ProfileActivityLocalServiceUtil.clearService();

			ProfileActivityServiceUtil.clearService();
			ProfilesLocalServiceUtil.clearService();

			ProfilesServiceUtil.clearService();
			ResolutionLocalServiceUtil.clearService();

			ResolutionServiceUtil.clearService();
			VoteLocalServiceUtil.clearService();

			VoteServiceUtil.clearService();
		}
	}
}