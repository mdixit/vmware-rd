/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Attachments;

/**
 * The persistence interface for the attachments service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see AttachmentsPersistenceImpl
 * @see AttachmentsUtil
 * @generated
 */
public interface AttachmentsPersistence extends BasePersistence<Attachments> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AttachmentsUtil} to access the attachments persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the attachments in the entity cache if it is enabled.
	*
	* @param attachments the attachments
	*/
	public void cacheResult(com.vmware.model.Attachments attachments);

	/**
	* Caches the attachmentses in the entity cache if it is enabled.
	*
	* @param attachmentses the attachmentses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.Attachments> attachmentses);

	/**
	* Creates a new attachments with the primary key. Does not add the attachments to the database.
	*
	* @param attach_id the primary key for the new attachments
	* @return the new attachments
	*/
	public com.vmware.model.Attachments create(int attach_id);

	/**
	* Removes the attachments with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attach_id the primary key of the attachments
	* @return the attachments that was removed
	* @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Attachments remove(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchAttachmentsException;

	public com.vmware.model.Attachments updateImpl(
		com.vmware.model.Attachments attachments, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attachments with the primary key or throws a {@link com.vmware.NoSuchAttachmentsException} if it could not be found.
	*
	* @param attach_id the primary key of the attachments
	* @return the attachments
	* @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Attachments findByPrimaryKey(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchAttachmentsException;

	/**
	* Returns the attachments with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attach_id the primary key of the attachments
	* @return the attachments, or <code>null</code> if a attachments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Attachments fetchByPrimaryKey(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the attachmentses.
	*
	* @return the attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Attachments> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attachmentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @return the range of attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Attachments> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attachmentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Attachments> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attachmentses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attachmentses.
	*
	* @return the number of attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the bugs associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> getBugs(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @return the range of bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> getBugs(int pk, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> getBugs(int pk, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the number of bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public int getBugsSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the bug is associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @param bugPK the primary key of the bug
	* @return <code>true</code> if the bug is associated with the attachments; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsBug(int pk, int bugPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the attachments has any bugs associated with it.
	*
	* @param pk the primary key of the attachments to check for associations with bugs
	* @return <code>true</code> if the attachments has any bugs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsBugs(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the profileses associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> getProfileses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the profileses associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @return the range of profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> getProfileses(int pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the profileses associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> getProfileses(int pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profileses associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the number of profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public int getProfilesesSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the profiles is associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @param profilesPK the primary key of the profiles
	* @return <code>true</code> if the profiles is associated with the attachments; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsProfiles(int pk, int profilesPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the attachments has any profileses associated with it.
	*
	* @param pk the primary key of the attachments to check for associations with profileses
	* @return <code>true</code> if the attachments has any profileses associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsProfileses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;
}