/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Attachments;

import java.util.List;

/**
 * The persistence utility for the attachments service. This utility wraps {@link AttachmentsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see AttachmentsPersistence
 * @see AttachmentsPersistenceImpl
 * @generated
 */
public class AttachmentsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Attachments attachments) {
		getPersistence().clearCache(attachments);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Attachments> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Attachments> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Attachments> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Attachments update(Attachments attachments, boolean merge)
		throws SystemException {
		return getPersistence().update(attachments, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Attachments update(Attachments attachments, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(attachments, merge, serviceContext);
	}

	/**
	* Caches the attachments in the entity cache if it is enabled.
	*
	* @param attachments the attachments
	*/
	public static void cacheResult(com.vmware.model.Attachments attachments) {
		getPersistence().cacheResult(attachments);
	}

	/**
	* Caches the attachmentses in the entity cache if it is enabled.
	*
	* @param attachmentses the attachmentses
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Attachments> attachmentses) {
		getPersistence().cacheResult(attachmentses);
	}

	/**
	* Creates a new attachments with the primary key. Does not add the attachments to the database.
	*
	* @param attach_id the primary key for the new attachments
	* @return the new attachments
	*/
	public static com.vmware.model.Attachments create(int attach_id) {
		return getPersistence().create(attach_id);
	}

	/**
	* Removes the attachments with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attach_id the primary key of the attachments
	* @return the attachments that was removed
	* @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Attachments remove(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchAttachmentsException {
		return getPersistence().remove(attach_id);
	}

	public static com.vmware.model.Attachments updateImpl(
		com.vmware.model.Attachments attachments, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(attachments, merge);
	}

	/**
	* Returns the attachments with the primary key or throws a {@link com.vmware.NoSuchAttachmentsException} if it could not be found.
	*
	* @param attach_id the primary key of the attachments
	* @return the attachments
	* @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Attachments findByPrimaryKey(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchAttachmentsException {
		return getPersistence().findByPrimaryKey(attach_id);
	}

	/**
	* Returns the attachments with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attach_id the primary key of the attachments
	* @return the attachments, or <code>null</code> if a attachments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Attachments fetchByPrimaryKey(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(attach_id);
	}

	/**
	* Returns all the attachmentses.
	*
	* @return the attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Attachments> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the attachmentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @return the range of attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Attachments> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the attachmentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Attachments> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the attachmentses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of attachmentses.
	*
	* @return the number of attachmentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the bugs associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugs(pk);
	}

	/**
	* Returns a range of all the bugs associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @return the range of bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugs(pk, start, end);
	}

	/**
	* Returns an ordered range of all the bugs associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugs(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of bugs associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the number of bugs associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static int getBugsSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugsSize(pk);
	}

	/**
	* Returns <code>true</code> if the bug is associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @param bugPK the primary key of the bug
	* @return <code>true</code> if the bug is associated with the attachments; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsBug(int pk, int bugPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsBug(pk, bugPK);
	}

	/**
	* Returns <code>true</code> if the attachments has any bugs associated with it.
	*
	* @param pk the primary key of the attachments to check for associations with bugs
	* @return <code>true</code> if the attachments has any bugs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsBugs(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsBugs(pk);
	}

	/**
	* Returns all the profileses associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> getProfileses(
		int pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfileses(pk);
	}

	/**
	* Returns a range of all the profileses associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @return the range of profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> getProfileses(
		int pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfileses(pk, start, end);
	}

	/**
	* Returns an ordered range of all the profileses associated with the attachments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the attachments
	* @param start the lower bound of the range of attachmentses
	* @param end the upper bound of the range of attachmentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> getProfileses(
		int pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfileses(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of profileses associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @return the number of profileses associated with the attachments
	* @throws SystemException if a system exception occurred
	*/
	public static int getProfilesesSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfilesesSize(pk);
	}

	/**
	* Returns <code>true</code> if the profiles is associated with the attachments.
	*
	* @param pk the primary key of the attachments
	* @param profilesPK the primary key of the profiles
	* @return <code>true</code> if the profiles is associated with the attachments; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsProfiles(int pk, int profilesPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsProfiles(pk, profilesPK);
	}

	/**
	* Returns <code>true</code> if the attachments has any profileses associated with it.
	*
	* @param pk the primary key of the attachments to check for associations with profileses
	* @return <code>true</code> if the attachments has any profileses associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsProfileses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsProfileses(pk);
	}

	public static AttachmentsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AttachmentsPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					AttachmentsPersistence.class.getName());

			ReferenceRegistry.registerReference(AttachmentsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AttachmentsPersistence persistence) {
	}

	private static AttachmentsPersistence _persistence;
}