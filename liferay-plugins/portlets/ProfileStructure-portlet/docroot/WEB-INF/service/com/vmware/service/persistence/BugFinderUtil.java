/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author iscisc
 */
public class BugFinderUtil {
	public static java.util.List<com.vmware.model.Bug> getBugByShortDesc(
		java.lang.String short_desc) throws com.liferay.portal.SystemException {
		return getFinder().getBugByShortDesc(short_desc);
	}

	public static BugFinder getFinder() {
		if (_finder == null) {
			_finder = (BugFinder)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					BugFinder.class.getName());

			ReferenceRegistry.registerReference(BugFinderUtil.class, "_finder");
		}

		return _finder;
	}

	public void setFinder(BugFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(BugFinderUtil.class, "_finder");
	}

	private static BugFinder _finder;
}