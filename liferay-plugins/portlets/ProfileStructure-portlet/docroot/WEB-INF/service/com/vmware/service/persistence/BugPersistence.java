/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Bug;

/**
 * The persistence interface for the bug service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugPersistenceImpl
 * @see BugUtil
 * @generated
 */
public interface BugPersistence extends BasePersistence<Bug> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BugUtil} to access the bug persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the bug in the entity cache if it is enabled.
	*
	* @param bug the bug
	*/
	public void cacheResult(com.vmware.model.Bug bug);

	/**
	* Caches the bugs in the entity cache if it is enabled.
	*
	* @param bugs the bugs
	*/
	public void cacheResult(java.util.List<com.vmware.model.Bug> bugs);

	/**
	* Creates a new bug with the primary key. Does not add the bug to the database.
	*
	* @param bug_id the primary key for the new bug
	* @return the new bug
	*/
	public com.vmware.model.Bug create(int bug_id);

	/**
	* Removes the bug with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_id the primary key of the bug
	* @return the bug that was removed
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug remove(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	public com.vmware.model.Bug updateImpl(com.vmware.model.Bug bug,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug with the primary key or throws a {@link com.vmware.NoSuchBugException} if it could not be found.
	*
	* @param bug_id the primary key of the bug
	* @return the bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByPrimaryKey(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the bug with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param bug_id the primary key of the bug
	* @return the bug, or <code>null</code> if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByPrimaryKey(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the bugs where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByAssignToId(
		int assigned_to)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByAssignToId(
		int assigned_to, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByAssignToId(
		int assigned_to, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByAssignToId_First(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByAssignToId_First(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByAssignToId_Last(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByAssignToId_Last(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByAssignToId_PrevAndNext(int bug_id,
		int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where priority = &#63;.
	*
	* @param priority the priority
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugPriority(
		java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugPriority(
		java.lang.String priority, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugPriority(
		java.lang.String priority, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugPriority_First(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugPriority_First(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugPriority_Last(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugPriority_Last(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where priority = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugPriority_PrevAndNext(int bug_id,
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugSeverityByUserId_First(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugSeverityByUserId_First(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugSeverityByUserId_Last(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugSeverityByUserId_Last(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugSeverityByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugPriorityByUserId(
		int assigned_to, java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugPriorityByUserId(
		int assigned_to, java.lang.String priority, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugPriorityByUserId(
		int assigned_to, java.lang.String priority, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugPriorityByUserId_First(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugPriorityByUserId_First(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugPriorityByUserId_Last(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugPriorityByUserId_Last(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugPriorityByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugStatusByUserId(
		int assigned_to, java.lang.String bug_status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugStatusByUserId(
		int assigned_to, java.lang.String bug_status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugStatusByUserId(
		int assigned_to, java.lang.String bug_status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugStatusByUserId_First(int assigned_to,
		java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugStatusByUserId_First(
		int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugStatusByUserId_Last(int assigned_to,
		java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugStatusByUserId_Last(int assigned_to,
		java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugStatusByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugKeywordsByUserId(
		int assigned_to, java.lang.String keywords)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugKeywordsByUserId(
		int assigned_to, java.lang.String keywords, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugKeywordsByUserId(
		int assigned_to, java.lang.String keywords, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugKeywordsByUserId_First(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugKeywordsByUserId_First(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugKeywordsByUserId_Last(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugKeywordsByUserId_Last(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugKeywordsByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where reporter = &#63;.
	*
	* @param reporter the reporter
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugsByReporter(
		int reporter)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where reporter = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param reporter the reporter
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugsByReporter(
		int reporter, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where reporter = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param reporter the reporter
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugsByReporter(
		int reporter, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugsByReporter_First(int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugsByReporter_First(int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugsByReporter_Last(int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugsByReporter_Last(int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where reporter = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugsByReporter_PrevAndNext(int bug_id,
		int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugShortDescByUserId_First(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugShortDescByUserId_First(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugShortDescByUserId_Last(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugShortDescByUserId_Last(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugShortDescByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugVotesByUserId(
		int assigned_to, int votes)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and votes = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugVotesByUserId(
		int assigned_to, int votes, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and votes = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugVotesByUserId(
		int assigned_to, int votes, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugVotesByUserId_First(int assigned_to,
		int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugVotesByUserId_First(int assigned_to,
		int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugVotesByUserId_Last(int assigned_to,
		int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugVotesByUserId_Last(int assigned_to,
		int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugVotesByUserId_PrevAndNext(
		int bug_id, int assigned_to, int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugResolutionByUserId(
		int assigned_to, java.lang.String resolution)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugResolutionByUserId(
		int assigned_to, java.lang.String resolution, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugResolutionByUserId(
		int assigned_to, java.lang.String resolution, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugResolutionByUserId_First(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugResolutionByUserId_First(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugResolutionByUserId_Last(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugResolutionByUserId_Last(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugResolutionByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugBySeverity(
		java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs where bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugBySeverity(
		java.lang.String bug_severity, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs where bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findByBugBySeverity(
		java.lang.String bug_severity, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugBySeverity_First(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the first bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugBySeverity_First(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug findByBugBySeverity_Last(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns the last bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug fetchByBugBySeverity_Last(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bugs before and after the current bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Bug[] findByBugBySeverity_PrevAndNext(int bug_id,
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException;

	/**
	* Returns all the bugs.
	*
	* @return the bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bugs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bugs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bugs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Bug> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAssignToId(int assigned_to)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where priority = &#63; from the database.
	*
	* @param priority the priority
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugPriority(java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and bug_severity = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugSeverityByUserId(int assigned_to,
		java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and priority = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugPriorityByUserId(int assigned_to,
		java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and bug_status = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugStatusByUserId(int assigned_to,
		java.lang.String bug_status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and keywords = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugKeywordsByUserId(int assigned_to,
		java.lang.String keywords)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where reporter = &#63; from the database.
	*
	* @param reporter the reporter
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugsByReporter(int reporter)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and short_desc = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugShortDescByUserId(int assigned_to,
		java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and votes = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugVotesByUserId(int assigned_to, int votes)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where assigned_to = &#63; and resolution = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugResolutionByUserId(int assigned_to,
		java.lang.String resolution)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs where bug_severity = &#63; from the database.
	*
	* @param bug_severity the bug_severity
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBugBySeverity(java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the bugs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByAssignToId(int assigned_to)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where priority = &#63;.
	*
	* @param priority the priority
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugPriority(java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugSeverityByUserId(int assigned_to,
		java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugPriorityByUserId(int assigned_to,
		java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugStatusByUserId(int assigned_to,
		java.lang.String bug_status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugKeywordsByUserId(int assigned_to,
		java.lang.String keywords)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where reporter = &#63;.
	*
	* @param reporter the reporter
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugsByReporter(int reporter)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugShortDescByUserId(int assigned_to,
		java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugVotesByUserId(int assigned_to, int votes)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugResolutionByUserId(int assigned_to,
		java.lang.String resolution)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugBySeverity(java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bugs.
	*
	* @return the number of bugs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}