/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.BugSeverity;

/**
 * The persistence interface for the bug severity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugSeverityPersistenceImpl
 * @see BugSeverityUtil
 * @generated
 */
public interface BugSeverityPersistence extends BasePersistence<BugSeverity> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BugSeverityUtil} to access the bug severity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the bug severity in the entity cache if it is enabled.
	*
	* @param bugSeverity the bug severity
	*/
	public void cacheResult(com.vmware.model.BugSeverity bugSeverity);

	/**
	* Caches the bug severities in the entity cache if it is enabled.
	*
	* @param bugSeverities the bug severities
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.BugSeverity> bugSeverities);

	/**
	* Creates a new bug severity with the primary key. Does not add the bug severity to the database.
	*
	* @param bug_severtiy_id the primary key for the new bug severity
	* @return the new bug severity
	*/
	public com.vmware.model.BugSeverity create(int bug_severtiy_id);

	/**
	* Removes the bug severity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_severtiy_id the primary key of the bug severity
	* @return the bug severity that was removed
	* @throws com.vmware.NoSuchBugSeverityException if a bug severity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity remove(int bug_severtiy_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugSeverityException;

	public com.vmware.model.BugSeverity updateImpl(
		com.vmware.model.BugSeverity bugSeverity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug severity with the primary key or throws a {@link com.vmware.NoSuchBugSeverityException} if it could not be found.
	*
	* @param bug_severtiy_id the primary key of the bug severity
	* @return the bug severity
	* @throws com.vmware.NoSuchBugSeverityException if a bug severity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity findByPrimaryKey(int bug_severtiy_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugSeverityException;

	/**
	* Returns the bug severity with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param bug_severtiy_id the primary key of the bug severity
	* @return the bug severity, or <code>null</code> if a bug severity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity fetchByPrimaryKey(int bug_severtiy_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug severity where value = &#63; or throws a {@link com.vmware.NoSuchBugSeverityException} if it could not be found.
	*
	* @param value the value
	* @return the matching bug severity
	* @throws com.vmware.NoSuchBugSeverityException if a matching bug severity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity findByBugSeverityByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugSeverityException;

	/**
	* Returns the bug severity where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param value the value
	* @return the matching bug severity, or <code>null</code> if a matching bug severity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity fetchByBugSeverityByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug severity where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param value the value
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching bug severity, or <code>null</code> if a matching bug severity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity fetchByBugSeverityByValue(
		java.lang.String value, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the bug severities.
	*
	* @return the bug severities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugSeverity> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bug severities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bug severities
	* @param end the upper bound of the range of bug severities (not inclusive)
	* @return the range of bug severities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugSeverity> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bug severities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bug severities
	* @param end the upper bound of the range of bug severities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bug severities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugSeverity> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the bug severity where value = &#63; from the database.
	*
	* @param value the value
	* @return the bug severity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugSeverity removeByBugSeverityByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugSeverityException;

	/**
	* Removes all the bug severities from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bug severities where value = &#63;.
	*
	* @param value the value
	* @return the number of matching bug severities
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugSeverityByValue(java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bug severities.
	*
	* @return the number of bug severities
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}