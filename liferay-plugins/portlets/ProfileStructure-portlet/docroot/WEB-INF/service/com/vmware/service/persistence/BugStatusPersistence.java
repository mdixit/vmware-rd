/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.BugStatus;

/**
 * The persistence interface for the bug status service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugStatusPersistenceImpl
 * @see BugStatusUtil
 * @generated
 */
public interface BugStatusPersistence extends BasePersistence<BugStatus> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BugStatusUtil} to access the bug status persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the bug status in the entity cache if it is enabled.
	*
	* @param bugStatus the bug status
	*/
	public void cacheResult(com.vmware.model.BugStatus bugStatus);

	/**
	* Caches the bug statuses in the entity cache if it is enabled.
	*
	* @param bugStatuses the bug statuses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.BugStatus> bugStatuses);

	/**
	* Creates a new bug status with the primary key. Does not add the bug status to the database.
	*
	* @param bug_status_id the primary key for the new bug status
	* @return the new bug status
	*/
	public com.vmware.model.BugStatus create(int bug_status_id);

	/**
	* Removes the bug status with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_status_id the primary key of the bug status
	* @return the bug status that was removed
	* @throws com.vmware.NoSuchBugStatusException if a bug status with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus remove(int bug_status_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugStatusException;

	public com.vmware.model.BugStatus updateImpl(
		com.vmware.model.BugStatus bugStatus, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug status with the primary key or throws a {@link com.vmware.NoSuchBugStatusException} if it could not be found.
	*
	* @param bug_status_id the primary key of the bug status
	* @return the bug status
	* @throws com.vmware.NoSuchBugStatusException if a bug status with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus findByPrimaryKey(int bug_status_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugStatusException;

	/**
	* Returns the bug status with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param bug_status_id the primary key of the bug status
	* @return the bug status, or <code>null</code> if a bug status with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus fetchByPrimaryKey(int bug_status_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug status where value = &#63; or throws a {@link com.vmware.NoSuchBugStatusException} if it could not be found.
	*
	* @param value the value
	* @return the matching bug status
	* @throws com.vmware.NoSuchBugStatusException if a matching bug status could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus findByBugStatusByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugStatusException;

	/**
	* Returns the bug status where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param value the value
	* @return the matching bug status, or <code>null</code> if a matching bug status could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus fetchByBugStatusByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the bug status where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param value the value
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching bug status, or <code>null</code> if a matching bug status could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus fetchByBugStatusByValue(
		java.lang.String value, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the bug statuses.
	*
	* @return the bug statuses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugStatus> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the bug statuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bug statuses
	* @param end the upper bound of the range of bug statuses (not inclusive)
	* @return the range of bug statuses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugStatus> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the bug statuses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bug statuses
	* @param end the upper bound of the range of bug statuses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bug statuses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.BugStatus> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the bug status where value = &#63; from the database.
	*
	* @param value the value
	* @return the bug status that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.BugStatus removeByBugStatusByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugStatusException;

	/**
	* Removes all the bug statuses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bug statuses where value = &#63;.
	*
	* @param value the value
	* @return the number of matching bug statuses
	* @throws SystemException if a system exception occurred
	*/
	public int countByBugStatusByValue(java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of bug statuses.
	*
	* @return the number of bug statuses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}