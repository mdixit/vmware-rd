/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Bug;

import java.util.List;

/**
 * The persistence utility for the bug service. This utility wraps {@link BugPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugPersistence
 * @see BugPersistenceImpl
 * @generated
 */
public class BugUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Bug bug) {
		getPersistence().clearCache(bug);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Bug> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Bug> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Bug> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Bug update(Bug bug, boolean merge) throws SystemException {
		return getPersistence().update(bug, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Bug update(Bug bug, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(bug, merge, serviceContext);
	}

	/**
	* Caches the bug in the entity cache if it is enabled.
	*
	* @param bug the bug
	*/
	public static void cacheResult(com.vmware.model.Bug bug) {
		getPersistence().cacheResult(bug);
	}

	/**
	* Caches the bugs in the entity cache if it is enabled.
	*
	* @param bugs the bugs
	*/
	public static void cacheResult(java.util.List<com.vmware.model.Bug> bugs) {
		getPersistence().cacheResult(bugs);
	}

	/**
	* Creates a new bug with the primary key. Does not add the bug to the database.
	*
	* @param bug_id the primary key for the new bug
	* @return the new bug
	*/
	public static com.vmware.model.Bug create(int bug_id) {
		return getPersistence().create(bug_id);
	}

	/**
	* Removes the bug with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_id the primary key of the bug
	* @return the bug that was removed
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug remove(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence().remove(bug_id);
	}

	public static com.vmware.model.Bug updateImpl(com.vmware.model.Bug bug,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(bug, merge);
	}

	/**
	* Returns the bug with the primary key or throws a {@link com.vmware.NoSuchBugException} if it could not be found.
	*
	* @param bug_id the primary key of the bug
	* @return the bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByPrimaryKey(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence().findByPrimaryKey(bug_id);
	}

	/**
	* Returns the bug with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param bug_id the primary key of the bug
	* @return the bug, or <code>null</code> if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByPrimaryKey(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(bug_id);
	}

	/**
	* Returns all the bugs where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByAssignToId(
		int assigned_to)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByAssignToId(assigned_to);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByAssignToId(
		int assigned_to, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByAssignToId(assigned_to, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByAssignToId(
		int assigned_to, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAssignToId(assigned_to, start, end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByAssignToId_First(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByAssignToId_First(assigned_to, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByAssignToId_First(
		int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAssignToId_First(assigned_to, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByAssignToId_Last(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByAssignToId_Last(assigned_to, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByAssignToId_Last(int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAssignToId_Last(assigned_to, orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByAssignToId_PrevAndNext(
		int bug_id, int assigned_to,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByAssignToId_PrevAndNext(bug_id, assigned_to,
			orderByComparator);
	}

	/**
	* Returns all the bugs where priority = &#63;.
	*
	* @param priority the priority
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugPriority(
		java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugPriority(priority);
	}

	/**
	* Returns a range of all the bugs where priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugPriority(
		java.lang.String priority, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugPriority(priority, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugPriority(
		java.lang.String priority, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugPriority(priority, start, end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugPriority_First(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugPriority_First(priority, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugPriority_First(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugPriority_First(priority, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugPriority_Last(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugPriority_Last(priority, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where priority = &#63;.
	*
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugPriority_Last(
		java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugPriority_Last(priority, orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where priority = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugPriority_PrevAndNext(
		int bug_id, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugPriority_PrevAndNext(bug_id, priority,
			orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugSeverityByUserId(assigned_to, bug_severity);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugSeverityByUserId(assigned_to, bug_severity, start,
			end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugSeverityByUserId(assigned_to, bug_severity, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugSeverityByUserId_First(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugSeverityByUserId_First(assigned_to, bug_severity,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugSeverityByUserId_First(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugSeverityByUserId_First(assigned_to, bug_severity,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugSeverityByUserId_Last(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugSeverityByUserId_Last(assigned_to, bug_severity,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugSeverityByUserId_Last(
		int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugSeverityByUserId_Last(assigned_to, bug_severity,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugSeverityByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugSeverityByUserId_PrevAndNext(bug_id, assigned_to,
			bug_severity, orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugPriorityByUserId(
		int assigned_to, java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugPriorityByUserId(assigned_to, priority);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugPriorityByUserId(
		int assigned_to, java.lang.String priority, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugPriorityByUserId(assigned_to, priority, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and priority = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugPriorityByUserId(
		int assigned_to, java.lang.String priority, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugPriorityByUserId(assigned_to, priority, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugPriorityByUserId_First(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugPriorityByUserId_First(assigned_to, priority,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugPriorityByUserId_First(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugPriorityByUserId_First(assigned_to, priority,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugPriorityByUserId_Last(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugPriorityByUserId_Last(assigned_to, priority,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugPriorityByUserId_Last(
		int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugPriorityByUserId_Last(assigned_to, priority,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugPriorityByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String priority,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugPriorityByUserId_PrevAndNext(bug_id, assigned_to,
			priority, orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugStatusByUserId(
		int assigned_to, java.lang.String bug_status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugStatusByUserId(assigned_to, bug_status);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugStatusByUserId(
		int assigned_to, java.lang.String bug_status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugStatusByUserId(assigned_to, bug_status, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugStatusByUserId(
		int assigned_to, java.lang.String bug_status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugStatusByUserId(assigned_to, bug_status, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugStatusByUserId_First(
		int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugStatusByUserId_First(assigned_to, bug_status,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugStatusByUserId_First(
		int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugStatusByUserId_First(assigned_to, bug_status,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugStatusByUserId_Last(
		int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugStatusByUserId_Last(assigned_to, bug_status,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugStatusByUserId_Last(
		int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugStatusByUserId_Last(assigned_to, bug_status,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugStatusByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String bug_status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugStatusByUserId_PrevAndNext(bug_id, assigned_to,
			bug_status, orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugKeywordsByUserId(
		int assigned_to, java.lang.String keywords)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugKeywordsByUserId(assigned_to, keywords);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugKeywordsByUserId(
		int assigned_to, java.lang.String keywords, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugKeywordsByUserId(assigned_to, keywords, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugKeywordsByUserId(
		int assigned_to, java.lang.String keywords, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugKeywordsByUserId(assigned_to, keywords, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugKeywordsByUserId_First(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugKeywordsByUserId_First(assigned_to, keywords,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugKeywordsByUserId_First(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugKeywordsByUserId_First(assigned_to, keywords,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugKeywordsByUserId_Last(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugKeywordsByUserId_Last(assigned_to, keywords,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugKeywordsByUserId_Last(
		int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugKeywordsByUserId_Last(assigned_to, keywords,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugKeywordsByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String keywords,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugKeywordsByUserId_PrevAndNext(bug_id, assigned_to,
			keywords, orderByComparator);
	}

	/**
	* Returns all the bugs where reporter = &#63;.
	*
	* @param reporter the reporter
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugsByReporter(
		int reporter)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugsByReporter(reporter);
	}

	/**
	* Returns a range of all the bugs where reporter = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param reporter the reporter
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugsByReporter(
		int reporter, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugsByReporter(reporter, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where reporter = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param reporter the reporter
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugsByReporter(
		int reporter, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugsByReporter(reporter, start, end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugsByReporter_First(
		int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugsByReporter_First(reporter, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugsByReporter_First(
		int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugsByReporter_First(reporter, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugsByReporter_Last(int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugsByReporter_Last(reporter, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where reporter = &#63;.
	*
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugsByReporter_Last(
		int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugsByReporter_Last(reporter, orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where reporter = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param reporter the reporter
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugsByReporter_PrevAndNext(
		int bug_id, int reporter,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugsByReporter_PrevAndNext(bug_id, reporter,
			orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugShortDescByUserId(assigned_to, short_desc);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugShortDescByUserId(assigned_to, short_desc, start,
			end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugShortDescByUserId(assigned_to, short_desc, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugShortDescByUserId_First(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugShortDescByUserId_First(assigned_to, short_desc,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugShortDescByUserId_First(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugShortDescByUserId_First(assigned_to, short_desc,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugShortDescByUserId_Last(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugShortDescByUserId_Last(assigned_to, short_desc,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugShortDescByUserId_Last(
		int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugShortDescByUserId_Last(assigned_to, short_desc,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugShortDescByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String short_desc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugShortDescByUserId_PrevAndNext(bug_id, assigned_to,
			short_desc, orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugVotesByUserId(
		int assigned_to, int votes)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugVotesByUserId(assigned_to, votes);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and votes = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugVotesByUserId(
		int assigned_to, int votes, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugVotesByUserId(assigned_to, votes, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and votes = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugVotesByUserId(
		int assigned_to, int votes, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugVotesByUserId(assigned_to, votes, start, end,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugVotesByUserId_First(
		int assigned_to, int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugVotesByUserId_First(assigned_to, votes,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugVotesByUserId_First(
		int assigned_to, int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugVotesByUserId_First(assigned_to, votes,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugVotesByUserId_Last(
		int assigned_to, int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugVotesByUserId_Last(assigned_to, votes,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugVotesByUserId_Last(
		int assigned_to, int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugVotesByUserId_Last(assigned_to, votes,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugVotesByUserId_PrevAndNext(
		int bug_id, int assigned_to, int votes,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugVotesByUserId_PrevAndNext(bug_id, assigned_to,
			votes, orderByComparator);
	}

	/**
	* Returns all the bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugResolutionByUserId(
		int assigned_to, java.lang.String resolution)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugResolutionByUserId(assigned_to, resolution);
	}

	/**
	* Returns a range of all the bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugResolutionByUserId(
		int assigned_to, java.lang.String resolution, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugResolutionByUserId(assigned_to, resolution, start,
			end);
	}

	/**
	* Returns an ordered range of all the bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugResolutionByUserId(
		int assigned_to, java.lang.String resolution, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugResolutionByUserId(assigned_to, resolution, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugResolutionByUserId_First(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugResolutionByUserId_First(assigned_to, resolution,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugResolutionByUserId_First(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugResolutionByUserId_First(assigned_to, resolution,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugResolutionByUserId_Last(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugResolutionByUserId_Last(assigned_to, resolution,
			orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugResolutionByUserId_Last(
		int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugResolutionByUserId_Last(assigned_to, resolution,
			orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugResolutionByUserId_PrevAndNext(
		int bug_id, int assigned_to, java.lang.String resolution,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugResolutionByUserId_PrevAndNext(bug_id,
			assigned_to, resolution, orderByComparator);
	}

	/**
	* Returns all the bugs where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @return the matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugBySeverity(
		java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugBySeverity(bug_severity);
	}

	/**
	* Returns a range of all the bugs where bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugBySeverity(
		java.lang.String bug_severity, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugBySeverity(bug_severity, start, end);
	}

	/**
	* Returns an ordered range of all the bugs where bug_severity = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_severity the bug_severity
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findByBugBySeverity(
		java.lang.String bug_severity, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugBySeverity(bug_severity, start, end,
			orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugBySeverity_First(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugBySeverity_First(bug_severity, orderByComparator);
	}

	/**
	* Returns the first bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugBySeverity_First(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugBySeverity_First(bug_severity, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug
	* @throws com.vmware.NoSuchBugException if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug findByBugBySeverity_Last(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugBySeverity_Last(bug_severity, orderByComparator);
	}

	/**
	* Returns the last bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bug, or <code>null</code> if a matching bug could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug fetchByBugBySeverity_Last(
		java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugBySeverity_Last(bug_severity, orderByComparator);
	}

	/**
	* Returns the bugs before and after the current bug in the ordered set where bug_severity = &#63;.
	*
	* @param bug_id the primary key of the current bug
	* @param bug_severity the bug_severity
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bug
	* @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Bug[] findByBugBySeverity_PrevAndNext(
		int bug_id, java.lang.String bug_severity,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugException {
		return getPersistence()
				   .findByBugBySeverity_PrevAndNext(bug_id, bug_severity,
			orderByComparator);
	}

	/**
	* Returns all the bugs.
	*
	* @return the bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the bugs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @return the range of bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the bugs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs
	* @param end the upper bound of the range of bugs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bugs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByAssignToId(int assigned_to)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByAssignToId(assigned_to);
	}

	/**
	* Removes all the bugs where priority = &#63; from the database.
	*
	* @param priority the priority
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugPriority(java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugPriority(priority);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and bug_severity = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugSeverityByUserId(int assigned_to,
		java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugSeverityByUserId(assigned_to, bug_severity);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and priority = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugPriorityByUserId(int assigned_to,
		java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugPriorityByUserId(assigned_to, priority);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and bug_status = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugStatusByUserId(int assigned_to,
		java.lang.String bug_status)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugStatusByUserId(assigned_to, bug_status);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and keywords = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugKeywordsByUserId(int assigned_to,
		java.lang.String keywords)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugKeywordsByUserId(assigned_to, keywords);
	}

	/**
	* Removes all the bugs where reporter = &#63; from the database.
	*
	* @param reporter the reporter
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugsByReporter(int reporter)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugsByReporter(reporter);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and short_desc = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugShortDescByUserId(int assigned_to,
		java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugShortDescByUserId(assigned_to, short_desc);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and votes = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugVotesByUserId(int assigned_to, int votes)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugVotesByUserId(assigned_to, votes);
	}

	/**
	* Removes all the bugs where assigned_to = &#63; and resolution = &#63; from the database.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugResolutionByUserId(int assigned_to,
		java.lang.String resolution)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugResolutionByUserId(assigned_to, resolution);
	}

	/**
	* Removes all the bugs where bug_severity = &#63; from the database.
	*
	* @param bug_severity the bug_severity
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugBySeverity(java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugBySeverity(bug_severity);
	}

	/**
	* Removes all the bugs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of bugs where assigned_to = &#63;.
	*
	* @param assigned_to the assigned_to
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAssignToId(int assigned_to)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByAssignToId(assigned_to);
	}

	/**
	* Returns the number of bugs where priority = &#63;.
	*
	* @param priority the priority
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugPriority(java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugPriority(priority);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and bug_severity = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_severity the bug_severity
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugSeverityByUserId(int assigned_to,
		java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBugSeverityByUserId(assigned_to, bug_severity);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and priority = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param priority the priority
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugPriorityByUserId(int assigned_to,
		java.lang.String priority)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugPriorityByUserId(assigned_to, priority);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and bug_status = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param bug_status the bug_status
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugStatusByUserId(int assigned_to,
		java.lang.String bug_status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugStatusByUserId(assigned_to, bug_status);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and keywords = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param keywords the keywords
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugKeywordsByUserId(int assigned_to,
		java.lang.String keywords)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugKeywordsByUserId(assigned_to, keywords);
	}

	/**
	* Returns the number of bugs where reporter = &#63;.
	*
	* @param reporter the reporter
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugsByReporter(int reporter)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugsByReporter(reporter);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and short_desc = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param short_desc the short_desc
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugShortDescByUserId(int assigned_to,
		java.lang.String short_desc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBugShortDescByUserId(assigned_to, short_desc);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and votes = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param votes the votes
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugVotesByUserId(int assigned_to, int votes)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugVotesByUserId(assigned_to, votes);
	}

	/**
	* Returns the number of bugs where assigned_to = &#63; and resolution = &#63;.
	*
	* @param assigned_to the assigned_to
	* @param resolution the resolution
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugResolutionByUserId(int assigned_to,
		java.lang.String resolution)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBugResolutionByUserId(assigned_to, resolution);
	}

	/**
	* Returns the number of bugs where bug_severity = &#63;.
	*
	* @param bug_severity the bug_severity
	* @return the number of matching bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugBySeverity(java.lang.String bug_severity)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugBySeverity(bug_severity);
	}

	/**
	* Returns the number of bugs.
	*
	* @return the number of bugs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BugPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BugPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					BugPersistence.class.getName());

			ReferenceRegistry.registerReference(BugUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BugPersistence persistence) {
	}

	private static BugPersistence _persistence;
}