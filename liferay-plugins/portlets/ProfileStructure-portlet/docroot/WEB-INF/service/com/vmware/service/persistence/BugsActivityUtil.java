/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.BugsActivity;

import java.util.List;

/**
 * The persistence utility for the bugs activity service. This utility wraps {@link BugsActivityPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugsActivityPersistence
 * @see BugsActivityPersistenceImpl
 * @generated
 */
public class BugsActivityUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(BugsActivity bugsActivity) {
		getPersistence().clearCache(bugsActivity);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BugsActivity> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BugsActivity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BugsActivity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static BugsActivity update(BugsActivity bugsActivity, boolean merge)
		throws SystemException {
		return getPersistence().update(bugsActivity, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static BugsActivity update(BugsActivity bugsActivity, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(bugsActivity, merge, serviceContext);
	}

	/**
	* Caches the bugs activity in the entity cache if it is enabled.
	*
	* @param bugsActivity the bugs activity
	*/
	public static void cacheResult(com.vmware.model.BugsActivity bugsActivity) {
		getPersistence().cacheResult(bugsActivity);
	}

	/**
	* Caches the bugs activities in the entity cache if it is enabled.
	*
	* @param bugsActivities the bugs activities
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.BugsActivity> bugsActivities) {
		getPersistence().cacheResult(bugsActivities);
	}

	/**
	* Creates a new bugs activity with the primary key. Does not add the bugs activity to the database.
	*
	* @param bug_activity_id the primary key for the new bugs activity
	* @return the new bugs activity
	*/
	public static com.vmware.model.BugsActivity create(int bug_activity_id) {
		return getPersistence().create(bug_activity_id);
	}

	/**
	* Removes the bugs activity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity that was removed
	* @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity remove(int bug_activity_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence().remove(bug_activity_id);
	}

	public static com.vmware.model.BugsActivity updateImpl(
		com.vmware.model.BugsActivity bugsActivity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(bugsActivity, merge);
	}

	/**
	* Returns the bugs activity with the primary key or throws a {@link com.vmware.NoSuchBugsActivityException} if it could not be found.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByPrimaryKey(
		int bug_activity_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence().findByPrimaryKey(bug_activity_id);
	}

	/**
	* Returns the bugs activity with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param bug_activity_id the primary key of the bugs activity
	* @return the bugs activity, or <code>null</code> if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByPrimaryKey(
		int bug_activity_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(bug_activity_id);
	}

	/**
	* Returns all the bugs activities where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @return the matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByBugId(
		int bug_id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugActivityByBugId(bug_id);
	}

	/**
	* Returns a range of all the bugs activities where bug_id = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_id the bug_id
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByBugId(
		int bug_id, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugActivityByBugId(bug_id, start, end);
	}

	/**
	* Returns an ordered range of all the bugs activities where bug_id = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_id the bug_id
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByBugId(
		int bug_id, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugActivityByBugId(bug_id, start, end,
			orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByBugId_First(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByBugId_First(bug_id, orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByBugId_First(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByBugId_First(bug_id, orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByBugId_Last(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByBugId_Last(bug_id, orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByBugId_Last(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByBugId_Last(bug_id, orderByComparator);
	}

	/**
	* Returns the bugs activities before and after the current bugs activity in the ordered set where bug_id = &#63;.
	*
	* @param bug_activity_id the primary key of the current bugs activity
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity[] findByBugActivityByBugId_PrevAndNext(
		int bug_activity_id, int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByBugId_PrevAndNext(bug_activity_id,
			bug_id, orderByComparator);
	}

	/**
	* Returns all the bugs activities where attach_id = &#63;.
	*
	* @param attach_id the attach_id
	* @return the matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByAttachId(
		int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugActivityByAttachId(attach_id);
	}

	/**
	* Returns a range of all the bugs activities where attach_id = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attach_id the attach_id
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByAttachId(
		int attach_id, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugActivityByAttachId(attach_id, start, end);
	}

	/**
	* Returns an ordered range of all the bugs activities where attach_id = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attach_id the attach_id
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByAttachId(
		int attach_id, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugActivityByAttachId(attach_id, start, end,
			orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where attach_id = &#63;.
	*
	* @param attach_id the attach_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByAttachId_First(
		int attach_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByAttachId_First(attach_id,
			orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where attach_id = &#63;.
	*
	* @param attach_id the attach_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByAttachId_First(
		int attach_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByAttachId_First(attach_id,
			orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where attach_id = &#63;.
	*
	* @param attach_id the attach_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByAttachId_Last(
		int attach_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByAttachId_Last(attach_id,
			orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where attach_id = &#63;.
	*
	* @param attach_id the attach_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByAttachId_Last(
		int attach_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByAttachId_Last(attach_id,
			orderByComparator);
	}

	/**
	* Returns the bugs activities before and after the current bugs activity in the ordered set where attach_id = &#63;.
	*
	* @param bug_activity_id the primary key of the current bugs activity
	* @param attach_id the attach_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity[] findByBugActivityByAttachId_PrevAndNext(
		int bug_activity_id, int attach_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByAttachId_PrevAndNext(bug_activity_id,
			attach_id, orderByComparator);
	}

	/**
	* Returns all the bugs activities where who = &#63;.
	*
	* @param who the who
	* @return the matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByUserId(
		int who) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugActivityByUserId(who);
	}

	/**
	* Returns a range of all the bugs activities where who = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param who the who
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByUserId(
		int who, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugActivityByUserId(who, start, end);
	}

	/**
	* Returns an ordered range of all the bugs activities where who = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param who the who
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByUserId(
		int who, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugActivityByUserId(who, start, end, orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByUserId_First(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByUserId_First(who, orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByUserId_First(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByUserId_First(who, orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByUserId_Last(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByUserId_Last(who, orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByUserId_Last(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByUserId_Last(who, orderByComparator);
	}

	/**
	* Returns the bugs activities before and after the current bugs activity in the ordered set where who = &#63;.
	*
	* @param bug_activity_id the primary key of the current bugs activity
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity[] findByBugActivityByUserId_PrevAndNext(
		int bug_activity_id, int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByUserId_PrevAndNext(bug_activity_id, who,
			orderByComparator);
	}

	/**
	* Returns all the bugs activities where who = &#63; and fieldid = &#63;.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @return the matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByFieldIdAndUserId(
		int who, int fieldid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBugActivityByFieldIdAndUserId(who, fieldid);
	}

	/**
	* Returns a range of all the bugs activities where who = &#63; and fieldid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param who the who
	* @param fieldid the fieldid
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByFieldIdAndUserId(
		int who, int fieldid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugActivityByFieldIdAndUserId(who, fieldid, start, end);
	}

	/**
	* Returns an ordered range of all the bugs activities where who = &#63; and fieldid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param who the who
	* @param fieldid the fieldid
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findByBugActivityByFieldIdAndUserId(
		int who, int fieldid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBugActivityByFieldIdAndUserId(who, fieldid, start,
			end, orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByFieldIdAndUserId_First(
		int who, int fieldid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByFieldIdAndUserId_First(who, fieldid,
			orderByComparator);
	}

	/**
	* Returns the first bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByFieldIdAndUserId_First(
		int who, int fieldid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByFieldIdAndUserId_First(who, fieldid,
			orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity findByBugActivityByFieldIdAndUserId_Last(
		int who, int fieldid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByFieldIdAndUserId_Last(who, fieldid,
			orderByComparator);
	}

	/**
	* Returns the last bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity fetchByBugActivityByFieldIdAndUserId_Last(
		int who, int fieldid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBugActivityByFieldIdAndUserId_Last(who, fieldid,
			orderByComparator);
	}

	/**
	* Returns the bugs activities before and after the current bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	*
	* @param bug_activity_id the primary key of the current bugs activity
	* @param who the who
	* @param fieldid the fieldid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next bugs activity
	* @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.BugsActivity[] findByBugActivityByFieldIdAndUserId_PrevAndNext(
		int bug_activity_id, int who, int fieldid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchBugsActivityException {
		return getPersistence()
				   .findByBugActivityByFieldIdAndUserId_PrevAndNext(bug_activity_id,
			who, fieldid, orderByComparator);
	}

	/**
	* Returns all the bugs activities.
	*
	* @return the bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the bugs activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the bugs activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.BugsActivity> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the bugs activities where bug_id = &#63; from the database.
	*
	* @param bug_id the bug_id
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugActivityByBugId(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugActivityByBugId(bug_id);
	}

	/**
	* Removes all the bugs activities where attach_id = &#63; from the database.
	*
	* @param attach_id the attach_id
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugActivityByAttachId(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugActivityByAttachId(attach_id);
	}

	/**
	* Removes all the bugs activities where who = &#63; from the database.
	*
	* @param who the who
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugActivityByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugActivityByUserId(who);
	}

	/**
	* Removes all the bugs activities where who = &#63; and fieldid = &#63; from the database.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBugActivityByFieldIdAndUserId(int who,
		int fieldid) throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBugActivityByFieldIdAndUserId(who, fieldid);
	}

	/**
	* Removes all the bugs activities from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of bugs activities where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @return the number of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugActivityByBugId(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugActivityByBugId(bug_id);
	}

	/**
	* Returns the number of bugs activities where attach_id = &#63;.
	*
	* @param attach_id the attach_id
	* @return the number of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugActivityByAttachId(int attach_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugActivityByAttachId(attach_id);
	}

	/**
	* Returns the number of bugs activities where who = &#63;.
	*
	* @param who the who
	* @return the number of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugActivityByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBugActivityByUserId(who);
	}

	/**
	* Returns the number of bugs activities where who = &#63; and fieldid = &#63;.
	*
	* @param who the who
	* @param fieldid the fieldid
	* @return the number of matching bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBugActivityByFieldIdAndUserId(int who, int fieldid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBugActivityByFieldIdAndUserId(who, fieldid);
	}

	/**
	* Returns the number of bugs activities.
	*
	* @return the number of bugs activities
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the bugs associated with the bugs activity.
	*
	* @param pk the primary key of the bugs activity
	* @return the bugs associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugs(pk);
	}

	/**
	* Returns a range of all the bugs associated with the bugs activity.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the bugs activity
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of bugs associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugs(pk, start, end);
	}

	/**
	* Returns an ordered range of all the bugs associated with the bugs activity.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the bugs activity
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of bugs associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Bug> getBugs(int pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugs(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of bugs associated with the bugs activity.
	*
	* @param pk the primary key of the bugs activity
	* @return the number of bugs associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static int getBugsSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getBugsSize(pk);
	}

	/**
	* Returns <code>true</code> if the bug is associated with the bugs activity.
	*
	* @param pk the primary key of the bugs activity
	* @param bugPK the primary key of the bug
	* @return <code>true</code> if the bug is associated with the bugs activity; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsBug(int pk, int bugPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsBug(pk, bugPK);
	}

	/**
	* Returns <code>true</code> if the bugs activity has any bugs associated with it.
	*
	* @param pk the primary key of the bugs activity to check for associations with bugs
	* @return <code>true</code> if the bugs activity has any bugs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsBugs(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsBugs(pk);
	}

	/**
	* Returns all the attachmentses associated with the bugs activity.
	*
	* @param pk the primary key of the bugs activity
	* @return the attachmentses associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Attachments> getAttachmentses(
		int pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAttachmentses(pk);
	}

	/**
	* Returns a range of all the attachmentses associated with the bugs activity.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the bugs activity
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @return the range of attachmentses associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Attachments> getAttachmentses(
		int pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAttachmentses(pk, start, end);
	}

	/**
	* Returns an ordered range of all the attachmentses associated with the bugs activity.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the bugs activity
	* @param start the lower bound of the range of bugs activities
	* @param end the upper bound of the range of bugs activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attachmentses associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Attachments> getAttachmentses(
		int pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .getAttachmentses(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of attachmentses associated with the bugs activity.
	*
	* @param pk the primary key of the bugs activity
	* @return the number of attachmentses associated with the bugs activity
	* @throws SystemException if a system exception occurred
	*/
	public static int getAttachmentsesSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getAttachmentsesSize(pk);
	}

	/**
	* Returns <code>true</code> if the attachments is associated with the bugs activity.
	*
	* @param pk the primary key of the bugs activity
	* @param attachmentsPK the primary key of the attachments
	* @return <code>true</code> if the attachments is associated with the bugs activity; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsAttachments(int pk, int attachmentsPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsAttachments(pk, attachmentsPK);
	}

	/**
	* Returns <code>true</code> if the bugs activity has any attachmentses associated with it.
	*
	* @param pk the primary key of the bugs activity to check for associations with attachmentses
	* @return <code>true</code> if the bugs activity has any attachmentses associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsAttachmentses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsAttachmentses(pk);
	}

	public static BugsActivityPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BugsActivityPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					BugsActivityPersistence.class.getName());

			ReferenceRegistry.registerReference(BugsActivityUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BugsActivityPersistence persistence) {
	}

	private static BugsActivityPersistence _persistence;
}