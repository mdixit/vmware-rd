/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.CCEntity;

/**
 * The persistence interface for the c c entity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see CCEntityPersistenceImpl
 * @see CCEntityUtil
 * @generated
 */
public interface CCEntityPersistence extends BasePersistence<CCEntity> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CCEntityUtil} to access the c c entity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the c c entity in the entity cache if it is enabled.
	*
	* @param ccEntity the c c entity
	*/
	public void cacheResult(com.vmware.model.CCEntity ccEntity);

	/**
	* Caches the c c entities in the entity cache if it is enabled.
	*
	* @param ccEntities the c c entities
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.CCEntity> ccEntities);

	/**
	* Creates a new c c entity with the primary key. Does not add the c c entity to the database.
	*
	* @param ccEntityPK the primary key for the new c c entity
	* @return the new c c entity
	*/
	public com.vmware.model.CCEntity create(
		com.vmware.service.persistence.CCEntityPK ccEntityPK);

	/**
	* Removes the c c entity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity that was removed
	* @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity remove(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchCCEntityException;

	public com.vmware.model.CCEntity updateImpl(
		com.vmware.model.CCEntity ccEntity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c c entity with the primary key or throws a {@link com.vmware.NoSuchCCEntityException} if it could not be found.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity
	* @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity findByPrimaryKey(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchCCEntityException;

	/**
	* Returns the c c entity with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity, or <code>null</code> if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.CCEntity fetchByPrimaryKey(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c c entities.
	*
	* @return the c c entities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.CCEntity> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c c entities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of c c entities
	* @param end the upper bound of the range of c c entities (not inclusive)
	* @return the range of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.CCEntity> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c c entities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of c c entities
	* @param end the upper bound of the range of c c entities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.CCEntity> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c c entities from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c c entities.
	*
	* @return the number of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}