/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.CCEntity;

import java.util.List;

/**
 * The persistence utility for the c c entity service. This utility wraps {@link CCEntityPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see CCEntityPersistence
 * @see CCEntityPersistenceImpl
 * @generated
 */
public class CCEntityUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CCEntity ccEntity) {
		getPersistence().clearCache(ccEntity);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CCEntity> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CCEntity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CCEntity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CCEntity update(CCEntity ccEntity, boolean merge)
		throws SystemException {
		return getPersistence().update(ccEntity, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CCEntity update(CCEntity ccEntity, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(ccEntity, merge, serviceContext);
	}

	/**
	* Caches the c c entity in the entity cache if it is enabled.
	*
	* @param ccEntity the c c entity
	*/
	public static void cacheResult(com.vmware.model.CCEntity ccEntity) {
		getPersistence().cacheResult(ccEntity);
	}

	/**
	* Caches the c c entities in the entity cache if it is enabled.
	*
	* @param ccEntities the c c entities
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.CCEntity> ccEntities) {
		getPersistence().cacheResult(ccEntities);
	}

	/**
	* Creates a new c c entity with the primary key. Does not add the c c entity to the database.
	*
	* @param ccEntityPK the primary key for the new c c entity
	* @return the new c c entity
	*/
	public static com.vmware.model.CCEntity create(
		com.vmware.service.persistence.CCEntityPK ccEntityPK) {
		return getPersistence().create(ccEntityPK);
	}

	/**
	* Removes the c c entity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity that was removed
	* @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.CCEntity remove(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchCCEntityException {
		return getPersistence().remove(ccEntityPK);
	}

	public static com.vmware.model.CCEntity updateImpl(
		com.vmware.model.CCEntity ccEntity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(ccEntity, merge);
	}

	/**
	* Returns the c c entity with the primary key or throws a {@link com.vmware.NoSuchCCEntityException} if it could not be found.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity
	* @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.CCEntity findByPrimaryKey(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchCCEntityException {
		return getPersistence().findByPrimaryKey(ccEntityPK);
	}

	/**
	* Returns the c c entity with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ccEntityPK the primary key of the c c entity
	* @return the c c entity, or <code>null</code> if a c c entity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.CCEntity fetchByPrimaryKey(
		com.vmware.service.persistence.CCEntityPK ccEntityPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(ccEntityPK);
	}

	/**
	* Returns all the c c entities.
	*
	* @return the c c entities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.CCEntity> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c c entities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of c c entities
	* @param end the upper bound of the range of c c entities (not inclusive)
	* @return the range of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.CCEntity> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c c entities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of c c entities
	* @param end the upper bound of the range of c c entities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.CCEntity> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c c entities from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c c entities.
	*
	* @return the number of c c entities
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CCEntityPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CCEntityPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					CCEntityPersistence.class.getName());

			ReferenceRegistry.registerReference(CCEntityUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CCEntityPersistence persistence) {
	}

	private static CCEntityPersistence _persistence;
}