/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Components;

/**
 * The persistence interface for the components service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ComponentsPersistenceImpl
 * @see ComponentsUtil
 * @generated
 */
public interface ComponentsPersistence extends BasePersistence<Components> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ComponentsUtil} to access the components persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the components in the entity cache if it is enabled.
	*
	* @param components the components
	*/
	public void cacheResult(com.vmware.model.Components components);

	/**
	* Caches the componentses in the entity cache if it is enabled.
	*
	* @param componentses the componentses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.Components> componentses);

	/**
	* Creates a new components with the primary key. Does not add the components to the database.
	*
	* @param comp_id the primary key for the new components
	* @return the new components
	*/
	public com.vmware.model.Components create(int comp_id);

	/**
	* Removes the components with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param comp_id the primary key of the components
	* @return the components that was removed
	* @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Components remove(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchComponentsException;

	public com.vmware.model.Components updateImpl(
		com.vmware.model.Components components, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the components with the primary key or throws a {@link com.vmware.NoSuchComponentsException} if it could not be found.
	*
	* @param comp_id the primary key of the components
	* @return the components
	* @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Components findByPrimaryKey(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchComponentsException;

	/**
	* Returns the components with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param comp_id the primary key of the components
	* @return the components, or <code>null</code> if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Components fetchByPrimaryKey(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the componentses.
	*
	* @return the componentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Components> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the componentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @return the range of componentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Components> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the componentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of componentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Components> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the componentses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of componentses.
	*
	* @return the number of componentses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the profileses associated with the components.
	*
	* @param pk the primary key of the components
	* @return the profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> getProfileses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the profileses associated with the components.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the components
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @return the range of profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> getProfileses(int pk,
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the profileses associated with the components.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the components
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> getProfileses(int pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profileses associated with the components.
	*
	* @param pk the primary key of the components
	* @return the number of profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public int getProfilesesSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the profiles is associated with the components.
	*
	* @param pk the primary key of the components
	* @param profilesPK the primary key of the profiles
	* @return <code>true</code> if the profiles is associated with the components; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsProfiles(int pk, int profilesPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the components has any profileses associated with it.
	*
	* @param pk the primary key of the components to check for associations with profileses
	* @return <code>true</code> if the components has any profileses associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsProfileses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException;
}