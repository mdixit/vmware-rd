/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Components;

import java.util.List;

/**
 * The persistence utility for the components service. This utility wraps {@link ComponentsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ComponentsPersistence
 * @see ComponentsPersistenceImpl
 * @generated
 */
public class ComponentsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Components components) {
		getPersistence().clearCache(components);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Components> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Components> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Components> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Components update(Components components, boolean merge)
		throws SystemException {
		return getPersistence().update(components, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Components update(Components components, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(components, merge, serviceContext);
	}

	/**
	* Caches the components in the entity cache if it is enabled.
	*
	* @param components the components
	*/
	public static void cacheResult(com.vmware.model.Components components) {
		getPersistence().cacheResult(components);
	}

	/**
	* Caches the componentses in the entity cache if it is enabled.
	*
	* @param componentses the componentses
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Components> componentses) {
		getPersistence().cacheResult(componentses);
	}

	/**
	* Creates a new components with the primary key. Does not add the components to the database.
	*
	* @param comp_id the primary key for the new components
	* @return the new components
	*/
	public static com.vmware.model.Components create(int comp_id) {
		return getPersistence().create(comp_id);
	}

	/**
	* Removes the components with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param comp_id the primary key of the components
	* @return the components that was removed
	* @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components remove(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchComponentsException {
		return getPersistence().remove(comp_id);
	}

	public static com.vmware.model.Components updateImpl(
		com.vmware.model.Components components, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(components, merge);
	}

	/**
	* Returns the components with the primary key or throws a {@link com.vmware.NoSuchComponentsException} if it could not be found.
	*
	* @param comp_id the primary key of the components
	* @return the components
	* @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components findByPrimaryKey(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchComponentsException {
		return getPersistence().findByPrimaryKey(comp_id);
	}

	/**
	* Returns the components with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param comp_id the primary key of the components
	* @return the components, or <code>null</code> if a components with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Components fetchByPrimaryKey(int comp_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(comp_id);
	}

	/**
	* Returns all the componentses.
	*
	* @return the componentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Components> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the componentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @return the range of componentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Components> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the componentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of componentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Components> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the componentses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of componentses.
	*
	* @return the number of componentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	/**
	* Returns all the profileses associated with the components.
	*
	* @param pk the primary key of the components
	* @return the profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> getProfileses(
		int pk) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfileses(pk);
	}

	/**
	* Returns a range of all the profileses associated with the components.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the components
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @return the range of profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> getProfileses(
		int pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfileses(pk, start, end);
	}

	/**
	* Returns an ordered range of all the profileses associated with the components.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the components
	* @param start the lower bound of the range of componentses
	* @param end the upper bound of the range of componentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> getProfileses(
		int pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfileses(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of profileses associated with the components.
	*
	* @param pk the primary key of the components
	* @return the number of profileses associated with the components
	* @throws SystemException if a system exception occurred
	*/
	public static int getProfilesesSize(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().getProfilesesSize(pk);
	}

	/**
	* Returns <code>true</code> if the profiles is associated with the components.
	*
	* @param pk the primary key of the components
	* @param profilesPK the primary key of the profiles
	* @return <code>true</code> if the profiles is associated with the components; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsProfiles(int pk, int profilesPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsProfiles(pk, profilesPK);
	}

	/**
	* Returns <code>true</code> if the components has any profileses associated with it.
	*
	* @param pk the primary key of the components to check for associations with profileses
	* @return <code>true</code> if the components has any profileses associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public static boolean containsProfileses(int pk)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().containsProfileses(pk);
	}

	public static ComponentsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ComponentsPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					ComponentsPersistence.class.getName());

			ReferenceRegistry.registerReference(ComponentsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ComponentsPersistence persistence) {
	}

	private static ComponentsPersistence _persistence;
}