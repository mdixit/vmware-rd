/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Duplicate;

/**
 * The persistence interface for the duplicate service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see DuplicatePersistenceImpl
 * @see DuplicateUtil
 * @generated
 */
public interface DuplicatePersistence extends BasePersistence<Duplicate> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DuplicateUtil} to access the duplicate persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the duplicate in the entity cache if it is enabled.
	*
	* @param duplicate the duplicate
	*/
	public void cacheResult(com.vmware.model.Duplicate duplicate);

	/**
	* Caches the duplicates in the entity cache if it is enabled.
	*
	* @param duplicates the duplicates
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.Duplicate> duplicates);

	/**
	* Creates a new duplicate with the primary key. Does not add the duplicate to the database.
	*
	* @param dupe the primary key for the new duplicate
	* @return the new duplicate
	*/
	public com.vmware.model.Duplicate create(int dupe);

	/**
	* Removes the duplicate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dupe the primary key of the duplicate
	* @return the duplicate that was removed
	* @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate remove(int dupe)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException;

	public com.vmware.model.Duplicate updateImpl(
		com.vmware.model.Duplicate duplicate, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the duplicate with the primary key or throws a {@link com.vmware.NoSuchDuplicateException} if it could not be found.
	*
	* @param dupe the primary key of the duplicate
	* @return the duplicate
	* @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate findByPrimaryKey(int dupe)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException;

	/**
	* Returns the duplicate with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dupe the primary key of the duplicate
	* @return the duplicate, or <code>null</code> if a duplicate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate fetchByPrimaryKey(int dupe)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the duplicate where dupe_of = &#63; or throws a {@link com.vmware.NoSuchDuplicateException} if it could not be found.
	*
	* @param dupe_of the dupe_of
	* @return the matching duplicate
	* @throws com.vmware.NoSuchDuplicateException if a matching duplicate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate findByDuplicateByDupeofId(int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException;

	/**
	* Returns the duplicate where dupe_of = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param dupe_of the dupe_of
	* @return the matching duplicate, or <code>null</code> if a matching duplicate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate fetchByDuplicateByDupeofId(int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the duplicate where dupe_of = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param dupe_of the dupe_of
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching duplicate, or <code>null</code> if a matching duplicate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate fetchByDuplicateByDupeofId(int dupe_of,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the duplicates.
	*
	* @return the duplicates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Duplicate> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the duplicates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of duplicates
	* @param end the upper bound of the range of duplicates (not inclusive)
	* @return the range of duplicates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Duplicate> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the duplicates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of duplicates
	* @param end the upper bound of the range of duplicates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of duplicates
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Duplicate> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the duplicate where dupe_of = &#63; from the database.
	*
	* @param dupe_of the dupe_of
	* @return the duplicate that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Duplicate removeByDuplicateByDupeofId(int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException;

	/**
	* Removes all the duplicates from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of duplicates where dupe_of = &#63;.
	*
	* @param dupe_of the dupe_of
	* @return the number of matching duplicates
	* @throws SystemException if a system exception occurred
	*/
	public int countByDuplicateByDupeofId(int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of duplicates.
	*
	* @return the number of duplicates
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}