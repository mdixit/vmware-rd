/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Duplicate;

import java.util.List;

/**
 * The persistence utility for the duplicate service. This utility wraps {@link DuplicatePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see DuplicatePersistence
 * @see DuplicatePersistenceImpl
 * @generated
 */
public class DuplicateUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Duplicate duplicate) {
		getPersistence().clearCache(duplicate);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Duplicate> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Duplicate> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Duplicate> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Duplicate update(Duplicate duplicate, boolean merge)
		throws SystemException {
		return getPersistence().update(duplicate, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Duplicate update(Duplicate duplicate, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(duplicate, merge, serviceContext);
	}

	/**
	* Caches the duplicate in the entity cache if it is enabled.
	*
	* @param duplicate the duplicate
	*/
	public static void cacheResult(com.vmware.model.Duplicate duplicate) {
		getPersistence().cacheResult(duplicate);
	}

	/**
	* Caches the duplicates in the entity cache if it is enabled.
	*
	* @param duplicates the duplicates
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Duplicate> duplicates) {
		getPersistence().cacheResult(duplicates);
	}

	/**
	* Creates a new duplicate with the primary key. Does not add the duplicate to the database.
	*
	* @param dupe the primary key for the new duplicate
	* @return the new duplicate
	*/
	public static com.vmware.model.Duplicate create(int dupe) {
		return getPersistence().create(dupe);
	}

	/**
	* Removes the duplicate with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dupe the primary key of the duplicate
	* @return the duplicate that was removed
	* @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate remove(int dupe)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException {
		return getPersistence().remove(dupe);
	}

	public static com.vmware.model.Duplicate updateImpl(
		com.vmware.model.Duplicate duplicate, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(duplicate, merge);
	}

	/**
	* Returns the duplicate with the primary key or throws a {@link com.vmware.NoSuchDuplicateException} if it could not be found.
	*
	* @param dupe the primary key of the duplicate
	* @return the duplicate
	* @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate findByPrimaryKey(int dupe)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException {
		return getPersistence().findByPrimaryKey(dupe);
	}

	/**
	* Returns the duplicate with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dupe the primary key of the duplicate
	* @return the duplicate, or <code>null</code> if a duplicate with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate fetchByPrimaryKey(int dupe)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(dupe);
	}

	/**
	* Returns the duplicate where dupe_of = &#63; or throws a {@link com.vmware.NoSuchDuplicateException} if it could not be found.
	*
	* @param dupe_of the dupe_of
	* @return the matching duplicate
	* @throws com.vmware.NoSuchDuplicateException if a matching duplicate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate findByDuplicateByDupeofId(
		int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException {
		return getPersistence().findByDuplicateByDupeofId(dupe_of);
	}

	/**
	* Returns the duplicate where dupe_of = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param dupe_of the dupe_of
	* @return the matching duplicate, or <code>null</code> if a matching duplicate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate fetchByDuplicateByDupeofId(
		int dupe_of) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDuplicateByDupeofId(dupe_of);
	}

	/**
	* Returns the duplicate where dupe_of = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param dupe_of the dupe_of
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching duplicate, or <code>null</code> if a matching duplicate could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate fetchByDuplicateByDupeofId(
		int dupe_of, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDuplicateByDupeofId(dupe_of, retrieveFromCache);
	}

	/**
	* Returns all the duplicates.
	*
	* @return the duplicates
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Duplicate> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the duplicates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of duplicates
	* @param end the upper bound of the range of duplicates (not inclusive)
	* @return the range of duplicates
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Duplicate> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the duplicates.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of duplicates
	* @param end the upper bound of the range of duplicates (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of duplicates
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Duplicate> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the duplicate where dupe_of = &#63; from the database.
	*
	* @param dupe_of the dupe_of
	* @return the duplicate that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Duplicate removeByDuplicateByDupeofId(
		int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchDuplicateException {
		return getPersistence().removeByDuplicateByDupeofId(dupe_of);
	}

	/**
	* Removes all the duplicates from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of duplicates where dupe_of = &#63;.
	*
	* @param dupe_of the dupe_of
	* @return the number of matching duplicates
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDuplicateByDupeofId(int dupe_of)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDuplicateByDupeofId(dupe_of);
	}

	/**
	* Returns the number of duplicates.
	*
	* @return the number of duplicates
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DuplicatePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DuplicatePersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					DuplicatePersistence.class.getName());

			ReferenceRegistry.registerReference(DuplicateUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(DuplicatePersistence persistence) {
	}

	private static DuplicatePersistence _persistence;
}