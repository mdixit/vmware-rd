/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.FieldDefs;

/**
 * The persistence interface for the field defs service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see FieldDefsPersistenceImpl
 * @see FieldDefsUtil
 * @generated
 */
public interface FieldDefsPersistence extends BasePersistence<FieldDefs> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FieldDefsUtil} to access the field defs persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the field defs in the entity cache if it is enabled.
	*
	* @param fieldDefs the field defs
	*/
	public void cacheResult(com.vmware.model.FieldDefs fieldDefs);

	/**
	* Caches the field defses in the entity cache if it is enabled.
	*
	* @param fieldDefses the field defses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.FieldDefs> fieldDefses);

	/**
	* Creates a new field defs with the primary key. Does not add the field defs to the database.
	*
	* @param fielddef_id the primary key for the new field defs
	* @return the new field defs
	*/
	public com.vmware.model.FieldDefs create(int fielddef_id);

	/**
	* Removes the field defs with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param fielddef_id the primary key of the field defs
	* @return the field defs that was removed
	* @throws com.vmware.NoSuchFieldDefsException if a field defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs remove(int fielddef_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchFieldDefsException;

	public com.vmware.model.FieldDefs updateImpl(
		com.vmware.model.FieldDefs fieldDefs, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the field defs with the primary key or throws a {@link com.vmware.NoSuchFieldDefsException} if it could not be found.
	*
	* @param fielddef_id the primary key of the field defs
	* @return the field defs
	* @throws com.vmware.NoSuchFieldDefsException if a field defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs findByPrimaryKey(int fielddef_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchFieldDefsException;

	/**
	* Returns the field defs with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param fielddef_id the primary key of the field defs
	* @return the field defs, or <code>null</code> if a field defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs fetchByPrimaryKey(int fielddef_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the field defs where description = &#63; or throws a {@link com.vmware.NoSuchFieldDefsException} if it could not be found.
	*
	* @param description the description
	* @return the matching field defs
	* @throws com.vmware.NoSuchFieldDefsException if a matching field defs could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs findByFieldDefsByDesc(
		java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchFieldDefsException;

	/**
	* Returns the field defs where description = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param description the description
	* @return the matching field defs, or <code>null</code> if a matching field defs could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs fetchByFieldDefsByDesc(
		java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the field defs where description = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param description the description
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching field defs, or <code>null</code> if a matching field defs could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs fetchByFieldDefsByDesc(
		java.lang.String description, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the field defses.
	*
	* @return the field defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.FieldDefs> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the field defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of field defses
	* @param end the upper bound of the range of field defses (not inclusive)
	* @return the range of field defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.FieldDefs> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the field defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of field defses
	* @param end the upper bound of the range of field defses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of field defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.FieldDefs> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the field defs where description = &#63; from the database.
	*
	* @param description the description
	* @return the field defs that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.FieldDefs removeByFieldDefsByDesc(
		java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchFieldDefsException;

	/**
	* Removes all the field defses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of field defses where description = &#63;.
	*
	* @param description the description
	* @return the number of matching field defses
	* @throws SystemException if a system exception occurred
	*/
	public int countByFieldDefsByDesc(java.lang.String description)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of field defses.
	*
	* @return the number of field defses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}