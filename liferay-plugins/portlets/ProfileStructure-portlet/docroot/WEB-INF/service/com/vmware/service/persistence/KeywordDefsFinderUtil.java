/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author iscisc
 */
public class KeywordDefsFinderUtil {
	public static java.util.List<com.vmware.model.KeywordDefs> getKeywordDefByBugId(
		int bug_id) throws com.liferay.portal.SystemException {
		return getFinder().getKeywordDefByBugId(bug_id);
	}

	public static KeywordDefsFinder getFinder() {
		if (_finder == null) {
			_finder = (KeywordDefsFinder)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					KeywordDefsFinder.class.getName());

			ReferenceRegistry.registerReference(KeywordDefsFinderUtil.class,
				"_finder");
		}

		return _finder;
	}

	public void setFinder(KeywordDefsFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(KeywordDefsFinderUtil.class,
			"_finder");
	}

	private static KeywordDefsFinder _finder;
}