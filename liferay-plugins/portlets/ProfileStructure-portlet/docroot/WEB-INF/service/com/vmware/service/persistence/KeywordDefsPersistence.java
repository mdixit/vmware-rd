/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.KeywordDefs;

/**
 * The persistence interface for the keyword defs service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see KeywordDefsPersistenceImpl
 * @see KeywordDefsUtil
 * @generated
 */
public interface KeywordDefsPersistence extends BasePersistence<KeywordDefs> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link KeywordDefsUtil} to access the keyword defs persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the keyword defs in the entity cache if it is enabled.
	*
	* @param keywordDefs the keyword defs
	*/
	public void cacheResult(com.vmware.model.KeywordDefs keywordDefs);

	/**
	* Caches the keyword defses in the entity cache if it is enabled.
	*
	* @param keywordDefses the keyword defses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.KeywordDefs> keywordDefses);

	/**
	* Creates a new keyword defs with the primary key. Does not add the keyword defs to the database.
	*
	* @param key_def_id the primary key for the new keyword defs
	* @return the new keyword defs
	*/
	public com.vmware.model.KeywordDefs create(int key_def_id);

	/**
	* Removes the keyword defs with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs that was removed
	* @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs remove(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordDefsException;

	public com.vmware.model.KeywordDefs updateImpl(
		com.vmware.model.KeywordDefs keywordDefs, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the keyword defs with the primary key or throws a {@link com.vmware.NoSuchKeywordDefsException} if it could not be found.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs
	* @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs findByPrimaryKey(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordDefsException;

	/**
	* Returns the keyword defs with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs, or <code>null</code> if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.KeywordDefs fetchByPrimaryKey(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the keyword defses.
	*
	* @return the keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.KeywordDefs> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the keyword defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keyword defses
	* @param end the upper bound of the range of keyword defses (not inclusive)
	* @return the range of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.KeywordDefs> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the keyword defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keyword defses
	* @param end the upper bound of the range of keyword defses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.KeywordDefs> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the keyword defses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of keyword defses.
	*
	* @return the number of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}