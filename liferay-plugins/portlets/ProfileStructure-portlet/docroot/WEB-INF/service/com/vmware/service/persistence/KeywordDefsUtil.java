/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.KeywordDefs;

import java.util.List;

/**
 * The persistence utility for the keyword defs service. This utility wraps {@link KeywordDefsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see KeywordDefsPersistence
 * @see KeywordDefsPersistenceImpl
 * @generated
 */
public class KeywordDefsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(KeywordDefs keywordDefs) {
		getPersistence().clearCache(keywordDefs);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<KeywordDefs> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<KeywordDefs> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<KeywordDefs> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static KeywordDefs update(KeywordDefs keywordDefs, boolean merge)
		throws SystemException {
		return getPersistence().update(keywordDefs, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static KeywordDefs update(KeywordDefs keywordDefs, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(keywordDefs, merge, serviceContext);
	}

	/**
	* Caches the keyword defs in the entity cache if it is enabled.
	*
	* @param keywordDefs the keyword defs
	*/
	public static void cacheResult(com.vmware.model.KeywordDefs keywordDefs) {
		getPersistence().cacheResult(keywordDefs);
	}

	/**
	* Caches the keyword defses in the entity cache if it is enabled.
	*
	* @param keywordDefses the keyword defses
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.KeywordDefs> keywordDefses) {
		getPersistence().cacheResult(keywordDefses);
	}

	/**
	* Creates a new keyword defs with the primary key. Does not add the keyword defs to the database.
	*
	* @param key_def_id the primary key for the new keyword defs
	* @return the new keyword defs
	*/
	public static com.vmware.model.KeywordDefs create(int key_def_id) {
		return getPersistence().create(key_def_id);
	}

	/**
	* Removes the keyword defs with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs that was removed
	* @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.KeywordDefs remove(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordDefsException {
		return getPersistence().remove(key_def_id);
	}

	public static com.vmware.model.KeywordDefs updateImpl(
		com.vmware.model.KeywordDefs keywordDefs, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(keywordDefs, merge);
	}

	/**
	* Returns the keyword defs with the primary key or throws a {@link com.vmware.NoSuchKeywordDefsException} if it could not be found.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs
	* @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.KeywordDefs findByPrimaryKey(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordDefsException {
		return getPersistence().findByPrimaryKey(key_def_id);
	}

	/**
	* Returns the keyword defs with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param key_def_id the primary key of the keyword defs
	* @return the keyword defs, or <code>null</code> if a keyword defs with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.KeywordDefs fetchByPrimaryKey(int key_def_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(key_def_id);
	}

	/**
	* Returns all the keyword defses.
	*
	* @return the keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.KeywordDefs> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the keyword defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keyword defses
	* @param end the upper bound of the range of keyword defses (not inclusive)
	* @return the range of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.KeywordDefs> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the keyword defses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keyword defses
	* @param end the upper bound of the range of keyword defses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.KeywordDefs> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the keyword defses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of keyword defses.
	*
	* @return the number of keyword defses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static KeywordDefsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (KeywordDefsPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					KeywordDefsPersistence.class.getName());

			ReferenceRegistry.registerReference(KeywordDefsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(KeywordDefsPersistence persistence) {
	}

	private static KeywordDefsPersistence _persistence;
}