/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author iscisc
 */
public class KeywordPK implements Comparable<KeywordPK>, Serializable {
	public int bug_id;
	public int keywordid;

	public KeywordPK() {
	}

	public KeywordPK(int bug_id, int keywordid) {
		this.bug_id = bug_id;
		this.keywordid = keywordid;
	}

	public int getBug_id() {
		return bug_id;
	}

	public void setBug_id(int bug_id) {
		this.bug_id = bug_id;
	}

	public int getKeywordid() {
		return keywordid;
	}

	public void setKeywordid(int keywordid) {
		this.keywordid = keywordid;
	}

	public int compareTo(KeywordPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (bug_id < pk.bug_id) {
			value = -1;
		}
		else if (bug_id > pk.bug_id) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (keywordid < pk.keywordid) {
			value = -1;
		}
		else if (keywordid > pk.keywordid) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		KeywordPK pk = null;

		try {
			pk = (KeywordPK)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		if ((bug_id == pk.bug_id) && (keywordid == pk.keywordid)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(bug_id) + String.valueOf(keywordid)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("bug_id");
		sb.append(StringPool.EQUAL);
		sb.append(bug_id);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("keywordid");
		sb.append(StringPool.EQUAL);
		sb.append(keywordid);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}