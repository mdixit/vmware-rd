/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Keyword;

/**
 * The persistence interface for the keyword service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see KeywordPersistenceImpl
 * @see KeywordUtil
 * @generated
 */
public interface KeywordPersistence extends BasePersistence<Keyword> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link KeywordUtil} to access the keyword persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the keyword in the entity cache if it is enabled.
	*
	* @param keyword the keyword
	*/
	public void cacheResult(com.vmware.model.Keyword keyword);

	/**
	* Caches the keywords in the entity cache if it is enabled.
	*
	* @param keywords the keywords
	*/
	public void cacheResult(java.util.List<com.vmware.model.Keyword> keywords);

	/**
	* Creates a new keyword with the primary key. Does not add the keyword to the database.
	*
	* @param keywordPK the primary key for the new keyword
	* @return the new keyword
	*/
	public com.vmware.model.Keyword create(
		com.vmware.service.persistence.KeywordPK keywordPK);

	/**
	* Removes the keyword with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword that was removed
	* @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword remove(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordException;

	public com.vmware.model.Keyword updateImpl(
		com.vmware.model.Keyword keyword, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the keyword with the primary key or throws a {@link com.vmware.NoSuchKeywordException} if it could not be found.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword
	* @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword findByPrimaryKey(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordException;

	/**
	* Returns the keyword with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword, or <code>null</code> if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Keyword fetchByPrimaryKey(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the keywords.
	*
	* @return the keywords
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Keyword> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the keywords.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keywords
	* @param end the upper bound of the range of keywords (not inclusive)
	* @return the range of keywords
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Keyword> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the keywords.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keywords
	* @param end the upper bound of the range of keywords (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of keywords
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Keyword> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the keywords from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of keywords.
	*
	* @return the number of keywords
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}