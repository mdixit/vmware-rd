/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Keyword;

import java.util.List;

/**
 * The persistence utility for the keyword service. This utility wraps {@link KeywordPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see KeywordPersistence
 * @see KeywordPersistenceImpl
 * @generated
 */
public class KeywordUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Keyword keyword) {
		getPersistence().clearCache(keyword);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Keyword> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Keyword> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Keyword> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Keyword update(Keyword keyword, boolean merge)
		throws SystemException {
		return getPersistence().update(keyword, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Keyword update(Keyword keyword, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(keyword, merge, serviceContext);
	}

	/**
	* Caches the keyword in the entity cache if it is enabled.
	*
	* @param keyword the keyword
	*/
	public static void cacheResult(com.vmware.model.Keyword keyword) {
		getPersistence().cacheResult(keyword);
	}

	/**
	* Caches the keywords in the entity cache if it is enabled.
	*
	* @param keywords the keywords
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Keyword> keywords) {
		getPersistence().cacheResult(keywords);
	}

	/**
	* Creates a new keyword with the primary key. Does not add the keyword to the database.
	*
	* @param keywordPK the primary key for the new keyword
	* @return the new keyword
	*/
	public static com.vmware.model.Keyword create(
		com.vmware.service.persistence.KeywordPK keywordPK) {
		return getPersistence().create(keywordPK);
	}

	/**
	* Removes the keyword with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword that was removed
	* @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword remove(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordException {
		return getPersistence().remove(keywordPK);
	}

	public static com.vmware.model.Keyword updateImpl(
		com.vmware.model.Keyword keyword, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(keyword, merge);
	}

	/**
	* Returns the keyword with the primary key or throws a {@link com.vmware.NoSuchKeywordException} if it could not be found.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword
	* @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword findByPrimaryKey(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchKeywordException {
		return getPersistence().findByPrimaryKey(keywordPK);
	}

	/**
	* Returns the keyword with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param keywordPK the primary key of the keyword
	* @return the keyword, or <code>null</code> if a keyword with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Keyword fetchByPrimaryKey(
		com.vmware.service.persistence.KeywordPK keywordPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(keywordPK);
	}

	/**
	* Returns all the keywords.
	*
	* @return the keywords
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Keyword> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the keywords.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keywords
	* @param end the upper bound of the range of keywords (not inclusive)
	* @return the range of keywords
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Keyword> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the keywords.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of keywords
	* @param end the upper bound of the range of keywords (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of keywords
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Keyword> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the keywords from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of keywords.
	*
	* @return the number of keywords
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static KeywordPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (KeywordPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					KeywordPersistence.class.getName());

			ReferenceRegistry.registerReference(KeywordUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(KeywordPersistence persistence) {
	}

	private static KeywordPersistence _persistence;
}