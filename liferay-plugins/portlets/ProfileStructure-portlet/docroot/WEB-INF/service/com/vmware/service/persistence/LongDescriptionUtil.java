/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.LongDescription;

import java.util.List;

/**
 * The persistence utility for the long description service. This utility wraps {@link LongDescriptionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see LongDescriptionPersistence
 * @see LongDescriptionPersistenceImpl
 * @generated
 */
public class LongDescriptionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LongDescription longDescription) {
		getPersistence().clearCache(longDescription);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LongDescription> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LongDescription> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LongDescription> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LongDescription update(LongDescription longDescription,
		boolean merge) throws SystemException {
		return getPersistence().update(longDescription, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LongDescription update(LongDescription longDescription,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(longDescription, merge, serviceContext);
	}

	/**
	* Caches the long description in the entity cache if it is enabled.
	*
	* @param longDescription the long description
	*/
	public static void cacheResult(
		com.vmware.model.LongDescription longDescription) {
		getPersistence().cacheResult(longDescription);
	}

	/**
	* Caches the long descriptions in the entity cache if it is enabled.
	*
	* @param longDescriptions the long descriptions
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.LongDescription> longDescriptions) {
		getPersistence().cacheResult(longDescriptions);
	}

	/**
	* Creates a new long description with the primary key. Does not add the long description to the database.
	*
	* @param comment_id the primary key for the new long description
	* @return the new long description
	*/
	public static com.vmware.model.LongDescription create(int comment_id) {
		return getPersistence().create(comment_id);
	}

	/**
	* Removes the long description with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param comment_id the primary key of the long description
	* @return the long description that was removed
	* @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription remove(int comment_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence().remove(comment_id);
	}

	public static com.vmware.model.LongDescription updateImpl(
		com.vmware.model.LongDescription longDescription, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(longDescription, merge);
	}

	/**
	* Returns the long description with the primary key or throws a {@link com.vmware.NoSuchLongDescriptionException} if it could not be found.
	*
	* @param comment_id the primary key of the long description
	* @return the long description
	* @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription findByPrimaryKey(
		int comment_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence().findByPrimaryKey(comment_id);
	}

	/**
	* Returns the long description with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param comment_id the primary key of the long description
	* @return the long description, or <code>null</code> if a long description with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription fetchByPrimaryKey(
		int comment_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(comment_id);
	}

	/**
	* Returns all the long descriptions where who = &#63;.
	*
	* @param who the who
	* @return the matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findByLongDescriptionByUserId(
		int who) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLongDescriptionByUserId(who);
	}

	/**
	* Returns a range of all the long descriptions where who = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param who the who
	* @param start the lower bound of the range of long descriptions
	* @param end the upper bound of the range of long descriptions (not inclusive)
	* @return the range of matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findByLongDescriptionByUserId(
		int who, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLongDescriptionByUserId(who, start, end);
	}

	/**
	* Returns an ordered range of all the long descriptions where who = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param who the who
	* @param start the lower bound of the range of long descriptions
	* @param end the upper bound of the range of long descriptions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findByLongDescriptionByUserId(
		int who, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLongDescriptionByUserId(who, start, end,
			orderByComparator);
	}

	/**
	* Returns the first long description in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching long description
	* @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription findByLongDescriptionByUserId_First(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence()
				   .findByLongDescriptionByUserId_First(who, orderByComparator);
	}

	/**
	* Returns the first long description in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching long description, or <code>null</code> if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription fetchByLongDescriptionByUserId_First(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLongDescriptionByUserId_First(who, orderByComparator);
	}

	/**
	* Returns the last long description in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching long description
	* @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription findByLongDescriptionByUserId_Last(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence()
				   .findByLongDescriptionByUserId_Last(who, orderByComparator);
	}

	/**
	* Returns the last long description in the ordered set where who = &#63;.
	*
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching long description, or <code>null</code> if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription fetchByLongDescriptionByUserId_Last(
		int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLongDescriptionByUserId_Last(who, orderByComparator);
	}

	/**
	* Returns the long descriptions before and after the current long description in the ordered set where who = &#63;.
	*
	* @param comment_id the primary key of the current long description
	* @param who the who
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next long description
	* @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription[] findByLongDescriptionByUserId_PrevAndNext(
		int comment_id, int who,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence()
				   .findByLongDescriptionByUserId_PrevAndNext(comment_id, who,
			orderByComparator);
	}

	/**
	* Returns all the long descriptions where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @return the matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findByLongDescriptionByBugId(
		int bug_id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLongDescriptionByBugId(bug_id);
	}

	/**
	* Returns a range of all the long descriptions where bug_id = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_id the bug_id
	* @param start the lower bound of the range of long descriptions
	* @param end the upper bound of the range of long descriptions (not inclusive)
	* @return the range of matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findByLongDescriptionByBugId(
		int bug_id, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLongDescriptionByBugId(bug_id, start, end);
	}

	/**
	* Returns an ordered range of all the long descriptions where bug_id = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param bug_id the bug_id
	* @param start the lower bound of the range of long descriptions
	* @param end the upper bound of the range of long descriptions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findByLongDescriptionByBugId(
		int bug_id, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLongDescriptionByBugId(bug_id, start, end,
			orderByComparator);
	}

	/**
	* Returns the first long description in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching long description
	* @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription findByLongDescriptionByBugId_First(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence()
				   .findByLongDescriptionByBugId_First(bug_id, orderByComparator);
	}

	/**
	* Returns the first long description in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching long description, or <code>null</code> if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription fetchByLongDescriptionByBugId_First(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLongDescriptionByBugId_First(bug_id,
			orderByComparator);
	}

	/**
	* Returns the last long description in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching long description
	* @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription findByLongDescriptionByBugId_Last(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence()
				   .findByLongDescriptionByBugId_Last(bug_id, orderByComparator);
	}

	/**
	* Returns the last long description in the ordered set where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching long description, or <code>null</code> if a matching long description could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription fetchByLongDescriptionByBugId_Last(
		int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLongDescriptionByBugId_Last(bug_id, orderByComparator);
	}

	/**
	* Returns the long descriptions before and after the current long description in the ordered set where bug_id = &#63;.
	*
	* @param comment_id the primary key of the current long description
	* @param bug_id the bug_id
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next long description
	* @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.LongDescription[] findByLongDescriptionByBugId_PrevAndNext(
		int comment_id, int bug_id,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchLongDescriptionException {
		return getPersistence()
				   .findByLongDescriptionByBugId_PrevAndNext(comment_id,
			bug_id, orderByComparator);
	}

	/**
	* Returns all the long descriptions.
	*
	* @return the long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the long descriptions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of long descriptions
	* @param end the upper bound of the range of long descriptions (not inclusive)
	* @return the range of long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the long descriptions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of long descriptions
	* @param end the upper bound of the range of long descriptions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.LongDescription> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the long descriptions where who = &#63; from the database.
	*
	* @param who the who
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLongDescriptionByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLongDescriptionByUserId(who);
	}

	/**
	* Removes all the long descriptions where bug_id = &#63; from the database.
	*
	* @param bug_id the bug_id
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLongDescriptionByBugId(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLongDescriptionByBugId(bug_id);
	}

	/**
	* Removes all the long descriptions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of long descriptions where who = &#63;.
	*
	* @param who the who
	* @return the number of matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLongDescriptionByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLongDescriptionByUserId(who);
	}

	/**
	* Returns the number of long descriptions where bug_id = &#63;.
	*
	* @param bug_id the bug_id
	* @return the number of matching long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLongDescriptionByBugId(int bug_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLongDescriptionByBugId(bug_id);
	}

	/**
	* Returns the number of long descriptions.
	*
	* @return the number of long descriptions
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LongDescriptionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LongDescriptionPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					LongDescriptionPersistence.class.getName());

			ReferenceRegistry.registerReference(LongDescriptionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LongDescriptionPersistence persistence) {
	}

	private static LongDescriptionPersistence _persistence;
}