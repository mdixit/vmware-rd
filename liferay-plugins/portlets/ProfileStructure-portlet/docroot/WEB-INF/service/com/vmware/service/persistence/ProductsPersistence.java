/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Products;

/**
 * The persistence interface for the products service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProductsPersistenceImpl
 * @see ProductsUtil
 * @generated
 */
public interface ProductsPersistence extends BasePersistence<Products> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProductsUtil} to access the products persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the products in the entity cache if it is enabled.
	*
	* @param products the products
	*/
	public void cacheResult(com.vmware.model.Products products);

	/**
	* Caches the productses in the entity cache if it is enabled.
	*
	* @param productses the productses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.Products> productses);

	/**
	* Creates a new products with the primary key. Does not add the products to the database.
	*
	* @param prod_id the primary key for the new products
	* @return the new products
	*/
	public com.vmware.model.Products create(int prod_id);

	/**
	* Removes the products with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param prod_id the primary key of the products
	* @return the products that was removed
	* @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Products remove(int prod_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProductsException;

	public com.vmware.model.Products updateImpl(
		com.vmware.model.Products products, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the products with the primary key or throws a {@link com.vmware.NoSuchProductsException} if it could not be found.
	*
	* @param prod_id the primary key of the products
	* @return the products
	* @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Products findByPrimaryKey(int prod_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProductsException;

	/**
	* Returns the products with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param prod_id the primary key of the products
	* @return the products, or <code>null</code> if a products with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Products fetchByPrimaryKey(int prod_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the productses.
	*
	* @return the productses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Products> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the productses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of productses
	* @param end the upper bound of the range of productses (not inclusive)
	* @return the range of productses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Products> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the productses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of productses
	* @param end the upper bound of the range of productses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of productses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Products> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the productses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of productses.
	*
	* @return the number of productses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}