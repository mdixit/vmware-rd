/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Products;

import java.util.List;

/**
 * The persistence utility for the products service. This utility wraps {@link ProductsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProductsPersistence
 * @see ProductsPersistenceImpl
 * @generated
 */
public class ProductsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Products products) {
		getPersistence().clearCache(products);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Products> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Products> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Products> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Products update(Products products, boolean merge)
		throws SystemException {
		return getPersistence().update(products, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Products update(Products products, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(products, merge, serviceContext);
	}

	/**
	* Caches the products in the entity cache if it is enabled.
	*
	* @param products the products
	*/
	public static void cacheResult(com.vmware.model.Products products) {
		getPersistence().cacheResult(products);
	}

	/**
	* Caches the productses in the entity cache if it is enabled.
	*
	* @param productses the productses
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Products> productses) {
		getPersistence().cacheResult(productses);
	}

	/**
	* Creates a new products with the primary key. Does not add the products to the database.
	*
	* @param prod_id the primary key for the new products
	* @return the new products
	*/
	public static com.vmware.model.Products create(int prod_id) {
		return getPersistence().create(prod_id);
	}

	/**
	* Removes the products with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param prod_id the primary key of the products
	* @return the products that was removed
	* @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Products remove(int prod_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProductsException {
		return getPersistence().remove(prod_id);
	}

	public static com.vmware.model.Products updateImpl(
		com.vmware.model.Products products, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(products, merge);
	}

	/**
	* Returns the products with the primary key or throws a {@link com.vmware.NoSuchProductsException} if it could not be found.
	*
	* @param prod_id the primary key of the products
	* @return the products
	* @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Products findByPrimaryKey(int prod_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProductsException {
		return getPersistence().findByPrimaryKey(prod_id);
	}

	/**
	* Returns the products with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param prod_id the primary key of the products
	* @return the products, or <code>null</code> if a products with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Products fetchByPrimaryKey(int prod_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(prod_id);
	}

	/**
	* Returns all the productses.
	*
	* @return the productses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Products> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the productses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of productses
	* @param end the upper bound of the range of productses (not inclusive)
	* @return the range of productses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Products> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the productses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of productses
	* @param end the upper bound of the range of productses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of productses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Products> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the productses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of productses.
	*
	* @return the number of productses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ProductsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ProductsPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					ProductsPersistence.class.getName());

			ReferenceRegistry.registerReference(ProductsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ProductsPersistence persistence) {
	}

	private static ProductsPersistence _persistence;
}