/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author iscisc
 */
public class ProfileActivityPK implements Comparable<ProfileActivityPK>,
	Serializable {
	public int userid;
	public int who;

	public ProfileActivityPK() {
	}

	public ProfileActivityPK(int userid, int who) {
		this.userid = userid;
		this.who = who;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getWho() {
		return who;
	}

	public void setWho(int who) {
		this.who = who;
	}

	public int compareTo(ProfileActivityPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (userid < pk.userid) {
			value = -1;
		}
		else if (userid > pk.userid) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (who < pk.who) {
			value = -1;
		}
		else if (who > pk.who) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ProfileActivityPK pk = null;

		try {
			pk = (ProfileActivityPK)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		if ((userid == pk.userid) && (who == pk.who)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(userid) + String.valueOf(who)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("userid");
		sb.append(StringPool.EQUAL);
		sb.append(userid);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("who");
		sb.append(StringPool.EQUAL);
		sb.append(who);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}