/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.ProfileActivity;

/**
 * The persistence interface for the profile activity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProfileActivityPersistenceImpl
 * @see ProfileActivityUtil
 * @generated
 */
public interface ProfileActivityPersistence extends BasePersistence<ProfileActivity> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProfileActivityUtil} to access the profile activity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the profile activity in the entity cache if it is enabled.
	*
	* @param profileActivity the profile activity
	*/
	public void cacheResult(com.vmware.model.ProfileActivity profileActivity);

	/**
	* Caches the profile activities in the entity cache if it is enabled.
	*
	* @param profileActivities the profile activities
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.ProfileActivity> profileActivities);

	/**
	* Creates a new profile activity with the primary key. Does not add the profile activity to the database.
	*
	* @param profileActivityPK the primary key for the new profile activity
	* @return the new profile activity
	*/
	public com.vmware.model.ProfileActivity create(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK);

	/**
	* Removes the profile activity with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param profileActivityPK the primary key of the profile activity
	* @return the profile activity that was removed
	* @throws com.vmware.NoSuchProfileActivityException if a profile activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity remove(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfileActivityException;

	public com.vmware.model.ProfileActivity updateImpl(
		com.vmware.model.ProfileActivity profileActivity, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profile activity with the primary key or throws a {@link com.vmware.NoSuchProfileActivityException} if it could not be found.
	*
	* @param profileActivityPK the primary key of the profile activity
	* @return the profile activity
	* @throws com.vmware.NoSuchProfileActivityException if a profile activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity findByPrimaryKey(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfileActivityException;

	/**
	* Returns the profile activity with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param profileActivityPK the primary key of the profile activity
	* @return the profile activity, or <code>null</code> if a profile activity with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity fetchByPrimaryKey(
		com.vmware.service.persistence.ProfileActivityPK profileActivityPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profile activity where userid = &#63; or throws a {@link com.vmware.NoSuchProfileActivityException} if it could not be found.
	*
	* @param userid the userid
	* @return the matching profile activity
	* @throws com.vmware.NoSuchProfileActivityException if a matching profile activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity findByProfileActivityByUserId(
		int userid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfileActivityException;

	/**
	* Returns the profile activity where userid = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param userid the userid
	* @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity fetchByProfileActivityByUserId(
		int userid) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profile activity where userid = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param userid the userid
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity fetchByProfileActivityByUserId(
		int userid, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profile activity where fieldid = &#63; or throws a {@link com.vmware.NoSuchProfileActivityException} if it could not be found.
	*
	* @param fieldid the fieldid
	* @return the matching profile activity
	* @throws com.vmware.NoSuchProfileActivityException if a matching profile activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity findByFieldDefsByFieldId(
		int fieldid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfileActivityException;

	/**
	* Returns the profile activity where fieldid = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param fieldid the fieldid
	* @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity fetchByFieldDefsByFieldId(
		int fieldid) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profile activity where fieldid = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param fieldid the fieldid
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity fetchByFieldDefsByFieldId(
		int fieldid, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the profile activities.
	*
	* @return the profile activities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.ProfileActivity> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the profile activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profile activities
	* @param end the upper bound of the range of profile activities (not inclusive)
	* @return the range of profile activities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.ProfileActivity> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the profile activities.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profile activities
	* @param end the upper bound of the range of profile activities (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profile activities
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.ProfileActivity> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the profile activity where userid = &#63; from the database.
	*
	* @param userid the userid
	* @return the profile activity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity removeByProfileActivityByUserId(
		int userid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfileActivityException;

	/**
	* Removes the profile activity where fieldid = &#63; from the database.
	*
	* @param fieldid the fieldid
	* @return the profile activity that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.ProfileActivity removeByFieldDefsByFieldId(
		int fieldid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfileActivityException;

	/**
	* Removes all the profile activities from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profile activities where userid = &#63;.
	*
	* @param userid the userid
	* @return the number of matching profile activities
	* @throws SystemException if a system exception occurred
	*/
	public int countByProfileActivityByUserId(int userid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profile activities where fieldid = &#63;.
	*
	* @param fieldid the fieldid
	* @return the number of matching profile activities
	* @throws SystemException if a system exception occurred
	*/
	public int countByFieldDefsByFieldId(int fieldid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profile activities.
	*
	* @return the number of profile activities
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}