/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Profiles;

/**
 * The persistence interface for the profiles service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProfilesPersistenceImpl
 * @see ProfilesUtil
 * @generated
 */
public interface ProfilesPersistence extends BasePersistence<Profiles> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProfilesUtil} to access the profiles persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the profiles in the entity cache if it is enabled.
	*
	* @param profiles the profiles
	*/
	public void cacheResult(com.vmware.model.Profiles profiles);

	/**
	* Caches the profileses in the entity cache if it is enabled.
	*
	* @param profileses the profileses
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.Profiles> profileses);

	/**
	* Creates a new profiles with the primary key. Does not add the profiles to the database.
	*
	* @param userid the primary key for the new profiles
	* @return the new profiles
	*/
	public com.vmware.model.Profiles create(int userid);

	/**
	* Removes the profiles with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userid the primary key of the profiles
	* @return the profiles that was removed
	* @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles remove(int userid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException;

	public com.vmware.model.Profiles updateImpl(
		com.vmware.model.Profiles profiles, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profiles with the primary key or throws a {@link com.vmware.NoSuchProfilesException} if it could not be found.
	*
	* @param userid the primary key of the profiles
	* @return the profiles
	* @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles findByPrimaryKey(int userid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException;

	/**
	* Returns the profiles with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userid the primary key of the profiles
	* @return the profiles, or <code>null</code> if a profiles with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles fetchByPrimaryKey(int userid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profiles where login_name = &#63; or throws a {@link com.vmware.NoSuchProfilesException} if it could not be found.
	*
	* @param login_name the login_name
	* @return the matching profiles
	* @throws com.vmware.NoSuchProfilesException if a matching profiles could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles findByLoginName(
		java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException;

	/**
	* Returns the profiles where login_name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param login_name the login_name
	* @return the matching profiles, or <code>null</code> if a matching profiles could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles fetchByLoginName(
		java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the profiles where login_name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param login_name the login_name
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching profiles, or <code>null</code> if a matching profiles could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles fetchByLoginName(
		java.lang.String login_name, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the profileses.
	*
	* @return the profileses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the profileses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profileses
	* @param end the upper bound of the range of profileses (not inclusive)
	* @return the range of profileses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the profileses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profileses
	* @param end the upper bound of the range of profileses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profileses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Profiles> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the profiles where login_name = &#63; from the database.
	*
	* @param login_name the login_name
	* @return the profiles that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Profiles removeByLoginName(
		java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException;

	/**
	* Removes all the profileses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profileses where login_name = &#63;.
	*
	* @param login_name the login_name
	* @return the number of matching profileses
	* @throws SystemException if a system exception occurred
	*/
	public int countByLoginName(java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of profileses.
	*
	* @return the number of profileses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}