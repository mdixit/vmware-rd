/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Profiles;

import java.util.List;

/**
 * The persistence utility for the profiles service. This utility wraps {@link ProfilesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProfilesPersistence
 * @see ProfilesPersistenceImpl
 * @generated
 */
public class ProfilesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Profiles profiles) {
		getPersistence().clearCache(profiles);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Profiles> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Profiles> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Profiles> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Profiles update(Profiles profiles, boolean merge)
		throws SystemException {
		return getPersistence().update(profiles, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Profiles update(Profiles profiles, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(profiles, merge, serviceContext);
	}

	/**
	* Caches the profiles in the entity cache if it is enabled.
	*
	* @param profiles the profiles
	*/
	public static void cacheResult(com.vmware.model.Profiles profiles) {
		getPersistence().cacheResult(profiles);
	}

	/**
	* Caches the profileses in the entity cache if it is enabled.
	*
	* @param profileses the profileses
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Profiles> profileses) {
		getPersistence().cacheResult(profileses);
	}

	/**
	* Creates a new profiles with the primary key. Does not add the profiles to the database.
	*
	* @param userid the primary key for the new profiles
	* @return the new profiles
	*/
	public static com.vmware.model.Profiles create(int userid) {
		return getPersistence().create(userid);
	}

	/**
	* Removes the profiles with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userid the primary key of the profiles
	* @return the profiles that was removed
	* @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles remove(int userid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException {
		return getPersistence().remove(userid);
	}

	public static com.vmware.model.Profiles updateImpl(
		com.vmware.model.Profiles profiles, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(profiles, merge);
	}

	/**
	* Returns the profiles with the primary key or throws a {@link com.vmware.NoSuchProfilesException} if it could not be found.
	*
	* @param userid the primary key of the profiles
	* @return the profiles
	* @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles findByPrimaryKey(int userid)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException {
		return getPersistence().findByPrimaryKey(userid);
	}

	/**
	* Returns the profiles with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userid the primary key of the profiles
	* @return the profiles, or <code>null</code> if a profiles with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles fetchByPrimaryKey(int userid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(userid);
	}

	/**
	* Returns the profiles where login_name = &#63; or throws a {@link com.vmware.NoSuchProfilesException} if it could not be found.
	*
	* @param login_name the login_name
	* @return the matching profiles
	* @throws com.vmware.NoSuchProfilesException if a matching profiles could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles findByLoginName(
		java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException {
		return getPersistence().findByLoginName(login_name);
	}

	/**
	* Returns the profiles where login_name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param login_name the login_name
	* @return the matching profiles, or <code>null</code> if a matching profiles could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles fetchByLoginName(
		java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLoginName(login_name);
	}

	/**
	* Returns the profiles where login_name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param login_name the login_name
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching profiles, or <code>null</code> if a matching profiles could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles fetchByLoginName(
		java.lang.String login_name, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLoginName(login_name, retrieveFromCache);
	}

	/**
	* Returns all the profileses.
	*
	* @return the profileses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the profileses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profileses
	* @param end the upper bound of the range of profileses (not inclusive)
	* @return the range of profileses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the profileses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of profileses
	* @param end the upper bound of the range of profileses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profileses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Profiles> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the profiles where login_name = &#63; from the database.
	*
	* @param login_name the login_name
	* @return the profiles that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Profiles removeByLoginName(
		java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchProfilesException {
		return getPersistence().removeByLoginName(login_name);
	}

	/**
	* Removes all the profileses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of profileses where login_name = &#63;.
	*
	* @param login_name the login_name
	* @return the number of matching profileses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLoginName(java.lang.String login_name)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLoginName(login_name);
	}

	/**
	* Returns the number of profileses.
	*
	* @return the number of profileses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ProfilesPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ProfilesPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					ProfilesPersistence.class.getName());

			ReferenceRegistry.registerReference(ProfilesUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ProfilesPersistence persistence) {
	}

	private static ProfilesPersistence _persistence;
}