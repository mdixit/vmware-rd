/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Resolution;

/**
 * The persistence interface for the resolution service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ResolutionPersistenceImpl
 * @see ResolutionUtil
 * @generated
 */
public interface ResolutionPersistence extends BasePersistence<Resolution> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ResolutionUtil} to access the resolution persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the resolution in the entity cache if it is enabled.
	*
	* @param resolution the resolution
	*/
	public void cacheResult(com.vmware.model.Resolution resolution);

	/**
	* Caches the resolutions in the entity cache if it is enabled.
	*
	* @param resolutions the resolutions
	*/
	public void cacheResult(
		java.util.List<com.vmware.model.Resolution> resolutions);

	/**
	* Creates a new resolution with the primary key. Does not add the resolution to the database.
	*
	* @param res_id the primary key for the new resolution
	* @return the new resolution
	*/
	public com.vmware.model.Resolution create(int res_id);

	/**
	* Removes the resolution with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution that was removed
	* @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution remove(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException;

	public com.vmware.model.Resolution updateImpl(
		com.vmware.model.Resolution resolution, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the resolution with the primary key or throws a {@link com.vmware.NoSuchResolutionException} if it could not be found.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution
	* @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution findByPrimaryKey(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException;

	/**
	* Returns the resolution with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution, or <code>null</code> if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution fetchByPrimaryKey(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the resolution where value = &#63; or throws a {@link com.vmware.NoSuchResolutionException} if it could not be found.
	*
	* @param value the value
	* @return the matching resolution
	* @throws com.vmware.NoSuchResolutionException if a matching resolution could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution findByResolutionByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException;

	/**
	* Returns the resolution where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param value the value
	* @return the matching resolution, or <code>null</code> if a matching resolution could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution fetchByResolutionByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the resolution where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param value the value
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching resolution, or <code>null</code> if a matching resolution could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution fetchByResolutionByValue(
		java.lang.String value, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the resolutions.
	*
	* @return the resolutions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Resolution> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the resolutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of resolutions
	* @param end the upper bound of the range of resolutions (not inclusive)
	* @return the range of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Resolution> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the resolutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of resolutions
	* @param end the upper bound of the range of resolutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Resolution> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the resolution where value = &#63; from the database.
	*
	* @param value the value
	* @return the resolution that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Resolution removeByResolutionByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException;

	/**
	* Removes all the resolutions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of resolutions where value = &#63;.
	*
	* @param value the value
	* @return the number of matching resolutions
	* @throws SystemException if a system exception occurred
	*/
	public int countByResolutionByValue(java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of resolutions.
	*
	* @return the number of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}