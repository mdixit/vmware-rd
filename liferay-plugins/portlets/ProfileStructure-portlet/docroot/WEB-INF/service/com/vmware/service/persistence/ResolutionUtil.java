/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Resolution;

import java.util.List;

/**
 * The persistence utility for the resolution service. This utility wraps {@link ResolutionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ResolutionPersistence
 * @see ResolutionPersistenceImpl
 * @generated
 */
public class ResolutionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Resolution resolution) {
		getPersistence().clearCache(resolution);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Resolution> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Resolution> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Resolution> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Resolution update(Resolution resolution, boolean merge)
		throws SystemException {
		return getPersistence().update(resolution, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Resolution update(Resolution resolution, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(resolution, merge, serviceContext);
	}

	/**
	* Caches the resolution in the entity cache if it is enabled.
	*
	* @param resolution the resolution
	*/
	public static void cacheResult(com.vmware.model.Resolution resolution) {
		getPersistence().cacheResult(resolution);
	}

	/**
	* Caches the resolutions in the entity cache if it is enabled.
	*
	* @param resolutions the resolutions
	*/
	public static void cacheResult(
		java.util.List<com.vmware.model.Resolution> resolutions) {
		getPersistence().cacheResult(resolutions);
	}

	/**
	* Creates a new resolution with the primary key. Does not add the resolution to the database.
	*
	* @param res_id the primary key for the new resolution
	* @return the new resolution
	*/
	public static com.vmware.model.Resolution create(int res_id) {
		return getPersistence().create(res_id);
	}

	/**
	* Removes the resolution with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution that was removed
	* @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution remove(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException {
		return getPersistence().remove(res_id);
	}

	public static com.vmware.model.Resolution updateImpl(
		com.vmware.model.Resolution resolution, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(resolution, merge);
	}

	/**
	* Returns the resolution with the primary key or throws a {@link com.vmware.NoSuchResolutionException} if it could not be found.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution
	* @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution findByPrimaryKey(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException {
		return getPersistence().findByPrimaryKey(res_id);
	}

	/**
	* Returns the resolution with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param res_id the primary key of the resolution
	* @return the resolution, or <code>null</code> if a resolution with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution fetchByPrimaryKey(int res_id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(res_id);
	}

	/**
	* Returns the resolution where value = &#63; or throws a {@link com.vmware.NoSuchResolutionException} if it could not be found.
	*
	* @param value the value
	* @return the matching resolution
	* @throws com.vmware.NoSuchResolutionException if a matching resolution could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution findByResolutionByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException {
		return getPersistence().findByResolutionByValue(value);
	}

	/**
	* Returns the resolution where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param value the value
	* @return the matching resolution, or <code>null</code> if a matching resolution could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution fetchByResolutionByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByResolutionByValue(value);
	}

	/**
	* Returns the resolution where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param value the value
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching resolution, or <code>null</code> if a matching resolution could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution fetchByResolutionByValue(
		java.lang.String value, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByResolutionByValue(value, retrieveFromCache);
	}

	/**
	* Returns all the resolutions.
	*
	* @return the resolutions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Resolution> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the resolutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of resolutions
	* @param end the upper bound of the range of resolutions (not inclusive)
	* @return the range of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Resolution> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the resolutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of resolutions
	* @param end the upper bound of the range of resolutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Resolution> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the resolution where value = &#63; from the database.
	*
	* @param value the value
	* @return the resolution that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Resolution removeByResolutionByValue(
		java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchResolutionException {
		return getPersistence().removeByResolutionByValue(value);
	}

	/**
	* Removes all the resolutions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of resolutions where value = &#63;.
	*
	* @param value the value
	* @return the number of matching resolutions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByResolutionByValue(java.lang.String value)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByResolutionByValue(value);
	}

	/**
	* Returns the number of resolutions.
	*
	* @return the number of resolutions
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ResolutionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ResolutionPersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					ResolutionPersistence.class.getName());

			ReferenceRegistry.registerReference(ResolutionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ResolutionPersistence persistence) {
	}

	private static ResolutionPersistence _persistence;
}