/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author iscisc
 */
public class VotePK implements Comparable<VotePK>, Serializable {
	public int bug_id;
	public int who;

	public VotePK() {
	}

	public VotePK(int bug_id, int who) {
		this.bug_id = bug_id;
		this.who = who;
	}

	public int getBug_id() {
		return bug_id;
	}

	public void setBug_id(int bug_id) {
		this.bug_id = bug_id;
	}

	public int getWho() {
		return who;
	}

	public void setWho(int who) {
		this.who = who;
	}

	public int compareTo(VotePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (bug_id < pk.bug_id) {
			value = -1;
		}
		else if (bug_id > pk.bug_id) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (who < pk.who) {
			value = -1;
		}
		else if (who > pk.who) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		VotePK pk = null;

		try {
			pk = (VotePK)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		if ((bug_id == pk.bug_id) && (who == pk.who)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(bug_id) + String.valueOf(who)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("bug_id");
		sb.append(StringPool.EQUAL);
		sb.append(bug_id);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("who");
		sb.append(StringPool.EQUAL);
		sb.append(who);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}