/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.vmware.model.Vote;

/**
 * The persistence interface for the vote service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see VotePersistenceImpl
 * @see VoteUtil
 * @generated
 */
public interface VotePersistence extends BasePersistence<Vote> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VoteUtil} to access the vote persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the vote in the entity cache if it is enabled.
	*
	* @param vote the vote
	*/
	public void cacheResult(com.vmware.model.Vote vote);

	/**
	* Caches the votes in the entity cache if it is enabled.
	*
	* @param votes the votes
	*/
	public void cacheResult(java.util.List<com.vmware.model.Vote> votes);

	/**
	* Creates a new vote with the primary key. Does not add the vote to the database.
	*
	* @param votePK the primary key for the new vote
	* @return the new vote
	*/
	public com.vmware.model.Vote create(
		com.vmware.service.persistence.VotePK votePK);

	/**
	* Removes the vote with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param votePK the primary key of the vote
	* @return the vote that was removed
	* @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote remove(
		com.vmware.service.persistence.VotePK votePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException;

	public com.vmware.model.Vote updateImpl(com.vmware.model.Vote vote,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the vote with the primary key or throws a {@link com.vmware.NoSuchVoteException} if it could not be found.
	*
	* @param votePK the primary key of the vote
	* @return the vote
	* @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote findByPrimaryKey(
		com.vmware.service.persistence.VotePK votePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException;

	/**
	* Returns the vote with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param votePK the primary key of the vote
	* @return the vote, or <code>null</code> if a vote with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote fetchByPrimaryKey(
		com.vmware.service.persistence.VotePK votePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the vote where who = &#63; or throws a {@link com.vmware.NoSuchVoteException} if it could not be found.
	*
	* @param who the who
	* @return the matching vote
	* @throws com.vmware.NoSuchVoteException if a matching vote could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote findByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException;

	/**
	* Returns the vote where who = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param who the who
	* @return the matching vote, or <code>null</code> if a matching vote could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote fetchByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the vote where who = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param who the who
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching vote, or <code>null</code> if a matching vote could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote fetchByVoteCountByUserId(int who,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the votes.
	*
	* @return the votes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Vote> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the votes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of votes
	* @param end the upper bound of the range of votes (not inclusive)
	* @return the range of votes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Vote> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the votes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of votes
	* @param end the upper bound of the range of votes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of votes
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.vmware.model.Vote> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the vote where who = &#63; from the database.
	*
	* @param who the who
	* @return the vote that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.vmware.model.Vote removeByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException;

	/**
	* Removes all the votes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of votes where who = &#63;.
	*
	* @param who the who
	* @return the number of matching votes
	* @throws SystemException if a system exception occurred
	*/
	public int countByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of votes.
	*
	* @return the number of votes
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}