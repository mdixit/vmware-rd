/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.vmware.model.Vote;

import java.util.List;

/**
 * The persistence utility for the vote service. This utility wraps {@link VotePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see VotePersistence
 * @see VotePersistenceImpl
 * @generated
 */
public class VoteUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Vote vote) {
		getPersistence().clearCache(vote);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Vote> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Vote> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Vote> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Vote update(Vote vote, boolean merge)
		throws SystemException {
		return getPersistence().update(vote, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Vote update(Vote vote, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(vote, merge, serviceContext);
	}

	/**
	* Caches the vote in the entity cache if it is enabled.
	*
	* @param vote the vote
	*/
	public static void cacheResult(com.vmware.model.Vote vote) {
		getPersistence().cacheResult(vote);
	}

	/**
	* Caches the votes in the entity cache if it is enabled.
	*
	* @param votes the votes
	*/
	public static void cacheResult(java.util.List<com.vmware.model.Vote> votes) {
		getPersistence().cacheResult(votes);
	}

	/**
	* Creates a new vote with the primary key. Does not add the vote to the database.
	*
	* @param votePK the primary key for the new vote
	* @return the new vote
	*/
	public static com.vmware.model.Vote create(
		com.vmware.service.persistence.VotePK votePK) {
		return getPersistence().create(votePK);
	}

	/**
	* Removes the vote with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param votePK the primary key of the vote
	* @return the vote that was removed
	* @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote remove(
		com.vmware.service.persistence.VotePK votePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException {
		return getPersistence().remove(votePK);
	}

	public static com.vmware.model.Vote updateImpl(com.vmware.model.Vote vote,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(vote, merge);
	}

	/**
	* Returns the vote with the primary key or throws a {@link com.vmware.NoSuchVoteException} if it could not be found.
	*
	* @param votePK the primary key of the vote
	* @return the vote
	* @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote findByPrimaryKey(
		com.vmware.service.persistence.VotePK votePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException {
		return getPersistence().findByPrimaryKey(votePK);
	}

	/**
	* Returns the vote with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param votePK the primary key of the vote
	* @return the vote, or <code>null</code> if a vote with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote fetchByPrimaryKey(
		com.vmware.service.persistence.VotePK votePK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(votePK);
	}

	/**
	* Returns the vote where who = &#63; or throws a {@link com.vmware.NoSuchVoteException} if it could not be found.
	*
	* @param who the who
	* @return the matching vote
	* @throws com.vmware.NoSuchVoteException if a matching vote could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote findByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException {
		return getPersistence().findByVoteCountByUserId(who);
	}

	/**
	* Returns the vote where who = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param who the who
	* @return the matching vote, or <code>null</code> if a matching vote could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote fetchByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByVoteCountByUserId(who);
	}

	/**
	* Returns the vote where who = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param who the who
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching vote, or <code>null</code> if a matching vote could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote fetchByVoteCountByUserId(int who,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByVoteCountByUserId(who, retrieveFromCache);
	}

	/**
	* Returns all the votes.
	*
	* @return the votes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Vote> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the votes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of votes
	* @param end the upper bound of the range of votes (not inclusive)
	* @return the range of votes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Vote> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the votes.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of votes
	* @param end the upper bound of the range of votes (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of votes
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.vmware.model.Vote> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the vote where who = &#63; from the database.
	*
	* @param who the who
	* @return the vote that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.vmware.model.Vote removeByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.vmware.NoSuchVoteException {
		return getPersistence().removeByVoteCountByUserId(who);
	}

	/**
	* Removes all the votes from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of votes where who = &#63;.
	*
	* @param who the who
	* @return the number of matching votes
	* @throws SystemException if a system exception occurred
	*/
	public static int countByVoteCountByUserId(int who)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByVoteCountByUserId(who);
	}

	/**
	* Returns the number of votes.
	*
	* @return the number of votes
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static VotePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (VotePersistence)PortletBeanLocatorUtil.locate(com.vmware.service.ClpSerializer.getServletContextName(),
					VotePersistence.class.getName());

			ReferenceRegistry.registerReference(VoteUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(VotePersistence persistence) {
	}

	private static VotePersistence _persistence;
}