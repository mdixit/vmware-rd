package com.vmware;



import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vmware.model.Bug;

import com.vmware.model.KeywordDefs;
import com.vmware.model.Profiles;
import com.vmware.service.BugLocalServiceUtil;

import com.vmware.service.ProfilesLocalServiceUtil;

import java.io.IOException;
import java.util.List;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class Profile
 */

public class ProfilePortlet extends GenericPortlet {

    public void init() {
        viewJSP = getInitParameter("view-jsp");
    }
    
    public void doView(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
    	
    	ThemeDisplay themeDisplay =
			(ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	
    	Profiles pf = null;    	
    	
    	int userId = (int) themeDisplay.getUser().getUserId();
    	
    	System.out.println("userId=="+userId);
    	
        try {
			pf = ProfilesLocalServiceUtil.getProfiles(userId);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Bug> bugsList = null;
		int bugId=101;
		try {
			bugsList = BugLocalServiceUtil.getBugByShortDesc("h");
			
	    	List<KeywordDefs> kefDefList = BugLocalServiceUtil.getKeywordDefByBugId(bugId);
	    	System.out.println("kefDefList=="+kefDefList);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("bugsList=="+bugsList);
    	renderRequest.setAttribute("pf", pf);
        
        include(viewJSP, renderRequest, renderResponse);
    }
    
    @Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
    	
    	ThemeDisplay themeDisplay =
			(ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	
    	JSONObject responseText = JSONFactoryUtil.createJSONObject();
    	
    	Profiles pf1 = null;
    	
    	
    	
    	int userId = (int) themeDisplay.getUser().getUserId();
    	
    	 try {
 			pf1 = ProfilesLocalServiceUtil.getProfiles(userId);
 		} catch (PortalException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (SystemException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 		 		
 		responseText.put("loginName", pf1.getLogin_name());
 		
 		resourceResponse.getWriter().write(responseText.toString());
    	
    	//List<Bug> bugList = BugLocalServiceUtil.getBugByAssignTo(userId);
    	
    	//pf1 = BugLocalServiceUtil.getBugAssignedByBugId(bug_id)(userId);
    	
    	//System.out.println("Profiles=="+pf1.getRealname());
    
    }

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(ProfilePortlet.class);

}
