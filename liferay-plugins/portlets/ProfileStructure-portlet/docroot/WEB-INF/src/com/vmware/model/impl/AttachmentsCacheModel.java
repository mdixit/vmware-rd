/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Attachments;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Attachments in entity cache.
 *
 * @author iscisc
 * @see Attachments
 * @generated
 */
public class AttachmentsCacheModel implements CacheModel<Attachments>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{attach_id=");
		sb.append(attach_id);
		sb.append(", bug_id=");
		sb.append(bug_id);
		sb.append(", creation_ts=");
		sb.append(creation_ts);
		sb.append(", description=");
		sb.append(description);
		sb.append(", mimetype=");
		sb.append(mimetype);
		sb.append(", ispatch=");
		sb.append(ispatch);
		sb.append(", filename=");
		sb.append(filename);
		sb.append(", submitter_id=");
		sb.append(submitter_id);
		sb.append(", isobsolete=");
		sb.append(isobsolete);
		sb.append(", isprivate=");
		sb.append(isprivate);
		sb.append(", isurl=");
		sb.append(isurl);
		sb.append("}");

		return sb.toString();
	}

	public Attachments toEntityModel() {
		AttachmentsImpl attachmentsImpl = new AttachmentsImpl();

		attachmentsImpl.setAttach_id(attach_id);
		attachmentsImpl.setBug_id(bug_id);

		if (creation_ts == Long.MIN_VALUE) {
			attachmentsImpl.setCreation_ts(null);
		}
		else {
			attachmentsImpl.setCreation_ts(new Date(creation_ts));
		}

		if (description == null) {
			attachmentsImpl.setDescription(StringPool.BLANK);
		}
		else {
			attachmentsImpl.setDescription(description);
		}

		if (mimetype == null) {
			attachmentsImpl.setMimetype(StringPool.BLANK);
		}
		else {
			attachmentsImpl.setMimetype(mimetype);
		}

		attachmentsImpl.setIspatch(ispatch);

		if (filename == null) {
			attachmentsImpl.setFilename(StringPool.BLANK);
		}
		else {
			attachmentsImpl.setFilename(filename);
		}

		attachmentsImpl.setSubmitter_id(submitter_id);
		attachmentsImpl.setIsobsolete(isobsolete);
		attachmentsImpl.setIsprivate(isprivate);
		attachmentsImpl.setIsurl(isurl);

		attachmentsImpl.resetOriginalValues();

		return attachmentsImpl;
	}

	public int attach_id;
	public int bug_id;
	public long creation_ts;
	public String description;
	public String mimetype;
	public int ispatch;
	public String filename;
	public int submitter_id;
	public int isobsolete;
	public int isprivate;
	public int isurl;
}