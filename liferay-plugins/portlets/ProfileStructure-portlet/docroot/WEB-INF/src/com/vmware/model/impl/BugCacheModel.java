/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Bug;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Bug in entity cache.
 *
 * @author iscisc
 * @see Bug
 * @generated
 */
public class BugCacheModel implements CacheModel<Bug>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(105);

		sb.append("{bug_id=");
		sb.append(bug_id);
		sb.append(", assigned_to=");
		sb.append(assigned_to);
		sb.append(", bug_file_loc=");
		sb.append(bug_file_loc);
		sb.append(", bug_severity=");
		sb.append(bug_severity);
		sb.append(", bug_status=");
		sb.append(bug_status);
		sb.append(", creation_ts=");
		sb.append(creation_ts);
		sb.append(", delta_ts=");
		sb.append(delta_ts);
		sb.append(", short_desc=");
		sb.append(short_desc);
		sb.append(", host_op_sys=");
		sb.append(host_op_sys);
		sb.append(", guest_op_sys=");
		sb.append(guest_op_sys);
		sb.append(", priority=");
		sb.append(priority);
		sb.append(", rep_platform=");
		sb.append(rep_platform);
		sb.append(", product_id=");
		sb.append(product_id);
		sb.append(", component_id=");
		sb.append(component_id);
		sb.append(", qa_contact=");
		sb.append(qa_contact);
		sb.append(", reporter=");
		sb.append(reporter);
		sb.append(", category_id=");
		sb.append(category_id);
		sb.append(", resolution=");
		sb.append(resolution);
		sb.append(", target_milestone=");
		sb.append(target_milestone);
		sb.append(", status_whiteboard=");
		sb.append(status_whiteboard);
		sb.append(", votes=");
		sb.append(votes);
		sb.append(", keywords=");
		sb.append(keywords);
		sb.append(", lastdiffed=");
		sb.append(lastdiffed);
		sb.append(", everconfirmed=");
		sb.append(everconfirmed);
		sb.append(", reporter_accessible=");
		sb.append(reporter_accessible);
		sb.append(", cclist_accessible=");
		sb.append(cclist_accessible);
		sb.append(", estimated_time=");
		sb.append(estimated_time);
		sb.append(", remaining_time=");
		sb.append(remaining_time);
		sb.append(", deadline=");
		sb.append(deadline);
		sb.append(", bug_alias=");
		sb.append(bug_alias);
		sb.append(", found_in_product_id=");
		sb.append(found_in_product_id);
		sb.append(", found_in_version_id=");
		sb.append(found_in_version_id);
		sb.append(", found_in_phase_id=");
		sb.append(found_in_phase_id);
		sb.append(", cf_type=");
		sb.append(cf_type);
		sb.append(", cf_reported_by=");
		sb.append(cf_reported_by);
		sb.append(", cf_attempted=");
		sb.append(cf_attempted);
		sb.append(", cf_failed=");
		sb.append(cf_failed);
		sb.append(", cf_public_summary=");
		sb.append(cf_public_summary);
		sb.append(", cf_doc_impact=");
		sb.append(cf_doc_impact);
		sb.append(", cf_security=");
		sb.append(cf_security);
		sb.append(", cf_build=");
		sb.append(cf_build);
		sb.append(", cf_branch=");
		sb.append(cf_branch);
		sb.append(", cf_change=");
		sb.append(cf_change);
		sb.append(", cf_test_id=");
		sb.append(cf_test_id);
		sb.append(", cf_regression=");
		sb.append(cf_regression);
		sb.append(", cf_reviewer=");
		sb.append(cf_reviewer);
		sb.append(", cf_on_hold=");
		sb.append(cf_on_hold);
		sb.append(", cf_public_severity=");
		sb.append(cf_public_severity);
		sb.append(", cf_i18n_impact=");
		sb.append(cf_i18n_impact);
		sb.append(", cf_eta=");
		sb.append(cf_eta);
		sb.append(", cf_bug_source=");
		sb.append(cf_bug_source);
		sb.append(", cf_viss=");
		sb.append(cf_viss);
		sb.append("}");

		return sb.toString();
	}

	public Bug toEntityModel() {
		BugImpl bugImpl = new BugImpl();

		bugImpl.setBug_id(bug_id);
		bugImpl.setAssigned_to(assigned_to);

		if (bug_file_loc == null) {
			bugImpl.setBug_file_loc(StringPool.BLANK);
		}
		else {
			bugImpl.setBug_file_loc(bug_file_loc);
		}

		if (bug_severity == null) {
			bugImpl.setBug_severity(StringPool.BLANK);
		}
		else {
			bugImpl.setBug_severity(bug_severity);
		}

		if (bug_status == null) {
			bugImpl.setBug_status(StringPool.BLANK);
		}
		else {
			bugImpl.setBug_status(bug_status);
		}

		if (creation_ts == Long.MIN_VALUE) {
			bugImpl.setCreation_ts(null);
		}
		else {
			bugImpl.setCreation_ts(new Date(creation_ts));
		}

		if (delta_ts == Long.MIN_VALUE) {
			bugImpl.setDelta_ts(null);
		}
		else {
			bugImpl.setDelta_ts(new Date(delta_ts));
		}

		if (short_desc == null) {
			bugImpl.setShort_desc(StringPool.BLANK);
		}
		else {
			bugImpl.setShort_desc(short_desc);
		}

		if (host_op_sys == null) {
			bugImpl.setHost_op_sys(StringPool.BLANK);
		}
		else {
			bugImpl.setHost_op_sys(host_op_sys);
		}

		if (guest_op_sys == null) {
			bugImpl.setGuest_op_sys(StringPool.BLANK);
		}
		else {
			bugImpl.setGuest_op_sys(guest_op_sys);
		}

		if (priority == null) {
			bugImpl.setPriority(StringPool.BLANK);
		}
		else {
			bugImpl.setPriority(priority);
		}

		if (rep_platform == null) {
			bugImpl.setRep_platform(StringPool.BLANK);
		}
		else {
			bugImpl.setRep_platform(rep_platform);
		}

		bugImpl.setProduct_id(product_id);
		bugImpl.setComponent_id(component_id);
		bugImpl.setQa_contact(qa_contact);
		bugImpl.setReporter(reporter);
		bugImpl.setCategory_id(category_id);

		if (resolution == null) {
			bugImpl.setResolution(StringPool.BLANK);
		}
		else {
			bugImpl.setResolution(resolution);
		}

		if (target_milestone == null) {
			bugImpl.setTarget_milestone(StringPool.BLANK);
		}
		else {
			bugImpl.setTarget_milestone(target_milestone);
		}

		if (status_whiteboard == null) {
			bugImpl.setStatus_whiteboard(StringPool.BLANK);
		}
		else {
			bugImpl.setStatus_whiteboard(status_whiteboard);
		}

		bugImpl.setVotes(votes);

		if (keywords == null) {
			bugImpl.setKeywords(StringPool.BLANK);
		}
		else {
			bugImpl.setKeywords(keywords);
		}

		if (lastdiffed == Long.MIN_VALUE) {
			bugImpl.setLastdiffed(null);
		}
		else {
			bugImpl.setLastdiffed(new Date(lastdiffed));
		}

		bugImpl.setEverconfirmed(everconfirmed);
		bugImpl.setReporter_accessible(reporter_accessible);
		bugImpl.setCclist_accessible(cclist_accessible);
		bugImpl.setEstimated_time(estimated_time);
		bugImpl.setRemaining_time(remaining_time);

		if (deadline == Long.MIN_VALUE) {
			bugImpl.setDeadline(null);
		}
		else {
			bugImpl.setDeadline(new Date(deadline));
		}

		if (bug_alias == null) {
			bugImpl.setBug_alias(StringPool.BLANK);
		}
		else {
			bugImpl.setBug_alias(bug_alias);
		}

		bugImpl.setFound_in_product_id(found_in_product_id);
		bugImpl.setFound_in_version_id(found_in_version_id);
		bugImpl.setFound_in_phase_id(found_in_phase_id);

		if (cf_type == null) {
			bugImpl.setCf_type(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_type(cf_type);
		}

		if (cf_reported_by == null) {
			bugImpl.setCf_reported_by(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_reported_by(cf_reported_by);
		}

		bugImpl.setCf_attempted(cf_attempted);
		bugImpl.setCf_failed(cf_failed);

		if (cf_public_summary == null) {
			bugImpl.setCf_public_summary(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_public_summary(cf_public_summary);
		}

		bugImpl.setCf_doc_impact(cf_doc_impact);
		bugImpl.setCf_security(cf_security);
		bugImpl.setCf_build(cf_build);

		if (cf_branch == null) {
			bugImpl.setCf_branch(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_branch(cf_branch);
		}

		bugImpl.setCf_change(cf_change);
		bugImpl.setCf_test_id(cf_test_id);

		if (cf_regression == null) {
			bugImpl.setCf_regression(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_regression(cf_regression);
		}

		bugImpl.setCf_reviewer(cf_reviewer);

		if (cf_on_hold == null) {
			bugImpl.setCf_on_hold(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_on_hold(cf_on_hold);
		}

		if (cf_public_severity == null) {
			bugImpl.setCf_public_severity(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_public_severity(cf_public_severity);
		}

		bugImpl.setCf_i18n_impact(cf_i18n_impact);

		if (cf_eta == Long.MIN_VALUE) {
			bugImpl.setCf_eta(null);
		}
		else {
			bugImpl.setCf_eta(new Date(cf_eta));
		}

		if (cf_bug_source == null) {
			bugImpl.setCf_bug_source(StringPool.BLANK);
		}
		else {
			bugImpl.setCf_bug_source(cf_bug_source);
		}

		bugImpl.setCf_viss(cf_viss);

		bugImpl.resetOriginalValues();

		return bugImpl;
	}

	public int bug_id;
	public int assigned_to;
	public String bug_file_loc;
	public String bug_severity;
	public String bug_status;
	public long creation_ts;
	public long delta_ts;
	public String short_desc;
	public String host_op_sys;
	public String guest_op_sys;
	public String priority;
	public String rep_platform;
	public int product_id;
	public int component_id;
	public int qa_contact;
	public int reporter;
	public int category_id;
	public String resolution;
	public String target_milestone;
	public String status_whiteboard;
	public int votes;
	public String keywords;
	public long lastdiffed;
	public int everconfirmed;
	public int reporter_accessible;
	public int cclist_accessible;
	public float estimated_time;
	public float remaining_time;
	public long deadline;
	public String bug_alias;
	public int found_in_product_id;
	public int found_in_version_id;
	public int found_in_phase_id;
	public String cf_type;
	public String cf_reported_by;
	public int cf_attempted;
	public int cf_failed;
	public String cf_public_summary;
	public int cf_doc_impact;
	public int cf_security;
	public int cf_build;
	public String cf_branch;
	public int cf_change;
	public int cf_test_id;
	public String cf_regression;
	public int cf_reviewer;
	public String cf_on_hold;
	public String cf_public_severity;
	public int cf_i18n_impact;
	public long cf_eta;
	public String cf_bug_source;
	public float cf_viss;
}