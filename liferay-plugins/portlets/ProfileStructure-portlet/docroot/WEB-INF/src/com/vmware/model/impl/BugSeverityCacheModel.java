/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.BugSeverity;

import java.io.Serializable;

/**
 * The cache model class for representing BugSeverity in entity cache.
 *
 * @author iscisc
 * @see BugSeverity
 * @generated
 */
public class BugSeverityCacheModel implements CacheModel<BugSeverity>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{bug_severtiy_id=");
		sb.append(bug_severtiy_id);
		sb.append(", value=");
		sb.append(value);
		sb.append(", sortkey=");
		sb.append(sortkey);
		sb.append(", isactive=");
		sb.append(isactive);
		sb.append("}");

		return sb.toString();
	}

	public BugSeverity toEntityModel() {
		BugSeverityImpl bugSeverityImpl = new BugSeverityImpl();

		bugSeverityImpl.setBug_severtiy_id(bug_severtiy_id);

		if (value == null) {
			bugSeverityImpl.setValue(StringPool.BLANK);
		}
		else {
			bugSeverityImpl.setValue(value);
		}

		bugSeverityImpl.setSortkey(sortkey);
		bugSeverityImpl.setIsactive(isactive);

		bugSeverityImpl.resetOriginalValues();

		return bugSeverityImpl;
	}

	public int bug_severtiy_id;
	public String value;
	public int sortkey;
	public int isactive;
}