/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.BugStatus;

import java.io.Serializable;

/**
 * The cache model class for representing BugStatus in entity cache.
 *
 * @author iscisc
 * @see BugStatus
 * @generated
 */
public class BugStatusCacheModel implements CacheModel<BugStatus>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{bug_status_id=");
		sb.append(bug_status_id);
		sb.append(", value=");
		sb.append(value);
		sb.append(", sortkey=");
		sb.append(sortkey);
		sb.append(", isactive=");
		sb.append(isactive);
		sb.append("}");

		return sb.toString();
	}

	public BugStatus toEntityModel() {
		BugStatusImpl bugStatusImpl = new BugStatusImpl();

		bugStatusImpl.setBug_status_id(bug_status_id);

		if (value == null) {
			bugStatusImpl.setValue(StringPool.BLANK);
		}
		else {
			bugStatusImpl.setValue(value);
		}

		bugStatusImpl.setSortkey(sortkey);
		bugStatusImpl.setIsactive(isactive);

		bugStatusImpl.resetOriginalValues();

		return bugStatusImpl;
	}

	public int bug_status_id;
	public String value;
	public int sortkey;
	public int isactive;
}