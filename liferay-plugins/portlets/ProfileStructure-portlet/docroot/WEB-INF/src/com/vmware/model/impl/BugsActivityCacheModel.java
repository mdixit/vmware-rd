/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.BugsActivity;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing BugsActivity in entity cache.
 *
 * @author iscisc
 * @see BugsActivity
 * @generated
 */
public class BugsActivityCacheModel implements CacheModel<BugsActivity>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{bug_activity_id=");
		sb.append(bug_activity_id);
		sb.append(", bug_id=");
		sb.append(bug_id);
		sb.append(", attach_id=");
		sb.append(attach_id);
		sb.append(", who=");
		sb.append(who);
		sb.append(", bug_when=");
		sb.append(bug_when);
		sb.append(", fieldid=");
		sb.append(fieldid);
		sb.append(", added=");
		sb.append(added);
		sb.append(", removed=");
		sb.append(removed);
		sb.append("}");

		return sb.toString();
	}

	public BugsActivity toEntityModel() {
		BugsActivityImpl bugsActivityImpl = new BugsActivityImpl();

		bugsActivityImpl.setBug_activity_id(bug_activity_id);
		bugsActivityImpl.setBug_id(bug_id);
		bugsActivityImpl.setAttach_id(attach_id);
		bugsActivityImpl.setWho(who);

		if (bug_when == Long.MIN_VALUE) {
			bugsActivityImpl.setBug_when(null);
		}
		else {
			bugsActivityImpl.setBug_when(new Date(bug_when));
		}

		bugsActivityImpl.setFieldid(fieldid);

		if (added == null) {
			bugsActivityImpl.setAdded(StringPool.BLANK);
		}
		else {
			bugsActivityImpl.setAdded(added);
		}

		if (removed == null) {
			bugsActivityImpl.setRemoved(StringPool.BLANK);
		}
		else {
			bugsActivityImpl.setRemoved(removed);
		}

		bugsActivityImpl.resetOriginalValues();

		return bugsActivityImpl;
	}

	public int bug_activity_id;
	public int bug_id;
	public int attach_id;
	public int who;
	public long bug_when;
	public int fieldid;
	public String added;
	public String removed;
}