/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.CCEntity;

import java.io.Serializable;

/**
 * The cache model class for representing CCEntity in entity cache.
 *
 * @author iscisc
 * @see CCEntity
 * @generated
 */
public class CCEntityCacheModel implements CacheModel<CCEntity>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{bug_id=");
		sb.append(bug_id);
		sb.append(", who=");
		sb.append(who);
		sb.append("}");

		return sb.toString();
	}

	public CCEntity toEntityModel() {
		CCEntityImpl ccEntityImpl = new CCEntityImpl();

		ccEntityImpl.setBug_id(bug_id);
		ccEntityImpl.setWho(who);

		ccEntityImpl.resetOriginalValues();

		return ccEntityImpl;
	}

	public int bug_id;
	public int who;
}