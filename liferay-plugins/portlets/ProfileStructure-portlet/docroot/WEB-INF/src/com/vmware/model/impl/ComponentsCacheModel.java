/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Components;

import java.io.Serializable;

/**
 * The cache model class for representing Components in entity cache.
 *
 * @author iscisc
 * @see Components
 * @generated
 */
public class ComponentsCacheModel implements CacheModel<Components>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{comp_id=");
		sb.append(comp_id);
		sb.append(", category_id=");
		sb.append(category_id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", template=");
		sb.append(template);
		sb.append(", initialowner=");
		sb.append(initialowner);
		sb.append(", initialqacontact=");
		sb.append(initialqacontact);
		sb.append(", manager=");
		sb.append(manager);
		sb.append(", qa_manager=");
		sb.append(qa_manager);
		sb.append(", disallownew=");
		sb.append(disallownew);
		sb.append(", disable_template=");
		sb.append(disable_template);
		sb.append("}");

		return sb.toString();
	}

	public Components toEntityModel() {
		ComponentsImpl componentsImpl = new ComponentsImpl();

		componentsImpl.setComp_id(comp_id);
		componentsImpl.setCategory_id(category_id);

		if (name == null) {
			componentsImpl.setName(StringPool.BLANK);
		}
		else {
			componentsImpl.setName(name);
		}

		if (description == null) {
			componentsImpl.setDescription(StringPool.BLANK);
		}
		else {
			componentsImpl.setDescription(description);
		}

		if (template == null) {
			componentsImpl.setTemplate(StringPool.BLANK);
		}
		else {
			componentsImpl.setTemplate(template);
		}

		componentsImpl.setInitialowner(initialowner);
		componentsImpl.setInitialqacontact(initialqacontact);
		componentsImpl.setManager(manager);
		componentsImpl.setQa_manager(qa_manager);
		componentsImpl.setDisallownew(disallownew);
		componentsImpl.setDisable_template(disable_template);

		componentsImpl.resetOriginalValues();

		return componentsImpl;
	}

	public int comp_id;
	public int category_id;
	public String name;
	public String description;
	public String template;
	public int initialowner;
	public int initialqacontact;
	public int manager;
	public int qa_manager;
	public int disallownew;
	public int disable_template;
}