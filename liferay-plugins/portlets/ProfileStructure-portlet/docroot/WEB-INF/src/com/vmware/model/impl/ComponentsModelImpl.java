/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.model.Components;
import com.vmware.model.ComponentsModel;
import com.vmware.model.ComponentsSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the Components service. Represents a row in the &quot;components&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.vmware.model.ComponentsModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ComponentsImpl}.
 * </p>
 *
 * @author iscisc
 * @see ComponentsImpl
 * @see com.vmware.model.Components
 * @see com.vmware.model.ComponentsModel
 * @generated
 */
@JSON(strict = true)
public class ComponentsModelImpl extends BaseModelImpl<Components>
	implements ComponentsModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a components model instance should use the {@link com.vmware.model.Components} interface instead.
	 */
	public static final String TABLE_NAME = "components";
	public static final Object[][] TABLE_COLUMNS = {
			{ "id", Types.INTEGER },
			{ "category_id", Types.INTEGER },
			{ "name", Types.VARCHAR },
			{ "description", Types.VARCHAR },
			{ "template", Types.VARCHAR },
			{ "initialowner", Types.INTEGER },
			{ "initialqacontact", Types.INTEGER },
			{ "manager", Types.INTEGER },
			{ "qa_manager", Types.INTEGER },
			{ "disallownew", Types.INTEGER },
			{ "disable_template", Types.INTEGER }
		};
	public static final String TABLE_SQL_CREATE = "create table components (id INTEGER not null primary key,category_id INTEGER,name VARCHAR(75) null,description VARCHAR(75) null,template VARCHAR(75) null,initialowner INTEGER,initialqacontact INTEGER,manager INTEGER,qa_manager INTEGER,disallownew INTEGER,disable_template INTEGER)";
	public static final String TABLE_SQL_DROP = "drop table components";
	public static final String DATA_SOURCE = "bugzilla";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.vmware.model.Components"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.vmware.model.Components"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static Components toModel(ComponentsSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		Components model = new ComponentsImpl();

		model.setComp_id(soapModel.getComp_id());
		model.setCategory_id(soapModel.getCategory_id());
		model.setName(soapModel.getName());
		model.setDescription(soapModel.getDescription());
		model.setTemplate(soapModel.getTemplate());
		model.setInitialowner(soapModel.getInitialowner());
		model.setInitialqacontact(soapModel.getInitialqacontact());
		model.setManager(soapModel.getManager());
		model.setQa_manager(soapModel.getQa_manager());
		model.setDisallownew(soapModel.getDisallownew());
		model.setDisable_template(soapModel.getDisable_template());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<Components> toModels(ComponentsSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<Components> models = new ArrayList<Components>(soapModels.length);

		for (ComponentsSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.vmware.model.Components"));

	public ComponentsModelImpl() {
	}

	public int getPrimaryKey() {
		return _comp_id;
	}

	public void setPrimaryKey(int primaryKey) {
		setComp_id(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Integer(_comp_id);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	public Class<?> getModelClass() {
		return Components.class;
	}

	public String getModelClassName() {
		return Components.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("comp_id", getComp_id());
		attributes.put("category_id", getCategory_id());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("template", getTemplate());
		attributes.put("initialowner", getInitialowner());
		attributes.put("initialqacontact", getInitialqacontact());
		attributes.put("manager", getManager());
		attributes.put("qa_manager", getQa_manager());
		attributes.put("disallownew", getDisallownew());
		attributes.put("disable_template", getDisable_template());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer comp_id = (Integer)attributes.get("comp_id");

		if (comp_id != null) {
			setComp_id(comp_id);
		}

		Integer category_id = (Integer)attributes.get("category_id");

		if (category_id != null) {
			setCategory_id(category_id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String template = (String)attributes.get("template");

		if (template != null) {
			setTemplate(template);
		}

		Integer initialowner = (Integer)attributes.get("initialowner");

		if (initialowner != null) {
			setInitialowner(initialowner);
		}

		Integer initialqacontact = (Integer)attributes.get("initialqacontact");

		if (initialqacontact != null) {
			setInitialqacontact(initialqacontact);
		}

		Integer manager = (Integer)attributes.get("manager");

		if (manager != null) {
			setManager(manager);
		}

		Integer qa_manager = (Integer)attributes.get("qa_manager");

		if (qa_manager != null) {
			setQa_manager(qa_manager);
		}

		Integer disallownew = (Integer)attributes.get("disallownew");

		if (disallownew != null) {
			setDisallownew(disallownew);
		}

		Integer disable_template = (Integer)attributes.get("disable_template");

		if (disable_template != null) {
			setDisable_template(disable_template);
		}
	}

	@JSON
	public int getComp_id() {
		return _comp_id;
	}

	public void setComp_id(int comp_id) {
		_comp_id = comp_id;
	}

	@JSON
	public int getCategory_id() {
		return _category_id;
	}

	public void setCategory_id(int category_id) {
		_category_id = category_id;
	}

	@JSON
	public String getName() {
		if (_name == null) {
			return StringPool.BLANK;
		}
		else {
			return _name;
		}
	}

	public void setName(String name) {
		_name = name;
	}

	@JSON
	public String getDescription() {
		if (_description == null) {
			return StringPool.BLANK;
		}
		else {
			return _description;
		}
	}

	public void setDescription(String description) {
		_description = description;
	}

	@JSON
	public String getTemplate() {
		if (_template == null) {
			return StringPool.BLANK;
		}
		else {
			return _template;
		}
	}

	public void setTemplate(String template) {
		_template = template;
	}

	@JSON
	public int getInitialowner() {
		return _initialowner;
	}

	public void setInitialowner(int initialowner) {
		_initialowner = initialowner;
	}

	@JSON
	public int getInitialqacontact() {
		return _initialqacontact;
	}

	public void setInitialqacontact(int initialqacontact) {
		_initialqacontact = initialqacontact;
	}

	@JSON
	public int getManager() {
		return _manager;
	}

	public void setManager(int manager) {
		_manager = manager;
	}

	@JSON
	public int getQa_manager() {
		return _qa_manager;
	}

	public void setQa_manager(int qa_manager) {
		_qa_manager = qa_manager;
	}

	@JSON
	public int getDisallownew() {
		return _disallownew;
	}

	public void setDisallownew(int disallownew) {
		_disallownew = disallownew;
	}

	@JSON
	public int getDisable_template() {
		return _disable_template;
	}

	public void setDisable_template(int disable_template) {
		_disable_template = disable_template;
	}

	@Override
	public Components toEscapedModel() {
		if (_escapedModelProxy == null) {
			_escapedModelProxy = (Components)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelProxyInterfaces,
					new AutoEscapeBeanHandler(this));
		}

		return _escapedModelProxy;
	}

	@Override
	public Object clone() {
		ComponentsImpl componentsImpl = new ComponentsImpl();

		componentsImpl.setComp_id(getComp_id());
		componentsImpl.setCategory_id(getCategory_id());
		componentsImpl.setName(getName());
		componentsImpl.setDescription(getDescription());
		componentsImpl.setTemplate(getTemplate());
		componentsImpl.setInitialowner(getInitialowner());
		componentsImpl.setInitialqacontact(getInitialqacontact());
		componentsImpl.setManager(getManager());
		componentsImpl.setQa_manager(getQa_manager());
		componentsImpl.setDisallownew(getDisallownew());
		componentsImpl.setDisable_template(getDisable_template());

		componentsImpl.resetOriginalValues();

		return componentsImpl;
	}

	public int compareTo(Components components) {
		int primaryKey = components.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		Components components = null;

		try {
			components = (Components)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		int primaryKey = components.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<Components> toCacheModel() {
		ComponentsCacheModel componentsCacheModel = new ComponentsCacheModel();

		componentsCacheModel.comp_id = getComp_id();

		componentsCacheModel.category_id = getCategory_id();

		componentsCacheModel.name = getName();

		String name = componentsCacheModel.name;

		if ((name != null) && (name.length() == 0)) {
			componentsCacheModel.name = null;
		}

		componentsCacheModel.description = getDescription();

		String description = componentsCacheModel.description;

		if ((description != null) && (description.length() == 0)) {
			componentsCacheModel.description = null;
		}

		componentsCacheModel.template = getTemplate();

		String template = componentsCacheModel.template;

		if ((template != null) && (template.length() == 0)) {
			componentsCacheModel.template = null;
		}

		componentsCacheModel.initialowner = getInitialowner();

		componentsCacheModel.initialqacontact = getInitialqacontact();

		componentsCacheModel.manager = getManager();

		componentsCacheModel.qa_manager = getQa_manager();

		componentsCacheModel.disallownew = getDisallownew();

		componentsCacheModel.disable_template = getDisable_template();

		return componentsCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{comp_id=");
		sb.append(getComp_id());
		sb.append(", category_id=");
		sb.append(getCategory_id());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", description=");
		sb.append(getDescription());
		sb.append(", template=");
		sb.append(getTemplate());
		sb.append(", initialowner=");
		sb.append(getInitialowner());
		sb.append(", initialqacontact=");
		sb.append(getInitialqacontact());
		sb.append(", manager=");
		sb.append(getManager());
		sb.append(", qa_manager=");
		sb.append(getQa_manager());
		sb.append(", disallownew=");
		sb.append(getDisallownew());
		sb.append(", disable_template=");
		sb.append(getDisable_template());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Components");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>comp_id</column-name><column-value><![CDATA[");
		sb.append(getComp_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>category_id</column-name><column-value><![CDATA[");
		sb.append(getCategory_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>description</column-name><column-value><![CDATA[");
		sb.append(getDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>template</column-name><column-value><![CDATA[");
		sb.append(getTemplate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>initialowner</column-name><column-value><![CDATA[");
		sb.append(getInitialowner());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>initialqacontact</column-name><column-value><![CDATA[");
		sb.append(getInitialqacontact());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>manager</column-name><column-value><![CDATA[");
		sb.append(getManager());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>qa_manager</column-name><column-value><![CDATA[");
		sb.append(getQa_manager());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disallownew</column-name><column-value><![CDATA[");
		sb.append(getDisallownew());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>disable_template</column-name><column-value><![CDATA[");
		sb.append(getDisable_template());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = Components.class.getClassLoader();
	private static Class<?>[] _escapedModelProxyInterfaces = new Class[] {
			Components.class
		};
	private int _comp_id;
	private int _category_id;
	private String _name;
	private String _description;
	private String _template;
	private int _initialowner;
	private int _initialqacontact;
	private int _manager;
	private int _qa_manager;
	private int _disallownew;
	private int _disable_template;
	private Components _escapedModelProxy;
}