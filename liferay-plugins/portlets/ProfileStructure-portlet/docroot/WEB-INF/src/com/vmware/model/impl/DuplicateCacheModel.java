/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Duplicate;

import java.io.Serializable;

/**
 * The cache model class for representing Duplicate in entity cache.
 *
 * @author iscisc
 * @see Duplicate
 * @generated
 */
public class DuplicateCacheModel implements CacheModel<Duplicate>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{dupe=");
		sb.append(dupe);
		sb.append(", dupe_of=");
		sb.append(dupe_of);
		sb.append("}");

		return sb.toString();
	}

	public Duplicate toEntityModel() {
		DuplicateImpl duplicateImpl = new DuplicateImpl();

		duplicateImpl.setDupe(dupe);
		duplicateImpl.setDupe_of(dupe_of);

		duplicateImpl.resetOriginalValues();

		return duplicateImpl;
	}

	public int dupe;
	public int dupe_of;
}