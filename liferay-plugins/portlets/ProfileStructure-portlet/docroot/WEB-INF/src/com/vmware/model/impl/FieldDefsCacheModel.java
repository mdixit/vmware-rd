/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.FieldDefs;

import java.io.Serializable;

/**
 * The cache model class for representing FieldDefs in entity cache.
 *
 * @author iscisc
 * @see FieldDefs
 * @generated
 */
public class FieldDefsCacheModel implements CacheModel<FieldDefs>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{fielddef_id=");
		sb.append(fielddef_id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", type=");
		sb.append(type);
		sb.append(", custom=");
		sb.append(custom);
		sb.append(", description=");
		sb.append(description);
		sb.append(", mailhead=");
		sb.append(mailhead);
		sb.append(", sortkey=");
		sb.append(sortkey);
		sb.append(", obsolete=");
		sb.append(obsolete);
		sb.append(", enter_bug=");
		sb.append(enter_bug);
		sb.append("}");

		return sb.toString();
	}

	public FieldDefs toEntityModel() {
		FieldDefsImpl fieldDefsImpl = new FieldDefsImpl();

		fieldDefsImpl.setFielddef_id(fielddef_id);

		if (name == null) {
			fieldDefsImpl.setName(StringPool.BLANK);
		}
		else {
			fieldDefsImpl.setName(name);
		}

		fieldDefsImpl.setType(type);
		fieldDefsImpl.setCustom(custom);

		if (description == null) {
			fieldDefsImpl.setDescription(StringPool.BLANK);
		}
		else {
			fieldDefsImpl.setDescription(description);
		}

		fieldDefsImpl.setMailhead(mailhead);
		fieldDefsImpl.setSortkey(sortkey);
		fieldDefsImpl.setObsolete(obsolete);
		fieldDefsImpl.setEnter_bug(enter_bug);

		fieldDefsImpl.resetOriginalValues();

		return fieldDefsImpl;
	}

	public int fielddef_id;
	public String name;
	public int type;
	public int custom;
	public String description;
	public int mailhead;
	public int sortkey;
	public int obsolete;
	public int enter_bug;
}