/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Group;

import java.io.Serializable;

/**
 * The cache model class for representing Group in entity cache.
 *
 * @author iscisc
 * @see Group
 * @generated
 */
public class GroupCacheModel implements CacheModel<Group>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{group_id=");
		sb.append(group_id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", isbuggroup=");
		sb.append(isbuggroup);
		sb.append(", isactive=");
		sb.append(isactive);
		sb.append(", description=");
		sb.append(description);
		sb.append(", userregexp=");
		sb.append(userregexp);
		sb.append("}");

		return sb.toString();
	}

	public Group toEntityModel() {
		GroupImpl groupImpl = new GroupImpl();

		groupImpl.setGroup_id(group_id);

		if (name == null) {
			groupImpl.setName(StringPool.BLANK);
		}
		else {
			groupImpl.setName(name);
		}

		groupImpl.setIsbuggroup(isbuggroup);
		groupImpl.setIsactive(isactive);

		if (description == null) {
			groupImpl.setDescription(StringPool.BLANK);
		}
		else {
			groupImpl.setDescription(description);
		}

		if (userregexp == null) {
			groupImpl.setUserregexp(StringPool.BLANK);
		}
		else {
			groupImpl.setUserregexp(userregexp);
		}

		groupImpl.resetOriginalValues();

		return groupImpl;
	}

	public int group_id;
	public String name;
	public int isbuggroup;
	public int isactive;
	public String description;
	public String userregexp;
}