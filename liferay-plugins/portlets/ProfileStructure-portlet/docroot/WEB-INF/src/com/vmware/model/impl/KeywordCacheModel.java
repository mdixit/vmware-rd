/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Keyword;

import java.io.Serializable;

/**
 * The cache model class for representing Keyword in entity cache.
 *
 * @author iscisc
 * @see Keyword
 * @generated
 */
public class KeywordCacheModel implements CacheModel<Keyword>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{bug_id=");
		sb.append(bug_id);
		sb.append(", keywordid=");
		sb.append(keywordid);
		sb.append("}");

		return sb.toString();
	}

	public Keyword toEntityModel() {
		KeywordImpl keywordImpl = new KeywordImpl();

		keywordImpl.setBug_id(bug_id);
		keywordImpl.setKeywordid(keywordid);

		keywordImpl.resetOriginalValues();

		return keywordImpl;
	}

	public int bug_id;
	public int keywordid;
}