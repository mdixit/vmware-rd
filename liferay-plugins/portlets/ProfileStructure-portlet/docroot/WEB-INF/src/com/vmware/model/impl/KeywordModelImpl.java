/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.vmware.model.Keyword;
import com.vmware.model.KeywordModel;
import com.vmware.model.KeywordSoap;

import com.vmware.service.persistence.KeywordPK;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the Keyword service. Represents a row in the &quot;keywords&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.vmware.model.KeywordModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link KeywordImpl}.
 * </p>
 *
 * @author iscisc
 * @see KeywordImpl
 * @see com.vmware.model.Keyword
 * @see com.vmware.model.KeywordModel
 * @generated
 */
@JSON(strict = true)
public class KeywordModelImpl extends BaseModelImpl<Keyword>
	implements KeywordModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a keyword model instance should use the {@link com.vmware.model.Keyword} interface instead.
	 */
	public static final String TABLE_NAME = "keywords";
	public static final Object[][] TABLE_COLUMNS = {
			{ "bug_id", Types.INTEGER },
			{ "keywordid", Types.INTEGER }
		};
	public static final String TABLE_SQL_CREATE = "create table keywords (bug_id INTEGER not null,keywordid INTEGER not null,primary key (bug_id, keywordid))";
	public static final String TABLE_SQL_DROP = "drop table keywords";
	public static final String DATA_SOURCE = "bugzilla";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.vmware.model.Keyword"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.vmware.model.Keyword"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static Keyword toModel(KeywordSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		Keyword model = new KeywordImpl();

		model.setBug_id(soapModel.getBug_id());
		model.setKeywordid(soapModel.getKeywordid());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<Keyword> toModels(KeywordSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<Keyword> models = new ArrayList<Keyword>(soapModels.length);

		for (KeywordSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.vmware.model.Keyword"));

	public KeywordModelImpl() {
	}

	public KeywordPK getPrimaryKey() {
		return new KeywordPK(_bug_id, _keywordid);
	}

	public void setPrimaryKey(KeywordPK primaryKey) {
		setBug_id(primaryKey.bug_id);
		setKeywordid(primaryKey.keywordid);
	}

	public Serializable getPrimaryKeyObj() {
		return new KeywordPK(_bug_id, _keywordid);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((KeywordPK)primaryKeyObj);
	}

	public Class<?> getModelClass() {
		return Keyword.class;
	}

	public String getModelClassName() {
		return Keyword.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bug_id", getBug_id());
		attributes.put("keywordid", getKeywordid());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer bug_id = (Integer)attributes.get("bug_id");

		if (bug_id != null) {
			setBug_id(bug_id);
		}

		Integer keywordid = (Integer)attributes.get("keywordid");

		if (keywordid != null) {
			setKeywordid(keywordid);
		}
	}

	@JSON
	public int getBug_id() {
		return _bug_id;
	}

	public void setBug_id(int bug_id) {
		_bug_id = bug_id;
	}

	@JSON
	public int getKeywordid() {
		return _keywordid;
	}

	public void setKeywordid(int keywordid) {
		_keywordid = keywordid;
	}

	@Override
	public Keyword toEscapedModel() {
		if (_escapedModelProxy == null) {
			_escapedModelProxy = (Keyword)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelProxyInterfaces,
					new AutoEscapeBeanHandler(this));
		}

		return _escapedModelProxy;
	}

	@Override
	public Object clone() {
		KeywordImpl keywordImpl = new KeywordImpl();

		keywordImpl.setBug_id(getBug_id());
		keywordImpl.setKeywordid(getKeywordid());

		keywordImpl.resetOriginalValues();

		return keywordImpl;
	}

	public int compareTo(Keyword keyword) {
		KeywordPK primaryKey = keyword.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		Keyword keyword = null;

		try {
			keyword = (Keyword)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		KeywordPK primaryKey = keyword.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<Keyword> toCacheModel() {
		KeywordCacheModel keywordCacheModel = new KeywordCacheModel();

		keywordCacheModel.bug_id = getBug_id();

		keywordCacheModel.keywordid = getKeywordid();

		return keywordCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{bug_id=");
		sb.append(getBug_id());
		sb.append(", keywordid=");
		sb.append(getKeywordid());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.vmware.model.Keyword");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>bug_id</column-name><column-value><![CDATA[");
		sb.append(getBug_id());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>keywordid</column-name><column-value><![CDATA[");
		sb.append(getKeywordid());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = Keyword.class.getClassLoader();
	private static Class<?>[] _escapedModelProxyInterfaces = new Class[] {
			Keyword.class
		};
	private int _bug_id;
	private int _keywordid;
	private Keyword _escapedModelProxy;
}