/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.LongDescription;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing LongDescription in entity cache.
 *
 * @author iscisc
 * @see LongDescription
 * @generated
 */
public class LongDescriptionCacheModel implements CacheModel<LongDescription>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{comment_id=");
		sb.append(comment_id);
		sb.append(", bug_id=");
		sb.append(bug_id);
		sb.append(", who=");
		sb.append(who);
		sb.append(", bug_when=");
		sb.append(bug_when);
		sb.append(", work_time=");
		sb.append(work_time);
		sb.append(", thetext=");
		sb.append(thetext);
		sb.append(", isprivate=");
		sb.append(isprivate);
		sb.append(", already_wrapped=");
		sb.append(already_wrapped);
		sb.append(", type=");
		sb.append(type);
		sb.append(", extra_data=");
		sb.append(extra_data);
		sb.append("}");

		return sb.toString();
	}

	public LongDescription toEntityModel() {
		LongDescriptionImpl longDescriptionImpl = new LongDescriptionImpl();

		longDescriptionImpl.setComment_id(comment_id);
		longDescriptionImpl.setBug_id(bug_id);
		longDescriptionImpl.setWho(who);

		if (bug_when == Long.MIN_VALUE) {
			longDescriptionImpl.setBug_when(null);
		}
		else {
			longDescriptionImpl.setBug_when(new Date(bug_when));
		}

		longDescriptionImpl.setWork_time(work_time);

		if (thetext == null) {
			longDescriptionImpl.setThetext(StringPool.BLANK);
		}
		else {
			longDescriptionImpl.setThetext(thetext);
		}

		longDescriptionImpl.setIsprivate(isprivate);
		longDescriptionImpl.setAlready_wrapped(already_wrapped);
		longDescriptionImpl.setType(type);

		if (extra_data == null) {
			longDescriptionImpl.setExtra_data(StringPool.BLANK);
		}
		else {
			longDescriptionImpl.setExtra_data(extra_data);
		}

		longDescriptionImpl.resetOriginalValues();

		return longDescriptionImpl;
	}

	public int comment_id;
	public int bug_id;
	public int who;
	public long bug_when;
	public Float work_time;
	public String thetext;
	public int isprivate;
	public int already_wrapped;
	public int type;
	public String extra_data;
}