/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Products;

import java.io.Serializable;

/**
 * The cache model class for representing Products in entity cache.
 *
 * @author iscisc
 * @see Products
 * @generated
 */
public class ProductsCacheModel implements CacheModel<Products>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{prod_id=");
		sb.append(prod_id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", template=");
		sb.append(template);
		sb.append(", sortkey=");
		sb.append(sortkey);
		sb.append(", defaultcategory=");
		sb.append(defaultcategory);
		sb.append(", disallownew=");
		sb.append(disallownew);
		sb.append("}");

		return sb.toString();
	}

	public Products toEntityModel() {
		ProductsImpl productsImpl = new ProductsImpl();

		productsImpl.setProd_id(prod_id);

		if (name == null) {
			productsImpl.setName(StringPool.BLANK);
		}
		else {
			productsImpl.setName(name);
		}

		if (description == null) {
			productsImpl.setDescription(StringPool.BLANK);
		}
		else {
			productsImpl.setDescription(description);
		}

		if (template == null) {
			productsImpl.setTemplate(StringPool.BLANK);
		}
		else {
			productsImpl.setTemplate(template);
		}

		productsImpl.setSortkey(sortkey);
		productsImpl.setDefaultcategory(defaultcategory);
		productsImpl.setDisallownew(disallownew);

		productsImpl.resetOriginalValues();

		return productsImpl;
	}

	public int prod_id;
	public String name;
	public String description;
	public String template;
	public int sortkey;
	public int defaultcategory;
	public int disallownew;
}