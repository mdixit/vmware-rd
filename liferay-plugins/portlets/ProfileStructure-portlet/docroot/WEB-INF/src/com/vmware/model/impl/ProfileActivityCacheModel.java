/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.ProfileActivity;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing ProfileActivity in entity cache.
 *
 * @author iscisc
 * @see ProfileActivity
 * @generated
 */
public class ProfileActivityCacheModel implements CacheModel<ProfileActivity>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{userid=");
		sb.append(userid);
		sb.append(", who=");
		sb.append(who);
		sb.append(", profiles_when=");
		sb.append(profiles_when);
		sb.append(", fieldid=");
		sb.append(fieldid);
		sb.append(", oldvalue=");
		sb.append(oldvalue);
		sb.append(", newvalue=");
		sb.append(newvalue);
		sb.append("}");

		return sb.toString();
	}

	public ProfileActivity toEntityModel() {
		ProfileActivityImpl profileActivityImpl = new ProfileActivityImpl();

		profileActivityImpl.setUserid(userid);
		profileActivityImpl.setWho(who);

		if (profiles_when == Long.MIN_VALUE) {
			profileActivityImpl.setProfiles_when(null);
		}
		else {
			profileActivityImpl.setProfiles_when(new Date(profiles_when));
		}

		profileActivityImpl.setFieldid(fieldid);

		if (oldvalue == null) {
			profileActivityImpl.setOldvalue(StringPool.BLANK);
		}
		else {
			profileActivityImpl.setOldvalue(oldvalue);
		}

		if (newvalue == null) {
			profileActivityImpl.setNewvalue(StringPool.BLANK);
		}
		else {
			profileActivityImpl.setNewvalue(newvalue);
		}

		profileActivityImpl.resetOriginalValues();

		return profileActivityImpl;
	}

	public int userid;
	public int who;
	public long profiles_when;
	public int fieldid;
	public String oldvalue;
	public String newvalue;
}