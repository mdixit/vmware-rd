/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Profiles;

import java.io.Serializable;

/**
 * The cache model class for representing Profiles in entity cache.
 *
 * @author iscisc
 * @see Profiles
 * @generated
 */
public class ProfilesCacheModel implements CacheModel<Profiles>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{userid=");
		sb.append(userid);
		sb.append(", login_name=");
		sb.append(login_name);
		sb.append(", all_login_names=");
		sb.append(all_login_names);
		sb.append(", cryptpassword=");
		sb.append(cryptpassword);
		sb.append(", realname=");
		sb.append(realname);
		sb.append(", disabledtext=");
		sb.append(disabledtext);
		sb.append(", disable_mail=");
		sb.append(disable_mail);
		sb.append(", mybugslink=");
		sb.append(mybugslink);
		sb.append(", manager_id=");
		sb.append(manager_id);
		sb.append(", extern_id=");
		sb.append(extern_id);
		sb.append("}");

		return sb.toString();
	}

	public Profiles toEntityModel() {
		ProfilesImpl profilesImpl = new ProfilesImpl();

		profilesImpl.setUserid(userid);

		if (login_name == null) {
			profilesImpl.setLogin_name(StringPool.BLANK);
		}
		else {
			profilesImpl.setLogin_name(login_name);
		}

		if (all_login_names == null) {
			profilesImpl.setAll_login_names(StringPool.BLANK);
		}
		else {
			profilesImpl.setAll_login_names(all_login_names);
		}

		if (cryptpassword == null) {
			profilesImpl.setCryptpassword(StringPool.BLANK);
		}
		else {
			profilesImpl.setCryptpassword(cryptpassword);
		}

		if (realname == null) {
			profilesImpl.setRealname(StringPool.BLANK);
		}
		else {
			profilesImpl.setRealname(realname);
		}

		if (disabledtext == null) {
			profilesImpl.setDisabledtext(StringPool.BLANK);
		}
		else {
			profilesImpl.setDisabledtext(disabledtext);
		}

		profilesImpl.setDisable_mail(disable_mail);
		profilesImpl.setMybugslink(mybugslink);
		profilesImpl.setManager_id(manager_id);

		if (extern_id == null) {
			profilesImpl.setExtern_id(StringPool.BLANK);
		}
		else {
			profilesImpl.setExtern_id(extern_id);
		}

		profilesImpl.resetOriginalValues();

		return profilesImpl;
	}

	public int userid;
	public String login_name;
	public String all_login_names;
	public String cryptpassword;
	public String realname;
	public String disabledtext;
	public int disable_mail;
	public int mybugslink;
	public int manager_id;
	public String extern_id;
}