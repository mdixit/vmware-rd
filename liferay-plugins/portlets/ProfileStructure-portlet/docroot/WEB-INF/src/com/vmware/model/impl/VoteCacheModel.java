/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import com.vmware.model.Vote;

import java.io.Serializable;

/**
 * The cache model class for representing Vote in entity cache.
 *
 * @author iscisc
 * @see Vote
 * @generated
 */
public class VoteCacheModel implements CacheModel<Vote>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{bug_id=");
		sb.append(bug_id);
		sb.append(", who=");
		sb.append(who);
		sb.append(", vote_count=");
		sb.append(vote_count);
		sb.append("}");

		return sb.toString();
	}

	public Vote toEntityModel() {
		VoteImpl voteImpl = new VoteImpl();

		voteImpl.setBug_id(bug_id);
		voteImpl.setWho(who);
		voteImpl.setVote_count(vote_count);

		voteImpl.resetOriginalValues();

		return voteImpl;
	}

	public int bug_id;
	public int who;
	public int vote_count;
}