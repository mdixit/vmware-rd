/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.base;

import com.vmware.service.BugLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 */
public class BugLocalServiceClpInvoker {
	public BugLocalServiceClpInvoker() {
		_methodName0 = "addBug";

		_methodParameterTypes0 = new String[] { "com.vmware.model.Bug" };

		_methodName1 = "createBug";

		_methodParameterTypes1 = new String[] { "int" };

		_methodName2 = "deleteBug";

		_methodParameterTypes2 = new String[] { "int" };

		_methodName3 = "deleteBug";

		_methodParameterTypes3 = new String[] { "com.vmware.model.Bug" };

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "fetchBug";

		_methodParameterTypes9 = new String[] { "int" };

		_methodName10 = "getBug";

		_methodParameterTypes10 = new String[] { "int" };

		_methodName11 = "getPersistedModel";

		_methodParameterTypes11 = new String[] { "java.io.Serializable" };

		_methodName12 = "getBugs";

		_methodParameterTypes12 = new String[] { "int", "int" };

		_methodName13 = "getBugsCount";

		_methodParameterTypes13 = new String[] {  };

		_methodName14 = "updateBug";

		_methodParameterTypes14 = new String[] { "com.vmware.model.Bug" };

		_methodName15 = "updateBug";

		_methodParameterTypes15 = new String[] { "com.vmware.model.Bug", "boolean" };

		_methodName144 = "getBeanIdentifier";

		_methodParameterTypes144 = new String[] {  };

		_methodName145 = "setBeanIdentifier";

		_methodParameterTypes145 = new String[] { "java.lang.String" };

		_methodName150 = "getBugByAssignToId";

		_methodParameterTypes150 = new String[] { "int" };

		_methodName151 = "getBugSeverityByUserId";

		_methodParameterTypes151 = new String[] { "int", "java.lang.String" };

		_methodName152 = "getBugPriorityByUserId";

		_methodParameterTypes152 = new String[] { "int", "java.lang.String" };

		_methodName153 = "getBugStatusByUserId";

		_methodParameterTypes153 = new String[] { "int", "java.lang.String" };

		_methodName154 = "getBugKeywordsByUserId";

		_methodParameterTypes154 = new String[] { "int", "java.lang.String" };

		_methodName155 = "getBugsByReporter";

		_methodParameterTypes155 = new String[] { "int" };

		_methodName156 = "getBugShortDescByUserId";

		_methodParameterTypes156 = new String[] { "int", "java.lang.String" };

		_methodName157 = "getBugVotesByUserId";

		_methodParameterTypes157 = new String[] { "int", "int" };

		_methodName158 = "getBugPriority";

		_methodParameterTypes158 = new String[] { "java.lang.String" };

		_methodName159 = "getBugResolutionByUserId";

		_methodParameterTypes159 = new String[] { "int", "java.lang.String" };

		_methodName160 = "getBugReporterByBugId";

		_methodParameterTypes160 = new String[] { "int" };

		_methodName161 = "getBugQAContactByBugId";

		_methodParameterTypes161 = new String[] { "int" };

		_methodName162 = "getBugComponentsByBugId";

		_methodParameterTypes162 = new String[] { "int" };

		_methodName163 = "getBugProductsByBugId";

		_methodParameterTypes163 = new String[] { "int" };

		_methodName164 = "getBugSeverityByBugId";

		_methodParameterTypes164 = new String[] { "int" };

		_methodName165 = "getBugStatus";

		_methodParameterTypes165 = new String[] { "int" };

		_methodName166 = "getBugAssignedByBugId";

		_methodParameterTypes166 = new String[] { "int" };

		_methodName167 = "getBugResolutionByBugId";

		_methodParameterTypes167 = new String[] { "int" };

		_methodName168 = "getKeywordDefByBugId";

		_methodParameterTypes168 = new String[] { "int" };

		_methodName169 = "getBugByShortDesc";

		_methodParameterTypes169 = new String[] { "java.lang.String" };

		_methodName170 = "getDuplicateByDupeofId";

		_methodParameterTypes170 = new String[] { "int" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return BugLocalServiceUtil.addBug((com.vmware.model.Bug)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return BugLocalServiceUtil.createBug(((Integer)arguments[0]).intValue());
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return BugLocalServiceUtil.deleteBug(((Integer)arguments[0]).intValue());
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return BugLocalServiceUtil.deleteBug((com.vmware.model.Bug)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return BugLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return BugLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return BugLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return BugLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return BugLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return BugLocalServiceUtil.fetchBug(((Integer)arguments[0]).intValue());
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return BugLocalServiceUtil.getBug(((Integer)arguments[0]).intValue());
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return BugLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return BugLocalServiceUtil.getBugs(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return BugLocalServiceUtil.getBugsCount();
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return BugLocalServiceUtil.updateBug((com.vmware.model.Bug)arguments[0]);
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return BugLocalServiceUtil.updateBug((com.vmware.model.Bug)arguments[0],
				((Boolean)arguments[1]).booleanValue());
		}

		if (_methodName144.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes144, parameterTypes)) {
			return BugLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName145.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes145, parameterTypes)) {
			BugLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);
		}

		if (_methodName150.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes150, parameterTypes)) {
			return BugLocalServiceUtil.getBugByAssignToId(((Integer)arguments[0]).intValue());
		}

		if (_methodName151.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes151, parameterTypes)) {
			return BugLocalServiceUtil.getBugSeverityByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName152.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
			return BugLocalServiceUtil.getBugPriorityByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName153.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes153, parameterTypes)) {
			return BugLocalServiceUtil.getBugStatusByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName154.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
			return BugLocalServiceUtil.getBugKeywordsByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName155.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
			return BugLocalServiceUtil.getBugsByReporter(((Integer)arguments[0]).intValue());
		}

		if (_methodName156.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes156, parameterTypes)) {
			return BugLocalServiceUtil.getBugShortDescByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName157.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes157, parameterTypes)) {
			return BugLocalServiceUtil.getBugVotesByUserId(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName158.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes158, parameterTypes)) {
			return BugLocalServiceUtil.getBugPriority((java.lang.String)arguments[0]);
		}

		if (_methodName159.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes159, parameterTypes)) {
			return BugLocalServiceUtil.getBugResolutionByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName160.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes160, parameterTypes)) {
			return BugLocalServiceUtil.getBugReporterByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName161.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes161, parameterTypes)) {
			return BugLocalServiceUtil.getBugQAContactByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName162.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes162, parameterTypes)) {
			return BugLocalServiceUtil.getBugComponentsByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName163.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes163, parameterTypes)) {
			return BugLocalServiceUtil.getBugProductsByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName164.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes164, parameterTypes)) {
			return BugLocalServiceUtil.getBugSeverityByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName165.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes165, parameterTypes)) {
			return BugLocalServiceUtil.getBugStatus(((Integer)arguments[0]).intValue());
		}

		if (_methodName166.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes166, parameterTypes)) {
			return BugLocalServiceUtil.getBugAssignedByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName167.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes167, parameterTypes)) {
			return BugLocalServiceUtil.getBugResolutionByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName168.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes168, parameterTypes)) {
			return BugLocalServiceUtil.getKeywordDefByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName169.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes169, parameterTypes)) {
			return BugLocalServiceUtil.getBugByShortDesc((java.lang.String)arguments[0]);
		}

		if (_methodName170.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes170, parameterTypes)) {
			return BugLocalServiceUtil.getDuplicateByDupeofId(((Integer)arguments[0]).intValue());
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName144;
	private String[] _methodParameterTypes144;
	private String _methodName145;
	private String[] _methodParameterTypes145;
	private String _methodName150;
	private String[] _methodParameterTypes150;
	private String _methodName151;
	private String[] _methodParameterTypes151;
	private String _methodName152;
	private String[] _methodParameterTypes152;
	private String _methodName153;
	private String[] _methodParameterTypes153;
	private String _methodName154;
	private String[] _methodParameterTypes154;
	private String _methodName155;
	private String[] _methodParameterTypes155;
	private String _methodName156;
	private String[] _methodParameterTypes156;
	private String _methodName157;
	private String[] _methodParameterTypes157;
	private String _methodName158;
	private String[] _methodParameterTypes158;
	private String _methodName159;
	private String[] _methodParameterTypes159;
	private String _methodName160;
	private String[] _methodParameterTypes160;
	private String _methodName161;
	private String[] _methodParameterTypes161;
	private String _methodName162;
	private String[] _methodParameterTypes162;
	private String _methodName163;
	private String[] _methodParameterTypes163;
	private String _methodName164;
	private String[] _methodParameterTypes164;
	private String _methodName165;
	private String[] _methodParameterTypes165;
	private String _methodName166;
	private String[] _methodParameterTypes166;
	private String _methodName167;
	private String[] _methodParameterTypes167;
	private String _methodName168;
	private String[] _methodParameterTypes168;
	private String _methodName169;
	private String[] _methodParameterTypes169;
	private String _methodName170;
	private String[] _methodParameterTypes170;
}