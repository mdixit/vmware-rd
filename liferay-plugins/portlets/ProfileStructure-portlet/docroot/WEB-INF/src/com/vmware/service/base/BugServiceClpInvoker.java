/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.base;

import com.vmware.service.BugServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 */
public class BugServiceClpInvoker {
	public BugServiceClpInvoker() {
		_methodName128 = "getBeanIdentifier";

		_methodParameterTypes128 = new String[] {  };

		_methodName129 = "setBeanIdentifier";

		_methodParameterTypes129 = new String[] { "java.lang.String" };

		_methodName134 = "getBugByAssignToId";

		_methodParameterTypes134 = new String[] { "int" };

		_methodName135 = "getBugSeverityByUserId";

		_methodParameterTypes135 = new String[] { "int", "java.lang.String" };

		_methodName136 = "getBugPriorityByUserId";

		_methodParameterTypes136 = new String[] { "int", "java.lang.String" };

		_methodName137 = "getBugStatusByUserId";

		_methodParameterTypes137 = new String[] { "int", "java.lang.String" };

		_methodName138 = "getBugKeywordsByUserId";

		_methodParameterTypes138 = new String[] { "int", "java.lang.String" };

		_methodName139 = "getBugsByReporter";

		_methodParameterTypes139 = new String[] { "int" };

		_methodName140 = "getBugShortDescByUserId";

		_methodParameterTypes140 = new String[] { "int", "java.lang.String" };

		_methodName141 = "getBugVotesByUserId";

		_methodParameterTypes141 = new String[] { "int", "int" };

		_methodName142 = "getBugPriority";

		_methodParameterTypes142 = new String[] { "java.lang.String" };

		_methodName143 = "getBugResolutionByUserId";

		_methodParameterTypes143 = new String[] { "int", "java.lang.String" };

		_methodName144 = "getBugReporterByBugId";

		_methodParameterTypes144 = new String[] { "int" };

		_methodName145 = "getBugQAContactByBugId";

		_methodParameterTypes145 = new String[] { "int" };

		_methodName146 = "getBugComponentsByBugId";

		_methodParameterTypes146 = new String[] { "int" };

		_methodName147 = "getBugProductsByBugId";

		_methodParameterTypes147 = new String[] { "int" };

		_methodName148 = "getBugSeverityByBugId";

		_methodParameterTypes148 = new String[] { "int" };

		_methodName149 = "getBugStatus";

		_methodParameterTypes149 = new String[] { "int" };

		_methodName150 = "getBugAssignedByBugId";

		_methodParameterTypes150 = new String[] { "int" };

		_methodName151 = "getBugResolutionByBugId";

		_methodParameterTypes151 = new String[] { "int" };

		_methodName152 = "getDuplicateByDupeofId";

		_methodParameterTypes152 = new String[] { "int" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName128.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes128, parameterTypes)) {
			return BugServiceUtil.getBeanIdentifier();
		}

		if (_methodName129.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes129, parameterTypes)) {
			BugServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);
		}

		if (_methodName134.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes134, parameterTypes)) {
			return BugServiceUtil.getBugByAssignToId(((Integer)arguments[0]).intValue());
		}

		if (_methodName135.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes135, parameterTypes)) {
			return BugServiceUtil.getBugSeverityByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName136.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes136, parameterTypes)) {
			return BugServiceUtil.getBugPriorityByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName137.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes137, parameterTypes)) {
			return BugServiceUtil.getBugStatusByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName138.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes138, parameterTypes)) {
			return BugServiceUtil.getBugKeywordsByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName139.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes139, parameterTypes)) {
			return BugServiceUtil.getBugsByReporter(((Integer)arguments[0]).intValue());
		}

		if (_methodName140.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes140, parameterTypes)) {
			return BugServiceUtil.getBugShortDescByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName141.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes141, parameterTypes)) {
			return BugServiceUtil.getBugVotesByUserId(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName142.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes142, parameterTypes)) {
			return BugServiceUtil.getBugPriority((java.lang.String)arguments[0]);
		}

		if (_methodName143.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes143, parameterTypes)) {
			return BugServiceUtil.getBugResolutionByUserId(((Integer)arguments[0]).intValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName144.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes144, parameterTypes)) {
			return BugServiceUtil.getBugReporterByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName145.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes145, parameterTypes)) {
			return BugServiceUtil.getBugQAContactByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName146.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes146, parameterTypes)) {
			return BugServiceUtil.getBugComponentsByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName147.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes147, parameterTypes)) {
			return BugServiceUtil.getBugProductsByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName148.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes148, parameterTypes)) {
			return BugServiceUtil.getBugSeverityByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName149.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes149, parameterTypes)) {
			return BugServiceUtil.getBugStatus(((Integer)arguments[0]).intValue());
		}

		if (_methodName150.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes150, parameterTypes)) {
			return BugServiceUtil.getBugAssignedByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName151.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes151, parameterTypes)) {
			return BugServiceUtil.getBugResolutionByBugId(((Integer)arguments[0]).intValue());
		}

		if (_methodName152.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
			return BugServiceUtil.getDuplicateByDupeofId(((Integer)arguments[0]).intValue());
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName128;
	private String[] _methodParameterTypes128;
	private String _methodName129;
	private String[] _methodParameterTypes129;
	private String _methodName134;
	private String[] _methodParameterTypes134;
	private String _methodName135;
	private String[] _methodParameterTypes135;
	private String _methodName136;
	private String[] _methodParameterTypes136;
	private String _methodName137;
	private String[] _methodParameterTypes137;
	private String _methodName138;
	private String[] _methodParameterTypes138;
	private String _methodName139;
	private String[] _methodParameterTypes139;
	private String _methodName140;
	private String[] _methodParameterTypes140;
	private String _methodName141;
	private String[] _methodParameterTypes141;
	private String _methodName142;
	private String[] _methodParameterTypes142;
	private String _methodName143;
	private String[] _methodParameterTypes143;
	private String _methodName144;
	private String[] _methodParameterTypes144;
	private String _methodName145;
	private String[] _methodParameterTypes145;
	private String _methodName146;
	private String[] _methodParameterTypes146;
	private String _methodName147;
	private String[] _methodParameterTypes147;
	private String _methodName148;
	private String[] _methodParameterTypes148;
	private String _methodName149;
	private String[] _methodParameterTypes149;
	private String _methodName150;
	private String[] _methodParameterTypes150;
	private String _methodName151;
	private String[] _methodParameterTypes151;
	private String _methodName152;
	private String[] _methodParameterTypes152;
}