/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import com.vmware.service.BugServiceUtil;

import java.rmi.RemoteException;

/**
 * <p>
 * This class provides a SOAP utility for the
 * {@link com.vmware.service.BugServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 * </p>
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.vmware.model.BugSoap}.
 * If the method in the service utility returns a
 * {@link com.vmware.model.Bug}, that is translated to a
 * {@link com.vmware.model.BugSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at
 * http://localhost:8080/api/secure/axis. Set the property
 * <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author    iscisc
 * @see       BugServiceHttp
 * @see       com.vmware.model.BugSoap
 * @see       com.vmware.service.BugServiceUtil
 * @generated
 */
public class BugServiceSoap {
	public static com.vmware.model.BugSoap[] getBugByAssignToId(int assigned_to)
		throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugByAssignToId(assigned_to);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugSeverityByUserId(
		int assigned_to, java.lang.String bug_severity)
		throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugSeverityByUserId(assigned_to,
					bug_severity);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugPriorityByUserId(
		int assigned_to, java.lang.String priority) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugPriorityByUserId(assigned_to,
					priority);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugStatusByUserId(
		int assigned_to, java.lang.String bug_status) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugStatusByUserId(assigned_to,
					bug_status);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugKeywordsByUserId(
		int assigned_to, java.lang.String bug_status) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugKeywordsByUserId(assigned_to,
					bug_status);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugsByReporter(int reporter)
		throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugsByReporter(reporter);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugShortDescByUserId(
		int assigned_to, java.lang.String short_desc) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugShortDescByUserId(assigned_to,
					short_desc);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugVotesByUserId(
		int assigned_to, int votes) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugVotesByUserId(assigned_to,
					votes);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugPriority(
		java.lang.String priority) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugPriority(priority);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap[] getBugResolutionByUserId(
		int assigned_to, java.lang.String resolution) throws RemoteException {
		try {
			java.util.List<com.vmware.model.Bug> returnValue = BugServiceUtil.getBugResolutionByUserId(assigned_to,
					resolution);

			return com.vmware.model.BugSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.ProfilesSoap getBugReporterByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.Profiles returnValue = BugServiceUtil.getBugReporterByBugId(bug_id);

			return com.vmware.model.ProfilesSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.ProfilesSoap getBugQAContactByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.Profiles returnValue = BugServiceUtil.getBugQAContactByBugId(bug_id);

			return com.vmware.model.ProfilesSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.ComponentsSoap getBugComponentsByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.Components returnValue = BugServiceUtil.getBugComponentsByBugId(bug_id);

			return com.vmware.model.ComponentsSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.ProductsSoap getBugProductsByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.Products returnValue = BugServiceUtil.getBugProductsByBugId(bug_id);

			return com.vmware.model.ProductsSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSeveritySoap getBugSeverityByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.BugSeverity returnValue = BugServiceUtil.getBugSeverityByBugId(bug_id);

			return com.vmware.model.BugSeveritySoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugStatusSoap getBugStatus(int bug_id)
		throws RemoteException {
		try {
			com.vmware.model.BugStatus returnValue = BugServiceUtil.getBugStatus(bug_id);

			return com.vmware.model.BugStatusSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.ProfilesSoap getBugAssignedByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.Profiles returnValue = BugServiceUtil.getBugAssignedByBugId(bug_id);

			return com.vmware.model.ProfilesSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.ResolutionSoap getBugResolutionByBugId(
		int bug_id) throws RemoteException {
		try {
			com.vmware.model.Resolution returnValue = BugServiceUtil.getBugResolutionByBugId(bug_id);

			return com.vmware.model.ResolutionSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.vmware.model.BugSoap getDuplicateByDupeofId(int bug_id)
		throws RemoteException {
		try {
			com.vmware.model.Bug returnValue = BugServiceUtil.getDuplicateByDupeofId(bug_id);

			return com.vmware.model.BugSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(BugServiceSoap.class);
}