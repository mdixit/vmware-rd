/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.impl;

import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.vmware.NoSuchBugException;
import com.vmware.NoSuchBugSeverityException;
import com.vmware.NoSuchBugStatusException;
import com.vmware.NoSuchComponentsException;
import com.vmware.NoSuchProductsException;
import com.vmware.NoSuchProfilesException;
import com.vmware.NoSuchResolutionException;
import com.vmware.model.Bug;
import com.vmware.model.BugSeverity;
import com.vmware.model.BugStatus;
import com.vmware.model.Components;
import com.vmware.model.Keyword;
import com.vmware.model.KeywordDefs;
import com.vmware.model.Products;
import com.vmware.model.Profiles;
import com.vmware.model.Resolution;
import com.vmware.service.KeywordDefsLocalServiceUtil;
import com.vmware.service.base.BugServiceBaseImpl;
import com.vmware.service.persistence.BugSeverityUtil;
import com.vmware.service.persistence.BugStatusUtil;
import com.vmware.service.persistence.BugUtil;
import com.vmware.service.persistence.ComponentsUtil;
import com.vmware.service.persistence.ProductsUtil;
import com.vmware.service.persistence.ProfilesUtil;
import com.vmware.service.persistence.ResolutionUtil;

/**
 * The implementation of the bug remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.vmware.service.BugService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author iscisc
 * @see com.vmware.service.base.BugServiceBaseImpl
 * @see com.vmware.service.BugServiceUtil
 */
public class BugServiceImpl extends BugServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.vmware.service.BugServiceUtil} to access the bug remote service.
	 */
	
	public List<Bug> getBugByAssignToId(int assigned_to){
		List<Bug> bugList = null;
		try {
			bugList = BugUtil.findByAssignToId(assigned_to);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugList;
		
	}	
	
	public List<Bug> getBugSeverityByUserId(int assigned_to,String bug_severity){
		List<Bug> bugSeverityList = null;
		try {
			bugSeverityList = BugUtil.findByBugSeverityByUserId(assigned_to,bug_severity);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugSeverityList;
		
	}
	
	public List<Bug> getBugPriorityByUserId(int assigned_to,String priority){
		List<Bug> bugPriorityList = null;
		try {
			bugPriorityList = BugUtil.findByBugPriorityByUserId(assigned_to,priority);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugPriorityList;
		
	}
	
	public List<Bug> getBugStatusByUserId(int assigned_to,String bug_status){
		List<Bug> bugStatusList = null;
		try {
			bugStatusList = BugUtil.findByBugStatusByUserId(assigned_to,bug_status);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugStatusList;
		
	}
	
	public List<Bug> getBugKeywordsByUserId(int assigned_to,String bug_status){
		List<Bug> bugKeywordsList = null;
		try {
			bugKeywordsList = BugUtil.findByBugKeywordsByUserId(assigned_to,bug_status);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugKeywordsList;
		
	}
	
	public List<Bug> getBugsByReporter(int reporter){
		List<Bug> bugReporterList = null;
		try {
			bugReporterList = BugUtil.findByBugsByReporter(reporter);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugReporterList;
		
	}
	
	public List<Bug> getBugShortDescByUserId(int assigned_to,String short_desc){
		List<Bug> bugShortDescList = null;
		try {
			bugShortDescList = BugUtil.findByBugShortDescByUserId(assigned_to,short_desc);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugShortDescList;
		
	}
	
	public List<Bug> getBugVotesByUserId(int assigned_to,int votes){
		List<Bug> bugVotesList = null;
		try {
			bugVotesList = BugUtil.findByBugVotesByUserId(assigned_to,votes);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugVotesList;
		
	}
	
	public List<Bug> getBugPriority(String priority){
		List<Bug> bugPriorityList = null;
		try {
			bugPriorityList = BugUtil.findByBugPriority(priority);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugPriorityList;
		
	}
	
	public List<Bug> getBugResolutionByUserId(int assigned_to,String resolution){
		List<Bug> bugResolutionList = null;
		try {
			bugResolutionList = BugUtil.findByBugResolutionByUserId(assigned_to,resolution);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugResolutionList;
		
	}
	
	public Profiles getBugReporterByBugId(int bug_id){
		Profiles profilesReporter = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			profilesReporter = ProfilesUtil.findByPrimaryKey(bug.getReporter());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchProfilesException e) {
			e.printStackTrace();
		}
		
		return profilesReporter;
		
	}
	
	public Profiles getBugQAContactByBugId(int bug_id){
		Profiles profilesQaContact = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			profilesQaContact = ProfilesUtil.findByPrimaryKey(bug.getQa_contact());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchProfilesException e) {
			e.printStackTrace();
		}
		
		return profilesQaContact;
		
	}
	
	public Components getBugComponentsByBugId(int bug_id){
		Components bugComponents = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			bugComponents = ComponentsUtil.findByPrimaryKey(bug.getComponent_id());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchComponentsException e) {
			e.printStackTrace();
		}
		
		return bugComponents;
		
	}
	
	public Products getBugProductsByBugId(int bug_id){
		Products bugProducts = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			bugProducts = ProductsUtil.findByPrimaryKey(bug.getProduct_id());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchProductsException e) {
			e.printStackTrace();
		}
		
		return bugProducts;
		
	}
	
	public BugSeverity getBugSeverityByBugId(int bug_id){
		BugSeverity bugSeverity = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			bugSeverity = BugSeverityUtil.findByBugSeverityByValue(bug.getBug_severity());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchBugSeverityException e) {
			e.printStackTrace();
		}
		
		return bugSeverity;
		
	}
	
	public BugStatus getBugStatus(int bug_id){
		BugStatus bugStatus = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			bugStatus = BugStatusUtil.findByBugStatusByValue(bug.getBug_status());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchBugStatusException e) {
			e.printStackTrace();
		}
		
		return bugStatus;
		
	}
	
	public Profiles getBugAssignedByBugId(int bug_id){
		Profiles bugProfiles = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			bugProfiles = ProfilesUtil.findByPrimaryKey(bug.getAssigned_to());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchProfilesException e) {
			e.printStackTrace();
		}
		
		return bugProfiles;
		
	}
	
	public Resolution getBugResolutionByBugId(int bug_id){
		Resolution bugResolution = null;
		try {
			
			Bug bug = BugUtil.findByPrimaryKey(bug_id);
			bugResolution = ResolutionUtil.findByResolutionByValue(bug.getResolution());
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		catch (NoSuchResolutionException e) {
			e.printStackTrace();
		}
		
		return bugResolution;
		
	}
	
	public Bug getDuplicateByDupeofId(int bug_id){
		Bug bug = null;
		try {
			
			 bug = BugUtil.findByPrimaryKey(bug_id);			
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
		catch (NoSuchBugException e) {
			e.printStackTrace();
		}
		
		
		return bug;
		
	}
	
	
}