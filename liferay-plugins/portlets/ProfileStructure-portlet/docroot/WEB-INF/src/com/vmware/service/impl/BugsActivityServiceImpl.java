/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.vmware.model.BugsActivity;
import com.vmware.service.base.BugsActivityServiceBaseImpl;
import com.vmware.service.persistence.BugsActivityUtil;

/**
 * The implementation of the bugs activity remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.vmware.service.BugsActivityService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author iscisc
 * @see com.vmware.service.base.BugsActivityServiceBaseImpl
 * @see com.vmware.service.BugsActivityServiceUtil
 */
public class BugsActivityServiceImpl extends BugsActivityServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.vmware.service.BugsActivityServiceUtil} to access the bugs activity remote service.
	 */
	public List<BugsActivity> getBugActivityByUserId(int who){
		List<BugsActivity> bugActivityList = null;
		try {
			bugActivityList = BugsActivityUtil.findByBugActivityByUserId(who);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugActivityList;
		
	}
	
	public List<BugsActivity> getBugActivityByBugId(int bugId){
		List<BugsActivity> bugActivityList = null;
		try {
			bugActivityList = BugsActivityUtil.findByBugActivityByBugId(bugId);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugActivityList;
		
	}
	
	public List<BugsActivity> getBugActivityByUserIdAndFieldId(int userId,int fieldId){
		List<BugsActivity> bugActivityList = null;
		try {
			bugActivityList = BugsActivityUtil.findByBugActivityByFieldIdAndUserId(userId,fieldId);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugActivityList;
		
	}
	
	public List<BugsActivity> getBugActivityByBugActivityByAttachId(int attachId){
		List<BugsActivity> bugActivityList = null;
		try {
			bugActivityList = BugsActivityUtil.findByBugActivityByAttachId(attachId);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return bugActivityList;
		
	}
}