/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.vmware.model.Bug;
import com.vmware.model.LongDescription;
import com.vmware.service.base.LongDescriptionLocalServiceBaseImpl;
import com.vmware.service.persistence.BugUtil;
import com.vmware.service.persistence.LongDescriptionUtil;

/**
 * The implementation of the long description local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.vmware.service.LongDescriptionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see com.vmware.service.base.LongDescriptionLocalServiceBaseImpl
 * @see com.vmware.service.LongDescriptionLocalServiceUtil
 */
public class LongDescriptionLocalServiceImpl
	extends LongDescriptionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.vmware.service.LongDescriptionLocalServiceUtil} to access the long description local service.
	 */	
	public List<LongDescription> getLongDescByBugId(int bug_id){
		List<LongDescription> longDescList = null;
		try {
			longDescList = LongDescriptionUtil.findByLongDescriptionByBugId(bug_id);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return longDescList;
		
	}
	
	public List<LongDescription> getLongDescByUserId(int who){
		List<LongDescription> longDescList = null;
		try {
			longDescList = LongDescriptionUtil.findByLongDescriptionByUserId(who);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return longDescList;
		
	}

}