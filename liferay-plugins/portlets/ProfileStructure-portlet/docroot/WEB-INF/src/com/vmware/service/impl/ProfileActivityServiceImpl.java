/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.vmware.NoSuchFieldDefsException;
import com.vmware.NoSuchProfileActivityException;
import com.vmware.model.FieldDefs;
import com.vmware.model.ProfileActivity;
import com.vmware.service.base.ProfileActivityServiceBaseImpl;
import com.vmware.service.persistence.FieldDefsUtil;
import com.vmware.service.persistence.ProfileActivityUtil;

/**
 * The implementation of the profile activity remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.vmware.service.ProfileActivityService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author iscisc
 * @see com.vmware.service.base.ProfileActivityServiceBaseImpl
 * @see com.vmware.service.ProfileActivityServiceUtil
 */
public class ProfileActivityServiceImpl extends ProfileActivityServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.vmware.service.ProfileActivityServiceUtil} to access the profile activity remote service.
	 */
	
	public ProfileActivity getProfileName(int userId){
		ProfileActivity profilesActivity = null;		
		try {
			profilesActivity = ProfileActivityUtil.findByProfileActivityByUserId(userId);
		} catch (NoSuchProfileActivityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return profilesActivity;
		
	}
	
	public FieldDefs getFieldDefsByFieldId(int fieldId){
		
		FieldDefs fieldDefEntity = null;
		
		try {
			
			fieldDefEntity = FieldDefsUtil.findByPrimaryKey(fieldId);
		}
		catch (NoSuchFieldDefsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return fieldDefEntity;
		
	}

}