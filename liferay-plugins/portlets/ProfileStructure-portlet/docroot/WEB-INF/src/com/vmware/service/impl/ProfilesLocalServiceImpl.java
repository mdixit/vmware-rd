/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.vmware.NoSuchProfilesException;
import com.vmware.model.Profiles;
import com.vmware.service.base.ProfilesLocalServiceBaseImpl;
import com.vmware.service.persistence.ProfilesUtil;

/**
 * The implementation of the profiles local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.vmware.service.ProfilesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see com.vmware.service.base.ProfilesLocalServiceBaseImpl
 * @see com.vmware.service.ProfilesLocalServiceUtil
 */
public class ProfilesLocalServiceImpl extends ProfilesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.vmware.service.ProfilesLocalServiceUtil} to access the profiles local service.
	 */
	
	public Profiles getProfileName(String login_name){
		Profiles profies = null;		
		try {
			profies = ProfilesUtil.findByLoginName(login_name);
		} catch (NoSuchProfilesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return profies;
		
	}
}