/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.vmware.NoSuchBugException;
import com.vmware.NoSuchResolutionException;
import com.vmware.NoSuchVoteException;
import com.vmware.model.Bug;
import com.vmware.model.Profiles;
import com.vmware.model.Resolution;
import com.vmware.model.Vote;
import com.vmware.service.base.VoteLocalServiceBaseImpl;
import com.vmware.service.persistence.BugUtil;
import com.vmware.service.persistence.ResolutionUtil;
import com.vmware.service.persistence.VoteUtil;

/**
 * The implementation of the vote local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.vmware.service.VoteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author iscisc
 * @see com.vmware.service.base.VoteLocalServiceBaseImpl
 * @see com.vmware.service.VoteLocalServiceUtil
 */
public class VoteLocalServiceImpl extends VoteLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.vmware.service.VoteLocalServiceUtil} to access the vote local service.
	 */
	public Vote getBugVoteByUserId(int userId){
		Vote vote = null;
		try {
			
			vote = VoteUtil.findByVoteCountByUserId(userId);
			
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 		
		catch (NoSuchVoteException e) {
			e.printStackTrace();
		}
		
		return vote;
		
	}
}