/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchAttachmentsException;

import com.vmware.model.Attachments;
import com.vmware.model.impl.AttachmentsImpl;
import com.vmware.model.impl.AttachmentsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the attachments service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see AttachmentsPersistence
 * @see AttachmentsUtil
 * @generated
 */
public class AttachmentsPersistenceImpl extends BasePersistenceImpl<Attachments>
	implements AttachmentsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AttachmentsUtil} to access the attachments persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AttachmentsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			AttachmentsModelImpl.FINDER_CACHE_ENABLED, AttachmentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			AttachmentsModelImpl.FINDER_CACHE_ENABLED, AttachmentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			AttachmentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the attachments in the entity cache if it is enabled.
	 *
	 * @param attachments the attachments
	 */
	public void cacheResult(Attachments attachments) {
		EntityCacheUtil.putResult(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			AttachmentsImpl.class, attachments.getPrimaryKey(), attachments);

		attachments.resetOriginalValues();
	}

	/**
	 * Caches the attachmentses in the entity cache if it is enabled.
	 *
	 * @param attachmentses the attachmentses
	 */
	public void cacheResult(List<Attachments> attachmentses) {
		for (Attachments attachments : attachmentses) {
			if (EntityCacheUtil.getResult(
						AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
						AttachmentsImpl.class, attachments.getPrimaryKey()) == null) {
				cacheResult(attachments);
			}
			else {
				attachments.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all attachmentses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AttachmentsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AttachmentsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the attachments.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Attachments attachments) {
		EntityCacheUtil.removeResult(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			AttachmentsImpl.class, attachments.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Attachments> attachmentses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Attachments attachments : attachmentses) {
			EntityCacheUtil.removeResult(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
				AttachmentsImpl.class, attachments.getPrimaryKey());
		}
	}

	/**
	 * Creates a new attachments with the primary key. Does not add the attachments to the database.
	 *
	 * @param attach_id the primary key for the new attachments
	 * @return the new attachments
	 */
	public Attachments create(int attach_id) {
		Attachments attachments = new AttachmentsImpl();

		attachments.setNew(true);
		attachments.setPrimaryKey(attach_id);

		return attachments;
	}

	/**
	 * Removes the attachments with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attach_id the primary key of the attachments
	 * @return the attachments that was removed
	 * @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attachments remove(int attach_id)
		throws NoSuchAttachmentsException, SystemException {
		return remove(Integer.valueOf(attach_id));
	}

	/**
	 * Removes the attachments with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the attachments
	 * @return the attachments that was removed
	 * @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Attachments remove(Serializable primaryKey)
		throws NoSuchAttachmentsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Attachments attachments = (Attachments)session.get(AttachmentsImpl.class,
					primaryKey);

			if (attachments == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAttachmentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(attachments);
		}
		catch (NoSuchAttachmentsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Attachments removeImpl(Attachments attachments)
		throws SystemException {
		attachments = toUnwrappedModel(attachments);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, attachments);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(attachments);

		return attachments;
	}

	@Override
	public Attachments updateImpl(com.vmware.model.Attachments attachments,
		boolean merge) throws SystemException {
		attachments = toUnwrappedModel(attachments);

		boolean isNew = attachments.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, attachments, merge);

			attachments.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			AttachmentsImpl.class, attachments.getPrimaryKey(), attachments);

		return attachments;
	}

	protected Attachments toUnwrappedModel(Attachments attachments) {
		if (attachments instanceof AttachmentsImpl) {
			return attachments;
		}

		AttachmentsImpl attachmentsImpl = new AttachmentsImpl();

		attachmentsImpl.setNew(attachments.isNew());
		attachmentsImpl.setPrimaryKey(attachments.getPrimaryKey());

		attachmentsImpl.setAttach_id(attachments.getAttach_id());
		attachmentsImpl.setBug_id(attachments.getBug_id());
		attachmentsImpl.setCreation_ts(attachments.getCreation_ts());
		attachmentsImpl.setDescription(attachments.getDescription());
		attachmentsImpl.setMimetype(attachments.getMimetype());
		attachmentsImpl.setIspatch(attachments.getIspatch());
		attachmentsImpl.setFilename(attachments.getFilename());
		attachmentsImpl.setSubmitter_id(attachments.getSubmitter_id());
		attachmentsImpl.setIsobsolete(attachments.getIsobsolete());
		attachmentsImpl.setIsprivate(attachments.getIsprivate());
		attachmentsImpl.setIsurl(attachments.getIsurl());

		return attachmentsImpl;
	}

	/**
	 * Returns the attachments with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the attachments
	 * @return the attachments
	 * @throws com.liferay.portal.NoSuchModelException if a attachments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Attachments findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the attachments with the primary key or throws a {@link com.vmware.NoSuchAttachmentsException} if it could not be found.
	 *
	 * @param attach_id the primary key of the attachments
	 * @return the attachments
	 * @throws com.vmware.NoSuchAttachmentsException if a attachments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attachments findByPrimaryKey(int attach_id)
		throws NoSuchAttachmentsException, SystemException {
		Attachments attachments = fetchByPrimaryKey(attach_id);

		if (attachments == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + attach_id);
			}

			throw new NoSuchAttachmentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				attach_id);
		}

		return attachments;
	}

	/**
	 * Returns the attachments with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the attachments
	 * @return the attachments, or <code>null</code> if a attachments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Attachments fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the attachments with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param attach_id the primary key of the attachments
	 * @return the attachments, or <code>null</code> if a attachments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attachments fetchByPrimaryKey(int attach_id)
		throws SystemException {
		Attachments attachments = (Attachments)EntityCacheUtil.getResult(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
				AttachmentsImpl.class, attach_id);

		if (attachments == _nullAttachments) {
			return null;
		}

		if (attachments == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				attachments = (Attachments)session.get(AttachmentsImpl.class,
						Integer.valueOf(attach_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (attachments != null) {
					cacheResult(attachments);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
						AttachmentsImpl.class, attach_id, _nullAttachments);
				}

				closeSession(session);
			}
		}

		return attachments;
	}

	/**
	 * Returns all the attachmentses.
	 *
	 * @return the attachmentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attachments> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attachmentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attachmentses
	 * @param end the upper bound of the range of attachmentses (not inclusive)
	 * @return the range of attachmentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attachments> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the attachmentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attachmentses
	 * @param end the upper bound of the range of attachmentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of attachmentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attachments> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Attachments> list = (List<Attachments>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ATTACHMENTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ATTACHMENTS.concat(AttachmentsModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Attachments>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Attachments>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the attachmentses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Attachments attachments : findAll()) {
			remove(attachments);
		}
	}

	/**
	 * Returns the number of attachmentses.
	 *
	 * @return the number of attachmentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ATTACHMENTS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the bugs associated with the attachments.
	 *
	 * @param pk the primary key of the attachments
	 * @return the bugs associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Bug> getBugs(int pk) throws SystemException {
		return getBugs(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the bugs associated with the attachments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the attachments
	 * @param start the lower bound of the range of attachmentses
	 * @param end the upper bound of the range of attachmentses (not inclusive)
	 * @return the range of bugs associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Bug> getBugs(int pk, int start, int end)
		throws SystemException {
		return getBugs(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_BUGS = new FinderPath(com.vmware.model.impl.BugModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.BugModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.BugImpl.class,
			com.vmware.service.persistence.BugPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getBugs",
			new String[] {
				Integer.class.getName(), "java.lang.Integer",
				"java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_BUGS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the bugs associated with the attachments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the attachments
	 * @param start the lower bound of the range of attachmentses
	 * @param end the upper bound of the range of attachmentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bugs associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Bug> getBugs(int pk, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.vmware.model.Bug> list = (List<com.vmware.model.Bug>)FinderCacheUtil.getResult(FINDER_PATH_GET_BUGS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETBUGS.concat(ORDER_BY_CLAUSE)
									  .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETBUGS.concat(com.vmware.model.impl.BugModelImpl.ORDER_BY_SQL);
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("bugs", com.vmware.model.impl.BugImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.vmware.model.Bug>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_BUGS,
						finderArgs);
				}
				else {
					bugPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_BUGS, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_BUGS_SIZE = new FinderPath(com.vmware.model.impl.BugModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.BugModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.BugImpl.class,
			com.vmware.service.persistence.BugPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getBugsSize", new String[] { Integer.class.getName() });

	static {
		FINDER_PATH_GET_BUGS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of bugs associated with the attachments.
	 *
	 * @param pk the primary key of the attachments
	 * @return the number of bugs associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public int getBugsSize(int pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_BUGS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETBUGSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_BUGS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_BUG = new FinderPath(com.vmware.model.impl.BugModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.BugModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.BugImpl.class,
			com.vmware.service.persistence.BugPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsBug",
			new String[] { Integer.class.getName(), Integer.class.getName() });

	/**
	 * Returns <code>true</code> if the bug is associated with the attachments.
	 *
	 * @param pk the primary key of the attachments
	 * @param bugPK the primary key of the bug
	 * @return <code>true</code> if the bug is associated with the attachments; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsBug(int pk, int bugPK) throws SystemException {
		Object[] finderArgs = new Object[] { pk, bugPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_BUG,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsBug.contains(pk, bugPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_BUG, finderArgs,
					value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the attachments has any bugs associated with it.
	 *
	 * @param pk the primary key of the attachments to check for associations with bugs
	 * @return <code>true</code> if the attachments has any bugs associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsBugs(int pk) throws SystemException {
		if (getBugsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Returns all the profileses associated with the attachments.
	 *
	 * @param pk the primary key of the attachments
	 * @return the profileses associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Profiles> getProfileses(int pk)
		throws SystemException {
		return getProfileses(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the profileses associated with the attachments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the attachments
	 * @param start the lower bound of the range of attachmentses
	 * @param end the upper bound of the range of attachmentses (not inclusive)
	 * @return the range of profileses associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Profiles> getProfileses(int pk, int start,
		int end) throws SystemException {
		return getProfileses(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_PROFILESES = new FinderPath(com.vmware.model.impl.ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesImpl.class,
			com.vmware.service.persistence.ProfilesPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getProfileses",
			new String[] {
				Integer.class.getName(), "java.lang.Integer",
				"java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_PROFILESES.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the profileses associated with the attachments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the attachments
	 * @param start the lower bound of the range of attachmentses
	 * @param end the upper bound of the range of attachmentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of profileses associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Profiles> getProfileses(int pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.vmware.model.Profiles> list = (List<com.vmware.model.Profiles>)FinderCacheUtil.getResult(FINDER_PATH_GET_PROFILESES,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETPROFILESES.concat(ORDER_BY_CLAUSE)
											.concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETPROFILESES;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("profiles", com.vmware.model.impl.ProfilesImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.vmware.model.Profiles>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_PROFILESES,
						finderArgs);
				}
				else {
					profilesPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_PROFILESES,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_PROFILESES_SIZE = new FinderPath(com.vmware.model.impl.ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesImpl.class,
			com.vmware.service.persistence.ProfilesPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getProfilesesSize", new String[] { Integer.class.getName() });

	static {
		FINDER_PATH_GET_PROFILESES_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of profileses associated with the attachments.
	 *
	 * @param pk the primary key of the attachments
	 * @return the number of profileses associated with the attachments
	 * @throws SystemException if a system exception occurred
	 */
	public int getProfilesesSize(int pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_PROFILESES_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETPROFILESESSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_PROFILESES_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_PROFILES = new FinderPath(com.vmware.model.impl.ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesImpl.class,
			com.vmware.service.persistence.ProfilesPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsProfiles",
			new String[] { Integer.class.getName(), Integer.class.getName() });

	/**
	 * Returns <code>true</code> if the profiles is associated with the attachments.
	 *
	 * @param pk the primary key of the attachments
	 * @param profilesPK the primary key of the profiles
	 * @return <code>true</code> if the profiles is associated with the attachments; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsProfiles(int pk, int profilesPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, profilesPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_PROFILES,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsProfiles.contains(pk, profilesPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_PROFILES,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the attachments has any profileses associated with it.
	 *
	 * @param pk the primary key of the attachments to check for associations with profileses
	 * @return <code>true</code> if the attachments has any profileses associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsProfileses(int pk) throws SystemException {
		if (getProfilesesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Initializes the attachments persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Attachments")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Attachments>> listenersList = new ArrayList<ModelListener<Attachments>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Attachments>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsBug = new ContainsBug();

		containsProfiles = new ContainsProfiles();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AttachmentsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsBug containsBug;
	protected ContainsProfiles containsProfiles;

	protected class ContainsBug {
		protected ContainsBug() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSBUG,
					new int[] { java.sql.Types.INTEGER, java.sql.Types.INTEGER },
					RowMapper.COUNT);
		}

		protected boolean contains(int attach_id, int bug_id) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Integer(attach_id), new Integer(bug_id)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class ContainsProfiles {
		protected ContainsProfiles() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSPROFILES,
					new int[] { java.sql.Types.INTEGER, java.sql.Types.INTEGER },
					RowMapper.COUNT);
		}

		protected boolean contains(int attach_id, int userid) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Integer(attach_id), new Integer(userid)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	private static final String _SQL_SELECT_ATTACHMENTS = "SELECT attachments FROM Attachments attachments";
	private static final String _SQL_COUNT_ATTACHMENTS = "SELECT COUNT(attachments) FROM Attachments attachments";
	private static final String _SQL_GETBUGS = "SELECT {bugs.*} FROM bugs INNER JOIN attachments ON (attachments.attach_id = bugs.attach_id) WHERE (attachments.attach_id = ?)";
	private static final String _SQL_GETBUGSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM bugs WHERE attach_id = ?";
	private static final String _SQL_CONTAINSBUG = "SELECT COUNT(*) AS COUNT_VALUE FROM bugs WHERE attach_id = ? AND bug_id = ?";
	private static final String _SQL_GETPROFILESES = "SELECT {profiles.*} FROM profiles INNER JOIN attachments ON (attachments.attach_id = profiles.attach_id) WHERE (attachments.attach_id = ?)";
	private static final String _SQL_GETPROFILESESSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM profiles WHERE attach_id = ?";
	private static final String _SQL_CONTAINSPROFILES = "SELECT COUNT(*) AS COUNT_VALUE FROM profiles WHERE attach_id = ? AND userid = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "attachments.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Attachments exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AttachmentsPersistenceImpl.class);
	private static Attachments _nullAttachments = new AttachmentsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Attachments> toCacheModel() {
				return _nullAttachmentsCacheModel;
			}
		};

	private static CacheModel<Attachments> _nullAttachmentsCacheModel = new CacheModel<Attachments>() {
			public Attachments toEntityModel() {
				return _nullAttachments;
			}
		};
}