package com.vmware.service.persistence;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;

import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.vmware.model.Bug;
import com.vmware.model.impl.BugImpl;


public class BugFinderImpl extends BasePersistenceImpl<Bug> implements BugFinder{
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Bug> getBugByShortDesc(String short_desc) throws SystemException{
		
		Session session = null;
		
		try{
			session = openSession();
			
			String base = "SELECT bugzilla.bugs.* FROM bugzilla.bugs " ;
	       
			StringBuffer sqlBuilder = new StringBuffer(base);
			List<String> criteria = new ArrayList<String>();
			
			if (Validator.isNotNull(short_desc)) {
	            criteria.add("bugzilla.bugs.short_desc like ?");
	        }
			
			if (!criteria.isEmpty()) {
	            sqlBuilder.append("WHERE ");
	            sqlBuilder.append(criteria.remove(0) + " ");
	            for (String criterion : criteria) {
	                sqlBuilder.append(criterion);
	            }
	        }
			
			String sql = sqlBuilder.toString();
			
			SQLQuery q = session.createSQLQuery(sql);
	        q.setCacheable(false);
	        q.addEntity("bugzilla.bugs", BugImpl.class);

	        QueryPos qPos = QueryPos.getInstance(q);
	        
	        // Setting the positions
	        if (Validator.isNotNull(short_desc)) {
	            qPos.add("%"+short_desc+"%");
	            
	        }
	        
	        //execute the query and return a list from the db
	        return (List<Bug>) QueryUtil.list(q, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		}catch(Exception e){
			throw new SystemException(e);
		}
		finally {           
            closeSession(session);
        }    	
		
	}
}
