/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchBugException;

import com.vmware.model.Bug;
import com.vmware.model.impl.BugImpl;
import com.vmware.model.impl.BugModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the bug service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugPersistence
 * @see BugUtil
 * @generated
 */
public class BugPersistenceImpl extends BasePersistenceImpl<Bug>
	implements BugPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BugUtil} to access the bug persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BugImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSIGNTOID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAssignToId",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSIGNTOID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAssignToId",
			new String[] { Integer.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ASSIGNTOID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAssignToId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGPRIORITY =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBugPriority",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITY =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBugPriority",
			new String[] { String.class.getName() },
			BugModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGPRIORITY = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBugPriority",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugSeverityByUserId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugSeverityByUserId",
			new String[] { Integer.class.getName(), String.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.BUG_SEVERITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGSEVERITYBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugSeverityByUserId",
			new String[] { Integer.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugPriorityByUserId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugPriorityByUserId",
			new String[] { Integer.class.getName(), String.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGPRIORITYBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugPriorityByUserId",
			new String[] { Integer.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSTATUSBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBugStatusByUserId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSTATUSBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugStatusByUserId",
			new String[] { Integer.class.getName(), String.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.BUG_STATUS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGSTATUSBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugStatusByUserId",
			new String[] { Integer.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugKeywordsByUserId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugKeywordsByUserId",
			new String[] { Integer.class.getName(), String.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.KEYWORDS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGKEYWORDSBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugKeywordsByUserId",
			new String[] { Integer.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSBYREPORTER =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBugsByReporter",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSBYREPORTER =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBugsByReporter",
			new String[] { Integer.class.getName() },
			BugModelImpl.REPORTER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGSBYREPORTER = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBugsByReporter",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugShortDescByUserId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugShortDescByUserId",
			new String[] { Integer.class.getName(), String.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.SHORT_DESC_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGSHORTDESCBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugShortDescByUserId",
			new String[] { Integer.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGVOTESBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBugVotesByUserId",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGVOTESBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugVotesByUserId",
			new String[] { Integer.class.getName(), Integer.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.VOTES_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGVOTESBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugVotesByUserId",
			new String[] { Integer.class.getName(), Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugResolutionByUserId",
			new String[] {
				Integer.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugResolutionByUserId",
			new String[] { Integer.class.getName(), String.class.getName() },
			BugModelImpl.ASSIGNED_TO_COLUMN_BITMASK |
			BugModelImpl.RESOLUTION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGRESOLUTIONBYUSERID = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugResolutionByUserId",
			new String[] { Integer.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGBYSEVERITY =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBugBySeverity",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGBYSEVERITY =
		new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBugBySeverity",
			new String[] { String.class.getName() },
			BugModelImpl.BUG_SEVERITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGBYSEVERITY = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBugBySeverity",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, BugImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the bug in the entity cache if it is enabled.
	 *
	 * @param bug the bug
	 */
	public void cacheResult(Bug bug) {
		EntityCacheUtil.putResult(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugImpl.class, bug.getPrimaryKey(), bug);

		bug.resetOriginalValues();
	}

	/**
	 * Caches the bugs in the entity cache if it is enabled.
	 *
	 * @param bugs the bugs
	 */
	public void cacheResult(List<Bug> bugs) {
		for (Bug bug : bugs) {
			if (EntityCacheUtil.getResult(BugModelImpl.ENTITY_CACHE_ENABLED,
						BugImpl.class, bug.getPrimaryKey()) == null) {
				cacheResult(bug);
			}
			else {
				bug.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all bugs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BugImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BugImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the bug.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Bug bug) {
		EntityCacheUtil.removeResult(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugImpl.class, bug.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Bug> bugs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Bug bug : bugs) {
			EntityCacheUtil.removeResult(BugModelImpl.ENTITY_CACHE_ENABLED,
				BugImpl.class, bug.getPrimaryKey());
		}
	}

	/**
	 * Creates a new bug with the primary key. Does not add the bug to the database.
	 *
	 * @param bug_id the primary key for the new bug
	 * @return the new bug
	 */
	public Bug create(int bug_id) {
		Bug bug = new BugImpl();

		bug.setNew(true);
		bug.setPrimaryKey(bug_id);

		return bug;
	}

	/**
	 * Removes the bug with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bug_id the primary key of the bug
	 * @return the bug that was removed
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug remove(int bug_id) throws NoSuchBugException, SystemException {
		return remove(Integer.valueOf(bug_id));
	}

	/**
	 * Removes the bug with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the bug
	 * @return the bug that was removed
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Bug remove(Serializable primaryKey)
		throws NoSuchBugException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Bug bug = (Bug)session.get(BugImpl.class, primaryKey);

			if (bug == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBugException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(bug);
		}
		catch (NoSuchBugException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Bug removeImpl(Bug bug) throws SystemException {
		bug = toUnwrappedModel(bug);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, bug);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(bug);

		return bug;
	}

	@Override
	public Bug updateImpl(com.vmware.model.Bug bug, boolean merge)
		throws SystemException {
		bug = toUnwrappedModel(bug);

		boolean isNew = bug.isNew();

		BugModelImpl bugModelImpl = (BugModelImpl)bug;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, bug, merge);

			bug.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BugModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSIGNTOID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSIGNTOID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSIGNTOID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ASSIGNTOID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSIGNTOID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { bugModelImpl.getOriginalPriority() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGPRIORITY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITY,
					args);

				args = new Object[] { bugModelImpl.getPriority() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGPRIORITY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITY,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						
						bugModelImpl.getOriginalBug_severity()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						
						bugModelImpl.getBug_severity()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						
						bugModelImpl.getOriginalPriority()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGPRIORITYBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						
						bugModelImpl.getPriority()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGPRIORITYBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSTATUSBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						
						bugModelImpl.getOriginalBug_status()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSTATUSBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						
						bugModelImpl.getBug_status()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSTATUSBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						
						bugModelImpl.getOriginalKeywords()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGKEYWORDSBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						
						bugModelImpl.getKeywords()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGKEYWORDSBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSBYREPORTER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalReporter())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSBYREPORTER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSBYREPORTER,
					args);

				args = new Object[] { Integer.valueOf(bugModelImpl.getReporter()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSBYREPORTER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSBYREPORTER,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						
						bugModelImpl.getOriginalShort_desc()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSHORTDESCBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						
						bugModelImpl.getShort_desc()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSHORTDESCBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGVOTESBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						Integer.valueOf(bugModelImpl.getOriginalVotes())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGVOTESBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGVOTESBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						Integer.valueOf(bugModelImpl.getVotes())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGVOTESBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGVOTESBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugModelImpl.getOriginalAssigned_to()),
						
						bugModelImpl.getOriginalResolution()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGRESOLUTIONBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugModelImpl.getAssigned_to()),
						
						bugModelImpl.getResolution()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGRESOLUTIONBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID,
					args);
			}

			if ((bugModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGBYSEVERITY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bugModelImpl.getOriginalBug_severity()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGBYSEVERITY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGBYSEVERITY,
					args);

				args = new Object[] { bugModelImpl.getBug_severity() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGBYSEVERITY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGBYSEVERITY,
					args);
			}
		}

		EntityCacheUtil.putResult(BugModelImpl.ENTITY_CACHE_ENABLED,
			BugImpl.class, bug.getPrimaryKey(), bug);

		return bug;
	}

	protected Bug toUnwrappedModel(Bug bug) {
		if (bug instanceof BugImpl) {
			return bug;
		}

		BugImpl bugImpl = new BugImpl();

		bugImpl.setNew(bug.isNew());
		bugImpl.setPrimaryKey(bug.getPrimaryKey());

		bugImpl.setBug_id(bug.getBug_id());
		bugImpl.setAssigned_to(bug.getAssigned_to());
		bugImpl.setBug_file_loc(bug.getBug_file_loc());
		bugImpl.setBug_severity(bug.getBug_severity());
		bugImpl.setBug_status(bug.getBug_status());
		bugImpl.setCreation_ts(bug.getCreation_ts());
		bugImpl.setDelta_ts(bug.getDelta_ts());
		bugImpl.setShort_desc(bug.getShort_desc());
		bugImpl.setHost_op_sys(bug.getHost_op_sys());
		bugImpl.setGuest_op_sys(bug.getGuest_op_sys());
		bugImpl.setPriority(bug.getPriority());
		bugImpl.setRep_platform(bug.getRep_platform());
		bugImpl.setProduct_id(bug.getProduct_id());
		bugImpl.setComponent_id(bug.getComponent_id());
		bugImpl.setQa_contact(bug.getQa_contact());
		bugImpl.setReporter(bug.getReporter());
		bugImpl.setCategory_id(bug.getCategory_id());
		bugImpl.setResolution(bug.getResolution());
		bugImpl.setTarget_milestone(bug.getTarget_milestone());
		bugImpl.setStatus_whiteboard(bug.getStatus_whiteboard());
		bugImpl.setVotes(bug.getVotes());
		bugImpl.setKeywords(bug.getKeywords());
		bugImpl.setLastdiffed(bug.getLastdiffed());
		bugImpl.setEverconfirmed(bug.getEverconfirmed());
		bugImpl.setReporter_accessible(bug.getReporter_accessible());
		bugImpl.setCclist_accessible(bug.getCclist_accessible());
		bugImpl.setEstimated_time(bug.getEstimated_time());
		bugImpl.setRemaining_time(bug.getRemaining_time());
		bugImpl.setDeadline(bug.getDeadline());
		bugImpl.setBug_alias(bug.getBug_alias());
		bugImpl.setFound_in_product_id(bug.getFound_in_product_id());
		bugImpl.setFound_in_version_id(bug.getFound_in_version_id());
		bugImpl.setFound_in_phase_id(bug.getFound_in_phase_id());
		bugImpl.setCf_type(bug.getCf_type());
		bugImpl.setCf_reported_by(bug.getCf_reported_by());
		bugImpl.setCf_attempted(bug.getCf_attempted());
		bugImpl.setCf_failed(bug.getCf_failed());
		bugImpl.setCf_public_summary(bug.getCf_public_summary());
		bugImpl.setCf_doc_impact(bug.getCf_doc_impact());
		bugImpl.setCf_security(bug.getCf_security());
		bugImpl.setCf_build(bug.getCf_build());
		bugImpl.setCf_branch(bug.getCf_branch());
		bugImpl.setCf_change(bug.getCf_change());
		bugImpl.setCf_test_id(bug.getCf_test_id());
		bugImpl.setCf_regression(bug.getCf_regression());
		bugImpl.setCf_reviewer(bug.getCf_reviewer());
		bugImpl.setCf_on_hold(bug.getCf_on_hold());
		bugImpl.setCf_public_severity(bug.getCf_public_severity());
		bugImpl.setCf_i18n_impact(bug.getCf_i18n_impact());
		bugImpl.setCf_eta(bug.getCf_eta());
		bugImpl.setCf_bug_source(bug.getCf_bug_source());
		bugImpl.setCf_viss(bug.getCf_viss());

		return bugImpl;
	}

	/**
	 * Returns the bug with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the bug
	 * @return the bug
	 * @throws com.liferay.portal.NoSuchModelException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Bug findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bug with the primary key or throws a {@link com.vmware.NoSuchBugException} if it could not be found.
	 *
	 * @param bug_id the primary key of the bug
	 * @return the bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByPrimaryKey(int bug_id)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByPrimaryKey(bug_id);

		if (bug == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + bug_id);
			}

			throw new NoSuchBugException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				bug_id);
		}

		return bug;
	}

	/**
	 * Returns the bug with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the bug
	 * @return the bug, or <code>null</code> if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Bug fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bug with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bug_id the primary key of the bug
	 * @return the bug, or <code>null</code> if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByPrimaryKey(int bug_id) throws SystemException {
		Bug bug = (Bug)EntityCacheUtil.getResult(BugModelImpl.ENTITY_CACHE_ENABLED,
				BugImpl.class, bug_id);

		if (bug == _nullBug) {
			return null;
		}

		if (bug == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				bug = (Bug)session.get(BugImpl.class, Integer.valueOf(bug_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (bug != null) {
					cacheResult(bug);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BugModelImpl.ENTITY_CACHE_ENABLED,
						BugImpl.class, bug_id, _nullBug);
				}

				closeSession(session);
			}
		}

		return bug;
	}

	/**
	 * Returns all the bugs where assigned_to = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByAssignToId(int assigned_to)
		throws SystemException {
		return findByAssignToId(assigned_to, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByAssignToId(int assigned_to, int start, int end)
		throws SystemException {
		return findByAssignToId(assigned_to, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByAssignToId(int assigned_to, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ASSIGNTOID;
			finderArgs = new Object[] { assigned_to };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ASSIGNTOID;
			finderArgs = new Object[] { assigned_to, start, end, orderByComparator };
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_ASSIGNTOID_ASSIGNED_TO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByAssignToId_First(int assigned_to,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByAssignToId_First(assigned_to, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByAssignToId_First(int assigned_to,
		OrderByComparator orderByComparator) throws SystemException {
		List<Bug> list = findByAssignToId(assigned_to, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByAssignToId_Last(int assigned_to,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByAssignToId_Last(assigned_to, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByAssignToId_Last(int assigned_to,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByAssignToId(assigned_to);

		List<Bug> list = findByAssignToId(assigned_to, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByAssignToId_PrevAndNext(int bug_id, int assigned_to,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByAssignToId_PrevAndNext(session, bug, assigned_to,
					orderByComparator, true);

			array[1] = bug;

			array[2] = getByAssignToId_PrevAndNext(session, bug, assigned_to,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByAssignToId_PrevAndNext(Session session, Bug bug,
		int assigned_to, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_ASSIGNTOID_ASSIGNED_TO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where priority = &#63;.
	 *
	 * @param priority the priority
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugPriority(String priority)
		throws SystemException {
		return findByBugPriority(priority, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where priority = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param priority the priority
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugPriority(String priority, int start, int end)
		throws SystemException {
		return findByBugPriority(priority, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where priority = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param priority the priority
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugPriority(String priority, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITY;
			finderArgs = new Object[] { priority };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGPRIORITY;
			finderArgs = new Object[] { priority, start, end, orderByComparator };
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if (!Validator.equals(priority, bug.getPriority())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			if (priority == null) {
				query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_1);
			}
			else {
				if (priority.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (priority != null) {
					qPos.add(priority);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where priority = &#63;.
	 *
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugPriority_First(String priority,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugPriority_First(priority, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("priority=");
		msg.append(priority);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where priority = &#63;.
	 *
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugPriority_First(String priority,
		OrderByComparator orderByComparator) throws SystemException {
		List<Bug> list = findByBugPriority(priority, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where priority = &#63;.
	 *
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugPriority_Last(String priority,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugPriority_Last(priority, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("priority=");
		msg.append(priority);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where priority = &#63;.
	 *
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugPriority_Last(String priority,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugPriority(priority);

		List<Bug> list = findByBugPriority(priority, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where priority = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugPriority_PrevAndNext(int bug_id, String priority,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugPriority_PrevAndNext(session, bug, priority,
					orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugPriority_PrevAndNext(session, bug, priority,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugPriority_PrevAndNext(Session session, Bug bug,
		String priority, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		if (priority == null) {
			query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_1);
		}
		else {
			if (priority.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (priority != null) {
			qPos.add(priority);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugSeverityByUserId(int assigned_to,
		String bug_severity) throws SystemException {
		return findByBugSeverityByUserId(assigned_to, bug_severity,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugSeverityByUserId(int assigned_to,
		String bug_severity, int start, int end) throws SystemException {
		return findByBugSeverityByUserId(assigned_to, bug_severity, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugSeverityByUserId(int assigned_to,
		String bug_severity, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID;
			finderArgs = new Object[] { assigned_to, bug_severity };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSEVERITYBYUSERID;
			finderArgs = new Object[] {
					assigned_to, bug_severity,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						!Validator.equals(bug_severity, bug.getBug_severity())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_ASSIGNED_TO_2);

			if (bug_severity == null) {
				query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_1);
			}
			else {
				if (bug_severity.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (bug_severity != null) {
					qPos.add(bug_severity);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugSeverityByUserId_First(int assigned_to,
		String bug_severity, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugSeverityByUserId_First(assigned_to, bug_severity,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", bug_severity=");
		msg.append(bug_severity);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugSeverityByUserId_First(int assigned_to,
		String bug_severity, OrderByComparator orderByComparator)
		throws SystemException {
		List<Bug> list = findByBugSeverityByUserId(assigned_to, bug_severity,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugSeverityByUserId_Last(int assigned_to,
		String bug_severity, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugSeverityByUserId_Last(assigned_to, bug_severity,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", bug_severity=");
		msg.append(bug_severity);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugSeverityByUserId_Last(int assigned_to,
		String bug_severity, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugSeverityByUserId(assigned_to, bug_severity);

		List<Bug> list = findByBugSeverityByUserId(assigned_to, bug_severity,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugSeverityByUserId_PrevAndNext(int bug_id,
		int assigned_to, String bug_severity,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugSeverityByUserId_PrevAndNext(session, bug,
					assigned_to, bug_severity, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugSeverityByUserId_PrevAndNext(session, bug,
					assigned_to, bug_severity, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugSeverityByUserId_PrevAndNext(Session session,
		Bug bug, int assigned_to, String bug_severity,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_ASSIGNED_TO_2);

		if (bug_severity == null) {
			query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_1);
		}
		else {
			if (bug_severity.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (bug_severity != null) {
			qPos.add(bug_severity);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugPriorityByUserId(int assigned_to, String priority)
		throws SystemException {
		return findByBugPriorityByUserId(assigned_to, priority,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and priority = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugPriorityByUserId(int assigned_to,
		String priority, int start, int end) throws SystemException {
		return findByBugPriorityByUserId(assigned_to, priority, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and priority = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugPriorityByUserId(int assigned_to,
		String priority, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID;
			finderArgs = new Object[] { assigned_to, priority };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGPRIORITYBYUSERID;
			finderArgs = new Object[] {
					assigned_to, priority,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						!Validator.equals(priority, bug.getPriority())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_ASSIGNED_TO_2);

			if (priority == null) {
				query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_1);
			}
			else {
				if (priority.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (priority != null) {
					qPos.add(priority);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugPriorityByUserId_First(int assigned_to,
		String priority, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugPriorityByUserId_First(assigned_to, priority,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", priority=");
		msg.append(priority);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugPriorityByUserId_First(int assigned_to,
		String priority, OrderByComparator orderByComparator)
		throws SystemException {
		List<Bug> list = findByBugPriorityByUserId(assigned_to, priority, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugPriorityByUserId_Last(int assigned_to, String priority,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugPriorityByUserId_Last(assigned_to, priority,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", priority=");
		msg.append(priority);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugPriorityByUserId_Last(int assigned_to,
		String priority, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugPriorityByUserId(assigned_to, priority);

		List<Bug> list = findByBugPriorityByUserId(assigned_to, priority,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugPriorityByUserId_PrevAndNext(int bug_id,
		int assigned_to, String priority, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugPriorityByUserId_PrevAndNext(session, bug,
					assigned_to, priority, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugPriorityByUserId_PrevAndNext(session, bug,
					assigned_to, priority, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugPriorityByUserId_PrevAndNext(Session session,
		Bug bug, int assigned_to, String priority,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_ASSIGNED_TO_2);

		if (priority == null) {
			query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_1);
		}
		else {
			if (priority.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (priority != null) {
			qPos.add(priority);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugStatusByUserId(int assigned_to, String bug_status)
		throws SystemException {
		return findByBugStatusByUserId(assigned_to, bug_status,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugStatusByUserId(int assigned_to,
		String bug_status, int start, int end) throws SystemException {
		return findByBugStatusByUserId(assigned_to, bug_status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugStatusByUserId(int assigned_to,
		String bug_status, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSTATUSBYUSERID;
			finderArgs = new Object[] { assigned_to, bug_status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSTATUSBYUSERID;
			finderArgs = new Object[] {
					assigned_to, bug_status,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						!Validator.equals(bug_status, bug.getBug_status())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_ASSIGNED_TO_2);

			if (bug_status == null) {
				query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_1);
			}
			else {
				if (bug_status.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (bug_status != null) {
					qPos.add(bug_status);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugStatusByUserId_First(int assigned_to,
		String bug_status, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugStatusByUserId_First(assigned_to, bug_status,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", bug_status=");
		msg.append(bug_status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugStatusByUserId_First(int assigned_to,
		String bug_status, OrderByComparator orderByComparator)
		throws SystemException {
		List<Bug> list = findByBugStatusByUserId(assigned_to, bug_status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugStatusByUserId_Last(int assigned_to, String bug_status,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugStatusByUserId_Last(assigned_to, bug_status,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", bug_status=");
		msg.append(bug_status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugStatusByUserId_Last(int assigned_to,
		String bug_status, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugStatusByUserId(assigned_to, bug_status);

		List<Bug> list = findByBugStatusByUserId(assigned_to, bug_status,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugStatusByUserId_PrevAndNext(int bug_id,
		int assigned_to, String bug_status, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugStatusByUserId_PrevAndNext(session, bug,
					assigned_to, bug_status, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugStatusByUserId_PrevAndNext(session, bug,
					assigned_to, bug_status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugStatusByUserId_PrevAndNext(Session session, Bug bug,
		int assigned_to, String bug_status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_ASSIGNED_TO_2);

		if (bug_status == null) {
			query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_1);
		}
		else {
			if (bug_status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (bug_status != null) {
			qPos.add(bug_status);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugKeywordsByUserId(int assigned_to, String keywords)
		throws SystemException {
		return findByBugKeywordsByUserId(assigned_to, keywords,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and keywords = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugKeywordsByUserId(int assigned_to,
		String keywords, int start, int end) throws SystemException {
		return findByBugKeywordsByUserId(assigned_to, keywords, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and keywords = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugKeywordsByUserId(int assigned_to,
		String keywords, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID;
			finderArgs = new Object[] { assigned_to, keywords };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGKEYWORDSBYUSERID;
			finderArgs = new Object[] {
					assigned_to, keywords,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						!Validator.equals(keywords, bug.getKeywords())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_ASSIGNED_TO_2);

			if (keywords == null) {
				query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_1);
			}
			else {
				if (keywords.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (keywords != null) {
					qPos.add(keywords);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugKeywordsByUserId_First(int assigned_to,
		String keywords, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugKeywordsByUserId_First(assigned_to, keywords,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", keywords=");
		msg.append(keywords);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugKeywordsByUserId_First(int assigned_to,
		String keywords, OrderByComparator orderByComparator)
		throws SystemException {
		List<Bug> list = findByBugKeywordsByUserId(assigned_to, keywords, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugKeywordsByUserId_Last(int assigned_to, String keywords,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugKeywordsByUserId_Last(assigned_to, keywords,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", keywords=");
		msg.append(keywords);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugKeywordsByUserId_Last(int assigned_to,
		String keywords, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugKeywordsByUserId(assigned_to, keywords);

		List<Bug> list = findByBugKeywordsByUserId(assigned_to, keywords,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugKeywordsByUserId_PrevAndNext(int bug_id,
		int assigned_to, String keywords, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugKeywordsByUserId_PrevAndNext(session, bug,
					assigned_to, keywords, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugKeywordsByUserId_PrevAndNext(session, bug,
					assigned_to, keywords, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugKeywordsByUserId_PrevAndNext(Session session,
		Bug bug, int assigned_to, String keywords,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_ASSIGNED_TO_2);

		if (keywords == null) {
			query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_1);
		}
		else {
			if (keywords.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (keywords != null) {
			qPos.add(keywords);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where reporter = &#63;.
	 *
	 * @param reporter the reporter
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugsByReporter(int reporter)
		throws SystemException {
		return findByBugsByReporter(reporter, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where reporter = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param reporter the reporter
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugsByReporter(int reporter, int start, int end)
		throws SystemException {
		return findByBugsByReporter(reporter, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where reporter = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param reporter the reporter
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugsByReporter(int reporter, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSBYREPORTER;
			finderArgs = new Object[] { reporter };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSBYREPORTER;
			finderArgs = new Object[] { reporter, start, end, orderByComparator };
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((reporter != bug.getReporter())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSBYREPORTER_REPORTER_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(reporter);

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where reporter = &#63;.
	 *
	 * @param reporter the reporter
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugsByReporter_First(int reporter,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugsByReporter_First(reporter, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reporter=");
		msg.append(reporter);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where reporter = &#63;.
	 *
	 * @param reporter the reporter
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugsByReporter_First(int reporter,
		OrderByComparator orderByComparator) throws SystemException {
		List<Bug> list = findByBugsByReporter(reporter, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where reporter = &#63;.
	 *
	 * @param reporter the reporter
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugsByReporter_Last(int reporter,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugsByReporter_Last(reporter, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reporter=");
		msg.append(reporter);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where reporter = &#63;.
	 *
	 * @param reporter the reporter
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugsByReporter_Last(int reporter,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugsByReporter(reporter);

		List<Bug> list = findByBugsByReporter(reporter, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where reporter = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param reporter the reporter
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugsByReporter_PrevAndNext(int bug_id, int reporter,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugsByReporter_PrevAndNext(session, bug, reporter,
					orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugsByReporter_PrevAndNext(session, bug, reporter,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugsByReporter_PrevAndNext(Session session, Bug bug,
		int reporter, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGSBYREPORTER_REPORTER_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(reporter);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugShortDescByUserId(int assigned_to,
		String short_desc) throws SystemException {
		return findByBugShortDescByUserId(assigned_to, short_desc,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugShortDescByUserId(int assigned_to,
		String short_desc, int start, int end) throws SystemException {
		return findByBugShortDescByUserId(assigned_to, short_desc, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugShortDescByUserId(int assigned_to,
		String short_desc, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID;
			finderArgs = new Object[] { assigned_to, short_desc };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGSHORTDESCBYUSERID;
			finderArgs = new Object[] {
					assigned_to, short_desc,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						!Validator.equals(short_desc, bug.getShort_desc())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_ASSIGNED_TO_2);

			if (short_desc == null) {
				query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_1);
			}
			else {
				if (short_desc.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (short_desc != null) {
					qPos.add(short_desc);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugShortDescByUserId_First(int assigned_to,
		String short_desc, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugShortDescByUserId_First(assigned_to, short_desc,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", short_desc=");
		msg.append(short_desc);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugShortDescByUserId_First(int assigned_to,
		String short_desc, OrderByComparator orderByComparator)
		throws SystemException {
		List<Bug> list = findByBugShortDescByUserId(assigned_to, short_desc, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugShortDescByUserId_Last(int assigned_to,
		String short_desc, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugShortDescByUserId_Last(assigned_to, short_desc,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", short_desc=");
		msg.append(short_desc);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugShortDescByUserId_Last(int assigned_to,
		String short_desc, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugShortDescByUserId(assigned_to, short_desc);

		List<Bug> list = findByBugShortDescByUserId(assigned_to, short_desc,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugShortDescByUserId_PrevAndNext(int bug_id,
		int assigned_to, String short_desc, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugShortDescByUserId_PrevAndNext(session, bug,
					assigned_to, short_desc, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugShortDescByUserId_PrevAndNext(session, bug,
					assigned_to, short_desc, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugShortDescByUserId_PrevAndNext(Session session,
		Bug bug, int assigned_to, String short_desc,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_ASSIGNED_TO_2);

		if (short_desc == null) {
			query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_1);
		}
		else {
			if (short_desc.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (short_desc != null) {
			qPos.add(short_desc);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugVotesByUserId(int assigned_to, int votes)
		throws SystemException {
		return findByBugVotesByUserId(assigned_to, votes, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and votes = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugVotesByUserId(int assigned_to, int votes,
		int start, int end) throws SystemException {
		return findByBugVotesByUserId(assigned_to, votes, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and votes = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugVotesByUserId(int assigned_to, int votes,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGVOTESBYUSERID;
			finderArgs = new Object[] { assigned_to, votes };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGVOTESBYUSERID;
			finderArgs = new Object[] {
					assigned_to, votes,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						(votes != bug.getVotes())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGVOTESBYUSERID_ASSIGNED_TO_2);

			query.append(_FINDER_COLUMN_BUGVOTESBYUSERID_VOTES_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				qPos.add(votes);

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugVotesByUserId_First(int assigned_to, int votes,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugVotesByUserId_First(assigned_to, votes,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", votes=");
		msg.append(votes);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugVotesByUserId_First(int assigned_to, int votes,
		OrderByComparator orderByComparator) throws SystemException {
		List<Bug> list = findByBugVotesByUserId(assigned_to, votes, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugVotesByUserId_Last(int assigned_to, int votes,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugVotesByUserId_Last(assigned_to, votes,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", votes=");
		msg.append(votes);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugVotesByUserId_Last(int assigned_to, int votes,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugVotesByUserId(assigned_to, votes);

		List<Bug> list = findByBugVotesByUserId(assigned_to, votes, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugVotesByUserId_PrevAndNext(int bug_id,
		int assigned_to, int votes, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugVotesByUserId_PrevAndNext(session, bug,
					assigned_to, votes, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugVotesByUserId_PrevAndNext(session, bug,
					assigned_to, votes, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugVotesByUserId_PrevAndNext(Session session, Bug bug,
		int assigned_to, int votes, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGVOTESBYUSERID_ASSIGNED_TO_2);

		query.append(_FINDER_COLUMN_BUGVOTESBYUSERID_VOTES_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		qPos.add(votes);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugResolutionByUserId(int assigned_to,
		String resolution) throws SystemException {
		return findByBugResolutionByUserId(assigned_to, resolution,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where assigned_to = &#63; and resolution = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugResolutionByUserId(int assigned_to,
		String resolution, int start, int end) throws SystemException {
		return findByBugResolutionByUserId(assigned_to, resolution, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the bugs where assigned_to = &#63; and resolution = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugResolutionByUserId(int assigned_to,
		String resolution, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID;
			finderArgs = new Object[] { assigned_to, resolution };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGRESOLUTIONBYUSERID;
			finderArgs = new Object[] {
					assigned_to, resolution,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if ((assigned_to != bug.getAssigned_to()) ||
						!Validator.equals(resolution, bug.getResolution())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_ASSIGNED_TO_2);

			if (resolution == null) {
				query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_1);
			}
			else {
				if (resolution.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (resolution != null) {
					qPos.add(resolution);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugResolutionByUserId_First(int assigned_to,
		String resolution, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugResolutionByUserId_First(assigned_to, resolution,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", resolution=");
		msg.append(resolution);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugResolutionByUserId_First(int assigned_to,
		String resolution, OrderByComparator orderByComparator)
		throws SystemException {
		List<Bug> list = findByBugResolutionByUserId(assigned_to, resolution,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugResolutionByUserId_Last(int assigned_to,
		String resolution, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugResolutionByUserId_Last(assigned_to, resolution,
				orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("assigned_to=");
		msg.append(assigned_to);

		msg.append(", resolution=");
		msg.append(resolution);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugResolutionByUserId_Last(int assigned_to,
		String resolution, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugResolutionByUserId(assigned_to, resolution);

		List<Bug> list = findByBugResolutionByUserId(assigned_to, resolution,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugResolutionByUserId_PrevAndNext(int bug_id,
		int assigned_to, String resolution, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugResolutionByUserId_PrevAndNext(session, bug,
					assigned_to, resolution, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugResolutionByUserId_PrevAndNext(session, bug,
					assigned_to, resolution, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugResolutionByUserId_PrevAndNext(Session session,
		Bug bug, int assigned_to, String resolution,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_ASSIGNED_TO_2);

		if (resolution == null) {
			query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_1);
		}
		else {
			if (resolution.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(assigned_to);

		if (resolution != null) {
			qPos.add(resolution);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs where bug_severity = &#63;.
	 *
	 * @param bug_severity the bug_severity
	 * @return the matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugBySeverity(String bug_severity)
		throws SystemException {
		return findByBugBySeverity(bug_severity, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs where bug_severity = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param bug_severity the bug_severity
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugBySeverity(String bug_severity, int start, int end)
		throws SystemException {
		return findByBugBySeverity(bug_severity, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs where bug_severity = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param bug_severity the bug_severity
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findByBugBySeverity(String bug_severity, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGBYSEVERITY;
			finderArgs = new Object[] { bug_severity };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGBYSEVERITY;
			finderArgs = new Object[] {
					bug_severity,
					
					start, end, orderByComparator
				};
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Bug bug : list) {
				if (!Validator.equals(bug_severity, bug.getBug_severity())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUG_WHERE);

			if (bug_severity == null) {
				query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_1);
			}
			else {
				if (bug_severity.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bug_severity != null) {
					qPos.add(bug_severity);
				}

				list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bug in the ordered set where bug_severity = &#63;.
	 *
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugBySeverity_First(String bug_severity,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugBySeverity_First(bug_severity, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bug_severity=");
		msg.append(bug_severity);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the first bug in the ordered set where bug_severity = &#63;.
	 *
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugBySeverity_First(String bug_severity,
		OrderByComparator orderByComparator) throws SystemException {
		List<Bug> list = findByBugBySeverity(bug_severity, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bug in the ordered set where bug_severity = &#63;.
	 *
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug
	 * @throws com.vmware.NoSuchBugException if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug findByBugBySeverity_Last(String bug_severity,
		OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = fetchByBugBySeverity_Last(bug_severity, orderByComparator);

		if (bug != null) {
			return bug;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bug_severity=");
		msg.append(bug_severity);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugException(msg.toString());
	}

	/**
	 * Returns the last bug in the ordered set where bug_severity = &#63;.
	 *
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bug, or <code>null</code> if a matching bug could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug fetchByBugBySeverity_Last(String bug_severity,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugBySeverity(bug_severity);

		List<Bug> list = findByBugBySeverity(bug_severity, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs before and after the current bug in the ordered set where bug_severity = &#63;.
	 *
	 * @param bug_id the primary key of the current bug
	 * @param bug_severity the bug_severity
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bug
	 * @throws com.vmware.NoSuchBugException if a bug with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Bug[] findByBugBySeverity_PrevAndNext(int bug_id,
		String bug_severity, OrderByComparator orderByComparator)
		throws NoSuchBugException, SystemException {
		Bug bug = findByPrimaryKey(bug_id);

		Session session = null;

		try {
			session = openSession();

			Bug[] array = new BugImpl[3];

			array[0] = getByBugBySeverity_PrevAndNext(session, bug,
					bug_severity, orderByComparator, true);

			array[1] = bug;

			array[2] = getByBugBySeverity_PrevAndNext(session, bug,
					bug_severity, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Bug getByBugBySeverity_PrevAndNext(Session session, Bug bug,
		String bug_severity, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUG_WHERE);

		if (bug_severity == null) {
			query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_1);
		}
		else {
			if (bug_severity.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_3);
			}
			else {
				query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bug_severity != null) {
			qPos.add(bug_severity);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bug);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Bug> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs.
	 *
	 * @return the bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @return the range of bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bugs
	 * @param end the upper bound of the range of bugs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bugs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Bug> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Bug> list = (List<Bug>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BUG);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BUG.concat(BugModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Bug>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Bug>)QueryUtil.list(q, getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByAssignToId(int assigned_to) throws SystemException {
		for (Bug bug : findByAssignToId(assigned_to)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where priority = &#63; from the database.
	 *
	 * @param priority the priority
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugPriority(String priority) throws SystemException {
		for (Bug bug : findByBugPriority(priority)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and bug_severity = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugSeverityByUserId(int assigned_to, String bug_severity)
		throws SystemException {
		for (Bug bug : findByBugSeverityByUserId(assigned_to, bug_severity)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and priority = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugPriorityByUserId(int assigned_to, String priority)
		throws SystemException {
		for (Bug bug : findByBugPriorityByUserId(assigned_to, priority)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and bug_status = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugStatusByUserId(int assigned_to, String bug_status)
		throws SystemException {
		for (Bug bug : findByBugStatusByUserId(assigned_to, bug_status)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and keywords = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugKeywordsByUserId(int assigned_to, String keywords)
		throws SystemException {
		for (Bug bug : findByBugKeywordsByUserId(assigned_to, keywords)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where reporter = &#63; from the database.
	 *
	 * @param reporter the reporter
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugsByReporter(int reporter) throws SystemException {
		for (Bug bug : findByBugsByReporter(reporter)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and short_desc = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugShortDescByUserId(int assigned_to, String short_desc)
		throws SystemException {
		for (Bug bug : findByBugShortDescByUserId(assigned_to, short_desc)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and votes = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugVotesByUserId(int assigned_to, int votes)
		throws SystemException {
		for (Bug bug : findByBugVotesByUserId(assigned_to, votes)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where assigned_to = &#63; and resolution = &#63; from the database.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugResolutionByUserId(int assigned_to, String resolution)
		throws SystemException {
		for (Bug bug : findByBugResolutionByUserId(assigned_to, resolution)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs where bug_severity = &#63; from the database.
	 *
	 * @param bug_severity the bug_severity
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugBySeverity(String bug_severity)
		throws SystemException {
		for (Bug bug : findByBugBySeverity(bug_severity)) {
			remove(bug);
		}
	}

	/**
	 * Removes all the bugs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Bug bug : findAll()) {
			remove(bug);
		}
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByAssignToId(int assigned_to) throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ASSIGNTOID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_ASSIGNTOID_ASSIGNED_TO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ASSIGNTOID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where priority = &#63;.
	 *
	 * @param priority the priority
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugPriority(String priority) throws SystemException {
		Object[] finderArgs = new Object[] { priority };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGPRIORITY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUG_WHERE);

			if (priority == null) {
				query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_1);
			}
			else {
				if (priority.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGPRIORITY_PRIORITY_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (priority != null) {
					qPos.add(priority);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGPRIORITY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and bug_severity = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_severity the bug_severity
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugSeverityByUserId(int assigned_to, String bug_severity)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, bug_severity };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_ASSIGNED_TO_2);

			if (bug_severity == null) {
				query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_1);
			}
			else {
				if (bug_severity.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (bug_severity != null) {
					qPos.add(bug_severity);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and priority = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param priority the priority
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugPriorityByUserId(int assigned_to, String priority)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, priority };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGPRIORITYBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_ASSIGNED_TO_2);

			if (priority == null) {
				query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_1);
			}
			else {
				if (priority.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (priority != null) {
					qPos.add(priority);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGPRIORITYBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and bug_status = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param bug_status the bug_status
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugStatusByUserId(int assigned_to, String bug_status)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, bug_status };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_ASSIGNED_TO_2);

			if (bug_status == null) {
				query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_1);
			}
			else {
				if (bug_status.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (bug_status != null) {
					qPos.add(bug_status);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and keywords = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param keywords the keywords
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugKeywordsByUserId(int assigned_to, String keywords)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, keywords };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGKEYWORDSBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_ASSIGNED_TO_2);

			if (keywords == null) {
				query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_1);
			}
			else {
				if (keywords.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (keywords != null) {
					qPos.add(keywords);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGKEYWORDSBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where reporter = &#63;.
	 *
	 * @param reporter the reporter
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugsByReporter(int reporter) throws SystemException {
		Object[] finderArgs = new Object[] { reporter };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGSBYREPORTER,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSBYREPORTER_REPORTER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(reporter);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGSBYREPORTER,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and short_desc = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param short_desc the short_desc
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugShortDescByUserId(int assigned_to, String short_desc)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, short_desc };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGSHORTDESCBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_ASSIGNED_TO_2);

			if (short_desc == null) {
				query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_1);
			}
			else {
				if (short_desc.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (short_desc != null) {
					qPos.add(short_desc);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGSHORTDESCBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and votes = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param votes the votes
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugVotesByUserId(int assigned_to, int votes)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, votes };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGVOTESBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGVOTESBYUSERID_ASSIGNED_TO_2);

			query.append(_FINDER_COLUMN_BUGVOTESBYUSERID_VOTES_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				qPos.add(votes);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGVOTESBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where assigned_to = &#63; and resolution = &#63;.
	 *
	 * @param assigned_to the assigned_to
	 * @param resolution the resolution
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugResolutionByUserId(int assigned_to, String resolution)
		throws SystemException {
		Object[] finderArgs = new Object[] { assigned_to, resolution };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGRESOLUTIONBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUG_WHERE);

			query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_ASSIGNED_TO_2);

			if (resolution == null) {
				query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_1);
			}
			else {
				if (resolution.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(assigned_to);

				if (resolution != null) {
					qPos.add(resolution);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGRESOLUTIONBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs where bug_severity = &#63;.
	 *
	 * @param bug_severity the bug_severity
	 * @return the number of matching bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugBySeverity(String bug_severity)
		throws SystemException {
		Object[] finderArgs = new Object[] { bug_severity };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGBYSEVERITY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUG_WHERE);

			if (bug_severity == null) {
				query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_1);
			}
			else {
				if (bug_severity.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bug_severity != null) {
					qPos.add(bug_severity);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGBYSEVERITY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs.
	 *
	 * @return the number of bugs
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BUG);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the bug persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Bug")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Bug>> listenersList = new ArrayList<ModelListener<Bug>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Bug>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BugImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BUG = "SELECT bug FROM Bug bug";
	private static final String _SQL_SELECT_BUG_WHERE = "SELECT bug FROM Bug bug WHERE ";
	private static final String _SQL_COUNT_BUG = "SELECT COUNT(bug) FROM Bug bug";
	private static final String _SQL_COUNT_BUG_WHERE = "SELECT COUNT(bug) FROM Bug bug WHERE ";
	private static final String _FINDER_COLUMN_ASSIGNTOID_ASSIGNED_TO_2 = "bug.assigned_to = ?";
	private static final String _FINDER_COLUMN_BUGPRIORITY_PRIORITY_1 = "bug.priority IS NULL";
	private static final String _FINDER_COLUMN_BUGPRIORITY_PRIORITY_2 = "bug.priority = ?";
	private static final String _FINDER_COLUMN_BUGPRIORITY_PRIORITY_3 = "(bug.priority IS NULL OR bug.priority = ?)";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYUSERID_ASSIGNED_TO_2 =
		"bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_1 =
		"bug.bug_severity IS NULL";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_2 =
		"bug.bug_severity = ?";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYUSERID_BUG_SEVERITY_3 =
		"(bug.bug_severity IS NULL OR bug.bug_severity = ?)";
	private static final String _FINDER_COLUMN_BUGPRIORITYBYUSERID_ASSIGNED_TO_2 =
		"bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_1 = "bug.priority IS NULL";
	private static final String _FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_2 = "bug.priority = ?";
	private static final String _FINDER_COLUMN_BUGPRIORITYBYUSERID_PRIORITY_3 = "(bug.priority IS NULL OR bug.priority = ?)";
	private static final String _FINDER_COLUMN_BUGSTATUSBYUSERID_ASSIGNED_TO_2 = "bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_1 = "bug.bug_status IS NULL";
	private static final String _FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_2 = "bug.bug_status = ?";
	private static final String _FINDER_COLUMN_BUGSTATUSBYUSERID_BUG_STATUS_3 = "(bug.bug_status IS NULL OR bug.bug_status = ?)";
	private static final String _FINDER_COLUMN_BUGKEYWORDSBYUSERID_ASSIGNED_TO_2 =
		"bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_1 = "bug.keywords IS NULL";
	private static final String _FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_2 = "bug.keywords = ?";
	private static final String _FINDER_COLUMN_BUGKEYWORDSBYUSERID_KEYWORDS_3 = "(bug.keywords IS NULL OR bug.keywords = ?)";
	private static final String _FINDER_COLUMN_BUGSBYREPORTER_REPORTER_2 = "bug.reporter = ?";
	private static final String _FINDER_COLUMN_BUGSHORTDESCBYUSERID_ASSIGNED_TO_2 =
		"bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_1 =
		"bug.short_desc IS NULL";
	private static final String _FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_2 =
		"bug.short_desc = ?";
	private static final String _FINDER_COLUMN_BUGSHORTDESCBYUSERID_SHORT_DESC_3 =
		"(bug.short_desc IS NULL OR bug.short_desc = ?)";
	private static final String _FINDER_COLUMN_BUGVOTESBYUSERID_ASSIGNED_TO_2 = "bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGVOTESBYUSERID_VOTES_2 = "bug.votes = ?";
	private static final String _FINDER_COLUMN_BUGRESOLUTIONBYUSERID_ASSIGNED_TO_2 =
		"bug.assigned_to = ? AND ";
	private static final String _FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_1 =
		"bug.resolution IS NULL";
	private static final String _FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_2 =
		"bug.resolution = ?";
	private static final String _FINDER_COLUMN_BUGRESOLUTIONBYUSERID_RESOLUTION_3 =
		"(bug.resolution IS NULL OR bug.resolution = ?)";
	private static final String _FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_1 = "bug.bug_severity IS NULL";
	private static final String _FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_2 = "bug.bug_severity = ?";
	private static final String _FINDER_COLUMN_BUGBYSEVERITY_BUG_SEVERITY_3 = "(bug.bug_severity IS NULL OR bug.bug_severity = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "bug.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Bug exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Bug exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BugPersistenceImpl.class);
	private static Bug _nullBug = new BugImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Bug> toCacheModel() {
				return _nullBugCacheModel;
			}
		};

	private static CacheModel<Bug> _nullBugCacheModel = new CacheModel<Bug>() {
			public Bug toEntityModel() {
				return _nullBug;
			}
		};
}