/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchBugSeverityException;

import com.vmware.model.BugSeverity;
import com.vmware.model.impl.BugSeverityImpl;
import com.vmware.model.impl.BugSeverityModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the bug severity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugSeverityPersistence
 * @see BugSeverityUtil
 * @generated
 */
public class BugSeverityPersistenceImpl extends BasePersistenceImpl<BugSeverity>
	implements BugSeverityPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BugSeverityUtil} to access the bug severity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BugSeverityImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE = new FinderPath(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityModelImpl.FINDER_CACHE_ENABLED, BugSeverityImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByBugSeverityByValue",
			new String[] { String.class.getName() },
			BugSeverityModelImpl.VALUE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGSEVERITYBYVALUE = new FinderPath(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugSeverityByValue", new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityModelImpl.FINDER_CACHE_ENABLED, BugSeverityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityModelImpl.FINDER_CACHE_ENABLED, BugSeverityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the bug severity in the entity cache if it is enabled.
	 *
	 * @param bugSeverity the bug severity
	 */
	public void cacheResult(BugSeverity bugSeverity) {
		EntityCacheUtil.putResult(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityImpl.class, bugSeverity.getPrimaryKey(), bugSeverity);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
			new Object[] { bugSeverity.getValue() }, bugSeverity);

		bugSeverity.resetOriginalValues();
	}

	/**
	 * Caches the bug severities in the entity cache if it is enabled.
	 *
	 * @param bugSeverities the bug severities
	 */
	public void cacheResult(List<BugSeverity> bugSeverities) {
		for (BugSeverity bugSeverity : bugSeverities) {
			if (EntityCacheUtil.getResult(
						BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
						BugSeverityImpl.class, bugSeverity.getPrimaryKey()) == null) {
				cacheResult(bugSeverity);
			}
			else {
				bugSeverity.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all bug severities.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BugSeverityImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BugSeverityImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the bug severity.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BugSeverity bugSeverity) {
		EntityCacheUtil.removeResult(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityImpl.class, bugSeverity.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(bugSeverity);
	}

	@Override
	public void clearCache(List<BugSeverity> bugSeverities) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BugSeverity bugSeverity : bugSeverities) {
			EntityCacheUtil.removeResult(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
				BugSeverityImpl.class, bugSeverity.getPrimaryKey());

			clearUniqueFindersCache(bugSeverity);
		}
	}

	protected void clearUniqueFindersCache(BugSeverity bugSeverity) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
			new Object[] { bugSeverity.getValue() });
	}

	/**
	 * Creates a new bug severity with the primary key. Does not add the bug severity to the database.
	 *
	 * @param bug_severtiy_id the primary key for the new bug severity
	 * @return the new bug severity
	 */
	public BugSeverity create(int bug_severtiy_id) {
		BugSeverity bugSeverity = new BugSeverityImpl();

		bugSeverity.setNew(true);
		bugSeverity.setPrimaryKey(bug_severtiy_id);

		return bugSeverity;
	}

	/**
	 * Removes the bug severity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bug_severtiy_id the primary key of the bug severity
	 * @return the bug severity that was removed
	 * @throws com.vmware.NoSuchBugSeverityException if a bug severity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity remove(int bug_severtiy_id)
		throws NoSuchBugSeverityException, SystemException {
		return remove(Integer.valueOf(bug_severtiy_id));
	}

	/**
	 * Removes the bug severity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the bug severity
	 * @return the bug severity that was removed
	 * @throws com.vmware.NoSuchBugSeverityException if a bug severity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugSeverity remove(Serializable primaryKey)
		throws NoSuchBugSeverityException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BugSeverity bugSeverity = (BugSeverity)session.get(BugSeverityImpl.class,
					primaryKey);

			if (bugSeverity == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBugSeverityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(bugSeverity);
		}
		catch (NoSuchBugSeverityException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BugSeverity removeImpl(BugSeverity bugSeverity)
		throws SystemException {
		bugSeverity = toUnwrappedModel(bugSeverity);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, bugSeverity);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(bugSeverity);

		return bugSeverity;
	}

	@Override
	public BugSeverity updateImpl(com.vmware.model.BugSeverity bugSeverity,
		boolean merge) throws SystemException {
		bugSeverity = toUnwrappedModel(bugSeverity);

		boolean isNew = bugSeverity.isNew();

		BugSeverityModelImpl bugSeverityModelImpl = (BugSeverityModelImpl)bugSeverity;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, bugSeverity, merge);

			bugSeverity.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BugSeverityModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
			BugSeverityImpl.class, bugSeverity.getPrimaryKey(), bugSeverity);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
				new Object[] { bugSeverity.getValue() }, bugSeverity);
		}
		else {
			if ((bugSeverityModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bugSeverityModelImpl.getOriginalValue()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYVALUE,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
					new Object[] { bugSeverity.getValue() }, bugSeverity);
			}
		}

		return bugSeverity;
	}

	protected BugSeverity toUnwrappedModel(BugSeverity bugSeverity) {
		if (bugSeverity instanceof BugSeverityImpl) {
			return bugSeverity;
		}

		BugSeverityImpl bugSeverityImpl = new BugSeverityImpl();

		bugSeverityImpl.setNew(bugSeverity.isNew());
		bugSeverityImpl.setPrimaryKey(bugSeverity.getPrimaryKey());

		bugSeverityImpl.setBug_severtiy_id(bugSeverity.getBug_severtiy_id());
		bugSeverityImpl.setValue(bugSeverity.getValue());
		bugSeverityImpl.setSortkey(bugSeverity.getSortkey());
		bugSeverityImpl.setIsactive(bugSeverity.getIsactive());

		return bugSeverityImpl;
	}

	/**
	 * Returns the bug severity with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the bug severity
	 * @return the bug severity
	 * @throws com.liferay.portal.NoSuchModelException if a bug severity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugSeverity findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bug severity with the primary key or throws a {@link com.vmware.NoSuchBugSeverityException} if it could not be found.
	 *
	 * @param bug_severtiy_id the primary key of the bug severity
	 * @return the bug severity
	 * @throws com.vmware.NoSuchBugSeverityException if a bug severity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity findByPrimaryKey(int bug_severtiy_id)
		throws NoSuchBugSeverityException, SystemException {
		BugSeverity bugSeverity = fetchByPrimaryKey(bug_severtiy_id);

		if (bugSeverity == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + bug_severtiy_id);
			}

			throw new NoSuchBugSeverityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				bug_severtiy_id);
		}

		return bugSeverity;
	}

	/**
	 * Returns the bug severity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the bug severity
	 * @return the bug severity, or <code>null</code> if a bug severity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugSeverity fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bug severity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bug_severtiy_id the primary key of the bug severity
	 * @return the bug severity, or <code>null</code> if a bug severity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity fetchByPrimaryKey(int bug_severtiy_id)
		throws SystemException {
		BugSeverity bugSeverity = (BugSeverity)EntityCacheUtil.getResult(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
				BugSeverityImpl.class, bug_severtiy_id);

		if (bugSeverity == _nullBugSeverity) {
			return null;
		}

		if (bugSeverity == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				bugSeverity = (BugSeverity)session.get(BugSeverityImpl.class,
						Integer.valueOf(bug_severtiy_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (bugSeverity != null) {
					cacheResult(bugSeverity);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BugSeverityModelImpl.ENTITY_CACHE_ENABLED,
						BugSeverityImpl.class, bug_severtiy_id, _nullBugSeverity);
				}

				closeSession(session);
			}
		}

		return bugSeverity;
	}

	/**
	 * Returns the bug severity where value = &#63; or throws a {@link com.vmware.NoSuchBugSeverityException} if it could not be found.
	 *
	 * @param value the value
	 * @return the matching bug severity
	 * @throws com.vmware.NoSuchBugSeverityException if a matching bug severity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity findByBugSeverityByValue(String value)
		throws NoSuchBugSeverityException, SystemException {
		BugSeverity bugSeverity = fetchByBugSeverityByValue(value);

		if (bugSeverity == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("value=");
			msg.append(value);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBugSeverityException(msg.toString());
		}

		return bugSeverity;
	}

	/**
	 * Returns the bug severity where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param value the value
	 * @return the matching bug severity, or <code>null</code> if a matching bug severity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity fetchByBugSeverityByValue(String value)
		throws SystemException {
		return fetchByBugSeverityByValue(value, true);
	}

	/**
	 * Returns the bug severity where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param value the value
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching bug severity, or <code>null</code> if a matching bug severity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity fetchByBugSeverityByValue(String value,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { value };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
					finderArgs, this);
		}

		if (result instanceof BugSeverity) {
			BugSeverity bugSeverity = (BugSeverity)result;

			if (!Validator.equals(value, bugSeverity.getValue())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_BUGSEVERITY_WHERE);

			if (value == null) {
				query.append(_FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_1);
			}
			else {
				if (value.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (value != null) {
					qPos.add(value);
				}

				List<BugSeverity> list = q.list();

				result = list;

				BugSeverity bugSeverity = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
						finderArgs, list);
				}
				else {
					bugSeverity = list.get(0);

					cacheResult(bugSeverity);

					if ((bugSeverity.getValue() == null) ||
							!bugSeverity.getValue().equals(value)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
							finderArgs, bugSeverity);
					}
				}

				return bugSeverity;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BUGSEVERITYBYVALUE,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (BugSeverity)result;
			}
		}
	}

	/**
	 * Returns all the bug severities.
	 *
	 * @return the bug severities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugSeverity> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bug severities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bug severities
	 * @param end the upper bound of the range of bug severities (not inclusive)
	 * @return the range of bug severities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugSeverity> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the bug severities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bug severities
	 * @param end the upper bound of the range of bug severities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bug severities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugSeverity> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BugSeverity> list = (List<BugSeverity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BUGSEVERITY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BUGSEVERITY;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BugSeverity>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BugSeverity>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the bug severity where value = &#63; from the database.
	 *
	 * @param value the value
	 * @return the bug severity that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public BugSeverity removeByBugSeverityByValue(String value)
		throws NoSuchBugSeverityException, SystemException {
		BugSeverity bugSeverity = findByBugSeverityByValue(value);

		return remove(bugSeverity);
	}

	/**
	 * Removes all the bug severities from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BugSeverity bugSeverity : findAll()) {
			remove(bugSeverity);
		}
	}

	/**
	 * Returns the number of bug severities where value = &#63;.
	 *
	 * @param value the value
	 * @return the number of matching bug severities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugSeverityByValue(String value)
		throws SystemException {
		Object[] finderArgs = new Object[] { value };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYVALUE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUGSEVERITY_WHERE);

			if (value == null) {
				query.append(_FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_1);
			}
			else {
				if (value.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (value != null) {
					qPos.add(value);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGSEVERITYBYVALUE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bug severities.
	 *
	 * @return the number of bug severities
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BUGSEVERITY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the bug severity persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.BugSeverity")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BugSeverity>> listenersList = new ArrayList<ModelListener<BugSeverity>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BugSeverity>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BugSeverityImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BUGSEVERITY = "SELECT bugSeverity FROM BugSeverity bugSeverity";
	private static final String _SQL_SELECT_BUGSEVERITY_WHERE = "SELECT bugSeverity FROM BugSeverity bugSeverity WHERE ";
	private static final String _SQL_COUNT_BUGSEVERITY = "SELECT COUNT(bugSeverity) FROM BugSeverity bugSeverity";
	private static final String _SQL_COUNT_BUGSEVERITY_WHERE = "SELECT COUNT(bugSeverity) FROM BugSeverity bugSeverity WHERE ";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_1 = "bugSeverity.value IS NULL";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_2 = "bugSeverity.value = ?";
	private static final String _FINDER_COLUMN_BUGSEVERITYBYVALUE_VALUE_3 = "(bugSeverity.value IS NULL OR bugSeverity.value = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "bugSeverity.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BugSeverity exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BugSeverity exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BugSeverityPersistenceImpl.class);
	private static BugSeverity _nullBugSeverity = new BugSeverityImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BugSeverity> toCacheModel() {
				return _nullBugSeverityCacheModel;
			}
		};

	private static CacheModel<BugSeverity> _nullBugSeverityCacheModel = new CacheModel<BugSeverity>() {
			public BugSeverity toEntityModel() {
				return _nullBugSeverity;
			}
		};
}