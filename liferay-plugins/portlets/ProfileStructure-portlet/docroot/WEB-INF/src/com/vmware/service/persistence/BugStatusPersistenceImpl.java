/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchBugStatusException;

import com.vmware.model.BugStatus;
import com.vmware.model.impl.BugStatusImpl;
import com.vmware.model.impl.BugStatusModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the bug status service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugStatusPersistence
 * @see BugStatusUtil
 * @generated
 */
public class BugStatusPersistenceImpl extends BasePersistenceImpl<BugStatus>
	implements BugStatusPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BugStatusUtil} to access the bug status persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BugStatusImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE = new FinderPath(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusModelImpl.FINDER_CACHE_ENABLED, BugStatusImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByBugStatusByValue",
			new String[] { String.class.getName() },
			BugStatusModelImpl.VALUE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGSTATUSBYVALUE = new FinderPath(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugStatusByValue", new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusModelImpl.FINDER_CACHE_ENABLED, BugStatusImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusModelImpl.FINDER_CACHE_ENABLED, BugStatusImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the bug status in the entity cache if it is enabled.
	 *
	 * @param bugStatus the bug status
	 */
	public void cacheResult(BugStatus bugStatus) {
		EntityCacheUtil.putResult(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusImpl.class, bugStatus.getPrimaryKey(), bugStatus);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
			new Object[] { bugStatus.getValue() }, bugStatus);

		bugStatus.resetOriginalValues();
	}

	/**
	 * Caches the bug statuses in the entity cache if it is enabled.
	 *
	 * @param bugStatuses the bug statuses
	 */
	public void cacheResult(List<BugStatus> bugStatuses) {
		for (BugStatus bugStatus : bugStatuses) {
			if (EntityCacheUtil.getResult(
						BugStatusModelImpl.ENTITY_CACHE_ENABLED,
						BugStatusImpl.class, bugStatus.getPrimaryKey()) == null) {
				cacheResult(bugStatus);
			}
			else {
				bugStatus.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all bug statuses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BugStatusImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BugStatusImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the bug status.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BugStatus bugStatus) {
		EntityCacheUtil.removeResult(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusImpl.class, bugStatus.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(bugStatus);
	}

	@Override
	public void clearCache(List<BugStatus> bugStatuses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BugStatus bugStatus : bugStatuses) {
			EntityCacheUtil.removeResult(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
				BugStatusImpl.class, bugStatus.getPrimaryKey());

			clearUniqueFindersCache(bugStatus);
		}
	}

	protected void clearUniqueFindersCache(BugStatus bugStatus) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
			new Object[] { bugStatus.getValue() });
	}

	/**
	 * Creates a new bug status with the primary key. Does not add the bug status to the database.
	 *
	 * @param bug_status_id the primary key for the new bug status
	 * @return the new bug status
	 */
	public BugStatus create(int bug_status_id) {
		BugStatus bugStatus = new BugStatusImpl();

		bugStatus.setNew(true);
		bugStatus.setPrimaryKey(bug_status_id);

		return bugStatus;
	}

	/**
	 * Removes the bug status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bug_status_id the primary key of the bug status
	 * @return the bug status that was removed
	 * @throws com.vmware.NoSuchBugStatusException if a bug status with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus remove(int bug_status_id)
		throws NoSuchBugStatusException, SystemException {
		return remove(Integer.valueOf(bug_status_id));
	}

	/**
	 * Removes the bug status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the bug status
	 * @return the bug status that was removed
	 * @throws com.vmware.NoSuchBugStatusException if a bug status with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugStatus remove(Serializable primaryKey)
		throws NoSuchBugStatusException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BugStatus bugStatus = (BugStatus)session.get(BugStatusImpl.class,
					primaryKey);

			if (bugStatus == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBugStatusException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(bugStatus);
		}
		catch (NoSuchBugStatusException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BugStatus removeImpl(BugStatus bugStatus)
		throws SystemException {
		bugStatus = toUnwrappedModel(bugStatus);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, bugStatus);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(bugStatus);

		return bugStatus;
	}

	@Override
	public BugStatus updateImpl(com.vmware.model.BugStatus bugStatus,
		boolean merge) throws SystemException {
		bugStatus = toUnwrappedModel(bugStatus);

		boolean isNew = bugStatus.isNew();

		BugStatusModelImpl bugStatusModelImpl = (BugStatusModelImpl)bugStatus;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, bugStatus, merge);

			bugStatus.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BugStatusModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
			BugStatusImpl.class, bugStatus.getPrimaryKey(), bugStatus);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
				new Object[] { bugStatus.getValue() }, bugStatus);
		}
		else {
			if ((bugStatusModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						bugStatusModelImpl.getOriginalValue()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYVALUE,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
					new Object[] { bugStatus.getValue() }, bugStatus);
			}
		}

		return bugStatus;
	}

	protected BugStatus toUnwrappedModel(BugStatus bugStatus) {
		if (bugStatus instanceof BugStatusImpl) {
			return bugStatus;
		}

		BugStatusImpl bugStatusImpl = new BugStatusImpl();

		bugStatusImpl.setNew(bugStatus.isNew());
		bugStatusImpl.setPrimaryKey(bugStatus.getPrimaryKey());

		bugStatusImpl.setBug_status_id(bugStatus.getBug_status_id());
		bugStatusImpl.setValue(bugStatus.getValue());
		bugStatusImpl.setSortkey(bugStatus.getSortkey());
		bugStatusImpl.setIsactive(bugStatus.getIsactive());

		return bugStatusImpl;
	}

	/**
	 * Returns the bug status with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the bug status
	 * @return the bug status
	 * @throws com.liferay.portal.NoSuchModelException if a bug status with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugStatus findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bug status with the primary key or throws a {@link com.vmware.NoSuchBugStatusException} if it could not be found.
	 *
	 * @param bug_status_id the primary key of the bug status
	 * @return the bug status
	 * @throws com.vmware.NoSuchBugStatusException if a bug status with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus findByPrimaryKey(int bug_status_id)
		throws NoSuchBugStatusException, SystemException {
		BugStatus bugStatus = fetchByPrimaryKey(bug_status_id);

		if (bugStatus == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + bug_status_id);
			}

			throw new NoSuchBugStatusException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				bug_status_id);
		}

		return bugStatus;
	}

	/**
	 * Returns the bug status with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the bug status
	 * @return the bug status, or <code>null</code> if a bug status with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugStatus fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bug status with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bug_status_id the primary key of the bug status
	 * @return the bug status, or <code>null</code> if a bug status with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus fetchByPrimaryKey(int bug_status_id)
		throws SystemException {
		BugStatus bugStatus = (BugStatus)EntityCacheUtil.getResult(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
				BugStatusImpl.class, bug_status_id);

		if (bugStatus == _nullBugStatus) {
			return null;
		}

		if (bugStatus == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				bugStatus = (BugStatus)session.get(BugStatusImpl.class,
						Integer.valueOf(bug_status_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (bugStatus != null) {
					cacheResult(bugStatus);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BugStatusModelImpl.ENTITY_CACHE_ENABLED,
						BugStatusImpl.class, bug_status_id, _nullBugStatus);
				}

				closeSession(session);
			}
		}

		return bugStatus;
	}

	/**
	 * Returns the bug status where value = &#63; or throws a {@link com.vmware.NoSuchBugStatusException} if it could not be found.
	 *
	 * @param value the value
	 * @return the matching bug status
	 * @throws com.vmware.NoSuchBugStatusException if a matching bug status could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus findByBugStatusByValue(String value)
		throws NoSuchBugStatusException, SystemException {
		BugStatus bugStatus = fetchByBugStatusByValue(value);

		if (bugStatus == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("value=");
			msg.append(value);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBugStatusException(msg.toString());
		}

		return bugStatus;
	}

	/**
	 * Returns the bug status where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param value the value
	 * @return the matching bug status, or <code>null</code> if a matching bug status could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus fetchByBugStatusByValue(String value)
		throws SystemException {
		return fetchByBugStatusByValue(value, true);
	}

	/**
	 * Returns the bug status where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param value the value
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching bug status, or <code>null</code> if a matching bug status could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus fetchByBugStatusByValue(String value,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { value };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
					finderArgs, this);
		}

		if (result instanceof BugStatus) {
			BugStatus bugStatus = (BugStatus)result;

			if (!Validator.equals(value, bugStatus.getValue())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_BUGSTATUS_WHERE);

			if (value == null) {
				query.append(_FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_1);
			}
			else {
				if (value.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (value != null) {
					qPos.add(value);
				}

				List<BugStatus> list = q.list();

				result = list;

				BugStatus bugStatus = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
						finderArgs, list);
				}
				else {
					bugStatus = list.get(0);

					cacheResult(bugStatus);

					if ((bugStatus.getValue() == null) ||
							!bugStatus.getValue().equals(value)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
							finderArgs, bugStatus);
					}
				}

				return bugStatus;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BUGSTATUSBYVALUE,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (BugStatus)result;
			}
		}
	}

	/**
	 * Returns all the bug statuses.
	 *
	 * @return the bug statuses
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugStatus> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bug statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bug statuses
	 * @param end the upper bound of the range of bug statuses (not inclusive)
	 * @return the range of bug statuses
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugStatus> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the bug statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bug statuses
	 * @param end the upper bound of the range of bug statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bug statuses
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugStatus> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BugStatus> list = (List<BugStatus>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BUGSTATUS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BUGSTATUS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BugStatus>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BugStatus>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the bug status where value = &#63; from the database.
	 *
	 * @param value the value
	 * @return the bug status that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public BugStatus removeByBugStatusByValue(String value)
		throws NoSuchBugStatusException, SystemException {
		BugStatus bugStatus = findByBugStatusByValue(value);

		return remove(bugStatus);
	}

	/**
	 * Removes all the bug statuses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BugStatus bugStatus : findAll()) {
			remove(bugStatus);
		}
	}

	/**
	 * Returns the number of bug statuses where value = &#63;.
	 *
	 * @param value the value
	 * @return the number of matching bug statuses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugStatusByValue(String value) throws SystemException {
		Object[] finderArgs = new Object[] { value };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYVALUE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUGSTATUS_WHERE);

			if (value == null) {
				query.append(_FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_1);
			}
			else {
				if (value.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_3);
				}
				else {
					query.append(_FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (value != null) {
					qPos.add(value);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGSTATUSBYVALUE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bug statuses.
	 *
	 * @return the number of bug statuses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BUGSTATUS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the bug status persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.BugStatus")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BugStatus>> listenersList = new ArrayList<ModelListener<BugStatus>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BugStatus>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BugStatusImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BUGSTATUS = "SELECT bugStatus FROM BugStatus bugStatus";
	private static final String _SQL_SELECT_BUGSTATUS_WHERE = "SELECT bugStatus FROM BugStatus bugStatus WHERE ";
	private static final String _SQL_COUNT_BUGSTATUS = "SELECT COUNT(bugStatus) FROM BugStatus bugStatus";
	private static final String _SQL_COUNT_BUGSTATUS_WHERE = "SELECT COUNT(bugStatus) FROM BugStatus bugStatus WHERE ";
	private static final String _FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_1 = "bugStatus.value IS NULL";
	private static final String _FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_2 = "bugStatus.value = ?";
	private static final String _FINDER_COLUMN_BUGSTATUSBYVALUE_VALUE_3 = "(bugStatus.value IS NULL OR bugStatus.value = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "bugStatus.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BugStatus exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BugStatus exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BugStatusPersistenceImpl.class);
	private static BugStatus _nullBugStatus = new BugStatusImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BugStatus> toCacheModel() {
				return _nullBugStatusCacheModel;
			}
		};

	private static CacheModel<BugStatus> _nullBugStatusCacheModel = new CacheModel<BugStatus>() {
			public BugStatus toEntityModel() {
				return _nullBugStatus;
			}
		};
}