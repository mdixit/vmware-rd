/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchBugsActivityException;

import com.vmware.model.BugsActivity;
import com.vmware.model.impl.BugsActivityImpl;
import com.vmware.model.impl.BugsActivityModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the bugs activity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see BugsActivityPersistence
 * @see BugsActivityUtil
 * @generated
 */
public class BugsActivityPersistenceImpl extends BasePersistenceImpl<BugsActivity>
	implements BugsActivityPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BugsActivityUtil} to access the bugs activity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BugsActivityImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBugActivityByBugId",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugActivityByBugId",
			new String[] { Integer.class.getName() },
			BugsActivityModelImpl.BUG_ID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGACTIVITYBYBUGID = new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugActivityByBugId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugActivityByAttachId",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugActivityByAttachId",
			new String[] { Integer.class.getName() },
			BugsActivityModelImpl.ATTACH_ID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGACTIVITYBYATTACHID = new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugActivityByAttachId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugActivityByUserId",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugActivityByUserId",
			new String[] { Integer.class.getName() },
			BugsActivityModelImpl.WHO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGACTIVITYBYUSERID = new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugActivityByUserId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBugActivityByFieldIdAndUserId",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBugActivityByFieldIdAndUserId",
			new String[] { Integer.class.getName(), Integer.class.getName() },
			BugsActivityModelImpl.WHO_COLUMN_BITMASK |
			BugsActivityModelImpl.FIELDID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BUGACTIVITYBYFIELDIDANDUSERID =
		new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBugActivityByFieldIdAndUserId",
			new String[] { Integer.class.getName(), Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, BugsActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the bugs activity in the entity cache if it is enabled.
	 *
	 * @param bugsActivity the bugs activity
	 */
	public void cacheResult(BugsActivity bugsActivity) {
		EntityCacheUtil.putResult(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityImpl.class, bugsActivity.getPrimaryKey(), bugsActivity);

		bugsActivity.resetOriginalValues();
	}

	/**
	 * Caches the bugs activities in the entity cache if it is enabled.
	 *
	 * @param bugsActivities the bugs activities
	 */
	public void cacheResult(List<BugsActivity> bugsActivities) {
		for (BugsActivity bugsActivity : bugsActivities) {
			if (EntityCacheUtil.getResult(
						BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
						BugsActivityImpl.class, bugsActivity.getPrimaryKey()) == null) {
				cacheResult(bugsActivity);
			}
			else {
				bugsActivity.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all bugs activities.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BugsActivityImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BugsActivityImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the bugs activity.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BugsActivity bugsActivity) {
		EntityCacheUtil.removeResult(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityImpl.class, bugsActivity.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<BugsActivity> bugsActivities) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BugsActivity bugsActivity : bugsActivities) {
			EntityCacheUtil.removeResult(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
				BugsActivityImpl.class, bugsActivity.getPrimaryKey());
		}
	}

	/**
	 * Creates a new bugs activity with the primary key. Does not add the bugs activity to the database.
	 *
	 * @param bug_activity_id the primary key for the new bugs activity
	 * @return the new bugs activity
	 */
	public BugsActivity create(int bug_activity_id) {
		BugsActivity bugsActivity = new BugsActivityImpl();

		bugsActivity.setNew(true);
		bugsActivity.setPrimaryKey(bug_activity_id);

		return bugsActivity;
	}

	/**
	 * Removes the bugs activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bug_activity_id the primary key of the bugs activity
	 * @return the bugs activity that was removed
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity remove(int bug_activity_id)
		throws NoSuchBugsActivityException, SystemException {
		return remove(Integer.valueOf(bug_activity_id));
	}

	/**
	 * Removes the bugs activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the bugs activity
	 * @return the bugs activity that was removed
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugsActivity remove(Serializable primaryKey)
		throws NoSuchBugsActivityException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BugsActivity bugsActivity = (BugsActivity)session.get(BugsActivityImpl.class,
					primaryKey);

			if (bugsActivity == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBugsActivityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(bugsActivity);
		}
		catch (NoSuchBugsActivityException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BugsActivity removeImpl(BugsActivity bugsActivity)
		throws SystemException {
		bugsActivity = toUnwrappedModel(bugsActivity);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, bugsActivity);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(bugsActivity);

		return bugsActivity;
	}

	@Override
	public BugsActivity updateImpl(com.vmware.model.BugsActivity bugsActivity,
		boolean merge) throws SystemException {
		bugsActivity = toUnwrappedModel(bugsActivity);

		boolean isNew = bugsActivity.isNew();

		BugsActivityModelImpl bugsActivityModelImpl = (BugsActivityModelImpl)bugsActivity;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, bugsActivity, merge);

			bugsActivity.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BugsActivityModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((bugsActivityModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getOriginalBug_id())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYBUGID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID,
					args);

				args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getBug_id())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYBUGID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID,
					args);
			}

			if ((bugsActivityModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getOriginalAttach_id())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYATTACHID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID,
					args);

				args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getAttach_id())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYATTACHID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID,
					args);
			}

			if ((bugsActivityModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getOriginalWho())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getWho())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID,
					args);
			}

			if ((bugsActivityModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getOriginalWho()),
						Integer.valueOf(bugsActivityModelImpl.getOriginalFieldid())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYFIELDIDANDUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(bugsActivityModelImpl.getWho()),
						Integer.valueOf(bugsActivityModelImpl.getFieldid())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYFIELDIDANDUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID,
					args);
			}
		}

		EntityCacheUtil.putResult(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
			BugsActivityImpl.class, bugsActivity.getPrimaryKey(), bugsActivity);

		return bugsActivity;
	}

	protected BugsActivity toUnwrappedModel(BugsActivity bugsActivity) {
		if (bugsActivity instanceof BugsActivityImpl) {
			return bugsActivity;
		}

		BugsActivityImpl bugsActivityImpl = new BugsActivityImpl();

		bugsActivityImpl.setNew(bugsActivity.isNew());
		bugsActivityImpl.setPrimaryKey(bugsActivity.getPrimaryKey());

		bugsActivityImpl.setBug_activity_id(bugsActivity.getBug_activity_id());
		bugsActivityImpl.setBug_id(bugsActivity.getBug_id());
		bugsActivityImpl.setAttach_id(bugsActivity.getAttach_id());
		bugsActivityImpl.setWho(bugsActivity.getWho());
		bugsActivityImpl.setBug_when(bugsActivity.getBug_when());
		bugsActivityImpl.setFieldid(bugsActivity.getFieldid());
		bugsActivityImpl.setAdded(bugsActivity.getAdded());
		bugsActivityImpl.setRemoved(bugsActivity.getRemoved());

		return bugsActivityImpl;
	}

	/**
	 * Returns the bugs activity with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the bugs activity
	 * @return the bugs activity
	 * @throws com.liferay.portal.NoSuchModelException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugsActivity findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bugs activity with the primary key or throws a {@link com.vmware.NoSuchBugsActivityException} if it could not be found.
	 *
	 * @param bug_activity_id the primary key of the bugs activity
	 * @return the bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByPrimaryKey(int bug_activity_id)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByPrimaryKey(bug_activity_id);

		if (bugsActivity == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + bug_activity_id);
			}

			throw new NoSuchBugsActivityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				bug_activity_id);
		}

		return bugsActivity;
	}

	/**
	 * Returns the bugs activity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the bugs activity
	 * @return the bugs activity, or <code>null</code> if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BugsActivity fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the bugs activity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bug_activity_id the primary key of the bugs activity
	 * @return the bugs activity, or <code>null</code> if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByPrimaryKey(int bug_activity_id)
		throws SystemException {
		BugsActivity bugsActivity = (BugsActivity)EntityCacheUtil.getResult(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
				BugsActivityImpl.class, bug_activity_id);

		if (bugsActivity == _nullBugsActivity) {
			return null;
		}

		if (bugsActivity == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				bugsActivity = (BugsActivity)session.get(BugsActivityImpl.class,
						Integer.valueOf(bug_activity_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (bugsActivity != null) {
					cacheResult(bugsActivity);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BugsActivityModelImpl.ENTITY_CACHE_ENABLED,
						BugsActivityImpl.class, bug_activity_id,
						_nullBugsActivity);
				}

				closeSession(session);
			}
		}

		return bugsActivity;
	}

	/**
	 * Returns all the bugs activities where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @return the matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByBugId(int bug_id)
		throws SystemException {
		return findByBugActivityByBugId(bug_id, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs activities where bug_id = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param bug_id the bug_id
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByBugId(int bug_id, int start,
		int end) throws SystemException {
		return findByBugActivityByBugId(bug_id, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs activities where bug_id = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param bug_id the bug_id
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByBugId(int bug_id, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID;
			finderArgs = new Object[] { bug_id };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYBUGID;
			finderArgs = new Object[] { bug_id, start, end, orderByComparator };
		}

		List<BugsActivity> list = (List<BugsActivity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BugsActivity bugsActivity : list) {
				if ((bug_id != bugsActivity.getBug_id())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYBUGID_BUG_ID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bug_id);

				list = (List<BugsActivity>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bugs activity in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByBugId_First(int bug_id,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByBugId_First(bug_id,
				orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bug_id=");
		msg.append(bug_id);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the first bugs activity in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByBugId_First(int bug_id,
		OrderByComparator orderByComparator) throws SystemException {
		List<BugsActivity> list = findByBugActivityByBugId(bug_id, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bugs activity in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByBugId_Last(int bug_id,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByBugId_Last(bug_id,
				orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bug_id=");
		msg.append(bug_id);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the last bugs activity in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByBugId_Last(int bug_id,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugActivityByBugId(bug_id);

		List<BugsActivity> list = findByBugActivityByBugId(bug_id, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs activities before and after the current bugs activity in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_activity_id the primary key of the current bugs activity
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity[] findByBugActivityByBugId_PrevAndNext(
		int bug_activity_id, int bug_id, OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = findByPrimaryKey(bug_activity_id);

		Session session = null;

		try {
			session = openSession();

			BugsActivity[] array = new BugsActivityImpl[3];

			array[0] = getByBugActivityByBugId_PrevAndNext(session,
					bugsActivity, bug_id, orderByComparator, true);

			array[1] = bugsActivity;

			array[2] = getByBugActivityByBugId_PrevAndNext(session,
					bugsActivity, bug_id, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BugsActivity getByBugActivityByBugId_PrevAndNext(
		Session session, BugsActivity bugsActivity, int bug_id,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

		query.append(_FINDER_COLUMN_BUGACTIVITYBYBUGID_BUG_ID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(bug_id);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bugsActivity);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BugsActivity> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs activities where attach_id = &#63;.
	 *
	 * @param attach_id the attach_id
	 * @return the matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByAttachId(int attach_id)
		throws SystemException {
		return findByBugActivityByAttachId(attach_id, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs activities where attach_id = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param attach_id the attach_id
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByAttachId(int attach_id,
		int start, int end) throws SystemException {
		return findByBugActivityByAttachId(attach_id, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs activities where attach_id = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param attach_id the attach_id
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByAttachId(int attach_id,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID;
			finderArgs = new Object[] { attach_id };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYATTACHID;
			finderArgs = new Object[] { attach_id, start, end, orderByComparator };
		}

		List<BugsActivity> list = (List<BugsActivity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BugsActivity bugsActivity : list) {
				if ((attach_id != bugsActivity.getAttach_id())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYATTACHID_ATTACH_ID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(attach_id);

				list = (List<BugsActivity>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bugs activity in the ordered set where attach_id = &#63;.
	 *
	 * @param attach_id the attach_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByAttachId_First(int attach_id,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByAttachId_First(attach_id,
				orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("attach_id=");
		msg.append(attach_id);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the first bugs activity in the ordered set where attach_id = &#63;.
	 *
	 * @param attach_id the attach_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByAttachId_First(int attach_id,
		OrderByComparator orderByComparator) throws SystemException {
		List<BugsActivity> list = findByBugActivityByAttachId(attach_id, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bugs activity in the ordered set where attach_id = &#63;.
	 *
	 * @param attach_id the attach_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByAttachId_Last(int attach_id,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByAttachId_Last(attach_id,
				orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("attach_id=");
		msg.append(attach_id);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the last bugs activity in the ordered set where attach_id = &#63;.
	 *
	 * @param attach_id the attach_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByAttachId_Last(int attach_id,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugActivityByAttachId(attach_id);

		List<BugsActivity> list = findByBugActivityByAttachId(attach_id,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs activities before and after the current bugs activity in the ordered set where attach_id = &#63;.
	 *
	 * @param bug_activity_id the primary key of the current bugs activity
	 * @param attach_id the attach_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity[] findByBugActivityByAttachId_PrevAndNext(
		int bug_activity_id, int attach_id, OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = findByPrimaryKey(bug_activity_id);

		Session session = null;

		try {
			session = openSession();

			BugsActivity[] array = new BugsActivityImpl[3];

			array[0] = getByBugActivityByAttachId_PrevAndNext(session,
					bugsActivity, attach_id, orderByComparator, true);

			array[1] = bugsActivity;

			array[2] = getByBugActivityByAttachId_PrevAndNext(session,
					bugsActivity, attach_id, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BugsActivity getByBugActivityByAttachId_PrevAndNext(
		Session session, BugsActivity bugsActivity, int attach_id,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

		query.append(_FINDER_COLUMN_BUGACTIVITYBYATTACHID_ATTACH_ID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(attach_id);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bugsActivity);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BugsActivity> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs activities where who = &#63;.
	 *
	 * @param who the who
	 * @return the matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByUserId(int who)
		throws SystemException {
		return findByBugActivityByUserId(who, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs activities where who = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param who the who
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByUserId(int who, int start,
		int end) throws SystemException {
		return findByBugActivityByUserId(who, start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs activities where who = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param who the who
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByUserId(int who, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID;
			finderArgs = new Object[] { who };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYUSERID;
			finderArgs = new Object[] { who, start, end, orderByComparator };
		}

		List<BugsActivity> list = (List<BugsActivity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BugsActivity bugsActivity : list) {
				if ((who != bugsActivity.getWho())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYUSERID_WHO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				list = (List<BugsActivity>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bugs activity in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByUserId_First(int who,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByUserId_First(who,
				orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("who=");
		msg.append(who);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the first bugs activity in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByUserId_First(int who,
		OrderByComparator orderByComparator) throws SystemException {
		List<BugsActivity> list = findByBugActivityByUserId(who, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bugs activity in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByUserId_Last(int who,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByUserId_Last(who,
				orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("who=");
		msg.append(who);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the last bugs activity in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByUserId_Last(int who,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBugActivityByUserId(who);

		List<BugsActivity> list = findByBugActivityByUserId(who, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs activities before and after the current bugs activity in the ordered set where who = &#63;.
	 *
	 * @param bug_activity_id the primary key of the current bugs activity
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity[] findByBugActivityByUserId_PrevAndNext(
		int bug_activity_id, int who, OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = findByPrimaryKey(bug_activity_id);

		Session session = null;

		try {
			session = openSession();

			BugsActivity[] array = new BugsActivityImpl[3];

			array[0] = getByBugActivityByUserId_PrevAndNext(session,
					bugsActivity, who, orderByComparator, true);

			array[1] = bugsActivity;

			array[2] = getByBugActivityByUserId_PrevAndNext(session,
					bugsActivity, who, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BugsActivity getByBugActivityByUserId_PrevAndNext(
		Session session, BugsActivity bugsActivity, int who,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

		query.append(_FINDER_COLUMN_BUGACTIVITYBYUSERID_WHO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(who);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bugsActivity);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BugsActivity> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs activities where who = &#63; and fieldid = &#63;.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @return the matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByFieldIdAndUserId(int who,
		int fieldid) throws SystemException {
		return findByBugActivityByFieldIdAndUserId(who, fieldid,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs activities where who = &#63; and fieldid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByFieldIdAndUserId(int who,
		int fieldid, int start, int end) throws SystemException {
		return findByBugActivityByFieldIdAndUserId(who, fieldid, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the bugs activities where who = &#63; and fieldid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findByBugActivityByFieldIdAndUserId(int who,
		int fieldid, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID;
			finderArgs = new Object[] { who, fieldid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BUGACTIVITYBYFIELDIDANDUSERID;
			finderArgs = new Object[] {
					who, fieldid,
					
					start, end, orderByComparator
				};
		}

		List<BugsActivity> list = (List<BugsActivity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BugsActivity bugsActivity : list) {
				if ((who != bugsActivity.getWho()) ||
						(fieldid != bugsActivity.getFieldid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_WHO_2);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_FIELDID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				qPos.add(fieldid);

				list = (List<BugsActivity>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByFieldIdAndUserId_First(int who,
		int fieldid, OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByFieldIdAndUserId_First(who,
				fieldid, orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("who=");
		msg.append(who);

		msg.append(", fieldid=");
		msg.append(fieldid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the first bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByFieldIdAndUserId_First(int who,
		int fieldid, OrderByComparator orderByComparator)
		throws SystemException {
		List<BugsActivity> list = findByBugActivityByFieldIdAndUserId(who,
				fieldid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity findByBugActivityByFieldIdAndUserId_Last(int who,
		int fieldid, OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = fetchByBugActivityByFieldIdAndUserId_Last(who,
				fieldid, orderByComparator);

		if (bugsActivity != null) {
			return bugsActivity;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("who=");
		msg.append(who);

		msg.append(", fieldid=");
		msg.append(fieldid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBugsActivityException(msg.toString());
	}

	/**
	 * Returns the last bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching bugs activity, or <code>null</code> if a matching bugs activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity fetchByBugActivityByFieldIdAndUserId_Last(int who,
		int fieldid, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBugActivityByFieldIdAndUserId(who, fieldid);

		List<BugsActivity> list = findByBugActivityByFieldIdAndUserId(who,
				fieldid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the bugs activities before and after the current bugs activity in the ordered set where who = &#63; and fieldid = &#63;.
	 *
	 * @param bug_activity_id the primary key of the current bugs activity
	 * @param who the who
	 * @param fieldid the fieldid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next bugs activity
	 * @throws com.vmware.NoSuchBugsActivityException if a bugs activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BugsActivity[] findByBugActivityByFieldIdAndUserId_PrevAndNext(
		int bug_activity_id, int who, int fieldid,
		OrderByComparator orderByComparator)
		throws NoSuchBugsActivityException, SystemException {
		BugsActivity bugsActivity = findByPrimaryKey(bug_activity_id);

		Session session = null;

		try {
			session = openSession();

			BugsActivity[] array = new BugsActivityImpl[3];

			array[0] = getByBugActivityByFieldIdAndUserId_PrevAndNext(session,
					bugsActivity, who, fieldid, orderByComparator, true);

			array[1] = bugsActivity;

			array[2] = getByBugActivityByFieldIdAndUserId_PrevAndNext(session,
					bugsActivity, who, fieldid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BugsActivity getByBugActivityByFieldIdAndUserId_PrevAndNext(
		Session session, BugsActivity bugsActivity, int who, int fieldid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BUGSACTIVITY_WHERE);

		query.append(_FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_WHO_2);

		query.append(_FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_FIELDID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BugsActivityModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(who);

		qPos.add(fieldid);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(bugsActivity);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BugsActivity> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the bugs activities.
	 *
	 * @return the bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the bugs activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the bugs activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<BugsActivity> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BugsActivity> list = (List<BugsActivity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BUGSACTIVITY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BUGSACTIVITY.concat(BugsActivityModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BugsActivity>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BugsActivity>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the bugs activities where bug_id = &#63; from the database.
	 *
	 * @param bug_id the bug_id
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugActivityByBugId(int bug_id)
		throws SystemException {
		for (BugsActivity bugsActivity : findByBugActivityByBugId(bug_id)) {
			remove(bugsActivity);
		}
	}

	/**
	 * Removes all the bugs activities where attach_id = &#63; from the database.
	 *
	 * @param attach_id the attach_id
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugActivityByAttachId(int attach_id)
		throws SystemException {
		for (BugsActivity bugsActivity : findByBugActivityByAttachId(attach_id)) {
			remove(bugsActivity);
		}
	}

	/**
	 * Removes all the bugs activities where who = &#63; from the database.
	 *
	 * @param who the who
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugActivityByUserId(int who) throws SystemException {
		for (BugsActivity bugsActivity : findByBugActivityByUserId(who)) {
			remove(bugsActivity);
		}
	}

	/**
	 * Removes all the bugs activities where who = &#63; and fieldid = &#63; from the database.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBugActivityByFieldIdAndUserId(int who, int fieldid)
		throws SystemException {
		for (BugsActivity bugsActivity : findByBugActivityByFieldIdAndUserId(
				who, fieldid)) {
			remove(bugsActivity);
		}
	}

	/**
	 * Removes all the bugs activities from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BugsActivity bugsActivity : findAll()) {
			remove(bugsActivity);
		}
	}

	/**
	 * Returns the number of bugs activities where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @return the number of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugActivityByBugId(int bug_id) throws SystemException {
		Object[] finderArgs = new Object[] { bug_id };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYBUGID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYBUGID_BUG_ID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bug_id);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYBUGID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs activities where attach_id = &#63;.
	 *
	 * @param attach_id the attach_id
	 * @return the number of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugActivityByAttachId(int attach_id)
		throws SystemException {
		Object[] finderArgs = new Object[] { attach_id };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYATTACHID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYATTACHID_ATTACH_ID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(attach_id);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYATTACHID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs activities where who = &#63;.
	 *
	 * @param who the who
	 * @return the number of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugActivityByUserId(int who) throws SystemException {
		Object[] finderArgs = new Object[] { who };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYUSERID_WHO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs activities where who = &#63; and fieldid = &#63;.
	 *
	 * @param who the who
	 * @param fieldid the fieldid
	 * @return the number of matching bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBugActivityByFieldIdAndUserId(int who, int fieldid)
		throws SystemException {
		Object[] finderArgs = new Object[] { who, fieldid };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYFIELDIDANDUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BUGSACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_WHO_2);

			query.append(_FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_FIELDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				qPos.add(fieldid);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BUGACTIVITYBYFIELDIDANDUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of bugs activities.
	 *
	 * @return the number of bugs activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BUGSACTIVITY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the bugs associated with the bugs activity.
	 *
	 * @param pk the primary key of the bugs activity
	 * @return the bugs associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Bug> getBugs(int pk) throws SystemException {
		return getBugs(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the bugs associated with the bugs activity.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the bugs activity
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of bugs associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Bug> getBugs(int pk, int start, int end)
		throws SystemException {
		return getBugs(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_BUGS = new FinderPath(com.vmware.model.impl.BugModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.BugModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.BugImpl.class,
			com.vmware.service.persistence.BugPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getBugs",
			new String[] {
				Integer.class.getName(), "java.lang.Integer",
				"java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_BUGS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the bugs associated with the bugs activity.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the bugs activity
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bugs associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Bug> getBugs(int pk, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.vmware.model.Bug> list = (List<com.vmware.model.Bug>)FinderCacheUtil.getResult(FINDER_PATH_GET_BUGS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETBUGS.concat(ORDER_BY_CLAUSE)
									  .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETBUGS.concat(com.vmware.model.impl.BugModelImpl.ORDER_BY_SQL);
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("bugs", com.vmware.model.impl.BugImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.vmware.model.Bug>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_BUGS,
						finderArgs);
				}
				else {
					bugPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_BUGS, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_BUGS_SIZE = new FinderPath(com.vmware.model.impl.BugModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.BugModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.BugImpl.class,
			com.vmware.service.persistence.BugPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getBugsSize", new String[] { Integer.class.getName() });

	static {
		FINDER_PATH_GET_BUGS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of bugs associated with the bugs activity.
	 *
	 * @param pk the primary key of the bugs activity
	 * @return the number of bugs associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public int getBugsSize(int pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_BUGS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETBUGSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_BUGS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_BUG = new FinderPath(com.vmware.model.impl.BugModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.BugModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.BugImpl.class,
			com.vmware.service.persistence.BugPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsBug",
			new String[] { Integer.class.getName(), Integer.class.getName() });

	/**
	 * Returns <code>true</code> if the bug is associated with the bugs activity.
	 *
	 * @param pk the primary key of the bugs activity
	 * @param bugPK the primary key of the bug
	 * @return <code>true</code> if the bug is associated with the bugs activity; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsBug(int pk, int bugPK) throws SystemException {
		Object[] finderArgs = new Object[] { pk, bugPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_BUG,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsBug.contains(pk, bugPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_BUG, finderArgs,
					value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the bugs activity has any bugs associated with it.
	 *
	 * @param pk the primary key of the bugs activity to check for associations with bugs
	 * @return <code>true</code> if the bugs activity has any bugs associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsBugs(int pk) throws SystemException {
		if (getBugsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Returns all the attachmentses associated with the bugs activity.
	 *
	 * @param pk the primary key of the bugs activity
	 * @return the attachmentses associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Attachments> getAttachmentses(int pk)
		throws SystemException {
		return getAttachmentses(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the attachmentses associated with the bugs activity.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the bugs activity
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @return the range of attachmentses associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Attachments> getAttachmentses(int pk,
		int start, int end) throws SystemException {
		return getAttachmentses(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_ATTACHMENTSES = new FinderPath(com.vmware.model.impl.AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.AttachmentsModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.AttachmentsImpl.class,
			com.vmware.service.persistence.AttachmentsPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getAttachmentses",
			new String[] {
				Integer.class.getName(), "java.lang.Integer",
				"java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_ATTACHMENTSES.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the attachmentses associated with the bugs activity.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the bugs activity
	 * @param start the lower bound of the range of bugs activities
	 * @param end the upper bound of the range of bugs activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of attachmentses associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Attachments> getAttachmentses(int pk,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.vmware.model.Attachments> list = (List<com.vmware.model.Attachments>)FinderCacheUtil.getResult(FINDER_PATH_GET_ATTACHMENTSES,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETATTACHMENTSES.concat(ORDER_BY_CLAUSE)
											   .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETATTACHMENTSES.concat(com.vmware.model.impl.AttachmentsModelImpl.ORDER_BY_SQL);
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("attachments",
					com.vmware.model.impl.AttachmentsImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.vmware.model.Attachments>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_ATTACHMENTSES,
						finderArgs);
				}
				else {
					attachmentsPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_ATTACHMENTSES,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_ATTACHMENTSES_SIZE = new FinderPath(com.vmware.model.impl.AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.AttachmentsModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.AttachmentsImpl.class,
			com.vmware.service.persistence.AttachmentsPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getAttachmentsesSize", new String[] { Integer.class.getName() });

	static {
		FINDER_PATH_GET_ATTACHMENTSES_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of attachmentses associated with the bugs activity.
	 *
	 * @param pk the primary key of the bugs activity
	 * @return the number of attachmentses associated with the bugs activity
	 * @throws SystemException if a system exception occurred
	 */
	public int getAttachmentsesSize(int pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_ATTACHMENTSES_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETATTACHMENTSESSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_ATTACHMENTSES_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_ATTACHMENTS = new FinderPath(com.vmware.model.impl.AttachmentsModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.AttachmentsModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.AttachmentsImpl.class,
			com.vmware.service.persistence.AttachmentsPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsAttachments",
			new String[] { Integer.class.getName(), Integer.class.getName() });

	/**
	 * Returns <code>true</code> if the attachments is associated with the bugs activity.
	 *
	 * @param pk the primary key of the bugs activity
	 * @param attachmentsPK the primary key of the attachments
	 * @return <code>true</code> if the attachments is associated with the bugs activity; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsAttachments(int pk, int attachmentsPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, attachmentsPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_ATTACHMENTS,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsAttachments.contains(pk,
							attachmentsPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_ATTACHMENTS,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the bugs activity has any attachmentses associated with it.
	 *
	 * @param pk the primary key of the bugs activity to check for associations with attachmentses
	 * @return <code>true</code> if the bugs activity has any attachmentses associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsAttachmentses(int pk) throws SystemException {
		if (getAttachmentsesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Initializes the bugs activity persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.BugsActivity")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BugsActivity>> listenersList = new ArrayList<ModelListener<BugsActivity>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BugsActivity>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsBug = new ContainsBug();

		containsAttachments = new ContainsAttachments();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BugsActivityImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsBug containsBug;
	protected ContainsAttachments containsAttachments;

	protected class ContainsBug {
		protected ContainsBug() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSBUG,
					new int[] { java.sql.Types.INTEGER, java.sql.Types.INTEGER },
					RowMapper.COUNT);
		}

		protected boolean contains(int bug_activity_id, int bug_id) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Integer(bug_activity_id), new Integer(bug_id)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class ContainsAttachments {
		protected ContainsAttachments() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSATTACHMENTS,
					new int[] { java.sql.Types.INTEGER, java.sql.Types.INTEGER },
					RowMapper.COUNT);
		}

		protected boolean contains(int bug_activity_id, int attach_id) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Integer(bug_activity_id), new Integer(attach_id)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	private static final String _SQL_SELECT_BUGSACTIVITY = "SELECT bugsActivity FROM BugsActivity bugsActivity";
	private static final String _SQL_SELECT_BUGSACTIVITY_WHERE = "SELECT bugsActivity FROM BugsActivity bugsActivity WHERE ";
	private static final String _SQL_COUNT_BUGSACTIVITY = "SELECT COUNT(bugsActivity) FROM BugsActivity bugsActivity";
	private static final String _SQL_COUNT_BUGSACTIVITY_WHERE = "SELECT COUNT(bugsActivity) FROM BugsActivity bugsActivity WHERE ";
	private static final String _SQL_GETBUGS = "SELECT {bugs.*} FROM bugs INNER JOIN bugs_activity ON (bugs_activity.id = bugs.id) WHERE (bugs_activity.id = ?)";
	private static final String _SQL_GETBUGSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM bugs WHERE id = ?";
	private static final String _SQL_CONTAINSBUG = "SELECT COUNT(*) AS COUNT_VALUE FROM bugs WHERE id = ? AND bug_id = ?";
	private static final String _SQL_GETATTACHMENTSES = "SELECT {attachments.*} FROM attachments INNER JOIN bugs_activity ON (bugs_activity.id = attachments.id) WHERE (bugs_activity.id = ?)";
	private static final String _SQL_GETATTACHMENTSESSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM attachments WHERE id = ?";
	private static final String _SQL_CONTAINSATTACHMENTS = "SELECT COUNT(*) AS COUNT_VALUE FROM attachments WHERE id = ? AND attach_id = ?";
	private static final String _FINDER_COLUMN_BUGACTIVITYBYBUGID_BUG_ID_2 = "bugsActivity.bug_id = ?";
	private static final String _FINDER_COLUMN_BUGACTIVITYBYATTACHID_ATTACH_ID_2 =
		"bugsActivity.attach_id = ?";
	private static final String _FINDER_COLUMN_BUGACTIVITYBYUSERID_WHO_2 = "bugsActivity.who = ?";
	private static final String _FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_WHO_2 =
		"bugsActivity.who = ? AND ";
	private static final String _FINDER_COLUMN_BUGACTIVITYBYFIELDIDANDUSERID_FIELDID_2 =
		"bugsActivity.fieldid = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "bugsActivity.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BugsActivity exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BugsActivity exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BugsActivityPersistenceImpl.class);
	private static BugsActivity _nullBugsActivity = new BugsActivityImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BugsActivity> toCacheModel() {
				return _nullBugsActivityCacheModel;
			}
		};

	private static CacheModel<BugsActivity> _nullBugsActivityCacheModel = new CacheModel<BugsActivity>() {
			public BugsActivity toEntityModel() {
				return _nullBugsActivity;
			}
		};
}