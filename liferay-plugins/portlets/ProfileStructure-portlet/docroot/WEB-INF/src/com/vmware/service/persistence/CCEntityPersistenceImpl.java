/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchCCEntityException;

import com.vmware.model.CCEntity;
import com.vmware.model.impl.CCEntityImpl;
import com.vmware.model.impl.CCEntityModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c c entity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see CCEntityPersistence
 * @see CCEntityUtil
 * @generated
 */
public class CCEntityPersistenceImpl extends BasePersistenceImpl<CCEntity>
	implements CCEntityPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CCEntityUtil} to access the c c entity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CCEntityImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
			CCEntityModelImpl.FINDER_CACHE_ENABLED, CCEntityImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
			CCEntityModelImpl.FINDER_CACHE_ENABLED, CCEntityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
			CCEntityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the c c entity in the entity cache if it is enabled.
	 *
	 * @param ccEntity the c c entity
	 */
	public void cacheResult(CCEntity ccEntity) {
		EntityCacheUtil.putResult(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
			CCEntityImpl.class, ccEntity.getPrimaryKey(), ccEntity);

		ccEntity.resetOriginalValues();
	}

	/**
	 * Caches the c c entities in the entity cache if it is enabled.
	 *
	 * @param ccEntities the c c entities
	 */
	public void cacheResult(List<CCEntity> ccEntities) {
		for (CCEntity ccEntity : ccEntities) {
			if (EntityCacheUtil.getResult(
						CCEntityModelImpl.ENTITY_CACHE_ENABLED,
						CCEntityImpl.class, ccEntity.getPrimaryKey()) == null) {
				cacheResult(ccEntity);
			}
			else {
				ccEntity.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c c entities.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CCEntityImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CCEntityImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c c entity.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CCEntity ccEntity) {
		EntityCacheUtil.removeResult(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
			CCEntityImpl.class, ccEntity.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CCEntity> ccEntities) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CCEntity ccEntity : ccEntities) {
			EntityCacheUtil.removeResult(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
				CCEntityImpl.class, ccEntity.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c c entity with the primary key. Does not add the c c entity to the database.
	 *
	 * @param ccEntityPK the primary key for the new c c entity
	 * @return the new c c entity
	 */
	public CCEntity create(CCEntityPK ccEntityPK) {
		CCEntity ccEntity = new CCEntityImpl();

		ccEntity.setNew(true);
		ccEntity.setPrimaryKey(ccEntityPK);

		return ccEntity;
	}

	/**
	 * Removes the c c entity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ccEntityPK the primary key of the c c entity
	 * @return the c c entity that was removed
	 * @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CCEntity remove(CCEntityPK ccEntityPK)
		throws NoSuchCCEntityException, SystemException {
		return remove((Serializable)ccEntityPK);
	}

	/**
	 * Removes the c c entity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c c entity
	 * @return the c c entity that was removed
	 * @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CCEntity remove(Serializable primaryKey)
		throws NoSuchCCEntityException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CCEntity ccEntity = (CCEntity)session.get(CCEntityImpl.class,
					primaryKey);

			if (ccEntity == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCCEntityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ccEntity);
		}
		catch (NoSuchCCEntityException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CCEntity removeImpl(CCEntity ccEntity) throws SystemException {
		ccEntity = toUnwrappedModel(ccEntity);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, ccEntity);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(ccEntity);

		return ccEntity;
	}

	@Override
	public CCEntity updateImpl(com.vmware.model.CCEntity ccEntity, boolean merge)
		throws SystemException {
		ccEntity = toUnwrappedModel(ccEntity);

		boolean isNew = ccEntity.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, ccEntity, merge);

			ccEntity.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
			CCEntityImpl.class, ccEntity.getPrimaryKey(), ccEntity);

		return ccEntity;
	}

	protected CCEntity toUnwrappedModel(CCEntity ccEntity) {
		if (ccEntity instanceof CCEntityImpl) {
			return ccEntity;
		}

		CCEntityImpl ccEntityImpl = new CCEntityImpl();

		ccEntityImpl.setNew(ccEntity.isNew());
		ccEntityImpl.setPrimaryKey(ccEntity.getPrimaryKey());

		ccEntityImpl.setBug_id(ccEntity.getBug_id());
		ccEntityImpl.setWho(ccEntity.getWho());

		return ccEntityImpl;
	}

	/**
	 * Returns the c c entity with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c c entity
	 * @return the c c entity
	 * @throws com.liferay.portal.NoSuchModelException if a c c entity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CCEntity findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey((CCEntityPK)primaryKey);
	}

	/**
	 * Returns the c c entity with the primary key or throws a {@link com.vmware.NoSuchCCEntityException} if it could not be found.
	 *
	 * @param ccEntityPK the primary key of the c c entity
	 * @return the c c entity
	 * @throws com.vmware.NoSuchCCEntityException if a c c entity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CCEntity findByPrimaryKey(CCEntityPK ccEntityPK)
		throws NoSuchCCEntityException, SystemException {
		CCEntity ccEntity = fetchByPrimaryKey(ccEntityPK);

		if (ccEntity == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + ccEntityPK);
			}

			throw new NoSuchCCEntityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				ccEntityPK);
		}

		return ccEntity;
	}

	/**
	 * Returns the c c entity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c c entity
	 * @return the c c entity, or <code>null</code> if a c c entity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CCEntity fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey((CCEntityPK)primaryKey);
	}

	/**
	 * Returns the c c entity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ccEntityPK the primary key of the c c entity
	 * @return the c c entity, or <code>null</code> if a c c entity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CCEntity fetchByPrimaryKey(CCEntityPK ccEntityPK)
		throws SystemException {
		CCEntity ccEntity = (CCEntity)EntityCacheUtil.getResult(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
				CCEntityImpl.class, ccEntityPK);

		if (ccEntity == _nullCCEntity) {
			return null;
		}

		if (ccEntity == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				ccEntity = (CCEntity)session.get(CCEntityImpl.class, ccEntityPK);
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (ccEntity != null) {
					cacheResult(ccEntity);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CCEntityModelImpl.ENTITY_CACHE_ENABLED,
						CCEntityImpl.class, ccEntityPK, _nullCCEntity);
				}

				closeSession(session);
			}
		}

		return ccEntity;
	}

	/**
	 * Returns all the c c entities.
	 *
	 * @return the c c entities
	 * @throws SystemException if a system exception occurred
	 */
	public List<CCEntity> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c c entities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of c c entities
	 * @param end the upper bound of the range of c c entities (not inclusive)
	 * @return the range of c c entities
	 * @throws SystemException if a system exception occurred
	 */
	public List<CCEntity> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c c entities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of c c entities
	 * @param end the upper bound of the range of c c entities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c c entities
	 * @throws SystemException if a system exception occurred
	 */
	public List<CCEntity> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CCEntity> list = (List<CCEntity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CCENTITY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CCENTITY;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CCEntity>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CCEntity>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c c entities from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CCEntity ccEntity : findAll()) {
			remove(ccEntity);
		}
	}

	/**
	 * Returns the number of c c entities.
	 *
	 * @return the number of c c entities
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CCENTITY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c c entity persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.CCEntity")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CCEntity>> listenersList = new ArrayList<ModelListener<CCEntity>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CCEntity>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CCEntityImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CCENTITY = "SELECT ccEntity FROM CCEntity ccEntity";
	private static final String _SQL_COUNT_CCENTITY = "SELECT COUNT(ccEntity) FROM CCEntity ccEntity";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ccEntity.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CCEntity exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CCEntityPersistenceImpl.class);
	private static CCEntity _nullCCEntity = new CCEntityImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CCEntity> toCacheModel() {
				return _nullCCEntityCacheModel;
			}
		};

	private static CacheModel<CCEntity> _nullCCEntityCacheModel = new CacheModel<CCEntity>() {
			public CCEntity toEntityModel() {
				return _nullCCEntity;
			}
		};
}