/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchComponentsException;

import com.vmware.model.Components;
import com.vmware.model.impl.ComponentsImpl;
import com.vmware.model.impl.ComponentsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the components service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ComponentsPersistence
 * @see ComponentsUtil
 * @generated
 */
public class ComponentsPersistenceImpl extends BasePersistenceImpl<Components>
	implements ComponentsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ComponentsUtil} to access the components persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ComponentsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
			ComponentsModelImpl.FINDER_CACHE_ENABLED, ComponentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
			ComponentsModelImpl.FINDER_CACHE_ENABLED, ComponentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
			ComponentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the components in the entity cache if it is enabled.
	 *
	 * @param components the components
	 */
	public void cacheResult(Components components) {
		EntityCacheUtil.putResult(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
			ComponentsImpl.class, components.getPrimaryKey(), components);

		components.resetOriginalValues();
	}

	/**
	 * Caches the componentses in the entity cache if it is enabled.
	 *
	 * @param componentses the componentses
	 */
	public void cacheResult(List<Components> componentses) {
		for (Components components : componentses) {
			if (EntityCacheUtil.getResult(
						ComponentsModelImpl.ENTITY_CACHE_ENABLED,
						ComponentsImpl.class, components.getPrimaryKey()) == null) {
				cacheResult(components);
			}
			else {
				components.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all componentses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ComponentsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ComponentsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the components.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Components components) {
		EntityCacheUtil.removeResult(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
			ComponentsImpl.class, components.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Components> componentses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Components components : componentses) {
			EntityCacheUtil.removeResult(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
				ComponentsImpl.class, components.getPrimaryKey());
		}
	}

	/**
	 * Creates a new components with the primary key. Does not add the components to the database.
	 *
	 * @param comp_id the primary key for the new components
	 * @return the new components
	 */
	public Components create(int comp_id) {
		Components components = new ComponentsImpl();

		components.setNew(true);
		components.setPrimaryKey(comp_id);

		return components;
	}

	/**
	 * Removes the components with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param comp_id the primary key of the components
	 * @return the components that was removed
	 * @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Components remove(int comp_id)
		throws NoSuchComponentsException, SystemException {
		return remove(Integer.valueOf(comp_id));
	}

	/**
	 * Removes the components with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the components
	 * @return the components that was removed
	 * @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Components remove(Serializable primaryKey)
		throws NoSuchComponentsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Components components = (Components)session.get(ComponentsImpl.class,
					primaryKey);

			if (components == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchComponentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(components);
		}
		catch (NoSuchComponentsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Components removeImpl(Components components)
		throws SystemException {
		components = toUnwrappedModel(components);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, components);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(components);

		return components;
	}

	@Override
	public Components updateImpl(com.vmware.model.Components components,
		boolean merge) throws SystemException {
		components = toUnwrappedModel(components);

		boolean isNew = components.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, components, merge);

			components.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
			ComponentsImpl.class, components.getPrimaryKey(), components);

		return components;
	}

	protected Components toUnwrappedModel(Components components) {
		if (components instanceof ComponentsImpl) {
			return components;
		}

		ComponentsImpl componentsImpl = new ComponentsImpl();

		componentsImpl.setNew(components.isNew());
		componentsImpl.setPrimaryKey(components.getPrimaryKey());

		componentsImpl.setComp_id(components.getComp_id());
		componentsImpl.setCategory_id(components.getCategory_id());
		componentsImpl.setName(components.getName());
		componentsImpl.setDescription(components.getDescription());
		componentsImpl.setTemplate(components.getTemplate());
		componentsImpl.setInitialowner(components.getInitialowner());
		componentsImpl.setInitialqacontact(components.getInitialqacontact());
		componentsImpl.setManager(components.getManager());
		componentsImpl.setQa_manager(components.getQa_manager());
		componentsImpl.setDisallownew(components.getDisallownew());
		componentsImpl.setDisable_template(components.getDisable_template());

		return componentsImpl;
	}

	/**
	 * Returns the components with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the components
	 * @return the components
	 * @throws com.liferay.portal.NoSuchModelException if a components with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Components findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the components with the primary key or throws a {@link com.vmware.NoSuchComponentsException} if it could not be found.
	 *
	 * @param comp_id the primary key of the components
	 * @return the components
	 * @throws com.vmware.NoSuchComponentsException if a components with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Components findByPrimaryKey(int comp_id)
		throws NoSuchComponentsException, SystemException {
		Components components = fetchByPrimaryKey(comp_id);

		if (components == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + comp_id);
			}

			throw new NoSuchComponentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				comp_id);
		}

		return components;
	}

	/**
	 * Returns the components with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the components
	 * @return the components, or <code>null</code> if a components with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Components fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the components with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param comp_id the primary key of the components
	 * @return the components, or <code>null</code> if a components with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Components fetchByPrimaryKey(int comp_id) throws SystemException {
		Components components = (Components)EntityCacheUtil.getResult(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
				ComponentsImpl.class, comp_id);

		if (components == _nullComponents) {
			return null;
		}

		if (components == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				components = (Components)session.get(ComponentsImpl.class,
						Integer.valueOf(comp_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (components != null) {
					cacheResult(components);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ComponentsModelImpl.ENTITY_CACHE_ENABLED,
						ComponentsImpl.class, comp_id, _nullComponents);
				}

				closeSession(session);
			}
		}

		return components;
	}

	/**
	 * Returns all the componentses.
	 *
	 * @return the componentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Components> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the componentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of componentses
	 * @param end the upper bound of the range of componentses (not inclusive)
	 * @return the range of componentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Components> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the componentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of componentses
	 * @param end the upper bound of the range of componentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of componentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Components> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Components> list = (List<Components>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COMPONENTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COMPONENTS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Components>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Components>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the componentses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Components components : findAll()) {
			remove(components);
		}
	}

	/**
	 * Returns the number of componentses.
	 *
	 * @return the number of componentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COMPONENTS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the profileses associated with the components.
	 *
	 * @param pk the primary key of the components
	 * @return the profileses associated with the components
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Profiles> getProfileses(int pk)
		throws SystemException {
		return getProfileses(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the profileses associated with the components.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the components
	 * @param start the lower bound of the range of componentses
	 * @param end the upper bound of the range of componentses (not inclusive)
	 * @return the range of profileses associated with the components
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Profiles> getProfileses(int pk, int start,
		int end) throws SystemException {
		return getProfileses(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_PROFILESES = new FinderPath(com.vmware.model.impl.ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesImpl.class,
			com.vmware.service.persistence.ProfilesPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getProfileses",
			new String[] {
				Integer.class.getName(), "java.lang.Integer",
				"java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_PROFILESES.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the profileses associated with the components.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the components
	 * @param start the lower bound of the range of componentses
	 * @param end the upper bound of the range of componentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of profileses associated with the components
	 * @throws SystemException if a system exception occurred
	 */
	public List<com.vmware.model.Profiles> getProfileses(int pk, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<com.vmware.model.Profiles> list = (List<com.vmware.model.Profiles>)FinderCacheUtil.getResult(FINDER_PATH_GET_PROFILESES,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETPROFILESES.concat(ORDER_BY_CLAUSE)
											.concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETPROFILESES;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("profiles", com.vmware.model.impl.ProfilesImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<com.vmware.model.Profiles>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_PROFILESES,
						finderArgs);
				}
				else {
					profilesPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_PROFILESES,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_PROFILESES_SIZE = new FinderPath(com.vmware.model.impl.ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesImpl.class,
			com.vmware.service.persistence.ProfilesPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getProfilesesSize", new String[] { Integer.class.getName() });

	static {
		FINDER_PATH_GET_PROFILESES_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of profileses associated with the components.
	 *
	 * @param pk the primary key of the components
	 * @return the number of profileses associated with the components
	 * @throws SystemException if a system exception occurred
	 */
	public int getProfilesesSize(int pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_PROFILESES_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETPROFILESESSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_PROFILESES_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_PROFILES = new FinderPath(com.vmware.model.impl.ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesModelImpl.FINDER_CACHE_ENABLED,
			com.vmware.model.impl.ProfilesImpl.class,
			com.vmware.service.persistence.ProfilesPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsProfiles",
			new String[] { Integer.class.getName(), Integer.class.getName() });

	/**
	 * Returns <code>true</code> if the profiles is associated with the components.
	 *
	 * @param pk the primary key of the components
	 * @param profilesPK the primary key of the profiles
	 * @return <code>true</code> if the profiles is associated with the components; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsProfiles(int pk, int profilesPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, profilesPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_PROFILES,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsProfiles.contains(pk, profilesPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_PROFILES,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the components has any profileses associated with it.
	 *
	 * @param pk the primary key of the components to check for associations with profileses
	 * @return <code>true</code> if the components has any profileses associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsProfileses(int pk) throws SystemException {
		if (getProfilesesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Initializes the components persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Components")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Components>> listenersList = new ArrayList<ModelListener<Components>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Components>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsProfiles = new ContainsProfiles();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ComponentsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsProfiles containsProfiles;

	protected class ContainsProfiles {
		protected ContainsProfiles() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSPROFILES,
					new int[] { java.sql.Types.INTEGER, java.sql.Types.INTEGER },
					RowMapper.COUNT);
		}

		protected boolean contains(int comp_id, int userid) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Integer(comp_id), new Integer(userid)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	private static final String _SQL_SELECT_COMPONENTS = "SELECT components FROM Components components";
	private static final String _SQL_COUNT_COMPONENTS = "SELECT COUNT(components) FROM Components components";
	private static final String _SQL_GETPROFILESES = "SELECT {profiles.*} FROM profiles INNER JOIN components ON (components.id = profiles.id) WHERE (components.id = ?)";
	private static final String _SQL_GETPROFILESESSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM profiles WHERE id = ?";
	private static final String _SQL_CONTAINSPROFILES = "SELECT COUNT(*) AS COUNT_VALUE FROM profiles WHERE id = ? AND userid = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "components.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Components exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ComponentsPersistenceImpl.class);
	private static Components _nullComponents = new ComponentsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Components> toCacheModel() {
				return _nullComponentsCacheModel;
			}
		};

	private static CacheModel<Components> _nullComponentsCacheModel = new CacheModel<Components>() {
			public Components toEntityModel() {
				return _nullComponents;
			}
		};
}