/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchDuplicateException;

import com.vmware.model.Duplicate;
import com.vmware.model.impl.DuplicateImpl;
import com.vmware.model.impl.DuplicateModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the duplicate service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see DuplicatePersistence
 * @see DuplicateUtil
 * @generated
 */
public class DuplicatePersistenceImpl extends BasePersistenceImpl<Duplicate>
	implements DuplicatePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DuplicateUtil} to access the duplicate persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DuplicateImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID = new FinderPath(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateModelImpl.FINDER_CACHE_ENABLED, DuplicateImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByDuplicateByDupeofId",
			new String[] { Integer.class.getName() },
			DuplicateModelImpl.DUPE_OF_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DUPLICATEBYDUPEOFID = new FinderPath(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDuplicateByDupeofId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateModelImpl.FINDER_CACHE_ENABLED, DuplicateImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateModelImpl.FINDER_CACHE_ENABLED, DuplicateImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the duplicate in the entity cache if it is enabled.
	 *
	 * @param duplicate the duplicate
	 */
	public void cacheResult(Duplicate duplicate) {
		EntityCacheUtil.putResult(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateImpl.class, duplicate.getPrimaryKey(), duplicate);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
			new Object[] { Integer.valueOf(duplicate.getDupe_of()) }, duplicate);

		duplicate.resetOriginalValues();
	}

	/**
	 * Caches the duplicates in the entity cache if it is enabled.
	 *
	 * @param duplicates the duplicates
	 */
	public void cacheResult(List<Duplicate> duplicates) {
		for (Duplicate duplicate : duplicates) {
			if (EntityCacheUtil.getResult(
						DuplicateModelImpl.ENTITY_CACHE_ENABLED,
						DuplicateImpl.class, duplicate.getPrimaryKey()) == null) {
				cacheResult(duplicate);
			}
			else {
				duplicate.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all duplicates.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DuplicateImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DuplicateImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the duplicate.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Duplicate duplicate) {
		EntityCacheUtil.removeResult(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateImpl.class, duplicate.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(duplicate);
	}

	@Override
	public void clearCache(List<Duplicate> duplicates) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Duplicate duplicate : duplicates) {
			EntityCacheUtil.removeResult(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
				DuplicateImpl.class, duplicate.getPrimaryKey());

			clearUniqueFindersCache(duplicate);
		}
	}

	protected void clearUniqueFindersCache(Duplicate duplicate) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
			new Object[] { Integer.valueOf(duplicate.getDupe_of()) });
	}

	/**
	 * Creates a new duplicate with the primary key. Does not add the duplicate to the database.
	 *
	 * @param dupe the primary key for the new duplicate
	 * @return the new duplicate
	 */
	public Duplicate create(int dupe) {
		Duplicate duplicate = new DuplicateImpl();

		duplicate.setNew(true);
		duplicate.setPrimaryKey(dupe);

		return duplicate;
	}

	/**
	 * Removes the duplicate with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dupe the primary key of the duplicate
	 * @return the duplicate that was removed
	 * @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate remove(int dupe)
		throws NoSuchDuplicateException, SystemException {
		return remove(Integer.valueOf(dupe));
	}

	/**
	 * Removes the duplicate with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the duplicate
	 * @return the duplicate that was removed
	 * @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Duplicate remove(Serializable primaryKey)
		throws NoSuchDuplicateException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Duplicate duplicate = (Duplicate)session.get(DuplicateImpl.class,
					primaryKey);

			if (duplicate == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDuplicateException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(duplicate);
		}
		catch (NoSuchDuplicateException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Duplicate removeImpl(Duplicate duplicate)
		throws SystemException {
		duplicate = toUnwrappedModel(duplicate);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, duplicate);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(duplicate);

		return duplicate;
	}

	@Override
	public Duplicate updateImpl(com.vmware.model.Duplicate duplicate,
		boolean merge) throws SystemException {
		duplicate = toUnwrappedModel(duplicate);

		boolean isNew = duplicate.isNew();

		DuplicateModelImpl duplicateModelImpl = (DuplicateModelImpl)duplicate;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, duplicate, merge);

			duplicate.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DuplicateModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
			DuplicateImpl.class, duplicate.getPrimaryKey(), duplicate);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
				new Object[] { Integer.valueOf(duplicate.getDupe_of()) },
				duplicate);
		}
		else {
			if ((duplicateModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(duplicateModelImpl.getOriginalDupe_of())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DUPLICATEBYDUPEOFID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
					new Object[] { Integer.valueOf(duplicate.getDupe_of()) },
					duplicate);
			}
		}

		return duplicate;
	}

	protected Duplicate toUnwrappedModel(Duplicate duplicate) {
		if (duplicate instanceof DuplicateImpl) {
			return duplicate;
		}

		DuplicateImpl duplicateImpl = new DuplicateImpl();

		duplicateImpl.setNew(duplicate.isNew());
		duplicateImpl.setPrimaryKey(duplicate.getPrimaryKey());

		duplicateImpl.setDupe(duplicate.getDupe());
		duplicateImpl.setDupe_of(duplicate.getDupe_of());

		return duplicateImpl;
	}

	/**
	 * Returns the duplicate with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the duplicate
	 * @return the duplicate
	 * @throws com.liferay.portal.NoSuchModelException if a duplicate with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Duplicate findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the duplicate with the primary key or throws a {@link com.vmware.NoSuchDuplicateException} if it could not be found.
	 *
	 * @param dupe the primary key of the duplicate
	 * @return the duplicate
	 * @throws com.vmware.NoSuchDuplicateException if a duplicate with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate findByPrimaryKey(int dupe)
		throws NoSuchDuplicateException, SystemException {
		Duplicate duplicate = fetchByPrimaryKey(dupe);

		if (duplicate == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + dupe);
			}

			throw new NoSuchDuplicateException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				dupe);
		}

		return duplicate;
	}

	/**
	 * Returns the duplicate with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the duplicate
	 * @return the duplicate, or <code>null</code> if a duplicate with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Duplicate fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the duplicate with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dupe the primary key of the duplicate
	 * @return the duplicate, or <code>null</code> if a duplicate with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate fetchByPrimaryKey(int dupe) throws SystemException {
		Duplicate duplicate = (Duplicate)EntityCacheUtil.getResult(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
				DuplicateImpl.class, dupe);

		if (duplicate == _nullDuplicate) {
			return null;
		}

		if (duplicate == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				duplicate = (Duplicate)session.get(DuplicateImpl.class,
						Integer.valueOf(dupe));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (duplicate != null) {
					cacheResult(duplicate);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(DuplicateModelImpl.ENTITY_CACHE_ENABLED,
						DuplicateImpl.class, dupe, _nullDuplicate);
				}

				closeSession(session);
			}
		}

		return duplicate;
	}

	/**
	 * Returns the duplicate where dupe_of = &#63; or throws a {@link com.vmware.NoSuchDuplicateException} if it could not be found.
	 *
	 * @param dupe_of the dupe_of
	 * @return the matching duplicate
	 * @throws com.vmware.NoSuchDuplicateException if a matching duplicate could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate findByDuplicateByDupeofId(int dupe_of)
		throws NoSuchDuplicateException, SystemException {
		Duplicate duplicate = fetchByDuplicateByDupeofId(dupe_of);

		if (duplicate == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("dupe_of=");
			msg.append(dupe_of);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDuplicateException(msg.toString());
		}

		return duplicate;
	}

	/**
	 * Returns the duplicate where dupe_of = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param dupe_of the dupe_of
	 * @return the matching duplicate, or <code>null</code> if a matching duplicate could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate fetchByDuplicateByDupeofId(int dupe_of)
		throws SystemException {
		return fetchByDuplicateByDupeofId(dupe_of, true);
	}

	/**
	 * Returns the duplicate where dupe_of = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param dupe_of the dupe_of
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching duplicate, or <code>null</code> if a matching duplicate could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate fetchByDuplicateByDupeofId(int dupe_of,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { dupe_of };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
					finderArgs, this);
		}

		if (result instanceof Duplicate) {
			Duplicate duplicate = (Duplicate)result;

			if ((dupe_of != duplicate.getDupe_of())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_DUPLICATE_WHERE);

			query.append(_FINDER_COLUMN_DUPLICATEBYDUPEOFID_DUPE_OF_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dupe_of);

				List<Duplicate> list = q.list();

				result = list;

				Duplicate duplicate = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
						finderArgs, list);
				}
				else {
					duplicate = list.get(0);

					cacheResult(duplicate);

					if ((duplicate.getDupe_of() != dupe_of)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
							finderArgs, duplicate);
					}
				}

				return duplicate;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DUPLICATEBYDUPEOFID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Duplicate)result;
			}
		}
	}

	/**
	 * Returns all the duplicates.
	 *
	 * @return the duplicates
	 * @throws SystemException if a system exception occurred
	 */
	public List<Duplicate> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the duplicates.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of duplicates
	 * @param end the upper bound of the range of duplicates (not inclusive)
	 * @return the range of duplicates
	 * @throws SystemException if a system exception occurred
	 */
	public List<Duplicate> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the duplicates.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of duplicates
	 * @param end the upper bound of the range of duplicates (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of duplicates
	 * @throws SystemException if a system exception occurred
	 */
	public List<Duplicate> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Duplicate> list = (List<Duplicate>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DUPLICATE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DUPLICATE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Duplicate>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Duplicate>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the duplicate where dupe_of = &#63; from the database.
	 *
	 * @param dupe_of the dupe_of
	 * @return the duplicate that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Duplicate removeByDuplicateByDupeofId(int dupe_of)
		throws NoSuchDuplicateException, SystemException {
		Duplicate duplicate = findByDuplicateByDupeofId(dupe_of);

		return remove(duplicate);
	}

	/**
	 * Removes all the duplicates from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Duplicate duplicate : findAll()) {
			remove(duplicate);
		}
	}

	/**
	 * Returns the number of duplicates where dupe_of = &#63;.
	 *
	 * @param dupe_of the dupe_of
	 * @return the number of matching duplicates
	 * @throws SystemException if a system exception occurred
	 */
	public int countByDuplicateByDupeofId(int dupe_of)
		throws SystemException {
		Object[] finderArgs = new Object[] { dupe_of };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_DUPLICATEBYDUPEOFID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DUPLICATE_WHERE);

			query.append(_FINDER_COLUMN_DUPLICATEBYDUPEOFID_DUPE_OF_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dupe_of);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DUPLICATEBYDUPEOFID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of duplicates.
	 *
	 * @return the number of duplicates
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DUPLICATE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the duplicate persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Duplicate")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Duplicate>> listenersList = new ArrayList<ModelListener<Duplicate>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Duplicate>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DuplicateImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_DUPLICATE = "SELECT duplicate FROM Duplicate duplicate";
	private static final String _SQL_SELECT_DUPLICATE_WHERE = "SELECT duplicate FROM Duplicate duplicate WHERE ";
	private static final String _SQL_COUNT_DUPLICATE = "SELECT COUNT(duplicate) FROM Duplicate duplicate";
	private static final String _SQL_COUNT_DUPLICATE_WHERE = "SELECT COUNT(duplicate) FROM Duplicate duplicate WHERE ";
	private static final String _FINDER_COLUMN_DUPLICATEBYDUPEOFID_DUPE_OF_2 = "duplicate.dupe_of = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "duplicate.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Duplicate exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Duplicate exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DuplicatePersistenceImpl.class);
	private static Duplicate _nullDuplicate = new DuplicateImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Duplicate> toCacheModel() {
				return _nullDuplicateCacheModel;
			}
		};

	private static CacheModel<Duplicate> _nullDuplicateCacheModel = new CacheModel<Duplicate>() {
			public Duplicate toEntityModel() {
				return _nullDuplicate;
			}
		};
}