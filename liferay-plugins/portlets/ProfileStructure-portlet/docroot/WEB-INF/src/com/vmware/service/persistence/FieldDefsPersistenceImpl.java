/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchFieldDefsException;

import com.vmware.model.FieldDefs;
import com.vmware.model.impl.FieldDefsImpl;
import com.vmware.model.impl.FieldDefsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the field defs service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see FieldDefsPersistence
 * @see FieldDefsUtil
 * @generated
 */
public class FieldDefsPersistenceImpl extends BasePersistenceImpl<FieldDefs>
	implements FieldDefsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FieldDefsUtil} to access the field defs persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FieldDefsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC = new FinderPath(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsModelImpl.FINDER_CACHE_ENABLED, FieldDefsImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByFieldDefsByDesc",
			new String[] { String.class.getName() },
			FieldDefsModelImpl.DESCRIPTION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FIELDDEFSBYDESC = new FinderPath(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByFieldDefsByDesc", new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsModelImpl.FINDER_CACHE_ENABLED, FieldDefsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsModelImpl.FINDER_CACHE_ENABLED, FieldDefsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the field defs in the entity cache if it is enabled.
	 *
	 * @param fieldDefs the field defs
	 */
	public void cacheResult(FieldDefs fieldDefs) {
		EntityCacheUtil.putResult(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsImpl.class, fieldDefs.getPrimaryKey(), fieldDefs);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
			new Object[] { fieldDefs.getDescription() }, fieldDefs);

		fieldDefs.resetOriginalValues();
	}

	/**
	 * Caches the field defses in the entity cache if it is enabled.
	 *
	 * @param fieldDefses the field defses
	 */
	public void cacheResult(List<FieldDefs> fieldDefses) {
		for (FieldDefs fieldDefs : fieldDefses) {
			if (EntityCacheUtil.getResult(
						FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
						FieldDefsImpl.class, fieldDefs.getPrimaryKey()) == null) {
				cacheResult(fieldDefs);
			}
			else {
				fieldDefs.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all field defses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FieldDefsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FieldDefsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the field defs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FieldDefs fieldDefs) {
		EntityCacheUtil.removeResult(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsImpl.class, fieldDefs.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(fieldDefs);
	}

	@Override
	public void clearCache(List<FieldDefs> fieldDefses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (FieldDefs fieldDefs : fieldDefses) {
			EntityCacheUtil.removeResult(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
				FieldDefsImpl.class, fieldDefs.getPrimaryKey());

			clearUniqueFindersCache(fieldDefs);
		}
	}

	protected void clearUniqueFindersCache(FieldDefs fieldDefs) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
			new Object[] { fieldDefs.getDescription() });
	}

	/**
	 * Creates a new field defs with the primary key. Does not add the field defs to the database.
	 *
	 * @param fielddef_id the primary key for the new field defs
	 * @return the new field defs
	 */
	public FieldDefs create(int fielddef_id) {
		FieldDefs fieldDefs = new FieldDefsImpl();

		fieldDefs.setNew(true);
		fieldDefs.setPrimaryKey(fielddef_id);

		return fieldDefs;
	}

	/**
	 * Removes the field defs with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param fielddef_id the primary key of the field defs
	 * @return the field defs that was removed
	 * @throws com.vmware.NoSuchFieldDefsException if a field defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs remove(int fielddef_id)
		throws NoSuchFieldDefsException, SystemException {
		return remove(Integer.valueOf(fielddef_id));
	}

	/**
	 * Removes the field defs with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the field defs
	 * @return the field defs that was removed
	 * @throws com.vmware.NoSuchFieldDefsException if a field defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FieldDefs remove(Serializable primaryKey)
		throws NoSuchFieldDefsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			FieldDefs fieldDefs = (FieldDefs)session.get(FieldDefsImpl.class,
					primaryKey);

			if (fieldDefs == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFieldDefsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(fieldDefs);
		}
		catch (NoSuchFieldDefsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FieldDefs removeImpl(FieldDefs fieldDefs)
		throws SystemException {
		fieldDefs = toUnwrappedModel(fieldDefs);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, fieldDefs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(fieldDefs);

		return fieldDefs;
	}

	@Override
	public FieldDefs updateImpl(com.vmware.model.FieldDefs fieldDefs,
		boolean merge) throws SystemException {
		fieldDefs = toUnwrappedModel(fieldDefs);

		boolean isNew = fieldDefs.isNew();

		FieldDefsModelImpl fieldDefsModelImpl = (FieldDefsModelImpl)fieldDefs;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, fieldDefs, merge);

			fieldDefs.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !FieldDefsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
			FieldDefsImpl.class, fieldDefs.getPrimaryKey(), fieldDefs);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
				new Object[] { fieldDefs.getDescription() }, fieldDefs);
		}
		else {
			if ((fieldDefsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						fieldDefsModelImpl.getOriginalDescription()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FIELDDEFSBYDESC,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
					new Object[] { fieldDefs.getDescription() }, fieldDefs);
			}
		}

		return fieldDefs;
	}

	protected FieldDefs toUnwrappedModel(FieldDefs fieldDefs) {
		if (fieldDefs instanceof FieldDefsImpl) {
			return fieldDefs;
		}

		FieldDefsImpl fieldDefsImpl = new FieldDefsImpl();

		fieldDefsImpl.setNew(fieldDefs.isNew());
		fieldDefsImpl.setPrimaryKey(fieldDefs.getPrimaryKey());

		fieldDefsImpl.setFielddef_id(fieldDefs.getFielddef_id());
		fieldDefsImpl.setName(fieldDefs.getName());
		fieldDefsImpl.setType(fieldDefs.getType());
		fieldDefsImpl.setCustom(fieldDefs.getCustom());
		fieldDefsImpl.setDescription(fieldDefs.getDescription());
		fieldDefsImpl.setMailhead(fieldDefs.getMailhead());
		fieldDefsImpl.setSortkey(fieldDefs.getSortkey());
		fieldDefsImpl.setObsolete(fieldDefs.getObsolete());
		fieldDefsImpl.setEnter_bug(fieldDefs.getEnter_bug());

		return fieldDefsImpl;
	}

	/**
	 * Returns the field defs with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the field defs
	 * @return the field defs
	 * @throws com.liferay.portal.NoSuchModelException if a field defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FieldDefs findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the field defs with the primary key or throws a {@link com.vmware.NoSuchFieldDefsException} if it could not be found.
	 *
	 * @param fielddef_id the primary key of the field defs
	 * @return the field defs
	 * @throws com.vmware.NoSuchFieldDefsException if a field defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs findByPrimaryKey(int fielddef_id)
		throws NoSuchFieldDefsException, SystemException {
		FieldDefs fieldDefs = fetchByPrimaryKey(fielddef_id);

		if (fieldDefs == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + fielddef_id);
			}

			throw new NoSuchFieldDefsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				fielddef_id);
		}

		return fieldDefs;
	}

	/**
	 * Returns the field defs with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the field defs
	 * @return the field defs, or <code>null</code> if a field defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FieldDefs fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the field defs with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param fielddef_id the primary key of the field defs
	 * @return the field defs, or <code>null</code> if a field defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs fetchByPrimaryKey(int fielddef_id)
		throws SystemException {
		FieldDefs fieldDefs = (FieldDefs)EntityCacheUtil.getResult(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
				FieldDefsImpl.class, fielddef_id);

		if (fieldDefs == _nullFieldDefs) {
			return null;
		}

		if (fieldDefs == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				fieldDefs = (FieldDefs)session.get(FieldDefsImpl.class,
						Integer.valueOf(fielddef_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (fieldDefs != null) {
					cacheResult(fieldDefs);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(FieldDefsModelImpl.ENTITY_CACHE_ENABLED,
						FieldDefsImpl.class, fielddef_id, _nullFieldDefs);
				}

				closeSession(session);
			}
		}

		return fieldDefs;
	}

	/**
	 * Returns the field defs where description = &#63; or throws a {@link com.vmware.NoSuchFieldDefsException} if it could not be found.
	 *
	 * @param description the description
	 * @return the matching field defs
	 * @throws com.vmware.NoSuchFieldDefsException if a matching field defs could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs findByFieldDefsByDesc(String description)
		throws NoSuchFieldDefsException, SystemException {
		FieldDefs fieldDefs = fetchByFieldDefsByDesc(description);

		if (fieldDefs == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("description=");
			msg.append(description);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchFieldDefsException(msg.toString());
		}

		return fieldDefs;
	}

	/**
	 * Returns the field defs where description = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param description the description
	 * @return the matching field defs, or <code>null</code> if a matching field defs could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs fetchByFieldDefsByDesc(String description)
		throws SystemException {
		return fetchByFieldDefsByDesc(description, true);
	}

	/**
	 * Returns the field defs where description = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param description the description
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching field defs, or <code>null</code> if a matching field defs could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs fetchByFieldDefsByDesc(String description,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { description };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
					finderArgs, this);
		}

		if (result instanceof FieldDefs) {
			FieldDefs fieldDefs = (FieldDefs)result;

			if (!Validator.equals(description, fieldDefs.getDescription())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_FIELDDEFS_WHERE);

			if (description == null) {
				query.append(_FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_1);
			}
			else {
				if (description.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_3);
				}
				else {
					query.append(_FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (description != null) {
					qPos.add(description);
				}

				List<FieldDefs> list = q.list();

				result = list;

				FieldDefs fieldDefs = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
						finderArgs, list);
				}
				else {
					fieldDefs = list.get(0);

					cacheResult(fieldDefs);

					if ((fieldDefs.getDescription() == null) ||
							!fieldDefs.getDescription().equals(description)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
							finderArgs, fieldDefs);
					}
				}

				return fieldDefs;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYDESC,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (FieldDefs)result;
			}
		}
	}

	/**
	 * Returns all the field defses.
	 *
	 * @return the field defses
	 * @throws SystemException if a system exception occurred
	 */
	public List<FieldDefs> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the field defses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of field defses
	 * @param end the upper bound of the range of field defses (not inclusive)
	 * @return the range of field defses
	 * @throws SystemException if a system exception occurred
	 */
	public List<FieldDefs> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the field defses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of field defses
	 * @param end the upper bound of the range of field defses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of field defses
	 * @throws SystemException if a system exception occurred
	 */
	public List<FieldDefs> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<FieldDefs> list = (List<FieldDefs>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FIELDDEFS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FIELDDEFS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<FieldDefs>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<FieldDefs>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the field defs where description = &#63; from the database.
	 *
	 * @param description the description
	 * @return the field defs that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public FieldDefs removeByFieldDefsByDesc(String description)
		throws NoSuchFieldDefsException, SystemException {
		FieldDefs fieldDefs = findByFieldDefsByDesc(description);

		return remove(fieldDefs);
	}

	/**
	 * Removes all the field defses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (FieldDefs fieldDefs : findAll()) {
			remove(fieldDefs);
		}
	}

	/**
	 * Returns the number of field defses where description = &#63;.
	 *
	 * @param description the description
	 * @return the number of matching field defses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByFieldDefsByDesc(String description)
		throws SystemException {
		Object[] finderArgs = new Object[] { description };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_FIELDDEFSBYDESC,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_FIELDDEFS_WHERE);

			if (description == null) {
				query.append(_FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_1);
			}
			else {
				if (description.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_3);
				}
				else {
					query.append(_FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (description != null) {
					qPos.add(description);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FIELDDEFSBYDESC,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of field defses.
	 *
	 * @return the number of field defses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FIELDDEFS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the field defs persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.FieldDefs")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<FieldDefs>> listenersList = new ArrayList<ModelListener<FieldDefs>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<FieldDefs>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FieldDefsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_FIELDDEFS = "SELECT fieldDefs FROM FieldDefs fieldDefs";
	private static final String _SQL_SELECT_FIELDDEFS_WHERE = "SELECT fieldDefs FROM FieldDefs fieldDefs WHERE ";
	private static final String _SQL_COUNT_FIELDDEFS = "SELECT COUNT(fieldDefs) FROM FieldDefs fieldDefs";
	private static final String _SQL_COUNT_FIELDDEFS_WHERE = "SELECT COUNT(fieldDefs) FROM FieldDefs fieldDefs WHERE ";
	private static final String _FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_1 = "fieldDefs.description IS NULL";
	private static final String _FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_2 = "fieldDefs.description = ?";
	private static final String _FINDER_COLUMN_FIELDDEFSBYDESC_DESCRIPTION_3 = "(fieldDefs.description IS NULL OR fieldDefs.description = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "fieldDefs.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No FieldDefs exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No FieldDefs exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FieldDefsPersistenceImpl.class);
	private static FieldDefs _nullFieldDefs = new FieldDefsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<FieldDefs> toCacheModel() {
				return _nullFieldDefsCacheModel;
			}
		};

	private static CacheModel<FieldDefs> _nullFieldDefsCacheModel = new CacheModel<FieldDefs>() {
			public FieldDefs toEntityModel() {
				return _nullFieldDefs;
			}
		};
}