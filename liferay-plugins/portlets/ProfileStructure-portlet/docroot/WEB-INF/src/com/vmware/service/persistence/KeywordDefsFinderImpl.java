package com.vmware.service.persistence;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.vmware.model.KeywordDefs;
import com.vmware.model.impl.KeywordDefsImpl;

public class KeywordDefsFinderImpl extends BasePersistenceImpl<KeywordDefs> implements KeywordDefsFinder{
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<KeywordDefs> getKeywordDefByBugId(int bug_id) throws SystemException{
		
		Session session = null;
		
		try{
			session = openSession();		
			
			String base = "SELECT bugzilla.keyworddefs.* FROM bugzilla.keyworddefs " +
            "JOIN bugzilla.keywords " +
            "ON bugzilla.keyworddefs.id = bugzilla.keywords.keywordid ";
	       
			StringBuffer sqlBuilder = new StringBuffer(base);
			List<String> criteria = new ArrayList<String>();
			
			if (bug_id > 0) {
				criteria.add("bugzilla.keywords.bug_id = ?");
            }	        
			
			if (!criteria.isEmpty()) {
	            sqlBuilder.append("WHERE ");
	            sqlBuilder.append(criteria.remove(0) + " ");
	            for (String criterion : criteria) {
	                sqlBuilder.append(criterion);
	            }
	        }
			
			String sql = sqlBuilder.toString();
			
			SQLQuery q = session.createSQLQuery(sql);
	        q.setCacheable(false);
	        q.addEntity("bugzilla.keyworddefs", KeywordDefsImpl.class);	
	        
	        QueryPos qPos = QueryPos.getInstance(q);
            // Setting the positions
	        if (bug_id > 0) {
                qPos.add(bug_id);
            }
	        
	        //execute the query and return a list from the db
	        return (List<KeywordDefs>) QueryUtil.list(q, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		}catch(Exception e){
			throw new SystemException(e);
		}
		finally {           
            closeSession(session);
        }
	}
}
