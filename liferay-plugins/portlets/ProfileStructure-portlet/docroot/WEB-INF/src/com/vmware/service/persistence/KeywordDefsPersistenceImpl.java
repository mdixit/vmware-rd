/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchKeywordDefsException;

import com.vmware.model.KeywordDefs;
import com.vmware.model.impl.KeywordDefsImpl;
import com.vmware.model.impl.KeywordDefsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the keyword defs service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see KeywordDefsPersistence
 * @see KeywordDefsUtil
 * @generated
 */
public class KeywordDefsPersistenceImpl extends BasePersistenceImpl<KeywordDefs>
	implements KeywordDefsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link KeywordDefsUtil} to access the keyword defs persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = KeywordDefsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
			KeywordDefsModelImpl.FINDER_CACHE_ENABLED, KeywordDefsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
			KeywordDefsModelImpl.FINDER_CACHE_ENABLED, KeywordDefsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
			KeywordDefsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the keyword defs in the entity cache if it is enabled.
	 *
	 * @param keywordDefs the keyword defs
	 */
	public void cacheResult(KeywordDefs keywordDefs) {
		EntityCacheUtil.putResult(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
			KeywordDefsImpl.class, keywordDefs.getPrimaryKey(), keywordDefs);

		keywordDefs.resetOriginalValues();
	}

	/**
	 * Caches the keyword defses in the entity cache if it is enabled.
	 *
	 * @param keywordDefses the keyword defses
	 */
	public void cacheResult(List<KeywordDefs> keywordDefses) {
		for (KeywordDefs keywordDefs : keywordDefses) {
			if (EntityCacheUtil.getResult(
						KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
						KeywordDefsImpl.class, keywordDefs.getPrimaryKey()) == null) {
				cacheResult(keywordDefs);
			}
			else {
				keywordDefs.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all keyword defses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(KeywordDefsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(KeywordDefsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the keyword defs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(KeywordDefs keywordDefs) {
		EntityCacheUtil.removeResult(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
			KeywordDefsImpl.class, keywordDefs.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<KeywordDefs> keywordDefses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (KeywordDefs keywordDefs : keywordDefses) {
			EntityCacheUtil.removeResult(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
				KeywordDefsImpl.class, keywordDefs.getPrimaryKey());
		}
	}

	/**
	 * Creates a new keyword defs with the primary key. Does not add the keyword defs to the database.
	 *
	 * @param key_def_id the primary key for the new keyword defs
	 * @return the new keyword defs
	 */
	public KeywordDefs create(int key_def_id) {
		KeywordDefs keywordDefs = new KeywordDefsImpl();

		keywordDefs.setNew(true);
		keywordDefs.setPrimaryKey(key_def_id);

		return keywordDefs;
	}

	/**
	 * Removes the keyword defs with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param key_def_id the primary key of the keyword defs
	 * @return the keyword defs that was removed
	 * @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public KeywordDefs remove(int key_def_id)
		throws NoSuchKeywordDefsException, SystemException {
		return remove(Integer.valueOf(key_def_id));
	}

	/**
	 * Removes the keyword defs with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the keyword defs
	 * @return the keyword defs that was removed
	 * @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public KeywordDefs remove(Serializable primaryKey)
		throws NoSuchKeywordDefsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			KeywordDefs keywordDefs = (KeywordDefs)session.get(KeywordDefsImpl.class,
					primaryKey);

			if (keywordDefs == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchKeywordDefsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(keywordDefs);
		}
		catch (NoSuchKeywordDefsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected KeywordDefs removeImpl(KeywordDefs keywordDefs)
		throws SystemException {
		keywordDefs = toUnwrappedModel(keywordDefs);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, keywordDefs);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(keywordDefs);

		return keywordDefs;
	}

	@Override
	public KeywordDefs updateImpl(com.vmware.model.KeywordDefs keywordDefs,
		boolean merge) throws SystemException {
		keywordDefs = toUnwrappedModel(keywordDefs);

		boolean isNew = keywordDefs.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, keywordDefs, merge);

			keywordDefs.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
			KeywordDefsImpl.class, keywordDefs.getPrimaryKey(), keywordDefs);

		return keywordDefs;
	}

	protected KeywordDefs toUnwrappedModel(KeywordDefs keywordDefs) {
		if (keywordDefs instanceof KeywordDefsImpl) {
			return keywordDefs;
		}

		KeywordDefsImpl keywordDefsImpl = new KeywordDefsImpl();

		keywordDefsImpl.setNew(keywordDefs.isNew());
		keywordDefsImpl.setPrimaryKey(keywordDefs.getPrimaryKey());

		keywordDefsImpl.setKey_def_id(keywordDefs.getKey_def_id());
		keywordDefsImpl.setName(keywordDefs.getName());
		keywordDefsImpl.setDescription(keywordDefs.getDescription());
		keywordDefsImpl.setDisallownew(keywordDefs.getDisallownew());

		return keywordDefsImpl;
	}

	/**
	 * Returns the keyword defs with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the keyword defs
	 * @return the keyword defs
	 * @throws com.liferay.portal.NoSuchModelException if a keyword defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public KeywordDefs findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the keyword defs with the primary key or throws a {@link com.vmware.NoSuchKeywordDefsException} if it could not be found.
	 *
	 * @param key_def_id the primary key of the keyword defs
	 * @return the keyword defs
	 * @throws com.vmware.NoSuchKeywordDefsException if a keyword defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public KeywordDefs findByPrimaryKey(int key_def_id)
		throws NoSuchKeywordDefsException, SystemException {
		KeywordDefs keywordDefs = fetchByPrimaryKey(key_def_id);

		if (keywordDefs == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + key_def_id);
			}

			throw new NoSuchKeywordDefsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				key_def_id);
		}

		return keywordDefs;
	}

	/**
	 * Returns the keyword defs with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the keyword defs
	 * @return the keyword defs, or <code>null</code> if a keyword defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public KeywordDefs fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the keyword defs with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param key_def_id the primary key of the keyword defs
	 * @return the keyword defs, or <code>null</code> if a keyword defs with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public KeywordDefs fetchByPrimaryKey(int key_def_id)
		throws SystemException {
		KeywordDefs keywordDefs = (KeywordDefs)EntityCacheUtil.getResult(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
				KeywordDefsImpl.class, key_def_id);

		if (keywordDefs == _nullKeywordDefs) {
			return null;
		}

		if (keywordDefs == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				keywordDefs = (KeywordDefs)session.get(KeywordDefsImpl.class,
						Integer.valueOf(key_def_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (keywordDefs != null) {
					cacheResult(keywordDefs);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(KeywordDefsModelImpl.ENTITY_CACHE_ENABLED,
						KeywordDefsImpl.class, key_def_id, _nullKeywordDefs);
				}

				closeSession(session);
			}
		}

		return keywordDefs;
	}

	/**
	 * Returns all the keyword defses.
	 *
	 * @return the keyword defses
	 * @throws SystemException if a system exception occurred
	 */
	public List<KeywordDefs> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the keyword defses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of keyword defses
	 * @param end the upper bound of the range of keyword defses (not inclusive)
	 * @return the range of keyword defses
	 * @throws SystemException if a system exception occurred
	 */
	public List<KeywordDefs> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the keyword defses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of keyword defses
	 * @param end the upper bound of the range of keyword defses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of keyword defses
	 * @throws SystemException if a system exception occurred
	 */
	public List<KeywordDefs> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<KeywordDefs> list = (List<KeywordDefs>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_KEYWORDDEFS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_KEYWORDDEFS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<KeywordDefs>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<KeywordDefs>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the keyword defses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (KeywordDefs keywordDefs : findAll()) {
			remove(keywordDefs);
		}
	}

	/**
	 * Returns the number of keyword defses.
	 *
	 * @return the number of keyword defses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_KEYWORDDEFS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the keyword defs persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.KeywordDefs")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<KeywordDefs>> listenersList = new ArrayList<ModelListener<KeywordDefs>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<KeywordDefs>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(KeywordDefsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_KEYWORDDEFS = "SELECT keywordDefs FROM KeywordDefs keywordDefs";
	private static final String _SQL_COUNT_KEYWORDDEFS = "SELECT COUNT(keywordDefs) FROM KeywordDefs keywordDefs";
	private static final String _ORDER_BY_ENTITY_ALIAS = "keywordDefs.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No KeywordDefs exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(KeywordDefsPersistenceImpl.class);
	private static KeywordDefs _nullKeywordDefs = new KeywordDefsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<KeywordDefs> toCacheModel() {
				return _nullKeywordDefsCacheModel;
			}
		};

	private static CacheModel<KeywordDefs> _nullKeywordDefsCacheModel = new CacheModel<KeywordDefs>() {
			public KeywordDefs toEntityModel() {
				return _nullKeywordDefs;
			}
		};
}