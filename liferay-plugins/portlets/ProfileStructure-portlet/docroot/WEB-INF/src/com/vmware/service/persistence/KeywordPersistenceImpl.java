/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchKeywordException;

import com.vmware.model.Keyword;
import com.vmware.model.impl.KeywordImpl;
import com.vmware.model.impl.KeywordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the keyword service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see KeywordPersistence
 * @see KeywordUtil
 * @generated
 */
public class KeywordPersistenceImpl extends BasePersistenceImpl<Keyword>
	implements KeywordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link KeywordUtil} to access the keyword persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = KeywordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(KeywordModelImpl.ENTITY_CACHE_ENABLED,
			KeywordModelImpl.FINDER_CACHE_ENABLED, KeywordImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(KeywordModelImpl.ENTITY_CACHE_ENABLED,
			KeywordModelImpl.FINDER_CACHE_ENABLED, KeywordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(KeywordModelImpl.ENTITY_CACHE_ENABLED,
			KeywordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the keyword in the entity cache if it is enabled.
	 *
	 * @param keyword the keyword
	 */
	public void cacheResult(Keyword keyword) {
		EntityCacheUtil.putResult(KeywordModelImpl.ENTITY_CACHE_ENABLED,
			KeywordImpl.class, keyword.getPrimaryKey(), keyword);

		keyword.resetOriginalValues();
	}

	/**
	 * Caches the keywords in the entity cache if it is enabled.
	 *
	 * @param keywords the keywords
	 */
	public void cacheResult(List<Keyword> keywords) {
		for (Keyword keyword : keywords) {
			if (EntityCacheUtil.getResult(
						KeywordModelImpl.ENTITY_CACHE_ENABLED,
						KeywordImpl.class, keyword.getPrimaryKey()) == null) {
				cacheResult(keyword);
			}
			else {
				keyword.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all keywords.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(KeywordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(KeywordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the keyword.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Keyword keyword) {
		EntityCacheUtil.removeResult(KeywordModelImpl.ENTITY_CACHE_ENABLED,
			KeywordImpl.class, keyword.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Keyword> keywords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Keyword keyword : keywords) {
			EntityCacheUtil.removeResult(KeywordModelImpl.ENTITY_CACHE_ENABLED,
				KeywordImpl.class, keyword.getPrimaryKey());
		}
	}

	/**
	 * Creates a new keyword with the primary key. Does not add the keyword to the database.
	 *
	 * @param keywordPK the primary key for the new keyword
	 * @return the new keyword
	 */
	public Keyword create(KeywordPK keywordPK) {
		Keyword keyword = new KeywordImpl();

		keyword.setNew(true);
		keyword.setPrimaryKey(keywordPK);

		return keyword;
	}

	/**
	 * Removes the keyword with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param keywordPK the primary key of the keyword
	 * @return the keyword that was removed
	 * @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Keyword remove(KeywordPK keywordPK)
		throws NoSuchKeywordException, SystemException {
		return remove((Serializable)keywordPK);
	}

	/**
	 * Removes the keyword with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the keyword
	 * @return the keyword that was removed
	 * @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Keyword remove(Serializable primaryKey)
		throws NoSuchKeywordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Keyword keyword = (Keyword)session.get(KeywordImpl.class, primaryKey);

			if (keyword == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchKeywordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(keyword);
		}
		catch (NoSuchKeywordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Keyword removeImpl(Keyword keyword) throws SystemException {
		keyword = toUnwrappedModel(keyword);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, keyword);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(keyword);

		return keyword;
	}

	@Override
	public Keyword updateImpl(com.vmware.model.Keyword keyword, boolean merge)
		throws SystemException {
		keyword = toUnwrappedModel(keyword);

		boolean isNew = keyword.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, keyword, merge);

			keyword.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(KeywordModelImpl.ENTITY_CACHE_ENABLED,
			KeywordImpl.class, keyword.getPrimaryKey(), keyword);

		return keyword;
	}

	protected Keyword toUnwrappedModel(Keyword keyword) {
		if (keyword instanceof KeywordImpl) {
			return keyword;
		}

		KeywordImpl keywordImpl = new KeywordImpl();

		keywordImpl.setNew(keyword.isNew());
		keywordImpl.setPrimaryKey(keyword.getPrimaryKey());

		keywordImpl.setBug_id(keyword.getBug_id());
		keywordImpl.setKeywordid(keyword.getKeywordid());

		return keywordImpl;
	}

	/**
	 * Returns the keyword with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the keyword
	 * @return the keyword
	 * @throws com.liferay.portal.NoSuchModelException if a keyword with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Keyword findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey((KeywordPK)primaryKey);
	}

	/**
	 * Returns the keyword with the primary key or throws a {@link com.vmware.NoSuchKeywordException} if it could not be found.
	 *
	 * @param keywordPK the primary key of the keyword
	 * @return the keyword
	 * @throws com.vmware.NoSuchKeywordException if a keyword with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Keyword findByPrimaryKey(KeywordPK keywordPK)
		throws NoSuchKeywordException, SystemException {
		Keyword keyword = fetchByPrimaryKey(keywordPK);

		if (keyword == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + keywordPK);
			}

			throw new NoSuchKeywordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				keywordPK);
		}

		return keyword;
	}

	/**
	 * Returns the keyword with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the keyword
	 * @return the keyword, or <code>null</code> if a keyword with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Keyword fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey((KeywordPK)primaryKey);
	}

	/**
	 * Returns the keyword with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param keywordPK the primary key of the keyword
	 * @return the keyword, or <code>null</code> if a keyword with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Keyword fetchByPrimaryKey(KeywordPK keywordPK)
		throws SystemException {
		Keyword keyword = (Keyword)EntityCacheUtil.getResult(KeywordModelImpl.ENTITY_CACHE_ENABLED,
				KeywordImpl.class, keywordPK);

		if (keyword == _nullKeyword) {
			return null;
		}

		if (keyword == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				keyword = (Keyword)session.get(KeywordImpl.class, keywordPK);
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (keyword != null) {
					cacheResult(keyword);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(KeywordModelImpl.ENTITY_CACHE_ENABLED,
						KeywordImpl.class, keywordPK, _nullKeyword);
				}

				closeSession(session);
			}
		}

		return keyword;
	}

	/**
	 * Returns all the keywords.
	 *
	 * @return the keywords
	 * @throws SystemException if a system exception occurred
	 */
	public List<Keyword> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the keywords.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of keywords
	 * @param end the upper bound of the range of keywords (not inclusive)
	 * @return the range of keywords
	 * @throws SystemException if a system exception occurred
	 */
	public List<Keyword> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the keywords.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of keywords
	 * @param end the upper bound of the range of keywords (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of keywords
	 * @throws SystemException if a system exception occurred
	 */
	public List<Keyword> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Keyword> list = (List<Keyword>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_KEYWORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_KEYWORD;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Keyword>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Keyword>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the keywords from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Keyword keyword : findAll()) {
			remove(keyword);
		}
	}

	/**
	 * Returns the number of keywords.
	 *
	 * @return the number of keywords
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_KEYWORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the keyword persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Keyword")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Keyword>> listenersList = new ArrayList<ModelListener<Keyword>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Keyword>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(KeywordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_KEYWORD = "SELECT keyword FROM Keyword keyword";
	private static final String _SQL_COUNT_KEYWORD = "SELECT COUNT(keyword) FROM Keyword keyword";
	private static final String _ORDER_BY_ENTITY_ALIAS = "keyword.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Keyword exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(KeywordPersistenceImpl.class);
	private static Keyword _nullKeyword = new KeywordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Keyword> toCacheModel() {
				return _nullKeywordCacheModel;
			}
		};

	private static CacheModel<Keyword> _nullKeywordCacheModel = new CacheModel<Keyword>() {
			public Keyword toEntityModel() {
				return _nullKeyword;
			}
		};
}