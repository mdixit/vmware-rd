/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchLongDescriptionException;

import com.vmware.model.LongDescription;
import com.vmware.model.impl.LongDescriptionImpl;
import com.vmware.model.impl.LongDescriptionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the long description service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see LongDescriptionPersistence
 * @see LongDescriptionUtil
 * @generated
 */
public class LongDescriptionPersistenceImpl extends BasePersistenceImpl<LongDescription>
	implements LongDescriptionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LongDescriptionUtil} to access the long description persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LongDescriptionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID =
		new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED,
			LongDescriptionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByLongDescriptionByUserId",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID =
		new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED,
			LongDescriptionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByLongDescriptionByUserId",
			new String[] { Integer.class.getName() },
			LongDescriptionModelImpl.WHO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYUSERID = new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByLongDescriptionByUserId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID =
		new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED,
			LongDescriptionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByLongDescriptionByBugId",
			new String[] {
				Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID =
		new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED,
			LongDescriptionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByLongDescriptionByBugId",
			new String[] { Integer.class.getName() },
			LongDescriptionModelImpl.BUG_ID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYBUGID = new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByLongDescriptionByBugId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED,
			LongDescriptionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED,
			LongDescriptionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the long description in the entity cache if it is enabled.
	 *
	 * @param longDescription the long description
	 */
	public void cacheResult(LongDescription longDescription) {
		EntityCacheUtil.putResult(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionImpl.class, longDescription.getPrimaryKey(),
			longDescription);

		longDescription.resetOriginalValues();
	}

	/**
	 * Caches the long descriptions in the entity cache if it is enabled.
	 *
	 * @param longDescriptions the long descriptions
	 */
	public void cacheResult(List<LongDescription> longDescriptions) {
		for (LongDescription longDescription : longDescriptions) {
			if (EntityCacheUtil.getResult(
						LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
						LongDescriptionImpl.class,
						longDescription.getPrimaryKey()) == null) {
				cacheResult(longDescription);
			}
			else {
				longDescription.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all long descriptions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LongDescriptionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LongDescriptionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the long description.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LongDescription longDescription) {
		EntityCacheUtil.removeResult(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionImpl.class, longDescription.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LongDescription> longDescriptions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LongDescription longDescription : longDescriptions) {
			EntityCacheUtil.removeResult(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
				LongDescriptionImpl.class, longDescription.getPrimaryKey());
		}
	}

	/**
	 * Creates a new long description with the primary key. Does not add the long description to the database.
	 *
	 * @param comment_id the primary key for the new long description
	 * @return the new long description
	 */
	public LongDescription create(int comment_id) {
		LongDescription longDescription = new LongDescriptionImpl();

		longDescription.setNew(true);
		longDescription.setPrimaryKey(comment_id);

		return longDescription;
	}

	/**
	 * Removes the long description with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param comment_id the primary key of the long description
	 * @return the long description that was removed
	 * @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription remove(int comment_id)
		throws NoSuchLongDescriptionException, SystemException {
		return remove(Integer.valueOf(comment_id));
	}

	/**
	 * Removes the long description with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the long description
	 * @return the long description that was removed
	 * @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LongDescription remove(Serializable primaryKey)
		throws NoSuchLongDescriptionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LongDescription longDescription = (LongDescription)session.get(LongDescriptionImpl.class,
					primaryKey);

			if (longDescription == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLongDescriptionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(longDescription);
		}
		catch (NoSuchLongDescriptionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LongDescription removeImpl(LongDescription longDescription)
		throws SystemException {
		longDescription = toUnwrappedModel(longDescription);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, longDescription);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(longDescription);

		return longDescription;
	}

	@Override
	public LongDescription updateImpl(
		com.vmware.model.LongDescription longDescription, boolean merge)
		throws SystemException {
		longDescription = toUnwrappedModel(longDescription);

		boolean isNew = longDescription.isNew();

		LongDescriptionModelImpl longDescriptionModelImpl = (LongDescriptionModelImpl)longDescription;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, longDescription, merge);

			longDescription.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LongDescriptionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((longDescriptionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(longDescriptionModelImpl.getOriginalWho())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID,
					args);

				args = new Object[] {
						Integer.valueOf(longDescriptionModelImpl.getWho())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID,
					args);
			}

			if ((longDescriptionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(longDescriptionModelImpl.getOriginalBug_id())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYBUGID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID,
					args);

				args = new Object[] {
						Integer.valueOf(longDescriptionModelImpl.getBug_id())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYBUGID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID,
					args);
			}
		}

		EntityCacheUtil.putResult(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
			LongDescriptionImpl.class, longDescription.getPrimaryKey(),
			longDescription);

		return longDescription;
	}

	protected LongDescription toUnwrappedModel(LongDescription longDescription) {
		if (longDescription instanceof LongDescriptionImpl) {
			return longDescription;
		}

		LongDescriptionImpl longDescriptionImpl = new LongDescriptionImpl();

		longDescriptionImpl.setNew(longDescription.isNew());
		longDescriptionImpl.setPrimaryKey(longDescription.getPrimaryKey());

		longDescriptionImpl.setComment_id(longDescription.getComment_id());
		longDescriptionImpl.setBug_id(longDescription.getBug_id());
		longDescriptionImpl.setWho(longDescription.getWho());
		longDescriptionImpl.setBug_when(longDescription.getBug_when());
		longDescriptionImpl.setWork_time(longDescription.getWork_time());
		longDescriptionImpl.setThetext(longDescription.getThetext());
		longDescriptionImpl.setIsprivate(longDescription.getIsprivate());
		longDescriptionImpl.setAlready_wrapped(longDescription.getAlready_wrapped());
		longDescriptionImpl.setType(longDescription.getType());
		longDescriptionImpl.setExtra_data(longDescription.getExtra_data());

		return longDescriptionImpl;
	}

	/**
	 * Returns the long description with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the long description
	 * @return the long description
	 * @throws com.liferay.portal.NoSuchModelException if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LongDescription findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the long description with the primary key or throws a {@link com.vmware.NoSuchLongDescriptionException} if it could not be found.
	 *
	 * @param comment_id the primary key of the long description
	 * @return the long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription findByPrimaryKey(int comment_id)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = fetchByPrimaryKey(comment_id);

		if (longDescription == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + comment_id);
			}

			throw new NoSuchLongDescriptionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				comment_id);
		}

		return longDescription;
	}

	/**
	 * Returns the long description with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the long description
	 * @return the long description, or <code>null</code> if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LongDescription fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the long description with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param comment_id the primary key of the long description
	 * @return the long description, or <code>null</code> if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription fetchByPrimaryKey(int comment_id)
		throws SystemException {
		LongDescription longDescription = (LongDescription)EntityCacheUtil.getResult(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
				LongDescriptionImpl.class, comment_id);

		if (longDescription == _nullLongDescription) {
			return null;
		}

		if (longDescription == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				longDescription = (LongDescription)session.get(LongDescriptionImpl.class,
						Integer.valueOf(comment_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (longDescription != null) {
					cacheResult(longDescription);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LongDescriptionModelImpl.ENTITY_CACHE_ENABLED,
						LongDescriptionImpl.class, comment_id,
						_nullLongDescription);
				}

				closeSession(session);
			}
		}

		return longDescription;
	}

	/**
	 * Returns all the long descriptions where who = &#63;.
	 *
	 * @param who the who
	 * @return the matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findByLongDescriptionByUserId(int who)
		throws SystemException {
		return findByLongDescriptionByUserId(who, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the long descriptions where who = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param who the who
	 * @param start the lower bound of the range of long descriptions
	 * @param end the upper bound of the range of long descriptions (not inclusive)
	 * @return the range of matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findByLongDescriptionByUserId(int who,
		int start, int end) throws SystemException {
		return findByLongDescriptionByUserId(who, start, end, null);
	}

	/**
	 * Returns an ordered range of all the long descriptions where who = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param who the who
	 * @param start the lower bound of the range of long descriptions
	 * @param end the upper bound of the range of long descriptions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findByLongDescriptionByUserId(int who,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID;
			finderArgs = new Object[] { who };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LONGDESCRIPTIONBYUSERID;
			finderArgs = new Object[] { who, start, end, orderByComparator };
		}

		List<LongDescription> list = (List<LongDescription>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LongDescription longDescription : list) {
				if ((who != longDescription.getWho())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LONGDESCRIPTION_WHERE);

			query.append(_FINDER_COLUMN_LONGDESCRIPTIONBYUSERID_WHO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(LongDescriptionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				list = (List<LongDescription>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first long description in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription findByLongDescriptionByUserId_First(int who,
		OrderByComparator orderByComparator)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = fetchByLongDescriptionByUserId_First(who,
				orderByComparator);

		if (longDescription != null) {
			return longDescription;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("who=");
		msg.append(who);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLongDescriptionException(msg.toString());
	}

	/**
	 * Returns the first long description in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching long description, or <code>null</code> if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription fetchByLongDescriptionByUserId_First(int who,
		OrderByComparator orderByComparator) throws SystemException {
		List<LongDescription> list = findByLongDescriptionByUserId(who, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last long description in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription findByLongDescriptionByUserId_Last(int who,
		OrderByComparator orderByComparator)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = fetchByLongDescriptionByUserId_Last(who,
				orderByComparator);

		if (longDescription != null) {
			return longDescription;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("who=");
		msg.append(who);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLongDescriptionException(msg.toString());
	}

	/**
	 * Returns the last long description in the ordered set where who = &#63;.
	 *
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching long description, or <code>null</code> if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription fetchByLongDescriptionByUserId_Last(int who,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLongDescriptionByUserId(who);

		List<LongDescription> list = findByLongDescriptionByUserId(who,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the long descriptions before and after the current long description in the ordered set where who = &#63;.
	 *
	 * @param comment_id the primary key of the current long description
	 * @param who the who
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription[] findByLongDescriptionByUserId_PrevAndNext(
		int comment_id, int who, OrderByComparator orderByComparator)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = findByPrimaryKey(comment_id);

		Session session = null;

		try {
			session = openSession();

			LongDescription[] array = new LongDescriptionImpl[3];

			array[0] = getByLongDescriptionByUserId_PrevAndNext(session,
					longDescription, who, orderByComparator, true);

			array[1] = longDescription;

			array[2] = getByLongDescriptionByUserId_PrevAndNext(session,
					longDescription, who, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LongDescription getByLongDescriptionByUserId_PrevAndNext(
		Session session, LongDescription longDescription, int who,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LONGDESCRIPTION_WHERE);

		query.append(_FINDER_COLUMN_LONGDESCRIPTIONBYUSERID_WHO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(LongDescriptionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(who);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(longDescription);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LongDescription> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the long descriptions where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @return the matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findByLongDescriptionByBugId(int bug_id)
		throws SystemException {
		return findByLongDescriptionByBugId(bug_id, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the long descriptions where bug_id = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param bug_id the bug_id
	 * @param start the lower bound of the range of long descriptions
	 * @param end the upper bound of the range of long descriptions (not inclusive)
	 * @return the range of matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findByLongDescriptionByBugId(int bug_id,
		int start, int end) throws SystemException {
		return findByLongDescriptionByBugId(bug_id, start, end, null);
	}

	/**
	 * Returns an ordered range of all the long descriptions where bug_id = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param bug_id the bug_id
	 * @param start the lower bound of the range of long descriptions
	 * @param end the upper bound of the range of long descriptions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findByLongDescriptionByBugId(int bug_id,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID;
			finderArgs = new Object[] { bug_id };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LONGDESCRIPTIONBYBUGID;
			finderArgs = new Object[] { bug_id, start, end, orderByComparator };
		}

		List<LongDescription> list = (List<LongDescription>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LongDescription longDescription : list) {
				if ((bug_id != longDescription.getBug_id())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LONGDESCRIPTION_WHERE);

			query.append(_FINDER_COLUMN_LONGDESCRIPTIONBYBUGID_BUG_ID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(LongDescriptionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bug_id);

				list = (List<LongDescription>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first long description in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription findByLongDescriptionByBugId_First(int bug_id,
		OrderByComparator orderByComparator)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = fetchByLongDescriptionByBugId_First(bug_id,
				orderByComparator);

		if (longDescription != null) {
			return longDescription;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bug_id=");
		msg.append(bug_id);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLongDescriptionException(msg.toString());
	}

	/**
	 * Returns the first long description in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching long description, or <code>null</code> if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription fetchByLongDescriptionByBugId_First(int bug_id,
		OrderByComparator orderByComparator) throws SystemException {
		List<LongDescription> list = findByLongDescriptionByBugId(bug_id, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last long description in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription findByLongDescriptionByBugId_Last(int bug_id,
		OrderByComparator orderByComparator)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = fetchByLongDescriptionByBugId_Last(bug_id,
				orderByComparator);

		if (longDescription != null) {
			return longDescription;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("bug_id=");
		msg.append(bug_id);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLongDescriptionException(msg.toString());
	}

	/**
	 * Returns the last long description in the ordered set where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching long description, or <code>null</code> if a matching long description could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription fetchByLongDescriptionByBugId_Last(int bug_id,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLongDescriptionByBugId(bug_id);

		List<LongDescription> list = findByLongDescriptionByBugId(bug_id,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the long descriptions before and after the current long description in the ordered set where bug_id = &#63;.
	 *
	 * @param comment_id the primary key of the current long description
	 * @param bug_id the bug_id
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next long description
	 * @throws com.vmware.NoSuchLongDescriptionException if a long description with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LongDescription[] findByLongDescriptionByBugId_PrevAndNext(
		int comment_id, int bug_id, OrderByComparator orderByComparator)
		throws NoSuchLongDescriptionException, SystemException {
		LongDescription longDescription = findByPrimaryKey(comment_id);

		Session session = null;

		try {
			session = openSession();

			LongDescription[] array = new LongDescriptionImpl[3];

			array[0] = getByLongDescriptionByBugId_PrevAndNext(session,
					longDescription, bug_id, orderByComparator, true);

			array[1] = longDescription;

			array[2] = getByLongDescriptionByBugId_PrevAndNext(session,
					longDescription, bug_id, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LongDescription getByLongDescriptionByBugId_PrevAndNext(
		Session session, LongDescription longDescription, int bug_id,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LONGDESCRIPTION_WHERE);

		query.append(_FINDER_COLUMN_LONGDESCRIPTIONBYBUGID_BUG_ID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(LongDescriptionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(bug_id);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(longDescription);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LongDescription> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the long descriptions.
	 *
	 * @return the long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the long descriptions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of long descriptions
	 * @param end the upper bound of the range of long descriptions (not inclusive)
	 * @return the range of long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the long descriptions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of long descriptions
	 * @param end the upper bound of the range of long descriptions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public List<LongDescription> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LongDescription> list = (List<LongDescription>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LONGDESCRIPTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LONGDESCRIPTION.concat(LongDescriptionModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LongDescription>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LongDescription>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the long descriptions where who = &#63; from the database.
	 *
	 * @param who the who
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByLongDescriptionByUserId(int who)
		throws SystemException {
		for (LongDescription longDescription : findByLongDescriptionByUserId(
				who)) {
			remove(longDescription);
		}
	}

	/**
	 * Removes all the long descriptions where bug_id = &#63; from the database.
	 *
	 * @param bug_id the bug_id
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByLongDescriptionByBugId(int bug_id)
		throws SystemException {
		for (LongDescription longDescription : findByLongDescriptionByBugId(
				bug_id)) {
			remove(longDescription);
		}
	}

	/**
	 * Removes all the long descriptions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LongDescription longDescription : findAll()) {
			remove(longDescription);
		}
	}

	/**
	 * Returns the number of long descriptions where who = &#63;.
	 *
	 * @param who the who
	 * @return the number of matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLongDescriptionByUserId(int who)
		throws SystemException {
		Object[] finderArgs = new Object[] { who };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LONGDESCRIPTION_WHERE);

			query.append(_FINDER_COLUMN_LONGDESCRIPTIONBYUSERID_WHO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of long descriptions where bug_id = &#63;.
	 *
	 * @param bug_id the bug_id
	 * @return the number of matching long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLongDescriptionByBugId(int bug_id)
		throws SystemException {
		Object[] finderArgs = new Object[] { bug_id };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYBUGID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LONGDESCRIPTION_WHERE);

			query.append(_FINDER_COLUMN_LONGDESCRIPTIONBYBUGID_BUG_ID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(bug_id);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LONGDESCRIPTIONBYBUGID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of long descriptions.
	 *
	 * @return the number of long descriptions
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LONGDESCRIPTION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the long description persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.LongDescription")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LongDescription>> listenersList = new ArrayList<ModelListener<LongDescription>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LongDescription>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LongDescriptionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LONGDESCRIPTION = "SELECT longDescription FROM LongDescription longDescription";
	private static final String _SQL_SELECT_LONGDESCRIPTION_WHERE = "SELECT longDescription FROM LongDescription longDescription WHERE ";
	private static final String _SQL_COUNT_LONGDESCRIPTION = "SELECT COUNT(longDescription) FROM LongDescription longDescription";
	private static final String _SQL_COUNT_LONGDESCRIPTION_WHERE = "SELECT COUNT(longDescription) FROM LongDescription longDescription WHERE ";
	private static final String _FINDER_COLUMN_LONGDESCRIPTIONBYUSERID_WHO_2 = "longDescription.who = ?";
	private static final String _FINDER_COLUMN_LONGDESCRIPTIONBYBUGID_BUG_ID_2 = "longDescription.bug_id = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "longDescription.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LongDescription exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LongDescription exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LongDescriptionPersistenceImpl.class);
	private static LongDescription _nullLongDescription = new LongDescriptionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LongDescription> toCacheModel() {
				return _nullLongDescriptionCacheModel;
			}
		};

	private static CacheModel<LongDescription> _nullLongDescriptionCacheModel = new CacheModel<LongDescription>() {
			public LongDescription toEntityModel() {
				return _nullLongDescription;
			}
		};
}