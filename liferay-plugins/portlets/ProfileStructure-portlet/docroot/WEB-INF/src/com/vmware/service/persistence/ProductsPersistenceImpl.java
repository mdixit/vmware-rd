/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchProductsException;

import com.vmware.model.Products;
import com.vmware.model.impl.ProductsImpl;
import com.vmware.model.impl.ProductsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the products service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProductsPersistence
 * @see ProductsUtil
 * @generated
 */
public class ProductsPersistenceImpl extends BasePersistenceImpl<Products>
	implements ProductsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductsUtil} to access the products persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductsModelImpl.ENTITY_CACHE_ENABLED,
			ProductsModelImpl.FINDER_CACHE_ENABLED, ProductsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductsModelImpl.ENTITY_CACHE_ENABLED,
			ProductsModelImpl.FINDER_CACHE_ENABLED, ProductsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductsModelImpl.ENTITY_CACHE_ENABLED,
			ProductsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the products in the entity cache if it is enabled.
	 *
	 * @param products the products
	 */
	public void cacheResult(Products products) {
		EntityCacheUtil.putResult(ProductsModelImpl.ENTITY_CACHE_ENABLED,
			ProductsImpl.class, products.getPrimaryKey(), products);

		products.resetOriginalValues();
	}

	/**
	 * Caches the productses in the entity cache if it is enabled.
	 *
	 * @param productses the productses
	 */
	public void cacheResult(List<Products> productses) {
		for (Products products : productses) {
			if (EntityCacheUtil.getResult(
						ProductsModelImpl.ENTITY_CACHE_ENABLED,
						ProductsImpl.class, products.getPrimaryKey()) == null) {
				cacheResult(products);
			}
			else {
				products.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all productses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProductsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProductsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the products.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Products products) {
		EntityCacheUtil.removeResult(ProductsModelImpl.ENTITY_CACHE_ENABLED,
			ProductsImpl.class, products.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Products> productses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Products products : productses) {
			EntityCacheUtil.removeResult(ProductsModelImpl.ENTITY_CACHE_ENABLED,
				ProductsImpl.class, products.getPrimaryKey());
		}
	}

	/**
	 * Creates a new products with the primary key. Does not add the products to the database.
	 *
	 * @param prod_id the primary key for the new products
	 * @return the new products
	 */
	public Products create(int prod_id) {
		Products products = new ProductsImpl();

		products.setNew(true);
		products.setPrimaryKey(prod_id);

		return products;
	}

	/**
	 * Removes the products with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param prod_id the primary key of the products
	 * @return the products that was removed
	 * @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Products remove(int prod_id)
		throws NoSuchProductsException, SystemException {
		return remove(Integer.valueOf(prod_id));
	}

	/**
	 * Removes the products with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the products
	 * @return the products that was removed
	 * @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Products remove(Serializable primaryKey)
		throws NoSuchProductsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Products products = (Products)session.get(ProductsImpl.class,
					primaryKey);

			if (products == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(products);
		}
		catch (NoSuchProductsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Products removeImpl(Products products) throws SystemException {
		products = toUnwrappedModel(products);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, products);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(products);

		return products;
	}

	@Override
	public Products updateImpl(com.vmware.model.Products products, boolean merge)
		throws SystemException {
		products = toUnwrappedModel(products);

		boolean isNew = products.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, products, merge);

			products.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ProductsModelImpl.ENTITY_CACHE_ENABLED,
			ProductsImpl.class, products.getPrimaryKey(), products);

		return products;
	}

	protected Products toUnwrappedModel(Products products) {
		if (products instanceof ProductsImpl) {
			return products;
		}

		ProductsImpl productsImpl = new ProductsImpl();

		productsImpl.setNew(products.isNew());
		productsImpl.setPrimaryKey(products.getPrimaryKey());

		productsImpl.setProd_id(products.getProd_id());
		productsImpl.setName(products.getName());
		productsImpl.setDescription(products.getDescription());
		productsImpl.setTemplate(products.getTemplate());
		productsImpl.setSortkey(products.getSortkey());
		productsImpl.setDefaultcategory(products.getDefaultcategory());
		productsImpl.setDisallownew(products.getDisallownew());

		return productsImpl;
	}

	/**
	 * Returns the products with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the products
	 * @return the products
	 * @throws com.liferay.portal.NoSuchModelException if a products with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Products findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the products with the primary key or throws a {@link com.vmware.NoSuchProductsException} if it could not be found.
	 *
	 * @param prod_id the primary key of the products
	 * @return the products
	 * @throws com.vmware.NoSuchProductsException if a products with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Products findByPrimaryKey(int prod_id)
		throws NoSuchProductsException, SystemException {
		Products products = fetchByPrimaryKey(prod_id);

		if (products == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + prod_id);
			}

			throw new NoSuchProductsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				prod_id);
		}

		return products;
	}

	/**
	 * Returns the products with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the products
	 * @return the products, or <code>null</code> if a products with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Products fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the products with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param prod_id the primary key of the products
	 * @return the products, or <code>null</code> if a products with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Products fetchByPrimaryKey(int prod_id) throws SystemException {
		Products products = (Products)EntityCacheUtil.getResult(ProductsModelImpl.ENTITY_CACHE_ENABLED,
				ProductsImpl.class, prod_id);

		if (products == _nullProducts) {
			return null;
		}

		if (products == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				products = (Products)session.get(ProductsImpl.class,
						Integer.valueOf(prod_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (products != null) {
					cacheResult(products);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ProductsModelImpl.ENTITY_CACHE_ENABLED,
						ProductsImpl.class, prod_id, _nullProducts);
				}

				closeSession(session);
			}
		}

		return products;
	}

	/**
	 * Returns all the productses.
	 *
	 * @return the productses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Products> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the productses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of productses
	 * @param end the upper bound of the range of productses (not inclusive)
	 * @return the range of productses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Products> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the productses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of productses
	 * @param end the upper bound of the range of productses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of productses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Products> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Products> list = (List<Products>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRODUCTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Products>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Products>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the productses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Products products : findAll()) {
			remove(products);
		}
	}

	/**
	 * Returns the number of productses.
	 *
	 * @return the number of productses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the products persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Products")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Products>> listenersList = new ArrayList<ModelListener<Products>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Products>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProductsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_PRODUCTS = "SELECT products FROM Products products";
	private static final String _SQL_COUNT_PRODUCTS = "SELECT COUNT(products) FROM Products products";
	private static final String _ORDER_BY_ENTITY_ALIAS = "products.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Products exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProductsPersistenceImpl.class);
	private static Products _nullProducts = new ProductsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Products> toCacheModel() {
				return _nullProductsCacheModel;
			}
		};

	private static CacheModel<Products> _nullProductsCacheModel = new CacheModel<Products>() {
			public Products toEntityModel() {
				return _nullProducts;
			}
		};
}