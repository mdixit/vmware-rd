/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchProfileActivityException;

import com.vmware.model.ProfileActivity;
import com.vmware.model.impl.ProfileActivityImpl;
import com.vmware.model.impl.ProfileActivityModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the profile activity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProfileActivityPersistence
 * @see ProfileActivityUtil
 * @generated
 */
public class ProfileActivityPersistenceImpl extends BasePersistenceImpl<ProfileActivity>
	implements ProfileActivityPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProfileActivityUtil} to access the profile activity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProfileActivityImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED,
			ProfileActivityImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByProfileActivityByUserId",
			new String[] { Integer.class.getName() },
			ProfileActivityModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROFILEACTIVITYBYUSERID = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProfileActivityByUserId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED,
			ProfileActivityImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByFieldDefsByFieldId",
			new String[] { Integer.class.getName() },
			ProfileActivityModelImpl.FIELDID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FIELDDEFSBYFIELDID = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByFieldDefsByFieldId",
			new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED,
			ProfileActivityImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED,
			ProfileActivityImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the profile activity in the entity cache if it is enabled.
	 *
	 * @param profileActivity the profile activity
	 */
	public void cacheResult(ProfileActivity profileActivity) {
		EntityCacheUtil.putResult(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityImpl.class, profileActivity.getPrimaryKey(),
			profileActivity);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
			new Object[] { Integer.valueOf(profileActivity.getUserid()) },
			profileActivity);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
			new Object[] { Integer.valueOf(profileActivity.getFieldid()) },
			profileActivity);

		profileActivity.resetOriginalValues();
	}

	/**
	 * Caches the profile activities in the entity cache if it is enabled.
	 *
	 * @param profileActivities the profile activities
	 */
	public void cacheResult(List<ProfileActivity> profileActivities) {
		for (ProfileActivity profileActivity : profileActivities) {
			if (EntityCacheUtil.getResult(
						ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
						ProfileActivityImpl.class,
						profileActivity.getPrimaryKey()) == null) {
				cacheResult(profileActivity);
			}
			else {
				profileActivity.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all profile activities.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProfileActivityImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProfileActivityImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the profile activity.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProfileActivity profileActivity) {
		EntityCacheUtil.removeResult(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityImpl.class, profileActivity.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(profileActivity);
	}

	@Override
	public void clearCache(List<ProfileActivity> profileActivities) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProfileActivity profileActivity : profileActivities) {
			EntityCacheUtil.removeResult(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
				ProfileActivityImpl.class, profileActivity.getPrimaryKey());

			clearUniqueFindersCache(profileActivity);
		}
	}

	protected void clearUniqueFindersCache(ProfileActivity profileActivity) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
			new Object[] { Integer.valueOf(profileActivity.getUserid()) });

		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
			new Object[] { Integer.valueOf(profileActivity.getFieldid()) });
	}

	/**
	 * Creates a new profile activity with the primary key. Does not add the profile activity to the database.
	 *
	 * @param profileActivityPK the primary key for the new profile activity
	 * @return the new profile activity
	 */
	public ProfileActivity create(ProfileActivityPK profileActivityPK) {
		ProfileActivity profileActivity = new ProfileActivityImpl();

		profileActivity.setNew(true);
		profileActivity.setPrimaryKey(profileActivityPK);

		return profileActivity;
	}

	/**
	 * Removes the profile activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param profileActivityPK the primary key of the profile activity
	 * @return the profile activity that was removed
	 * @throws com.vmware.NoSuchProfileActivityException if a profile activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity remove(ProfileActivityPK profileActivityPK)
		throws NoSuchProfileActivityException, SystemException {
		return remove((Serializable)profileActivityPK);
	}

	/**
	 * Removes the profile activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the profile activity
	 * @return the profile activity that was removed
	 * @throws com.vmware.NoSuchProfileActivityException if a profile activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProfileActivity remove(Serializable primaryKey)
		throws NoSuchProfileActivityException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ProfileActivity profileActivity = (ProfileActivity)session.get(ProfileActivityImpl.class,
					primaryKey);

			if (profileActivity == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProfileActivityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(profileActivity);
		}
		catch (NoSuchProfileActivityException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProfileActivity removeImpl(ProfileActivity profileActivity)
		throws SystemException {
		profileActivity = toUnwrappedModel(profileActivity);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, profileActivity);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(profileActivity);

		return profileActivity;
	}

	@Override
	public ProfileActivity updateImpl(
		com.vmware.model.ProfileActivity profileActivity, boolean merge)
		throws SystemException {
		profileActivity = toUnwrappedModel(profileActivity);

		boolean isNew = profileActivity.isNew();

		ProfileActivityModelImpl profileActivityModelImpl = (ProfileActivityModelImpl)profileActivity;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, profileActivity, merge);

			profileActivity.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProfileActivityModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
			ProfileActivityImpl.class, profileActivity.getPrimaryKey(),
			profileActivity);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
				new Object[] { Integer.valueOf(profileActivity.getUserid()) },
				profileActivity);

			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
				new Object[] { Integer.valueOf(profileActivity.getFieldid()) },
				profileActivity);
		}
		else {
			if ((profileActivityModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(profileActivityModelImpl.getOriginalUserid())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROFILEACTIVITYBYUSERID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
					new Object[] { Integer.valueOf(profileActivity.getUserid()) },
					profileActivity);
			}

			if ((profileActivityModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(profileActivityModelImpl.getOriginalFieldid())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FIELDDEFSBYFIELDID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
					new Object[] { Integer.valueOf(profileActivity.getFieldid()) },
					profileActivity);
			}
		}

		return profileActivity;
	}

	protected ProfileActivity toUnwrappedModel(ProfileActivity profileActivity) {
		if (profileActivity instanceof ProfileActivityImpl) {
			return profileActivity;
		}

		ProfileActivityImpl profileActivityImpl = new ProfileActivityImpl();

		profileActivityImpl.setNew(profileActivity.isNew());
		profileActivityImpl.setPrimaryKey(profileActivity.getPrimaryKey());

		profileActivityImpl.setUserid(profileActivity.getUserid());
		profileActivityImpl.setWho(profileActivity.getWho());
		profileActivityImpl.setProfiles_when(profileActivity.getProfiles_when());
		profileActivityImpl.setFieldid(profileActivity.getFieldid());
		profileActivityImpl.setOldvalue(profileActivity.getOldvalue());
		profileActivityImpl.setNewvalue(profileActivity.getNewvalue());

		return profileActivityImpl;
	}

	/**
	 * Returns the profile activity with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the profile activity
	 * @return the profile activity
	 * @throws com.liferay.portal.NoSuchModelException if a profile activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProfileActivity findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey((ProfileActivityPK)primaryKey);
	}

	/**
	 * Returns the profile activity with the primary key or throws a {@link com.vmware.NoSuchProfileActivityException} if it could not be found.
	 *
	 * @param profileActivityPK the primary key of the profile activity
	 * @return the profile activity
	 * @throws com.vmware.NoSuchProfileActivityException if a profile activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity findByPrimaryKey(ProfileActivityPK profileActivityPK)
		throws NoSuchProfileActivityException, SystemException {
		ProfileActivity profileActivity = fetchByPrimaryKey(profileActivityPK);

		if (profileActivity == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + profileActivityPK);
			}

			throw new NoSuchProfileActivityException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				profileActivityPK);
		}

		return profileActivity;
	}

	/**
	 * Returns the profile activity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the profile activity
	 * @return the profile activity, or <code>null</code> if a profile activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProfileActivity fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey((ProfileActivityPK)primaryKey);
	}

	/**
	 * Returns the profile activity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param profileActivityPK the primary key of the profile activity
	 * @return the profile activity, or <code>null</code> if a profile activity with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity fetchByPrimaryKey(
		ProfileActivityPK profileActivityPK) throws SystemException {
		ProfileActivity profileActivity = (ProfileActivity)EntityCacheUtil.getResult(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
				ProfileActivityImpl.class, profileActivityPK);

		if (profileActivity == _nullProfileActivity) {
			return null;
		}

		if (profileActivity == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				profileActivity = (ProfileActivity)session.get(ProfileActivityImpl.class,
						profileActivityPK);
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (profileActivity != null) {
					cacheResult(profileActivity);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ProfileActivityModelImpl.ENTITY_CACHE_ENABLED,
						ProfileActivityImpl.class, profileActivityPK,
						_nullProfileActivity);
				}

				closeSession(session);
			}
		}

		return profileActivity;
	}

	/**
	 * Returns the profile activity where userid = &#63; or throws a {@link com.vmware.NoSuchProfileActivityException} if it could not be found.
	 *
	 * @param userid the userid
	 * @return the matching profile activity
	 * @throws com.vmware.NoSuchProfileActivityException if a matching profile activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity findByProfileActivityByUserId(int userid)
		throws NoSuchProfileActivityException, SystemException {
		ProfileActivity profileActivity = fetchByProfileActivityByUserId(userid);

		if (profileActivity == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userid=");
			msg.append(userid);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProfileActivityException(msg.toString());
		}

		return profileActivity;
	}

	/**
	 * Returns the profile activity where userid = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userid the userid
	 * @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity fetchByProfileActivityByUserId(int userid)
		throws SystemException {
		return fetchByProfileActivityByUserId(userid, true);
	}

	/**
	 * Returns the profile activity where userid = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userid the userid
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity fetchByProfileActivityByUserId(int userid,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { userid };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
					finderArgs, this);
		}

		if (result instanceof ProfileActivity) {
			ProfileActivity profileActivity = (ProfileActivity)result;

			if ((userid != profileActivity.getUserid())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PROFILEACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_PROFILEACTIVITYBYUSERID_USERID_2);

			query.append(ProfileActivityModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userid);

				List<ProfileActivity> list = q.list();

				result = list;

				ProfileActivity profileActivity = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
						finderArgs, list);
				}
				else {
					profileActivity = list.get(0);

					cacheResult(profileActivity);

					if ((profileActivity.getUserid() != userid)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
							finderArgs, profileActivity);
					}
				}

				return profileActivity;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PROFILEACTIVITYBYUSERID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (ProfileActivity)result;
			}
		}
	}

	/**
	 * Returns the profile activity where fieldid = &#63; or throws a {@link com.vmware.NoSuchProfileActivityException} if it could not be found.
	 *
	 * @param fieldid the fieldid
	 * @return the matching profile activity
	 * @throws com.vmware.NoSuchProfileActivityException if a matching profile activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity findByFieldDefsByFieldId(int fieldid)
		throws NoSuchProfileActivityException, SystemException {
		ProfileActivity profileActivity = fetchByFieldDefsByFieldId(fieldid);

		if (profileActivity == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("fieldid=");
			msg.append(fieldid);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProfileActivityException(msg.toString());
		}

		return profileActivity;
	}

	/**
	 * Returns the profile activity where fieldid = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param fieldid the fieldid
	 * @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity fetchByFieldDefsByFieldId(int fieldid)
		throws SystemException {
		return fetchByFieldDefsByFieldId(fieldid, true);
	}

	/**
	 * Returns the profile activity where fieldid = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param fieldid the fieldid
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching profile activity, or <code>null</code> if a matching profile activity could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity fetchByFieldDefsByFieldId(int fieldid,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { fieldid };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
					finderArgs, this);
		}

		if (result instanceof ProfileActivity) {
			ProfileActivity profileActivity = (ProfileActivity)result;

			if ((fieldid != profileActivity.getFieldid())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PROFILEACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_FIELDDEFSBYFIELDID_FIELDID_2);

			query.append(ProfileActivityModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(fieldid);

				List<ProfileActivity> list = q.list();

				result = list;

				ProfileActivity profileActivity = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
						finderArgs, list);
				}
				else {
					profileActivity = list.get(0);

					cacheResult(profileActivity);

					if ((profileActivity.getFieldid() != fieldid)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
							finderArgs, profileActivity);
					}
				}

				return profileActivity;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FIELDDEFSBYFIELDID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (ProfileActivity)result;
			}
		}
	}

	/**
	 * Returns all the profile activities.
	 *
	 * @return the profile activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<ProfileActivity> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profile activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of profile activities
	 * @param end the upper bound of the range of profile activities (not inclusive)
	 * @return the range of profile activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<ProfileActivity> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the profile activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of profile activities
	 * @param end the upper bound of the range of profile activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of profile activities
	 * @throws SystemException if a system exception occurred
	 */
	public List<ProfileActivity> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProfileActivity> list = (List<ProfileActivity>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PROFILEACTIVITY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROFILEACTIVITY.concat(ProfileActivityModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<ProfileActivity>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<ProfileActivity>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the profile activity where userid = &#63; from the database.
	 *
	 * @param userid the userid
	 * @return the profile activity that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity removeByProfileActivityByUserId(int userid)
		throws NoSuchProfileActivityException, SystemException {
		ProfileActivity profileActivity = findByProfileActivityByUserId(userid);

		return remove(profileActivity);
	}

	/**
	 * Removes the profile activity where fieldid = &#63; from the database.
	 *
	 * @param fieldid the fieldid
	 * @return the profile activity that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public ProfileActivity removeByFieldDefsByFieldId(int fieldid)
		throws NoSuchProfileActivityException, SystemException {
		ProfileActivity profileActivity = findByFieldDefsByFieldId(fieldid);

		return remove(profileActivity);
	}

	/**
	 * Removes all the profile activities from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (ProfileActivity profileActivity : findAll()) {
			remove(profileActivity);
		}
	}

	/**
	 * Returns the number of profile activities where userid = &#63;.
	 *
	 * @param userid the userid
	 * @return the number of matching profile activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByProfileActivityByUserId(int userid)
		throws SystemException {
		Object[] finderArgs = new Object[] { userid };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PROFILEACTIVITYBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROFILEACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_PROFILEACTIVITYBYUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userid);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PROFILEACTIVITYBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of profile activities where fieldid = &#63;.
	 *
	 * @param fieldid the fieldid
	 * @return the number of matching profile activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countByFieldDefsByFieldId(int fieldid) throws SystemException {
		Object[] finderArgs = new Object[] { fieldid };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_FIELDDEFSBYFIELDID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROFILEACTIVITY_WHERE);

			query.append(_FINDER_COLUMN_FIELDDEFSBYFIELDID_FIELDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(fieldid);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FIELDDEFSBYFIELDID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of profile activities.
	 *
	 * @return the number of profile activities
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROFILEACTIVITY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the profile activity persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.ProfileActivity")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ProfileActivity>> listenersList = new ArrayList<ModelListener<ProfileActivity>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ProfileActivity>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProfileActivityImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_PROFILEACTIVITY = "SELECT profileActivity FROM ProfileActivity profileActivity";
	private static final String _SQL_SELECT_PROFILEACTIVITY_WHERE = "SELECT profileActivity FROM ProfileActivity profileActivity WHERE ";
	private static final String _SQL_COUNT_PROFILEACTIVITY = "SELECT COUNT(profileActivity) FROM ProfileActivity profileActivity";
	private static final String _SQL_COUNT_PROFILEACTIVITY_WHERE = "SELECT COUNT(profileActivity) FROM ProfileActivity profileActivity WHERE ";
	private static final String _FINDER_COLUMN_PROFILEACTIVITYBYUSERID_USERID_2 = "profileActivity.id.userid = ?";
	private static final String _FINDER_COLUMN_FIELDDEFSBYFIELDID_FIELDID_2 = "profileActivity.fieldid = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "profileActivity.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProfileActivity exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProfileActivity exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProfileActivityPersistenceImpl.class);
	private static ProfileActivity _nullProfileActivity = new ProfileActivityImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ProfileActivity> toCacheModel() {
				return _nullProfileActivityCacheModel;
			}
		};

	private static CacheModel<ProfileActivity> _nullProfileActivityCacheModel = new CacheModel<ProfileActivity>() {
			public ProfileActivity toEntityModel() {
				return _nullProfileActivity;
			}
		};
}