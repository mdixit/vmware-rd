/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchProfilesException;

import com.vmware.model.Profiles;
import com.vmware.model.impl.ProfilesImpl;
import com.vmware.model.impl.ProfilesModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the profiles service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ProfilesPersistence
 * @see ProfilesUtil
 * @generated
 */
public class ProfilesPersistenceImpl extends BasePersistenceImpl<Profiles>
	implements ProfilesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProfilesUtil} to access the profiles persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProfilesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_LOGINNAME = new FinderPath(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesModelImpl.FINDER_CACHE_ENABLED, ProfilesImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByLoginName",
			new String[] { String.class.getName() },
			ProfilesModelImpl.LOGIN_NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LOGINNAME = new FinderPath(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLoginName",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesModelImpl.FINDER_CACHE_ENABLED, ProfilesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesModelImpl.FINDER_CACHE_ENABLED, ProfilesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the profiles in the entity cache if it is enabled.
	 *
	 * @param profiles the profiles
	 */
	public void cacheResult(Profiles profiles) {
		EntityCacheUtil.putResult(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesImpl.class, profiles.getPrimaryKey(), profiles);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOGINNAME,
			new Object[] { profiles.getLogin_name() }, profiles);

		profiles.resetOriginalValues();
	}

	/**
	 * Caches the profileses in the entity cache if it is enabled.
	 *
	 * @param profileses the profileses
	 */
	public void cacheResult(List<Profiles> profileses) {
		for (Profiles profiles : profileses) {
			if (EntityCacheUtil.getResult(
						ProfilesModelImpl.ENTITY_CACHE_ENABLED,
						ProfilesImpl.class, profiles.getPrimaryKey()) == null) {
				cacheResult(profiles);
			}
			else {
				profiles.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all profileses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProfilesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProfilesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the profiles.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Profiles profiles) {
		EntityCacheUtil.removeResult(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesImpl.class, profiles.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(profiles);
	}

	@Override
	public void clearCache(List<Profiles> profileses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Profiles profiles : profileses) {
			EntityCacheUtil.removeResult(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
				ProfilesImpl.class, profiles.getPrimaryKey());

			clearUniqueFindersCache(profiles);
		}
	}

	protected void clearUniqueFindersCache(Profiles profiles) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LOGINNAME,
			new Object[] { profiles.getLogin_name() });
	}

	/**
	 * Creates a new profiles with the primary key. Does not add the profiles to the database.
	 *
	 * @param userid the primary key for the new profiles
	 * @return the new profiles
	 */
	public Profiles create(int userid) {
		Profiles profiles = new ProfilesImpl();

		profiles.setNew(true);
		profiles.setPrimaryKey(userid);

		return profiles;
	}

	/**
	 * Removes the profiles with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userid the primary key of the profiles
	 * @return the profiles that was removed
	 * @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles remove(int userid)
		throws NoSuchProfilesException, SystemException {
		return remove(Integer.valueOf(userid));
	}

	/**
	 * Removes the profiles with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the profiles
	 * @return the profiles that was removed
	 * @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Profiles remove(Serializable primaryKey)
		throws NoSuchProfilesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Profiles profiles = (Profiles)session.get(ProfilesImpl.class,
					primaryKey);

			if (profiles == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProfilesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(profiles);
		}
		catch (NoSuchProfilesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Profiles removeImpl(Profiles profiles) throws SystemException {
		profiles = toUnwrappedModel(profiles);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, profiles);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(profiles);

		return profiles;
	}

	@Override
	public Profiles updateImpl(com.vmware.model.Profiles profiles, boolean merge)
		throws SystemException {
		profiles = toUnwrappedModel(profiles);

		boolean isNew = profiles.isNew();

		ProfilesModelImpl profilesModelImpl = (ProfilesModelImpl)profiles;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, profiles, merge);

			profiles.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProfilesModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
			ProfilesImpl.class, profiles.getPrimaryKey(), profiles);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOGINNAME,
				new Object[] { profiles.getLogin_name() }, profiles);
		}
		else {
			if ((profilesModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_LOGINNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						profilesModelImpl.getOriginalLogin_name()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LOGINNAME,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LOGINNAME,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOGINNAME,
					new Object[] { profiles.getLogin_name() }, profiles);
			}
		}

		return profiles;
	}

	protected Profiles toUnwrappedModel(Profiles profiles) {
		if (profiles instanceof ProfilesImpl) {
			return profiles;
		}

		ProfilesImpl profilesImpl = new ProfilesImpl();

		profilesImpl.setNew(profiles.isNew());
		profilesImpl.setPrimaryKey(profiles.getPrimaryKey());

		profilesImpl.setUserid(profiles.getUserid());
		profilesImpl.setLogin_name(profiles.getLogin_name());
		profilesImpl.setAll_login_names(profiles.getAll_login_names());
		profilesImpl.setCryptpassword(profiles.getCryptpassword());
		profilesImpl.setRealname(profiles.getRealname());
		profilesImpl.setDisabledtext(profiles.getDisabledtext());
		profilesImpl.setDisable_mail(profiles.getDisable_mail());
		profilesImpl.setMybugslink(profiles.getMybugslink());
		profilesImpl.setManager_id(profiles.getManager_id());
		profilesImpl.setExtern_id(profiles.getExtern_id());

		return profilesImpl;
	}

	/**
	 * Returns the profiles with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the profiles
	 * @return the profiles
	 * @throws com.liferay.portal.NoSuchModelException if a profiles with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Profiles findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the profiles with the primary key or throws a {@link com.vmware.NoSuchProfilesException} if it could not be found.
	 *
	 * @param userid the primary key of the profiles
	 * @return the profiles
	 * @throws com.vmware.NoSuchProfilesException if a profiles with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles findByPrimaryKey(int userid)
		throws NoSuchProfilesException, SystemException {
		Profiles profiles = fetchByPrimaryKey(userid);

		if (profiles == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + userid);
			}

			throw new NoSuchProfilesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				userid);
		}

		return profiles;
	}

	/**
	 * Returns the profiles with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the profiles
	 * @return the profiles, or <code>null</code> if a profiles with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Profiles fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the profiles with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userid the primary key of the profiles
	 * @return the profiles, or <code>null</code> if a profiles with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles fetchByPrimaryKey(int userid) throws SystemException {
		Profiles profiles = (Profiles)EntityCacheUtil.getResult(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
				ProfilesImpl.class, userid);

		if (profiles == _nullProfiles) {
			return null;
		}

		if (profiles == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				profiles = (Profiles)session.get(ProfilesImpl.class,
						Integer.valueOf(userid));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (profiles != null) {
					cacheResult(profiles);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ProfilesModelImpl.ENTITY_CACHE_ENABLED,
						ProfilesImpl.class, userid, _nullProfiles);
				}

				closeSession(session);
			}
		}

		return profiles;
	}

	/**
	 * Returns the profiles where login_name = &#63; or throws a {@link com.vmware.NoSuchProfilesException} if it could not be found.
	 *
	 * @param login_name the login_name
	 * @return the matching profiles
	 * @throws com.vmware.NoSuchProfilesException if a matching profiles could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles findByLoginName(String login_name)
		throws NoSuchProfilesException, SystemException {
		Profiles profiles = fetchByLoginName(login_name);

		if (profiles == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("login_name=");
			msg.append(login_name);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProfilesException(msg.toString());
		}

		return profiles;
	}

	/**
	 * Returns the profiles where login_name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param login_name the login_name
	 * @return the matching profiles, or <code>null</code> if a matching profiles could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles fetchByLoginName(String login_name)
		throws SystemException {
		return fetchByLoginName(login_name, true);
	}

	/**
	 * Returns the profiles where login_name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param login_name the login_name
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching profiles, or <code>null</code> if a matching profiles could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles fetchByLoginName(String login_name,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { login_name };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LOGINNAME,
					finderArgs, this);
		}

		if (result instanceof Profiles) {
			Profiles profiles = (Profiles)result;

			if (!Validator.equals(login_name, profiles.getLogin_name())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_PROFILES_WHERE);

			if (login_name == null) {
				query.append(_FINDER_COLUMN_LOGINNAME_LOGIN_NAME_1);
			}
			else {
				if (login_name.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_LOGINNAME_LOGIN_NAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_LOGINNAME_LOGIN_NAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (login_name != null) {
					qPos.add(login_name);
				}

				List<Profiles> list = q.list();

				result = list;

				Profiles profiles = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOGINNAME,
						finderArgs, list);
				}
				else {
					profiles = list.get(0);

					cacheResult(profiles);

					if ((profiles.getLogin_name() == null) ||
							!profiles.getLogin_name().equals(login_name)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LOGINNAME,
							finderArgs, profiles);
					}
				}

				return profiles;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LOGINNAME,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Profiles)result;
			}
		}
	}

	/**
	 * Returns all the profileses.
	 *
	 * @return the profileses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Profiles> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profileses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of profileses
	 * @param end the upper bound of the range of profileses (not inclusive)
	 * @return the range of profileses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Profiles> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the profileses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of profileses
	 * @param end the upper bound of the range of profileses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of profileses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Profiles> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Profiles> list = (List<Profiles>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PROFILES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROFILES;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Profiles>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Profiles>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the profiles where login_name = &#63; from the database.
	 *
	 * @param login_name the login_name
	 * @return the profiles that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Profiles removeByLoginName(String login_name)
		throws NoSuchProfilesException, SystemException {
		Profiles profiles = findByLoginName(login_name);

		return remove(profiles);
	}

	/**
	 * Removes all the profileses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Profiles profiles : findAll()) {
			remove(profiles);
		}
	}

	/**
	 * Returns the number of profileses where login_name = &#63;.
	 *
	 * @param login_name the login_name
	 * @return the number of matching profileses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLoginName(String login_name) throws SystemException {
		Object[] finderArgs = new Object[] { login_name };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LOGINNAME,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROFILES_WHERE);

			if (login_name == null) {
				query.append(_FINDER_COLUMN_LOGINNAME_LOGIN_NAME_1);
			}
			else {
				if (login_name.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_LOGINNAME_LOGIN_NAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_LOGINNAME_LOGIN_NAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (login_name != null) {
					qPos.add(login_name);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LOGINNAME,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of profileses.
	 *
	 * @return the number of profileses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROFILES);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the profiles persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Profiles")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Profiles>> listenersList = new ArrayList<ModelListener<Profiles>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Profiles>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProfilesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_PROFILES = "SELECT profiles FROM Profiles profiles";
	private static final String _SQL_SELECT_PROFILES_WHERE = "SELECT profiles FROM Profiles profiles WHERE ";
	private static final String _SQL_COUNT_PROFILES = "SELECT COUNT(profiles) FROM Profiles profiles";
	private static final String _SQL_COUNT_PROFILES_WHERE = "SELECT COUNT(profiles) FROM Profiles profiles WHERE ";
	private static final String _FINDER_COLUMN_LOGINNAME_LOGIN_NAME_1 = "profiles.login_name IS NULL";
	private static final String _FINDER_COLUMN_LOGINNAME_LOGIN_NAME_2 = "profiles.login_name = ?";
	private static final String _FINDER_COLUMN_LOGINNAME_LOGIN_NAME_3 = "(profiles.login_name IS NULL OR profiles.login_name = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "profiles.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Profiles exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Profiles exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProfilesPersistenceImpl.class);
	private static Profiles _nullProfiles = new ProfilesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Profiles> toCacheModel() {
				return _nullProfilesCacheModel;
			}
		};

	private static CacheModel<Profiles> _nullProfilesCacheModel = new CacheModel<Profiles>() {
			public Profiles toEntityModel() {
				return _nullProfiles;
			}
		};
}