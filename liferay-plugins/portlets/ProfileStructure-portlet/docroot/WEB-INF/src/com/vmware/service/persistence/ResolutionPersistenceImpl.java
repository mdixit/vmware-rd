/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchResolutionException;

import com.vmware.model.Resolution;
import com.vmware.model.impl.ResolutionImpl;
import com.vmware.model.impl.ResolutionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the resolution service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see ResolutionPersistence
 * @see ResolutionUtil
 * @generated
 */
public class ResolutionPersistenceImpl extends BasePersistenceImpl<Resolution>
	implements ResolutionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ResolutionUtil} to access the resolution persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ResolutionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE = new FinderPath(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionModelImpl.FINDER_CACHE_ENABLED, ResolutionImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByResolutionByValue",
			new String[] { String.class.getName() },
			ResolutionModelImpl.VALUE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RESOLUTIONBYVALUE = new FinderPath(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByResolutionByValue", new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionModelImpl.FINDER_CACHE_ENABLED, ResolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionModelImpl.FINDER_CACHE_ENABLED, ResolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the resolution in the entity cache if it is enabled.
	 *
	 * @param resolution the resolution
	 */
	public void cacheResult(Resolution resolution) {
		EntityCacheUtil.putResult(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionImpl.class, resolution.getPrimaryKey(), resolution);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
			new Object[] { resolution.getValue() }, resolution);

		resolution.resetOriginalValues();
	}

	/**
	 * Caches the resolutions in the entity cache if it is enabled.
	 *
	 * @param resolutions the resolutions
	 */
	public void cacheResult(List<Resolution> resolutions) {
		for (Resolution resolution : resolutions) {
			if (EntityCacheUtil.getResult(
						ResolutionModelImpl.ENTITY_CACHE_ENABLED,
						ResolutionImpl.class, resolution.getPrimaryKey()) == null) {
				cacheResult(resolution);
			}
			else {
				resolution.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all resolutions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ResolutionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ResolutionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the resolution.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Resolution resolution) {
		EntityCacheUtil.removeResult(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionImpl.class, resolution.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(resolution);
	}

	@Override
	public void clearCache(List<Resolution> resolutions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Resolution resolution : resolutions) {
			EntityCacheUtil.removeResult(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
				ResolutionImpl.class, resolution.getPrimaryKey());

			clearUniqueFindersCache(resolution);
		}
	}

	protected void clearUniqueFindersCache(Resolution resolution) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
			new Object[] { resolution.getValue() });
	}

	/**
	 * Creates a new resolution with the primary key. Does not add the resolution to the database.
	 *
	 * @param res_id the primary key for the new resolution
	 * @return the new resolution
	 */
	public Resolution create(int res_id) {
		Resolution resolution = new ResolutionImpl();

		resolution.setNew(true);
		resolution.setPrimaryKey(res_id);

		return resolution;
	}

	/**
	 * Removes the resolution with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param res_id the primary key of the resolution
	 * @return the resolution that was removed
	 * @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution remove(int res_id)
		throws NoSuchResolutionException, SystemException {
		return remove(Integer.valueOf(res_id));
	}

	/**
	 * Removes the resolution with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the resolution
	 * @return the resolution that was removed
	 * @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Resolution remove(Serializable primaryKey)
		throws NoSuchResolutionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Resolution resolution = (Resolution)session.get(ResolutionImpl.class,
					primaryKey);

			if (resolution == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchResolutionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(resolution);
		}
		catch (NoSuchResolutionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Resolution removeImpl(Resolution resolution)
		throws SystemException {
		resolution = toUnwrappedModel(resolution);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, resolution);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(resolution);

		return resolution;
	}

	@Override
	public Resolution updateImpl(com.vmware.model.Resolution resolution,
		boolean merge) throws SystemException {
		resolution = toUnwrappedModel(resolution);

		boolean isNew = resolution.isNew();

		ResolutionModelImpl resolutionModelImpl = (ResolutionModelImpl)resolution;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, resolution, merge);

			resolution.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ResolutionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
			ResolutionImpl.class, resolution.getPrimaryKey(), resolution);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
				new Object[] { resolution.getValue() }, resolution);
		}
		else {
			if ((resolutionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						resolutionModelImpl.getOriginalValue()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RESOLUTIONBYVALUE,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
					new Object[] { resolution.getValue() }, resolution);
			}
		}

		return resolution;
	}

	protected Resolution toUnwrappedModel(Resolution resolution) {
		if (resolution instanceof ResolutionImpl) {
			return resolution;
		}

		ResolutionImpl resolutionImpl = new ResolutionImpl();

		resolutionImpl.setNew(resolution.isNew());
		resolutionImpl.setPrimaryKey(resolution.getPrimaryKey());

		resolutionImpl.setRes_id(resolution.getRes_id());
		resolutionImpl.setValue(resolution.getValue());
		resolutionImpl.setSortkey(resolution.getSortkey());
		resolutionImpl.setIsactive(resolution.getIsactive());

		return resolutionImpl;
	}

	/**
	 * Returns the resolution with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the resolution
	 * @return the resolution
	 * @throws com.liferay.portal.NoSuchModelException if a resolution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Resolution findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the resolution with the primary key or throws a {@link com.vmware.NoSuchResolutionException} if it could not be found.
	 *
	 * @param res_id the primary key of the resolution
	 * @return the resolution
	 * @throws com.vmware.NoSuchResolutionException if a resolution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution findByPrimaryKey(int res_id)
		throws NoSuchResolutionException, SystemException {
		Resolution resolution = fetchByPrimaryKey(res_id);

		if (resolution == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + res_id);
			}

			throw new NoSuchResolutionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				res_id);
		}

		return resolution;
	}

	/**
	 * Returns the resolution with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the resolution
	 * @return the resolution, or <code>null</code> if a resolution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Resolution fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Integer)primaryKey).intValue());
	}

	/**
	 * Returns the resolution with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param res_id the primary key of the resolution
	 * @return the resolution, or <code>null</code> if a resolution with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution fetchByPrimaryKey(int res_id) throws SystemException {
		Resolution resolution = (Resolution)EntityCacheUtil.getResult(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
				ResolutionImpl.class, res_id);

		if (resolution == _nullResolution) {
			return null;
		}

		if (resolution == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				resolution = (Resolution)session.get(ResolutionImpl.class,
						Integer.valueOf(res_id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (resolution != null) {
					cacheResult(resolution);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ResolutionModelImpl.ENTITY_CACHE_ENABLED,
						ResolutionImpl.class, res_id, _nullResolution);
				}

				closeSession(session);
			}
		}

		return resolution;
	}

	/**
	 * Returns the resolution where value = &#63; or throws a {@link com.vmware.NoSuchResolutionException} if it could not be found.
	 *
	 * @param value the value
	 * @return the matching resolution
	 * @throws com.vmware.NoSuchResolutionException if a matching resolution could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution findByResolutionByValue(String value)
		throws NoSuchResolutionException, SystemException {
		Resolution resolution = fetchByResolutionByValue(value);

		if (resolution == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("value=");
			msg.append(value);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchResolutionException(msg.toString());
		}

		return resolution;
	}

	/**
	 * Returns the resolution where value = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param value the value
	 * @return the matching resolution, or <code>null</code> if a matching resolution could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution fetchByResolutionByValue(String value)
		throws SystemException {
		return fetchByResolutionByValue(value, true);
	}

	/**
	 * Returns the resolution where value = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param value the value
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching resolution, or <code>null</code> if a matching resolution could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution fetchByResolutionByValue(String value,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { value };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
					finderArgs, this);
		}

		if (result instanceof Resolution) {
			Resolution resolution = (Resolution)result;

			if (!Validator.equals(value, resolution.getValue())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_RESOLUTION_WHERE);

			if (value == null) {
				query.append(_FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_1);
			}
			else {
				if (value.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_3);
				}
				else {
					query.append(_FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (value != null) {
					qPos.add(value);
				}

				List<Resolution> list = q.list();

				result = list;

				Resolution resolution = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
						finderArgs, list);
				}
				else {
					resolution = list.get(0);

					cacheResult(resolution);

					if ((resolution.getValue() == null) ||
							!resolution.getValue().equals(value)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
							finderArgs, resolution);
					}
				}

				return resolution;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_RESOLUTIONBYVALUE,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Resolution)result;
			}
		}
	}

	/**
	 * Returns all the resolutions.
	 *
	 * @return the resolutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Resolution> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the resolutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of resolutions
	 * @param end the upper bound of the range of resolutions (not inclusive)
	 * @return the range of resolutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Resolution> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the resolutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of resolutions
	 * @param end the upper bound of the range of resolutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of resolutions
	 * @throws SystemException if a system exception occurred
	 */
	public List<Resolution> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Resolution> list = (List<Resolution>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_RESOLUTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_RESOLUTION;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Resolution>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Resolution>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the resolution where value = &#63; from the database.
	 *
	 * @param value the value
	 * @return the resolution that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Resolution removeByResolutionByValue(String value)
		throws NoSuchResolutionException, SystemException {
		Resolution resolution = findByResolutionByValue(value);

		return remove(resolution);
	}

	/**
	 * Removes all the resolutions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Resolution resolution : findAll()) {
			remove(resolution);
		}
	}

	/**
	 * Returns the number of resolutions where value = &#63;.
	 *
	 * @param value the value
	 * @return the number of matching resolutions
	 * @throws SystemException if a system exception occurred
	 */
	public int countByResolutionByValue(String value) throws SystemException {
		Object[] finderArgs = new Object[] { value };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_RESOLUTIONBYVALUE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_RESOLUTION_WHERE);

			if (value == null) {
				query.append(_FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_1);
			}
			else {
				if (value.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_3);
				}
				else {
					query.append(_FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (value != null) {
					qPos.add(value);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_RESOLUTIONBYVALUE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of resolutions.
	 *
	 * @return the number of resolutions
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_RESOLUTION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the resolution persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Resolution")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Resolution>> listenersList = new ArrayList<ModelListener<Resolution>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Resolution>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ResolutionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_RESOLUTION = "SELECT resolution FROM Resolution resolution";
	private static final String _SQL_SELECT_RESOLUTION_WHERE = "SELECT resolution FROM Resolution resolution WHERE ";
	private static final String _SQL_COUNT_RESOLUTION = "SELECT COUNT(resolution) FROM Resolution resolution";
	private static final String _SQL_COUNT_RESOLUTION_WHERE = "SELECT COUNT(resolution) FROM Resolution resolution WHERE ";
	private static final String _FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_1 = "resolution.value IS NULL";
	private static final String _FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_2 = "resolution.value = ?";
	private static final String _FINDER_COLUMN_RESOLUTIONBYVALUE_VALUE_3 = "(resolution.value IS NULL OR resolution.value = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "resolution.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Resolution exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Resolution exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ResolutionPersistenceImpl.class);
	private static Resolution _nullResolution = new ResolutionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Resolution> toCacheModel() {
				return _nullResolutionCacheModel;
			}
		};

	private static CacheModel<Resolution> _nullResolutionCacheModel = new CacheModel<Resolution>() {
			public Resolution toEntityModel() {
				return _nullResolution;
			}
		};
}