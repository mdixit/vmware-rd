/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.vmware.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.vmware.NoSuchVoteException;

import com.vmware.model.Vote;
import com.vmware.model.impl.VoteImpl;
import com.vmware.model.impl.VoteModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vote service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author iscisc
 * @see VotePersistence
 * @see VoteUtil
 * @generated
 */
public class VotePersistenceImpl extends BasePersistenceImpl<Vote>
	implements VotePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VoteUtil} to access the vote persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VoteImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID = new FinderPath(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteModelImpl.FINDER_CACHE_ENABLED, VoteImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByVoteCountByUserId",
			new String[] { Integer.class.getName() },
			VoteModelImpl.WHO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_VOTECOUNTBYUSERID = new FinderPath(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByVoteCountByUserId", new String[] { Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteModelImpl.FINDER_CACHE_ENABLED, VoteImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteModelImpl.FINDER_CACHE_ENABLED, VoteImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the vote in the entity cache if it is enabled.
	 *
	 * @param vote the vote
	 */
	public void cacheResult(Vote vote) {
		EntityCacheUtil.putResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteImpl.class, vote.getPrimaryKey(), vote);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
			new Object[] { Integer.valueOf(vote.getWho()) }, vote);

		vote.resetOriginalValues();
	}

	/**
	 * Caches the votes in the entity cache if it is enabled.
	 *
	 * @param votes the votes
	 */
	public void cacheResult(List<Vote> votes) {
		for (Vote vote : votes) {
			if (EntityCacheUtil.getResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
						VoteImpl.class, vote.getPrimaryKey()) == null) {
				cacheResult(vote);
			}
			else {
				vote.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all votes.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(VoteImpl.class.getName());
		}

		EntityCacheUtil.clearCache(VoteImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the vote.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Vote vote) {
		EntityCacheUtil.removeResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteImpl.class, vote.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(vote);
	}

	@Override
	public void clearCache(List<Vote> votes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Vote vote : votes) {
			EntityCacheUtil.removeResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
				VoteImpl.class, vote.getPrimaryKey());

			clearUniqueFindersCache(vote);
		}
	}

	protected void clearUniqueFindersCache(Vote vote) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
			new Object[] { Integer.valueOf(vote.getWho()) });
	}

	/**
	 * Creates a new vote with the primary key. Does not add the vote to the database.
	 *
	 * @param votePK the primary key for the new vote
	 * @return the new vote
	 */
	public Vote create(VotePK votePK) {
		Vote vote = new VoteImpl();

		vote.setNew(true);
		vote.setPrimaryKey(votePK);

		return vote;
	}

	/**
	 * Removes the vote with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param votePK the primary key of the vote
	 * @return the vote that was removed
	 * @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Vote remove(VotePK votePK)
		throws NoSuchVoteException, SystemException {
		return remove((Serializable)votePK);
	}

	/**
	 * Removes the vote with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the vote
	 * @return the vote that was removed
	 * @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Vote remove(Serializable primaryKey)
		throws NoSuchVoteException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Vote vote = (Vote)session.get(VoteImpl.class, primaryKey);

			if (vote == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVoteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(vote);
		}
		catch (NoSuchVoteException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Vote removeImpl(Vote vote) throws SystemException {
		vote = toUnwrappedModel(vote);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, vote);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(vote);

		return vote;
	}

	@Override
	public Vote updateImpl(com.vmware.model.Vote vote, boolean merge)
		throws SystemException {
		vote = toUnwrappedModel(vote);

		boolean isNew = vote.isNew();

		VoteModelImpl voteModelImpl = (VoteModelImpl)vote;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, vote, merge);

			vote.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !VoteModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
			VoteImpl.class, vote.getPrimaryKey(), vote);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
				new Object[] { Integer.valueOf(vote.getWho()) }, vote);
		}
		else {
			if ((voteModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Integer.valueOf(voteModelImpl.getOriginalWho())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VOTECOUNTBYUSERID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
					new Object[] { Integer.valueOf(vote.getWho()) }, vote);
			}
		}

		return vote;
	}

	protected Vote toUnwrappedModel(Vote vote) {
		if (vote instanceof VoteImpl) {
			return vote;
		}

		VoteImpl voteImpl = new VoteImpl();

		voteImpl.setNew(vote.isNew());
		voteImpl.setPrimaryKey(vote.getPrimaryKey());

		voteImpl.setBug_id(vote.getBug_id());
		voteImpl.setWho(vote.getWho());
		voteImpl.setVote_count(vote.getVote_count());

		return voteImpl;
	}

	/**
	 * Returns the vote with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the vote
	 * @return the vote
	 * @throws com.liferay.portal.NoSuchModelException if a vote with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Vote findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey((VotePK)primaryKey);
	}

	/**
	 * Returns the vote with the primary key or throws a {@link com.vmware.NoSuchVoteException} if it could not be found.
	 *
	 * @param votePK the primary key of the vote
	 * @return the vote
	 * @throws com.vmware.NoSuchVoteException if a vote with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Vote findByPrimaryKey(VotePK votePK)
		throws NoSuchVoteException, SystemException {
		Vote vote = fetchByPrimaryKey(votePK);

		if (vote == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + votePK);
			}

			throw new NoSuchVoteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				votePK);
		}

		return vote;
	}

	/**
	 * Returns the vote with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the vote
	 * @return the vote, or <code>null</code> if a vote with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Vote fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey((VotePK)primaryKey);
	}

	/**
	 * Returns the vote with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param votePK the primary key of the vote
	 * @return the vote, or <code>null</code> if a vote with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Vote fetchByPrimaryKey(VotePK votePK) throws SystemException {
		Vote vote = (Vote)EntityCacheUtil.getResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
				VoteImpl.class, votePK);

		if (vote == _nullVote) {
			return null;
		}

		if (vote == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				vote = (Vote)session.get(VoteImpl.class, votePK);
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (vote != null) {
					cacheResult(vote);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(VoteModelImpl.ENTITY_CACHE_ENABLED,
						VoteImpl.class, votePK, _nullVote);
				}

				closeSession(session);
			}
		}

		return vote;
	}

	/**
	 * Returns the vote where who = &#63; or throws a {@link com.vmware.NoSuchVoteException} if it could not be found.
	 *
	 * @param who the who
	 * @return the matching vote
	 * @throws com.vmware.NoSuchVoteException if a matching vote could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Vote findByVoteCountByUserId(int who)
		throws NoSuchVoteException, SystemException {
		Vote vote = fetchByVoteCountByUserId(who);

		if (vote == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("who=");
			msg.append(who);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchVoteException(msg.toString());
		}

		return vote;
	}

	/**
	 * Returns the vote where who = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param who the who
	 * @return the matching vote, or <code>null</code> if a matching vote could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Vote fetchByVoteCountByUserId(int who) throws SystemException {
		return fetchByVoteCountByUserId(who, true);
	}

	/**
	 * Returns the vote where who = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param who the who
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching vote, or <code>null</code> if a matching vote could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Vote fetchByVoteCountByUserId(int who, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { who };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
					finderArgs, this);
		}

		if (result instanceof Vote) {
			Vote vote = (Vote)result;

			if ((who != vote.getWho())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_VOTE_WHERE);

			query.append(_FINDER_COLUMN_VOTECOUNTBYUSERID_WHO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				List<Vote> list = q.list();

				result = list;

				Vote vote = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
						finderArgs, list);
				}
				else {
					vote = list.get(0);

					cacheResult(vote);

					if ((vote.getWho() != who)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
							finderArgs, vote);
					}
				}

				return vote;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VOTECOUNTBYUSERID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Vote)result;
			}
		}
	}

	/**
	 * Returns all the votes.
	 *
	 * @return the votes
	 * @throws SystemException if a system exception occurred
	 */
	public List<Vote> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the votes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of votes
	 * @param end the upper bound of the range of votes (not inclusive)
	 * @return the range of votes
	 * @throws SystemException if a system exception occurred
	 */
	public List<Vote> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the votes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of votes
	 * @param end the upper bound of the range of votes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of votes
	 * @throws SystemException if a system exception occurred
	 */
	public List<Vote> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Vote> list = (List<Vote>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_VOTE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VOTE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Vote>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Vote>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the vote where who = &#63; from the database.
	 *
	 * @param who the who
	 * @return the vote that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Vote removeByVoteCountByUserId(int who)
		throws NoSuchVoteException, SystemException {
		Vote vote = findByVoteCountByUserId(who);

		return remove(vote);
	}

	/**
	 * Removes all the votes from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Vote vote : findAll()) {
			remove(vote);
		}
	}

	/**
	 * Returns the number of votes where who = &#63;.
	 *
	 * @param who the who
	 * @return the number of matching votes
	 * @throws SystemException if a system exception occurred
	 */
	public int countByVoteCountByUserId(int who) throws SystemException {
		Object[] finderArgs = new Object[] { who };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_VOTECOUNTBYUSERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VOTE_WHERE);

			query.append(_FINDER_COLUMN_VOTECOUNTBYUSERID_WHO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(who);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_VOTECOUNTBYUSERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of votes.
	 *
	 * @return the number of votes
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VOTE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the vote persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.vmware.model.Vote")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Vote>> listenersList = new ArrayList<ModelListener<Vote>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Vote>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(VoteImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttachmentsPersistence.class)
	protected AttachmentsPersistence attachmentsPersistence;
	@BeanReference(type = BugPersistence.class)
	protected BugPersistence bugPersistence;
	@BeanReference(type = BugsActivityPersistence.class)
	protected BugsActivityPersistence bugsActivityPersistence;
	@BeanReference(type = BugSeverityPersistence.class)
	protected BugSeverityPersistence bugSeverityPersistence;
	@BeanReference(type = BugStatusPersistence.class)
	protected BugStatusPersistence bugStatusPersistence;
	@BeanReference(type = CCEntityPersistence.class)
	protected CCEntityPersistence ccEntityPersistence;
	@BeanReference(type = ComponentsPersistence.class)
	protected ComponentsPersistence componentsPersistence;
	@BeanReference(type = DuplicatePersistence.class)
	protected DuplicatePersistence duplicatePersistence;
	@BeanReference(type = FieldDefsPersistence.class)
	protected FieldDefsPersistence fieldDefsPersistence;
	@BeanReference(type = GroupPersistence.class)
	protected GroupPersistence groupPersistence;
	@BeanReference(type = KeywordPersistence.class)
	protected KeywordPersistence keywordPersistence;
	@BeanReference(type = KeywordDefsPersistence.class)
	protected KeywordDefsPersistence keywordDefsPersistence;
	@BeanReference(type = LongDescriptionPersistence.class)
	protected LongDescriptionPersistence longDescriptionPersistence;
	@BeanReference(type = ProductsPersistence.class)
	protected ProductsPersistence productsPersistence;
	@BeanReference(type = ProfileActivityPersistence.class)
	protected ProfileActivityPersistence profileActivityPersistence;
	@BeanReference(type = ProfilesPersistence.class)
	protected ProfilesPersistence profilesPersistence;
	@BeanReference(type = ResolutionPersistence.class)
	protected ResolutionPersistence resolutionPersistence;
	@BeanReference(type = VotePersistence.class)
	protected VotePersistence votePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_VOTE = "SELECT vote FROM Vote vote";
	private static final String _SQL_SELECT_VOTE_WHERE = "SELECT vote FROM Vote vote WHERE ";
	private static final String _SQL_COUNT_VOTE = "SELECT COUNT(vote) FROM Vote vote";
	private static final String _SQL_COUNT_VOTE_WHERE = "SELECT COUNT(vote) FROM Vote vote WHERE ";
	private static final String _FINDER_COLUMN_VOTECOUNTBYUSERID_WHO_2 = "vote.id.who = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "vote.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Vote exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Vote exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(VotePersistenceImpl.class);
	private static Vote _nullVote = new VoteImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Vote> toCacheModel() {
				return _nullVoteCacheModel;
			}
		};

	private static CacheModel<Vote> _nullVoteCacheModel = new CacheModel<Vote>() {
			public Vote toEntityModel() {
				return _nullVote;
			}
		};
}