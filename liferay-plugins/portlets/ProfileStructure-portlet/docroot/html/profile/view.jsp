<%--
/**
* Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the Free
* Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
* details.
*/
--%>


<%@page import="com.vmware.model.Profiles"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />

<liferay-theme:defineObjects />

<liferay-portlet:renderURL var="renderURL" />
<liferay-portlet:actionURL var="actionURL" />
<liferay-portlet:resourceURL var="resourceURL" />

This is the <b>Profile Portlet</b> portlet in View mode.

<script type="text/javascript">
	var profilesStructure ={
		resourceUrl:'<%= resourceURL %>',
		contextPath :'<%=renderRequest.getContextPath()%>'	
	};
</script>
<%
	Profiles pfEntity = (Profiles)renderRequest.getAttribute("pf");
	System.out.println("pfEntity=="+pfEntity.getLogin_name());
%>

<script type="text/javascript" src="<%=renderRequest.getContextPath()%>/js/service.js" language="JavaScript"> </script>



