Liferay.Service.register("Liferay.Service.vmware", "com.vmware.service", "ProfileStructure-portlet");

Liferay.Service.registerClass(
	Liferay.Service.vmware, "Bug",
	{
		getBugByAssignToId: true,
		getBugSeverityByUserId: true,
		getBugPriorityByUserId: true,
		getBugStatusByUserId: true,
		getBugKeywordsByUserId: true,
		getBugsByReporter: true,
		getBugShortDescByUserId: true,
		getBugVotesByUserId: true,
		getBugPriority: true,
		getBugResolutionByUserId: true,
		getBugReporterByBugId: true,
		getBugQAContactByBugId: true,
		getBugComponentsByBugId: true,
		getBugProductsByBugId: true,
		getBugSeverityByBugId: true,
		getBugStatus: true,
		getBugAssignedByBugId: true,
		getBugResolutionByBugId: true,
		getDuplicateByDupeofId: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.vmware, "BugsActivity",
	{
		getBugActivityByUserId: true,
		getBugActivityByBugId: true,
		getBugActivityByUserIdAndFieldId: true,
		getBugActivityByBugActivityByAttachId: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.vmware, "LongDescription",
	{
		getLongDescByBugId: true,
		getLongDescByUserId: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.vmware, "ProfileActivity",
	{
		getProfileName: true,
		getFieldDefsByFieldId: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.vmware, "Profiles",
	{
		getProfileName: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.vmware, "Vote",
	{
		getBugVoteByUserId: true
	}
);